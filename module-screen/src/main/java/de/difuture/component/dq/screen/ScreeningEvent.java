package de.difuture.component.dq.screen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.difuture.component.dq.common.Screen;

/**
 * Encapsulates all data related to a screening event
 * @author Helmut Spengler
 */
public abstract class ScreeningEvent {
    
    /** The screen performed */
    protected final Screen screen;
    
    /** The screened source entity */
    protected final String sourceEntity;
    
    /** The screened attribute */
    protected final String sourceEntityAttr;
    
    /** For self-logging */
    final static Logger logger = LoggerFactory.getLogger(ScreenLogger.class);
    
    /**
     * Constructor
     * @param screen the screen performed
     * @param sourceEntity the screened source entity
     * @param sourceEntityAttr the screened attribute
     */
    protected ScreeningEvent (Screen screen, String sourceEntity, String sourceEntityAttr) {
        
        if (null == screen) {
            String message = "Parameter 'screen' may not be null.";
            logger.error(message);
            throw new IllegalArgumentException(message);
        } else if (null == sourceEntity || "".equals(sourceEntity)) {
            String message = "Parameter 'sourceEntity' may not be null or empty. Value was " + (sourceEntity == null ? "null." : "empty.");
            logger.error(message);
            throw new IllegalArgumentException(message);
        } else if (null == sourceEntityAttr || "".equals(sourceEntityAttr)) {
            String message = "Parameter 'sourceEntityAttr' may not be null or empty. Value was " + (sourceEntityAttr == null ? "null." : "empty.");
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
        
        this.screen = screen;
        this.sourceEntity = sourceEntity;
        this.sourceEntityAttr = sourceEntityAttr;
    }
}
