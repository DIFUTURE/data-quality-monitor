package de.difuture.component.dq.screen;

import de.difuture.component.dq.common.Screen;

/**
 * Encapsulates a pass event.
 * @author Helmut Spengler
 */
public class PassEvent extends ScreeningEvent {    

    /**
     * Builder class for PassEvent
     * @author Helmut Spengler
     */
    public static class PassEventBuilder {
        
        /** The screen performed */
        private Screen screen;
        
        /** The screened source entity */
        private String sourceEntity;
        
        /** The screened attribute */
        private String sourceEntityAttr;  

        /** Constructor
         * @param screen the screen
         */
        public PassEventBuilder(Screen screen) {
            this.screen = screen;
        }

        /**
         * Define the sourceEntity
         * @param sourceEntity the source entity
         * @return builder containing the sourceEntity 
         */
        public PassEventBuilder inEntity(String sourceEntity){
            this.sourceEntity = sourceEntity;
            return this;
        }

        /**
         * Define the sourceEntityAttr
         * @param sourceEntityAttr the attribute
         * @return builder containing the attribute
         */
        public PassEventBuilder inAttr(String sourceEntityAttr){
            this.sourceEntityAttr = sourceEntityAttr;
            return this;
        }

        /**
         * Build the ScreeningEvent
         * @return the screeningEvent
         */
        public PassEvent build(){
            return new PassEvent(screen, sourceEntity, sourceEntityAttr);
        }
    }

    /**
     * Constructor
     * @param screen the screen
     * @param sourceEntity the source entity
     * @param sourceEntityAttr the attribute
     */
    public PassEvent (Screen screen, String sourceEntity, String sourceEntityAttr) {
        super(screen, sourceEntity, sourceEntityAttr);
    }

    /**
     * Constructor
     * @param failEvent failEvent to construct a passEvent from
     */
    public PassEvent (FailEvent failEvent) {
        this(failEvent.screen, failEvent.sourceEntity, failEvent.sourceEntityAttr);
    }
}
