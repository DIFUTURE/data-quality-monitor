/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.screen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.difuture.component.dq.common.ExceptionAction;
import de.difuture.component.dq.common.Phase;
import de.difuture.component.dq.common.Screen;
import de.difuture.component.dq.common.Status;
import de.difuture.component.dq.common.Util;
import de.difuture.component.dq.screen.FailEvent.FailEventBuilder;
import de.difuture.component.dq.screen.PassEvent.PassEventBuilder;



/**
 * Logging utility for persisting data quality (DQ) information collected
 * during the ETL process to a database. Implements singleton pattern. * 
 * @author Helmut Spengler
 *
 */
public class ScreenLogger {

    /**
     * Represents a composite key for the passCounts hashMap
     * @author Helmut Spengler
     */
    private class CompositeHashKey {

        /** The JobId */
        private final Integer jobPk;

        /** The screen */
        private final Screen screen;

        /** The entity */
        private final String entity;

        /** The attribute */
        private final String attribute;

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getEnclosingInstance().hashCode();
            result = prime * result + ((attribute == null) ? 0 : attribute.hashCode());
            result = prime * result + ((entity == null) ? 0 : entity.hashCode());
            result = prime * result + ((screen == null) ? 0 : screen.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            CompositeHashKey other = (CompositeHashKey) obj;
            if (!getEnclosingInstance().equals(other.getEnclosingInstance())) return false;
            if (attribute == null) {
                if (other.attribute != null) return false;
            } else if (!attribute.equals(other.attribute)) return false;
            if (entity == null) {
                if (other.entity != null) return false;
            } else if (!entity.equals(other.entity)) return false;
            if (screen != other.screen) return false;
            return true;
        }

        /**
         * Get the enclosing instance of this inner class. Needed for hashCode().
         * @return the instance
         */
        private ScreenLogger getEnclosingInstance() {
            return ScreenLogger.this;
        }

        /**
         * Constructor
         * @param jobPk primary key of the job
         * @param screen the screen 
         * @param entity the entity
         * @param attribute the attribute
         */
        public CompositeHashKey(Integer jobPk, Screen screen, String entity, String attribute) {
            this.screen = screen;
            this.entity = entity;
            this.attribute = attribute;
            this.jobPk = jobPk;
        }
    }

    /**
     * Encapsulates all information relevant for an ETL job in the context of logging
     * @author Helmut Spengler
     */
    public class Job {

        /** The jobPk */
        private final int pk;

        /** The counts of passed screens per job, screen, entity, and attribute */
        private final Map<CompositeHashKey, Integer>passCounts;

        /** The counts of failed screens per job, screen, entity, and attribute */
        private final Map<CompositeHashKey, Integer>failCounts;

        /**
         * Constructor
         * @param pk the primary key of the job
         */
        public Job(int pk) {
            this.pk = pk;
            this.passCounts = new HashMap<>();
            this.failCounts = new HashMap<>();
        }

        /**
         * Getter
         * @return passCounts per composite key
         */
        public Map<CompositeHashKey, Integer> getPassCounts() {
            return passCounts;
        }

        /**
         * Getter
         * @return failCounts per composite key
         */
        public Map<CompositeHashKey, Integer> getFailCounts() {
            return failCounts;
        }
    }

    /** For self-logging */
    final static Logger logger = LoggerFactory.getLogger(ScreenLogger.class);

    /** Singleton instance of this class */
    private static ScreenLogger instance;

    /** Database connection */
    private final Connection dbCon;

    /**
     * Main method as example for usage of class and for testing
     * 
     * @param args command line parameters
     * 
     * @throws SQLException if the event cannot be written to the database
     */
    public static void main(String[] args) throws SQLException {

        // Generate random events

        // Acquire DB connection and screenLogger instance
        Connection connection = Util.getInstance().getDatabaseConnection("localhost", "5632", "es", "postgres", "aU6s367W");
        ScreenLogger.init(connection);
        ScreenLogger screenLogger = ScreenLogger.getInstance();
        Package thePackage = screenLogger.getClass().getPackage();
        logger.info("This is " + thePackage.getImplementationTitle() + " " + thePackage.getImplementationVersion());

        int waitSeconds = 20;
        generateDemoEvents( 1,  15, 0, 18,  12, 6, 27, logger, screenLogger, waitSeconds);
        generateDemoEvents(40, 16, 24, 13, 12, 37, 20, logger, screenLogger, waitSeconds);
        generateDemoEvents(75, 80, 60, 10, 50, 77, 16, logger, screenLogger, waitSeconds);
        generateDemoEvents(12, 21, 25, 60, 70, 67, 35, logger, screenLogger, waitSeconds);
        generateDemoEvents(22,  5, 18, 30, 40, 47, 15, logger, screenLogger, waitSeconds);
        generateDemoEvents( 2,   3, 5,  0,  0,  2,  2, logger, screenLogger, waitSeconds);

        // Close db connection
        connection.close();        
    }

    /**
     * Prohibit call of constructor from outside this class.
     */
    private ScreenLogger() {
        this.dbCon = null;
    };

    /**
     * Constructor
     * @param dbCon the database connection
     */
    private ScreenLogger(Connection dbCon) {
        this.dbCon = dbCon;
    };

    /**
     * Return the singleton instance of this class
     * 
     * @return the logger
     */
    public static synchronized ScreenLogger getInstance() {
        if (ScreenLogger.instance.dbCon == null) {
            throw new RuntimeException("dbCon is null. You have to call ScreenLogger.init(Connection dbCon) first.");
        }
        return ScreenLogger.instance;
    }

    /**
     * Return the singleton instance of this class, setting or updating the database connection
     * @param dbCon the database connection
     * @return the singleton instance
     * 
     */
    public static synchronized ScreenLogger init(Connection dbCon) {
        if (instance != null)
        {
            // in my opinion this is optional, but for the purists it ensures
            // that you only ever get the same instance when you call getInstance
            throw new AssertionError("You already initialized me");
        }

        instance = new ScreenLogger(dbCon);

        return instance;
    }

    /**
     * Open database connection, validate schema and persist the start parameters of the ETL process
     * @param phase the lifecycle phase for which logging is to be performed
     * @param jobName name of the job
     * @param sourceSystem name of the source system
     * @return a job object. Needed for logging single events and for holding the counts of passed screens
     * @throws SQLException if the job information cannot be persisted to the database or
     * if no job identifier cannot be obtained
     */
    public Job jobStarted(Phase phase, String jobName, String sourceSystem) throws SQLException {

        // Validate schema and populate with defined screens and screen categories, if necessary
        Util.getInstance().validateEventStoreSchema(dbCon);

        // Prepare SQL statement
        String SQL_INSERT = "INSERT INTO dq.job_dimension\n" +
                "(job_name, job_start, source_system, phase, status)\n" +
                "VALUES\n" +
                "(?, ?, ?, ?, ?);";
        PreparedStatement insertStmt = dbCon.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);

        // Set parameters
        insertStmt.setString(1, jobName);
        insertStmt.setTimestamp(2, new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
        insertStmt.setString(3, sourceSystem);
        insertStmt.setString(4, phase.toString());
        insertStmt.setString(5, Status.RUNNING.toString());

        // Execute statement
        int affectedRows = insertStmt.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Creating entry in job_dimension failed, no rows affected.");
        }

        // Get the unique id of the job, which has been autogenerated from the database
        int jobPk = -1;
        try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                jobPk = generatedKeys.getInt(1);
            }
            else {
                throw new SQLException("Creating entry in job_dimension failed, no ID obtained.");
            }
        }
        return new Job(jobPk);
    }

    /**
     * Persist the finish parameters (including the numbers of pass() events) of the ETL process and close database connection.
     * @param job the job object
     * @param status the status
     * @throws SQLException if the job information cannot be persisted to the database
     */
    public void jobFinished(Job job, Status status) throws SQLException {

        // Prepare statement
        String SQL_UPDATE = "UPDATE dq.job_dimension\n" + 
                "SET job_finish=?, status=?\n" + 
                "WHERE job_pk=?;";
        PreparedStatement updateStmt = dbCon.prepareStatement(SQL_UPDATE);

        // Set parameters
        updateStmt.setTimestamp(1, new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
        updateStmt.setString(2, status.toString());
        updateStmt.setInt(3, job.pk);

        // Execute statement
        int affectedRows = updateStmt.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Creating entry in job_dimension failed, no rows affected.");
        }

        // Write the counts of passed screens
        writeCounts(job, true);

        // Write the counts of failed screens
        writeCounts(job, false);
    }

    /**
     * Write the number of pass() events to the database.
     * @param job the job object
     * @param passed whether the passed counts should be written or the failed counts
     * @throws SQLException  if the numbers cannot be written to the database
     */
    private void writeCounts(Job job, boolean passed) throws SQLException {

        for (Map.Entry<CompositeHashKey,Integer> entry : (passed ? job.getPassCounts() : job.getFailCounts()).entrySet()) {

            // Prepare SQL statement
            String SQL_INSERT = "INSERT INTO dq.event_count_fact\n" +
                    "(job_fk, screen_fk, entity_name, attr_name, row_count, passed)\n" +
                    "VALUES\n" +
                    "(?, ?, ?, ?, ?, ?);";
            PreparedStatement insertStmt = dbCon.prepareStatement(SQL_INSERT);

            // Set parameters
            insertStmt.setInt(    1, entry.getKey().jobPk);
            insertStmt.setInt(    2, entry.getKey().screen.getPk());
            insertStmt.setString( 3, entry.getKey().entity);
            insertStmt.setString( 4, entry.getKey().attribute);
            insertStmt.setInt(    5, entry.getValue());
            insertStmt.setBoolean(6, passed);

            // Execute statement
            int affectedRows = insertStmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating entry in job_dimension failed, no rows affected.");
            }
        }
    }

    /**
     * Persist an event to the event store. Currently the severity score persisted
     * equals the default severity score as of the screen definition
     * @param jobId the jobId
     * @param screen the screen that caused the event
     * @param sourceEntity the source entity
     * @param sourceEntityKeyAttr the key attribute for the source entity
     * @param sourceEntityKey the id of the object causing the event
     * @param sourceEntityErrorAttr the attribute causing the event
     * @param sourceEntityErrorVal the value of the attribute causing the event
     * @param actionPerformed the action that has been performed (e.g.
     * ignored the object, corrected the information)
     * @param info additional information
     * @throws SQLException if the event cannot be persisted to the database
     * @deprecated Use method fail() instead
     */
    public void issue(int jobId, Screen screen, String sourceEntity, String sourceEntityKeyAttr, String sourceEntityKey, String sourceEntityErrorAttr, String sourceEntityErrorVal, ExceptionAction actionPerformed, String info) throws SQLException {
        fail(new Job(jobId), new FailEvent(screen, sourceEntity, sourceEntityKeyAttr, sourceEntityKey, sourceEntityErrorAttr, sourceEntityErrorVal, actionPerformed, info));
    }

    /**
     * Persist an event to the event store. Currently the severity score persisted
     * equals the default severity score as of the screen definition
     * @param job the job
     * @param failEvent event containing error information
     * @throws SQLException if the event cannot be persisted to the database
     */
    public void fail(Job job, FailEvent failEvent) throws SQLException {

        // Get default severity score for screen
        String SQL_GET_SEVERITY_SCORE = "SELECT default_severity_score FROM dq.screen_dimension WHERE screen_pk='" + failEvent.screen.getPk() + "';";
        Statement getSeverityScore = dbCon.createStatement();
        ResultSet rs = getSeverityScore.executeQuery(SQL_GET_SEVERITY_SCORE);        
        double defaultSeverityScore = -1d;
        while (rs.next()) {
            defaultSeverityScore = rs.getDouble(1);
        }

        // Prepare statements
        String SQL_INSERT = "INSERT INTO dq.error_event_fact\n" + 
                "(job_fk, screen_fk, source_entity_name, source_entity_key_attr, source_entity_key, source_entity_error_attr, source_entity_error_val, final_severity_score, action_performed, info)\n" + 
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 
        PreparedStatement insertStmt = dbCon.prepareStatement(SQL_INSERT);

        // Set parameters
        insertStmt.setInt  (1, job.pk);
        insertStmt.setInt  (2, failEvent.screen.getPk());
        insertStmt.setString(3, failEvent.sourceEntity);
        insertStmt.setString(4, failEvent.sourceEntityKeyAttr);
        insertStmt.setString(5, failEvent.sourceEntityKey);
        insertStmt.setString(6, failEvent.sourceEntityAttr != null ? failEvent.sourceEntityAttr : "");
        insertStmt.setString(7, failEvent.sourceEntityErrorVal != null ? failEvent.sourceEntityErrorVal : "");
        insertStmt.setDouble(8, defaultSeverityScore);
        insertStmt.setString(9, failEvent.actionPerformed.toString());
        insertStmt.setString(10, failEvent.info != null ? failEvent.info : "");

        // Execute query
        int affectedRows = insertStmt.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Creating entry in job_dimension failed, no rows affected.");
        }

        job.getFailCounts().merge(new CompositeHashKey(job.pk, failEvent.screen, failEvent.sourceEntity, failEvent.sourceEntityAttr), 1, Integer::sum);
    }

    /**
     * Register a positive screen result
     * @param job the job  
     * @param passEvent the pass event containing detailed information
     */
    public void pass(Job job, PassEvent passEvent) {

        job.getPassCounts().merge(new CompositeHashKey(job.pk, passEvent.screen, passEvent.sourceEntity, passEvent.sourceEntityAttr), 1, Integer::sum);
    }

    /**
     * Register baseline info for score calculation
     * @param jobId the jobId
     * @param entityName name of the entity
     * @param entityCount number of objects related to this entity
     * @throws SQLException if the information cannot be persisted to the database
     * @deprecated Use methods fail() and pass() instead
     */
    public void baseline(int jobId, String entityName, int entityCount) throws SQLException { 

        // Check if there is already an according baseline entry
        String SQL_BASELINE_COUNT = "SELECT COUNT(*) FROM dq.baseline_dimension bd WHERE job_fk=? AND entity_name=?";        
        PreparedStatement baselineCount = dbCon.prepareStatement(SQL_BASELINE_COUNT);
        baselineCount.setInt(1, jobId);
        baselineCount.setString(2, entityName);

        int count = -1;
        ResultSet rs = baselineCount.executeQuery();
        while (rs.next()) {
            count = rs.getInt(1);
        }

        // If no according baseline entry already exists, create it
        if (count < 1) {
            String SQL_INSERT = "INSERT INTO dq.baseline_dimension\n" + 
                    "(job_fk, entity_name, row_count)\n" + 
                    "VALUES(?, ?, ?)"; 
            PreparedStatement insertStmt = dbCon.prepareStatement(SQL_INSERT);

            insertStmt.setInt   (1, jobId);
            insertStmt.setString(2, entityName);
            insertStmt.setInt   (3, entityCount);

            int affectedRows = insertStmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating entry in baseline_dimension_dimension failed, no rows affected.");
            }
        } else {

            // Ideally, setting the baseline value should happen only once
            logger.warn(String.format("Baseline for entity %s in job %d already exists. Updating with new value", entityName, jobId));

            String SQL_UPDATE = "UPDATE dq.baseline_dimension\n" + 
                    "SET row_count=? WHERE job_fk=? AND entity_name=?"; 
            PreparedStatement updataStmt = dbCon.prepareStatement(SQL_UPDATE);

            updataStmt.setInt    (1, entityCount);
            updataStmt.setInt    (2, jobId);
            updataStmt.setString (3, entityName);

            int affectedRows = updataStmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Updating entry in baseline_dimension_dimension failed, no rows affected.");
            }
        }
    }

    /**
     * Generate demo events
     * @param numValueRangeViolation number of screens of type VALUE_RANGE_VIOLATION
     * @param numMissingValue number of screens of type MISSING_VALUE
     * @param numReferentialIntegrityViolation number of screens of type REFERENTIAL_INTEGRITY_VIOLATION
     * @param numInsufficientPrecision number of screens of type INSUFFICIENT_PRECISION
     * @param numInvalidFormat number of screens of type INVALID_FORMAT
     * @param numInvalidDataType number of screens of type INVALID_DATATYPE
     * @param numInvalidCategory number of screens of type INVALID_CATEGORY
     * @param systemLogger the system logger
     * @param screenLogger the screen logger
     * @param waitSeconds waiting time
     * @throws SQLException when events cannot be peristet
     */
    private static void generateDemoEvents(int numValueRangeViolation, int numMissingValue, int numReferentialIntegrityViolation, int numInsufficientPrecision, int numInvalidFormat, int numInvalidDataType, int numInvalidCategory, Logger systemLogger, ScreenLogger screenLogger, int waitSeconds) throws SQLException {

        // Signal start of the ETL process
        Job job = screenLogger.jobStarted(Phase.STAGE, "ExampleJob", "ExampleSystem");

        // Log events of type VALUE_RANGE_VIOLATION
        for (int i = 0; i < numValueRangeViolation; i++) {
            screenLogger.fail(job,
                              new FailEventBuilder(Screen.VALUE_RANGE_VIOLATION).
                              inEntity("lab_values.csv").
                              inAttr("Glucose").
                              withKeyAttr("lab_id").
                              withKey("4711").
                              withVal("-65").
                              withAction(ExceptionAction.NONE).
                              withInfo("Negative value for mg/dL").
                              build());
        }        
        for (int i = 0; i < numValueRangeViolation * 23; i++) {
            screenLogger.pass(job,
                              new PassEventBuilder(Screen.VALUE_RANGE_VIOLATION).
                              inEntity("lab_values.csv").
                              inAttr("Glucose").
                              build());
        }

        // Log events of type MISSING_VALUE
        for (int i = 0; i < numMissingValue; i++) {
            screenLogger.fail(job,
                              new FailEventBuilder(Screen.MISSING_VALUE).
                              inEntity("lab_values.csv").
                              inAttr("measurement-date").
                              withKeyAttr("lab_id").
                              withKey("1227").
                              withVal("EMPTY").
                              withAction(ExceptionAction.NONE).
                              withInfo("Missing measurement value").
                              build());
        }
        for (int i = 0; i < numMissingValue * 47; i++) {
            screenLogger.pass(job,
                              new PassEventBuilder(Screen.MISSING_VALUE).
                              inEntity("lab_values.csv").
                              inAttr("measurement-date").
                              build());
        }

        // Log events of type REFERENTIAL_INTEGRITY_VIOLATION
        for (int i = 0; i < numReferentialIntegrityViolation; i++) {
            screenLogger.fail(job,
                              new FailEventBuilder(Screen.REFERENTIAL_INTEGRITY_VIOLATION).
                              inEntity("diagnoses.csv").
                              inAttr("patient_id").
                              withKeyAttr("visit_id").
                              withKey("44").
                              withVal("516").
                              withAction(ExceptionAction.NONE).
                              withInfo("Patient not found in patient table").
                              build());
        }
        for (int i = 0; i < numReferentialIntegrityViolation * 5; i++) {
            screenLogger.pass(job,
                              new PassEventBuilder(Screen.REFERENTIAL_INTEGRITY_VIOLATION).
                              inEntity("diagnoses.csv").
                              inAttr("patient_id").
                              build());
        }

        // Log events of type INSUFFICIENT_PRECISION
        for (int i = 0; i < numInsufficientPrecision; i++) {
            screenLogger.fail(job,
                              new FailEventBuilder(Screen.INSUFFICIENT_PRECISION).
                              inEntity("lab_values.csv").
                              inAttr("measurement-date").
                              withKeyAttr("lab_id").
                              withKey("109").
                              withVal(null).
                              withAction(ExceptionAction.NONE).
                              withInfo("Only year - no month and day - provided").
                              build());
        }        
        // Log events of type INSUFFICIENT_PRECISION
        for (int i = 0; i < Math.round((double) numInsufficientPrecision / 3d); i++) {
            screenLogger.pass(job,
                              new PassEventBuilder(Screen.INSUFFICIENT_PRECISION).
                              inEntity("lab_values.csv").
                              inAttr("measurement-date").
                              build());
        }

        // Log events of type INVALID_FORMAT
        for (int i = 0; i < numInvalidFormat; i++) {
            screenLogger.fail(job,
                              new FailEventBuilder(Screen.INVALID_FORMAT).
                              inEntity("lab_values.csv").
                              inAttr("measurement-date").
                              withKeyAttr("lab_id").
                              withKey("508").
                              withVal("2020/01/20").
                              withAction(ExceptionAction.CORRECT).
                              withInfo("Date not in ISO format").
                              build());
        }
        for (int i = 0; i < 30000d; i++) {
            screenLogger.pass(job,
                              new PassEventBuilder(Screen.INVALID_FORMAT).
                              inEntity("lab_values.csv").
                              inAttr("measurement-date").
                              build());
        }

        // Log events of type INVALID_DATATYPE
        for (int i = 0; i < numInvalidDataType; i++) {
            screenLogger.fail(job,
                              new FailEventBuilder(Screen.INVALID_DATATYPE).
                              inEntity("lab_values.csv").
                              inAttr("Glucose").
                              withKeyAttr("lab_id").
                              withKey("1024").
                              withVal("Freetext").
                              withAction(ExceptionAction.NONE).
                              withInfo("Not a number").
                              build());
        }
        for (int i = 0; i < numInvalidDataType * 8; i++) {
            screenLogger.pass(job,
                              new PassEventBuilder(Screen.INVALID_DATATYPE).
                              inEntity("lab_values.csv").
                              inAttr("Glucose").
                              build());
        }

        // Log events of type INVALID_CATEGORY
        for (int i = 0; i < numInvalidCategory; i++) {
            screenLogger.fail(job,
                              new FailEventBuilder(Screen.INVALID_CATEGORY).
                              inEntity("diagnoses.csv").
                              inAttr("icd-10-code").
                              withKeyAttr("visit_id").
                              withKey("1224").
                              withVal("H96").
                              withAction(ExceptionAction.NONE).
                              withInfo("Non-existent ICD-10 code").
                              build());
        }
        for (int i = 0; i < Math.round((double) numInvalidCategory * 2.74d); i++) {
            screenLogger.pass(job,
                              new PassEventBuilder(Screen.INVALID_CATEGORY).
                              inEntity("diagnoses.csv").
                              inAttr("icd-10-code").
                              build());
        }

        wait(waitSeconds);

        // Signal end of the ETL process
        screenLogger.jobFinished(job, Status.COMPLETED);
    }

    /**
     * Wait a given number of seconds
     * @param waitSeconds number of seconds to wait
     */
    private static void wait(int waitSeconds) {
        // Wait
        logger.info(String.format("Waiting %s seconds", waitSeconds));
        try {
            Thread.sleep(waitSeconds * 1000);
        } catch (InterruptedException e) {
            logger.error("Something went wrong during Thread.sleep()");
            e.printStackTrace();
        }
    }
}
