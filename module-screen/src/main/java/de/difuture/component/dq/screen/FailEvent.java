package de.difuture.component.dq.screen;

import de.difuture.component.dq.common.ExceptionAction;
import de.difuture.component.dq.common.Screen;

/**
 * Encapsulates a fail event.
 * @author Helmut Spengler
 */
public class FailEvent extends ScreeningEvent {

    /**
     * Builder class for FailEvent
     * @author Helmut Spengler
     */
    public static class FailEventBuilder {
        
        /** The screen performed */
        private Screen screen;
        
        /** The screened source entity */
        private String sourceEntity;
        
        /** The screened attribute */
        private String sourceEntityAttr;  
        
        /** The key attribute */
        private String sourceEntityKeyAttr;
        
        /** The key of the entry */
        private String sourceEntityKey;
        
        /** The erroneous entry */
        private String sourceEntityErrorVal;

        /** The action performed */
        private ExceptionAction actionPerformed;

        /** Additional information */
        private String info;


        /** Constructor
         * @param screen the screen
         */
        public FailEventBuilder(Screen screen) {
            this.screen = screen;
        }

        /**
         * Define the sourceEntity
         * @param sourceEntity the source entity
         * @return builder containing the sourceEntity 
         */
        public FailEventBuilder inEntity(String sourceEntity){
            this.sourceEntity = sourceEntity;
            return this;
        }

        /**
         * Define the sourceEntityAttr
         * @param sourceEntityAttr the attribute
         * @return builder containing the attribute
         */
        public FailEventBuilder inAttr(String sourceEntityAttr){
            this.sourceEntityAttr = sourceEntityAttr;
            return this;
        }

        /**
         * Define the sourceEntityKeyAttr
         * @param sourceEntityKeyAttr the key attribute
         * @return builder containing the key attribute
         */
        public FailEventBuilder withKeyAttr(String sourceEntityKeyAttr){
            this.sourceEntityKeyAttr = sourceEntityKeyAttr;
            return this;
        }

        /**
         * Define the key value of the entry
         * @param sourceEntityKey the key value
         * @return builder containing the key value
         */
        public FailEventBuilder withKey(String sourceEntityKey){
            this.sourceEntityKey = sourceEntityKey;
            return this;
        }

        /**
         * Define the erroneous value
         * @param sourceEntityErrorVal the key value
         * @return builder containing the key value
         */
        public FailEventBuilder withVal(String sourceEntityErrorVal){
            this.sourceEntityErrorVal = sourceEntityErrorVal;
            return this;
        }

        /**
         * Define the action performed
         * @param actionPerformed the action performed
         * @return builder containing the action performed
         */
        public FailEventBuilder withAction(ExceptionAction actionPerformed){
            this.actionPerformed = actionPerformed;
            return this;
        }

        /**
         * Define additional information
         * @param info additional information
         * @return builder containing additional information
         */
        public FailEventBuilder withInfo(String info){
            this.info = info;
            return this;
        }

        /**
         * Build the ScreeningEvent
         * @return the screeningEvent
         */
        public FailEvent build(){
            return new FailEvent(screen, sourceEntity, sourceEntityKeyAttr, sourceEntityKey, sourceEntityAttr, sourceEntityErrorVal, actionPerformed, info);
        }
    }
    
    /** The key attribute */
    protected final String sourceEntityKeyAttr;
    
    /** The key of the entry */
    protected final String sourceEntityKey;
    
    /** The erroneous entry */
    protected final String sourceEntityErrorVal;

    /** The action performed */
    protected final ExceptionAction actionPerformed;

    /** Additional information */
    protected final String info;

    /**
     * Constructor
     * @param screen the screen
     * @param sourceEntity the source entity
     * @param sourceEntityKeyAttr the key attribute
     * @param sourceEntityKey the key of the entry
     * @param sourceEntityAttr the attribute
     * @param sourceEntityErrorVal the erroneous value
     * @param actionPerformed the action performed
     * @param info additional information
     */
    public FailEvent (Screen screen, String sourceEntity, String sourceEntityKeyAttr, String sourceEntityKey, String sourceEntityAttr, String sourceEntityErrorVal, ExceptionAction actionPerformed, String info) {
        super(screen, sourceEntity, sourceEntityAttr);
        
        if (null == sourceEntityKeyAttr || "".equals(sourceEntityKeyAttr)) {
            String message = "Parameter 'sourceEntityKeyAttr' may not be null or empty. Value was " + (sourceEntityKeyAttr == null ? "null." : "empty.");
            logger.error(message);
            throw new IllegalArgumentException(message);
        } else if (null == sourceEntityKey || "".equals(sourceEntityKey)) {
            String message = "Parameter 'sourceEntityKey' may not be null or empty. Value was " + (sourceEntityKey == null ? "null." : "empty.");
            logger.error(message);
            throw new IllegalArgumentException(message);
        } else if (null == actionPerformed) {
            String message = "Parameter 'actionPerformed' may not be null.";
            logger.error(message);
            throw new IllegalArgumentException(message);
        }
        // TODO implement tests for checks for null or empty
        this.sourceEntityKeyAttr = sourceEntityKeyAttr;
        this.sourceEntityKey = sourceEntityKey;
        this.sourceEntityErrorVal = sourceEntityErrorVal;
        this.actionPerformed = actionPerformed;
        this.info = info;
    }
}
