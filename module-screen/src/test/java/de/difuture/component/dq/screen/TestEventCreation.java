package de.difuture.component.dq.screen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import de.difuture.component.dq.common.ExceptionAction;
import de.difuture.component.dq.common.Screen;
import de.difuture.component.dq.screen.FailEvent.FailEventBuilder;
import de.difuture.component.dq.screen.PassEvent.PassEventBuilder;

class TestEventCreation {
    
    static private Screen testScreen = Screen.DATE_INVERSION;
    static private String sourceEntity = "sourceEntity";
    static private String sourceEntityAttr = "sourceEntityAttr";
    static private String sourceEntityKeyAttr = "sourceEntityKeyAttr";
    static private String sourceEntityKey = "sourceEntityKey";
    static private String sourceEntityErrorVal = "sourceEntityErrorVal";
    static private ExceptionAction exceptionAction = ExceptionAction.CORRECT;
    static private String info = "info";

    @Test
    public void testFailEventBuilder() {
        FailEvent failEvent =
                new FailEventBuilder(testScreen).
                inEntity(sourceEntity).
                inAttr(sourceEntityAttr).
                withKeyAttr(sourceEntityKeyAttr).
                withKey(sourceEntityKey).
                withVal(sourceEntityErrorVal).
                withAction(exceptionAction).
                withInfo(info).
                build();

        assertEquals(testScreen, failEvent.screen);
        assertEquals(sourceEntity, failEvent.sourceEntity);
        assertEquals(sourceEntityAttr, failEvent.sourceEntityAttr);
        assertEquals(sourceEntityKeyAttr, failEvent.sourceEntityKeyAttr);
        assertEquals(sourceEntityKey, failEvent.sourceEntityKey);
        assertEquals(sourceEntityErrorVal, failEvent.sourceEntityErrorVal);
        assertEquals(exceptionAction, failEvent.actionPerformed);
        assertEquals(info, failEvent.info);
    }

    @Test
    public void testExceptionOnEmptyScreen() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new PassEventBuilder(null).
            inEntity(sourceEntity).
            inAttr(sourceEntityAttr).
            build();
        });
     
        String expectedMessage = "Parameter 'screen' may not be null.";
        String actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testExceptionOnEmptysourceEntity() {
        //null
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
//            inEntity(null). // Omit on purpose
            inAttr(sourceEntityAttr).
            build();
        });
     
        String expectedMessage = "Parameter 'sourceEntity' may not be null or empty. Value was null.";
        String actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
         // Empty string
        exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity("").
            inAttr(sourceEntityAttr).
            build();
        });
     
        expectedMessage = "Parameter 'sourceEntity' may not be null or empty. Value was empty.";
        actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testExceptionOnEmptysourceEntityAttr() {
        //null
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity(sourceEntity).
//            inAttr(null). // Omit on purpose
            build();
        });
     
        String expectedMessage = "Parameter 'sourceEntityAttr' may not be null or empty. Value was null.";
        String actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
        
        
         // Empty string
        exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity(sourceEntity).
            inAttr("").
            build();
        });
     
        expectedMessage = "Parameter 'sourceEntityAttr' may not be null or empty. Value was empty.";
        actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testExceptionOnEmptysourceEntityKeyAttr() {
        //null
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity(sourceEntity).
            inAttr(sourceEntityAttr).
//          withKeyAttr(sourceEntityKeyAttr). // Omit on purpose
            build();
        });
     
        String expectedMessage = "Parameter 'sourceEntityKeyAttr' may not be null or empty. Value was null.";
        String actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
        
     
         // Empty string
        exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity(sourceEntity).
            inAttr(sourceEntityAttr).
            withKeyAttr("").
            build();
        });
     
        expectedMessage = "Parameter 'sourceEntityKeyAttr' may not be null or empty. Value was empty.";
        actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testExceptionOnEmptysourceEntityKey() {
        //null
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity(sourceEntity).
            inAttr(sourceEntityAttr).
            withKeyAttr(sourceEntityKeyAttr).
//            withKey(sourceEntityKey). // Omit on purpose
            build();
        });
     
        String expectedMessage = "Parameter 'sourceEntityKey' may not be null or empty. Value was null.";
        String actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
        
     
         // Empty string
        exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity(sourceEntity).
            inAttr(sourceEntityAttr).
            withKeyAttr(sourceEntityKeyAttr).
            withKey("").
            build();
        });
     
        expectedMessage = "Parameter 'sourceEntityKey' may not be null or empty. Value was empty.";
        actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testExceptionOnEmptyAction() {
        //null
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new FailEventBuilder(testScreen).
            inEntity(sourceEntity).
            inAttr(sourceEntityAttr).
            withKeyAttr(sourceEntityKeyAttr).
            withKey(sourceEntityKey).
//            withAction(actionPerformed). // Omit this on purpose
            build();
        });
     
        String expectedMessage = "Parameter 'actionPerformed' may not be null.";
        String actualMessage = exception.getMessage();
     
        assertEquals(expectedMessage, actualMessage);
    }
}
