package de.difuture.component.dq.screen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import de.difuture.component.dq.common.ExceptionAction;
import de.difuture.component.dq.common.Phase;
import de.difuture.component.dq.common.Screen;
import de.difuture.component.dq.common.Status;
import de.difuture.component.dq.common.Util;
import de.difuture.component.dq.screen.ScreenLogger.Job;

/**
 * Test correct counting of screening events. This test class requires
 * the environment variable ${DQ_ES} set to "available" as well as
 * a running event store instance. The simplest way of creating one is
 * starting the dockerized demo-event store as described in the readme
 * of this project.
 * 
 * @author Helmut Spengler
 *
 */
@EnabledIfEnvironmentVariable(named = "DQ_ES", matches = "available")
class TestScreening {
    
    static Connection connection = null;
    static final int EXPECTED_NUMBER_OF_TOTAL_FAILS  = 1210;
    static final int EXPECTED_NUMBER_OF_TOTAL_PASSES = 3640;

    @BeforeAll
    static void createDataQualityEvents() throws Exception {        
        // Acquire DB connection and screenLogger instance
        connection = Util.getInstance().getDatabaseConnection("localhost", "5632", "es", "postgres", "aU6s367W");
        
        ScreenLogger.init(connection);
        ScreenLogger screenLogger = ScreenLogger.getInstance();
        Job job = screenLogger.jobStarted(Phase.STAGE, "ExampleJob", "ExampleSystem");
        
        // Log events of type VALUE_RANGE_VIOLATION
        for (int i = 0; i < 22; i++) {
            screenLogger.fail(job, new FailEvent(Screen.VALUE_RANGE_VIOLATION, "lab_values.csv", "lab_id", "4711", "Glucose", "-65", ExceptionAction.NONE, "Negative value for mg/dL"));
        }        
        for (int i = 0; i < 122; i++) {
            screenLogger.pass(job, new PassEvent(Screen.VALUE_RANGE_VIOLATION, "lab_values.csv", "Glucose"));
        }
        
        // ... and again
        for (int i = 0; i < 33; i++) {
            screenLogger.fail(job, new FailEvent(Screen.VALUE_RANGE_VIOLATION, "lab_values.csv", "lab_id", "4711", "Glucose", "-65", ExceptionAction.NONE, "Negative value for mg/dL"));
        }        
        for (int i = 0; i < 133; i++) {
            screenLogger.pass(job, new PassEvent(Screen.VALUE_RANGE_VIOLATION, "lab_values.csv", "Glucose"));
        }
        
        // Log events of type MISSING_VALUE
        //First for the same combination of entity/attribute as before
        for (int i = 0; i < 233; i++) {
            screenLogger.fail(job, new FailEvent(Screen.MISSING_VALUE, "lab_values.csv", "lab_id", "4711", "Glucose", "-65", ExceptionAction.NONE, "Negative value for mg/dL"));
        }        
        for (int i = 0; i < 525; i++) {
            screenLogger.pass(job, new PassEvent(Screen.MISSING_VALUE, "lab_values.csv", "Glucose"));
        }
        
        
        for (int i = 0; i < 201; i++) {
            screenLogger.fail(job, new FailEvent(Screen.MISSING_VALUE, "lab_values.csv", "lab_id", "1227", "measurement-date", "EMPTY", ExceptionAction.NONE, "Missing measurement value"));
        }
        for (int i = 0; i < 2001; i++) {
            screenLogger.pass(job, new PassEvent(Screen.MISSING_VALUE, "lab_values.csv", "measurement-date"));
        }
        
        // Computational conformance - only fails
        for (int i = 0; i < 721; i++) {
            screenLogger.fail(job, new FailEvent(Screen.WRONG_SCORE_CALCULATION, "scores.csv", "patient_id", "447", "EDSS", "500000", ExceptionAction.NONE, "Score wrong"));
        }
        
        // Temporal plausibility - only passes
        for (int i = 0; i < 859; i++) {
            screenLogger.pass(job, new PassEvent(Screen.DATE_INVERSION, "adt.csv", "admission-date"));
        }
        
        screenLogger.jobFinished(job, Status.COMPLETED);
    }

    @AfterAll
    static void tearDownAfterClass() throws SQLException  {
        connection.createStatement().execute("UPDATE dq.schema_version SET \"version\" = 'TEST' WHERE \"version\" IS NULL;");
        connection.close();
    }

    @Test
    void testErrorCountInErrorEventFactTable() throws SQLException {
        
        String SQL = "SELECT COUNT(*) as count FROM dq.error_event_fact eef\n" + 
                "JOIN dq.job_dimension jd ON eef.job_fk = jd.job_pk \n" + 
                "WHERE\n" + 
                "  jd.status = 'COMPLETED' AND\n" + 
                "  jd.job_finish = (SELECT MAX(jd2.job_finish) FROM dq.job_dimension jd2)";
        
        
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(SQL);
        int count = -1;
        while(rs.next()){
            count = rs.getInt("count");
        }
        
        assertNotEquals(-1, count, "Count query didn't deliver a result.");
        assertEquals(EXPECTED_NUMBER_OF_TOTAL_FAILS, count);
    }

    @Test
    void testErrorCountInEventCountFact() throws SQLException {
        
        String SQL = "SELECT COUNT(*) FROM dq.event_count_fact ecf\n" + 
                "                JOIN dq.job_dimension jd ON ecf.job_fk = jd.job_pk\n" + 
                "                WHERE\n" + 
                "                  jd.status = 'COMPLETED' AND\n" + 
                "                  jd.job_finish = (SELECT MAX(jd2.job_finish) FROM dq.job_dimension jd2)";
        
        
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(SQL);
        int count = -1;
        while(rs.next()){
            count = rs.getInt("count");
        }
        
        assertNotEquals(-1, count, "Query didn't deliver a result.");
        assertEquals(8, count);
    }

    @Test
    void testTotalNumberOfFailsAndPassesInEventCountFact() throws SQLException {
        
        // Template
        String SQLBASE = "SELECT SUM(ecf.row_count) as sum FROM dq.event_count_fact ecf\n" + 
                "JOIN dq.job_dimension jd ON ecf.job_fk = jd.job_pk\n" + 
                "WHERE\n" + 
                "  jd.status = 'COMPLETED' AND\n" + 
                "  jd.job_finish = (SELECT MAX(jd2.job_finish) FROM dq.job_dimension jd2) AND\n" + 
                "  ecf.passed = %s";
        
        // Fail
        String SQLFAIL = String.format(SQLBASE, "false");        
        
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(SQLFAIL);
        int sum = -1;
        while(rs.next()){
            sum = rs.getInt("sum");
        }        
        assertNotEquals(-1, sum, "Query didn't deliver a result.");
        assertEquals(EXPECTED_NUMBER_OF_TOTAL_FAILS, sum);
        
        //Pass
        String SQLPASS = String.format(SQLBASE, "true");
        
        stmt = connection.createStatement();
        rs = stmt.executeQuery(SQLPASS);
        sum = -1;
        while(rs.next()){
            sum = rs.getInt("sum");
        }        
        assertNotEquals(-1, sum, "Query didn't deliver a result.");
        assertEquals(EXPECTED_NUMBER_OF_TOTAL_PASSES, sum);
    }

    @Test
    void testCumulativeNumberOfFailsAndPassesInEventCountFact() throws SQLException {
        
        // Template
        String SQLBASE = "SELECT ecf.row_count FROM dq.event_count_fact ecf\n" + 
                "JOIN dq.job_dimension jd ON ecf.job_fk = jd.job_pk\n" + 
                "JOIN dq.screen_dimension sd ON ecf.screen_fk = sd.screen_pk \n" + 
                "WHERE\n" + 
                "  jd.status = 'COMPLETED' AND\n" + 
                "  jd.job_finish = (SELECT MAX(jd2.job_finish) FROM dq.job_dimension jd2) AND\n" + 
                "  sd.screen_short_name = 'pa_val_range_viol' AND\n" + 
                "  ecf.entity_name = 'lab_values.csv' AND\n" + 
                "  ecf.attr_name = 'Glucose' AND\n" + 
                "  ecf.passed = %s;";
        
        // Fail
        String SQLFAIL = String.format(SQLBASE, "false");        
        
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(SQLFAIL);
        int rc = -1;
        while(rs.next()){
            rc = rs.getInt("row_count");
        }        
        assertNotEquals(-1, rc, "Query didn't deliver a result.");
        assertEquals(55, rc);
        
        //Pass
        String SQLPASS = String.format(SQLBASE, "true");
        
        stmt = connection.createStatement();
        rs = stmt.executeQuery(SQLPASS);
        rc = -1;
        while(rs.next()){
            rc = rs.getInt("row_count");
        }        
        assertNotEquals(-1, rc, "Query didn't deliver a result.");
        assertEquals(255, rc);
    }
}
