#!/bin/bash

set -e

cd ..
mvn clean package
cp module-audit/target/component-dqmonitor-audit-1.1.2-RELEASE-jar-with-dependencies.jar docker/provisioning/audit-service
cd docker
docker build --no-cache -t audit-service ./provisioning/audit-service
