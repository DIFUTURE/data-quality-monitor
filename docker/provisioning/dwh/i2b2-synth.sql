--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 9.5.19

CREATE ROLE i2b2demodata;
ALTER ROLE i2b2demodata WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md5678d6fa0de7731774d1d1657ab543737';
CREATE ROLE i2b2hive;
ALTER ROLE i2b2hive WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md5ebeb641f4f6e6a2b935a2198c2faacb6';
CREATE ROLE i2b2imdata;
ALTER ROLE i2b2imdata WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md5ff3e62c8e0197291175ac4455cf3b5a7';
CREATE ROLE i2b2metadata;
ALTER ROLE i2b2metadata WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md587129cf4492d3d0744cb0eecf002ae23';
CREATE ROLE i2b2pm;
ALTER ROLE i2b2pm WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md51a262dcde76c98174a38b31031579ec2';
CREATE ROLE i2b2workdata;
ALTER ROLE i2b2workdata WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md569b322e962f70bdd1ecdb89602337f1d';


--
-- Database creation
--

CREATE DATABASE i2b2 WITH TEMPLATE = template0 OWNER = postgres ENCODING = 'UTF8';
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;\connect i2b2

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 9.5.19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: i2b2demodata; Type: SCHEMA; Schema: -; Owner: postgres
--

--
-- Name: i2b2demodata; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA i2b2demodata;


ALTER SCHEMA i2b2demodata OWNER TO postgres;

--
-- Name: i2b2hive; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA i2b2hive;


ALTER SCHEMA i2b2hive OWNER TO postgres;

--
-- Name: i2b2imdata; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA i2b2imdata;


ALTER SCHEMA i2b2imdata OWNER TO postgres;

--
-- Name: i2b2metadata; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA i2b2metadata;


ALTER SCHEMA i2b2metadata OWNER TO postgres;

--
-- Name: i2b2pm; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA i2b2pm;


ALTER SCHEMA i2b2pm OWNER TO postgres;

--
-- Name: i2b2workdata; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA i2b2workdata;


ALTER SCHEMA i2b2workdata OWNER TO postgres;

--
-- Name: ts_batch; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA ts_batch;


ALTER SCHEMA ts_batch OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: create_temp_concept_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_concept_table(tempconcepttablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    EXECUTE 'create table ' ||  tempConceptTableName || ' (
        CONCEPT_CD varchar(50) NOT NULL, 
        CONCEPT_PATH varchar(900) NOT NULL , 
        NAME_CHAR varchar(2000), 
        CONCEPT_BLOB text, 
        UPDATE_DATE timestamp, 
        DOWNLOAD_DATE timestamp, 
        IMPORT_DATE timestamp, 
        SOURCESYSTEM_CD varchar(50)
    ) WITH OIDS';
    EXECUTE 'CREATE INDEX idx_' || tempConceptTableName || '_pat_id ON ' || tempConceptTableName || '  (CONCEPT_PATH)';
    EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_concept_table(tempconcepttablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: create_temp_eid_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_eid_table(temppatientmappingtablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    EXECUTE 'create table ' ||  tempPatientMappingTableName || ' (
        ENCOUNTER_MAP_ID        varchar(200) NOT NULL,
        ENCOUNTER_MAP_ID_SOURCE     varchar(50) NOT NULL,
        PROJECT_ID              VARCHAR(50) NOT NULL,
        PATIENT_MAP_ID          varchar(200), 
        PATIENT_MAP_ID_SOURCE   varchar(50), 
        ENCOUNTER_ID            varchar(200) NOT NULL,
        ENCOUNTER_ID_SOURCE     varchar(50) ,
        ENCOUNTER_NUM           numeric, 
        ENCOUNTER_MAP_ID_STATUS    varchar(50),
        PROCESS_STATUS_FLAG     char(1),
        UPDATE_DATE timestamp, 
        DOWNLOAD_DATE timestamp, 
        IMPORT_DATE timestamp, 
        SOURCESYSTEM_CD varchar(50)
    ) WITH OIDS';
    EXECUTE 'CREATE INDEX idx_' || tempPatientMappingTableName || '_eid_id ON ' || tempPatientMappingTableName || '  (ENCOUNTER_ID, ENCOUNTER_ID_SOURCE, ENCOUNTER_MAP_ID, ENCOUNTER_MAP_ID_SOURCE, ENCOUNTER_NUM)';
    EXECUTE 'CREATE INDEX idx_' || tempPatientMappingTableName || '_stateid_eid_id ON ' || tempPatientMappingTableName || '  (PROCESS_STATUS_FLAG)';  
    EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '%%%', SQLSTATE || ' - ' || SQLERRM;
END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_eid_table(temppatientmappingtablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: create_temp_modifier_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_modifier_table(tempmodifiertablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
EXECUTE 'create table ' ||  tempModifierTableName || ' (
        MODIFIER_CD varchar(50) NOT NULL, 
        MODIFIER_PATH varchar(900) NOT NULL , 
        NAME_CHAR varchar(2000), 
        MODIFIER_BLOB text, 
        UPDATE_DATE timestamp, 
        DOWNLOAD_DATE timestamp, 
        IMPORT_DATE timestamp, 
        SOURCESYSTEM_CD varchar(50)
         ) WITH OIDS';
 EXECUTE 'CREATE INDEX idx_' || tempModifierTableName || '_pat_id ON ' || tempModifierTableName || '  (MODIFIER_PATH)';
EXCEPTION
        WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_modifier_table(tempmodifiertablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: create_temp_patient_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_patient_table(temppatientdimensiontablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    -- Create temp table to store encounter/visit information
    EXECUTE 'create table ' ||  tempPatientDimensionTableName || ' (
        patient_id varchar(200), 
        patient_id_source varchar(50),
        patient_num numeric(38,0),
        vital_status_cd varchar(50), 
        birth_date timestamp, 
        death_date timestamp, 
        sex_cd char(50), 
        age_in_years_num numeric(5,0), 
        language_cd varchar(50), 
        race_cd varchar(50 ), 
        marital_status_cd varchar(50), 
        religion_cd varchar(50), 
        zip_cd varchar(50), 
        statecityzip_path varchar(700), 
        patient_blob text, 
        update_date timestamp, 
        download_date timestamp, 
        import_date timestamp, 
        sourcesystem_cd varchar(50)
    )';
    EXECUTE 'CREATE INDEX idx_' || tempPatientDimensionTableName || '_pat_id ON ' || tempPatientDimensionTableName || '  (patient_id, patient_id_source,patient_num)';
    EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE '%%%', SQLSTATE || ' - ' || SQLERRM;
END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_patient_table(temppatientdimensiontablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: create_temp_pid_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_pid_table(temppatientmappingtablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
	EXECUTE 'create table ' ||  tempPatientMappingTableName || ' (
		PATIENT_MAP_ID varchar(200), 
		PATIENT_MAP_ID_SOURCE varchar(50), 
		PATIENT_ID_STATUS varchar(50), 
		PATIENT_ID  varchar(200),
		PATIENT_ID_SOURCE varchar(50),
		PROJECT_ID   VARCHAR(50) ,
		PATIENT_NUM numeric(38,0),
		PATIENT_MAP_ID_STATUS varchar(50), 
		PROCESS_STATUS_FLAG char(1), 
		UPDATE_DATE timestamp, 
		DOWNLOAD_DATE timestamp, 
		IMPORT_DATE timestamp, 
		SOURCESYSTEM_CD varchar(50)
	) WITH OIDS';
	EXECUTE 'CREATE INDEX idx_' || tempPatientMappingTableName || '_pid_id ON ' || tempPatientMappingTableName || '  ( PATIENT_ID, PATIENT_ID_SOURCE )';
	EXECUTE 'CREATE INDEX idx_' || tempPatientMappingTableName || 'map_pid_id ON ' || tempPatientMappingTableName || '  
	( PATIENT_ID, PATIENT_ID_SOURCE,PATIENT_MAP_ID, PATIENT_MAP_ID_SOURCE,  PATIENT_NUM )';
	EXECUTE 'CREATE INDEX idx_' || tempPatientMappingTableName || 'stat_pid_id ON ' || tempPatientMappingTableName || '  
	(PROCESS_STATUS_FLAG)';
	EXCEPTION
	WHEN OTHERS THEN
		RAISE NOTICE '%%%', SQLSTATE || ' - ' || SQLERRM;
END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_pid_table(temppatientmappingtablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: create_temp_provider_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_provider_table(tempprovidertablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    EXECUTE 'create table ' ||  tempProviderTableName || ' (
        PROVIDER_ID varchar(50) NOT NULL, 
        PROVIDER_PATH varchar(700) NOT NULL, 
        NAME_CHAR varchar(2000), 
        PROVIDER_BLOB text, 
        UPDATE_DATE timestamp, 
        DOWNLOAD_DATE timestamp, 
        IMPORT_DATE timestamp, 
        SOURCESYSTEM_CD varchar(50), 
        UPLOAD_ID numeric
    ) WITH OIDS';
    EXECUTE 'CREATE INDEX idx_' || tempProviderTableName || '_ppath_id ON ' || tempProviderTableName || '  (PROVIDER_PATH)';
    EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      

END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_provider_table(tempprovidertablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: create_temp_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_table(temptablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    EXECUTE 'create table ' ||  tempTableName || '  (
        encounter_num  numeric(38,0),
        encounter_id varchar(200) not null, 
        encounter_id_source varchar(50) not null,
        concept_cd       varchar(50) not null, 
        patient_num numeric(38,0), 
        patient_id  varchar(200) not null,
        patient_id_source  varchar(50) not null,
        provider_id   varchar(50),
        start_date   timestamp, 
        modifier_cd varchar(100),
        instance_num numeric(18,0),
        valtype_cd varchar(50),
        tval_char varchar(255),
        nval_num numeric(18,5),
        valueflag_cd char(50),
        quantity_num numeric(18,5),
        confidence_num numeric(18,0),
        observation_blob text,
        units_cd varchar(50),
        end_date    timestamp,
        location_cd varchar(50),
        update_date  timestamp,
        download_date timestamp,
        import_date timestamp,
        sourcesystem_cd varchar(50) ,
        upload_id integer
    ) WITH OIDS';
    EXECUTE 'CREATE INDEX idx_' || tempTableName || '_pk ON ' || tempTableName || '  ( encounter_num,patient_num,concept_cd,provider_id,start_date,modifier_cd,instance_num)';
    EXECUTE 'CREATE INDEX idx_' || tempTableName || '_enc_pat_id ON ' || tempTableName || '  (encounter_id,encounter_id_source, patient_id,patient_id_source )';
    EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM; 
END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_table(temptablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: create_temp_visit_table(text); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.create_temp_visit_table(temptablename text, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    -- Create temp table to store encounter/visit information
    EXECUTE 'create table ' ||  tempTableName || ' (
        encounter_id                    varchar(200) not null,
        encounter_id_source             varchar(50) not null, 
        project_id                      varchar(50) not null,
        patient_id                      varchar(200) not null,
        patient_id_source               varchar(50) not null,
        encounter_num                   numeric(38,0), 
        inout_cd                        varchar(50),
        location_cd                     varchar(50),
        location_path                   varchar(900),
        start_date                      timestamp, 
        end_date                        timestamp,
        visit_blob                      text,
        update_date                     timestamp,
        download_date                   timestamp,
        import_date                     timestamp,
        sourcesystem_cd                 varchar(50)
    ) WITH OIDS';
    EXECUTE 'CREATE INDEX idx_' || tempTableName || '_enc_id ON ' || tempTableName || '  ( encounter_id,encounter_id_source,patient_id,patient_id_source )';
    EXECUTE 'CREATE INDEX idx_' || tempTableName || '_patient_id ON ' || tempTableName || '  ( patient_id,patient_id_source )';
    EXCEPTION
    WHEN OTHERS THEN    
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;    
END;
$$;


ALTER FUNCTION i2b2demodata.create_temp_visit_table(temptablename text, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_concept_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_concept_fromtemp(tempconcepttablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    --Delete duplicate rows with same encounter and patient combination
    EXECUTE 'DELETE 
    FROM
    ' || tempConceptTableName || ' t1 
    WHERE
    oid > (SELECT  
        min(oid) 
        FROM 
        ' || tempConceptTableName || ' t2
        WHERE 
        t1.concept_cd = t2.concept_cd 
        AND t1.concept_path = t2.concept_path
    )';
    EXECUTE ' UPDATE concept_dimension  
    SET  
    concept_cd=temp.concept_cd
    ,name_char=temp.name_char
    ,concept_blob=temp.concept_blob
    ,update_date=temp.update_date
    ,download_date=temp.download_date
    ,import_date=Now()
    ,sourcesystem_cd=temp.sourcesystem_cd
    ,upload_id=' || UPLOAD_ID  || '
    FROM 
    ' || tempConceptTableName || '  temp   
    WHERE 
    temp.concept_path = concept_dimension.concept_path 
    AND temp.update_date >= concept_dimension.update_date 
    AND EXISTS (SELECT 1 
        FROM ' || tempConceptTableName || ' temp  
        WHERE temp.concept_path = concept_dimension.concept_path 
        AND temp.update_date >= concept_dimension.update_date
    )
    ';
    --Create new patient(patient_mapping) if temp table patient_ide does not exists 
    -- in patient_mapping table.
    EXECUTE 'INSERT INTO concept_dimension  (
        concept_cd
        ,concept_path
        ,name_char
        ,concept_blob
        ,update_date
        ,download_date
        ,import_date
        ,sourcesystem_cd
        ,upload_id
    )
    SELECT  
    concept_cd
    ,concept_path
    ,name_char
    ,concept_blob
    ,update_date
    ,download_date
    ,Now()
    ,sourcesystem_cd
    ,' || upload_id || '
    FROM ' || tempConceptTableName || '  temp
    WHERE NOT EXISTS (SELECT concept_cd 
        FROM concept_dimension cd 
        WHERE cd.concept_path = temp.concept_path)
    ';
    EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.insert_concept_fromtemp(tempconcepttablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_eid_map_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_eid_map_fromtemp(tempeidtablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE

existingEncounterNum varchar(32);
maxEncounterNum bigint;
distinctEidCur REFCURSOR;
disEncounterId varchar(100); 
disEncounterIdSource varchar(100);

BEGIN
    EXECUTE ' delete  from ' || tempEidTableName ||  ' t1  where 
    oid > (select min(oid) from ' || tempEidTableName || ' t2 
        where t1.encounter_map_id = t2.encounter_map_id
        and t1.encounter_map_id_source = t2.encounter_map_id_source
        and t1.encounter_id = t2.encounter_id
        and t1.encounter_id_source = t2.encounter_id_source) ';
    LOCK TABLE  encounter_mapping IN EXCLUSIVE MODE NOWAIT;
    select max(encounter_num) into STRICT  maxEncounterNum from encounter_mapping ; 
    if coalesce(maxEncounterNum::text, '') = '' then 
        maxEncounterNum := 0;
    end if;
    open distinctEidCur for EXECUTE 'SELECT distinct encounter_id,encounter_id_source from ' || tempEidTableName ||' ' ;
    loop
        FETCH distinctEidCur INTO disEncounterId, disEncounterIdSource;
        IF NOT FOUND THEN EXIT; END IF; -- apply on distinctEidCur
            -- dbms_output.put_line(disEncounterId);
            if  disEncounterIdSource = 'HIVE'  THEN 
                begin
                    --check if hive number exist, if so assign that number to reset of map_id's within that pid
                    select encounter_num into existingEncounterNum from encounter_mapping where encounter_num = CAST(disEncounterId AS numeric) and encounter_ide_source = 'HIVE';
                    EXCEPTION  when NO_DATA_FOUND THEN
                        existingEncounterNum := null;
                end;
                if (existingEncounterNum IS NOT NULL AND existingEncounterNum::text <> '') then 
                    EXECUTE ' update ' || tempEidTableName ||' set encounter_num = CAST(encounter_id AS numeric), process_status_flag = ''P''
                    where encounter_id = $1 and not exists (select 1 from encounter_mapping em where em.encounter_ide = encounter_map_id
                        and em.encounter_ide_source = encounter_map_id_source)' using disEncounterId;
                else 
                    -- generate new patient_num i.e. take max(_num) + 1 
                    if maxEncounterNum < CAST(disEncounterId AS numeric) then 
                        maxEncounterNum := disEncounterId;
                    end if ;
                    EXECUTE ' update ' || tempEidTableName ||' set encounter_num = CAST(encounter_id AS numeric), process_status_flag = ''P'' where 
                    encounter_id =  $1 and encounter_id_source = ''HIVE'' and not exists (select 1 from encounter_mapping em where em.encounter_ide = encounter_map_id
                        and em.encounter_ide_source = encounter_map_id_source)' using disEncounterId;
                end if;    
                -- test if record fectched
                -- dbms_output.put_line(' HIVE ');
            else 
                begin
                    select encounter_num into STRICT  existingEncounterNum from encounter_mapping where encounter_ide = disEncounterId and 
                    encounter_ide_source = disEncounterIdSource ; 
                    -- test if record fetched. 
                    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        existingEncounterNum := null;
                end;
                if existingEncounterNum is not  null then 
                    EXECUTE ' update ' || tempEidTableName ||' set encounter_num = CAST($1 AS numeric) , process_status_flag = ''P''
                    where encounter_id = $2 and not exists (select 1 from encounter_mapping em where em.encounter_ide = encounter_map_id
                        and em.encounter_ide_source = encounter_map_id_source)' using existingEncounterNum, disEncounterId;
                else 
                    maxEncounterNum := maxEncounterNum + 1 ;
                    --TODO : add update colunn
                    EXECUTE ' insert into ' || tempEidTableName ||' (encounter_map_id,encounter_map_id_source,encounter_id,encounter_id_source,encounter_num,process_status_flag
                        ,encounter_map_id_status,update_date,download_date,import_date,sourcesystem_cd,project_id) 
                    values($1,''HIVE'',$2,''HIVE'',$3,''P'',''A'',Now(),Now(),Now(),''edu.harvard.i2b2.crc'',''HIVE'')' using maxEncounterNum,maxEncounterNum,maxEncounterNum; 
                    EXECUTE ' update ' || tempEidTableName ||' set encounter_num =  $1 , process_status_flag = ''P'' 
                    where encounter_id = $2 and  not exists (select 1 from 
                        encounter_mapping em where em.encounter_ide = encounter_map_id
                        and em.encounter_ide_source = encounter_map_id_source)' using maxEncounterNum, disEncounterId;
                end if ;
                -- dbms_output.put_line(' NOT HIVE ');
            end if; 
    END LOOP;
    close distinctEidCur ;
    -- do the mapping update if the update date is old

EXECUTE 'UPDATE encounter_mapping
SET 
encounter_num = CAST(temp.encounter_id AS numeric)
,encounter_ide_status = temp.encounter_map_id_status
,update_date = temp.update_date
,download_date  = temp.download_date
,import_date = Now()
,sourcesystem_cd  = temp.sourcesystem_cd
,upload_id = ' || upload_id ||'
FROM '|| tempEidTableName || '  temp
WHERE 
temp.encounter_map_id = encounter_mapping.encounter_ide 
and temp.encounter_map_id_source = encounter_mapping.encounter_ide_source
and temp.encounter_id_source = ''HIVE''
and coalesce(temp.process_status_flag::text, '''') = ''''  
and coalesce(encounter_mapping.update_date,to_date(''01-JAN-1900'',''DD-MON-YYYY'')) <= coalesce(temp.update_date,to_date(''01-JAN-1900'',''DD-MON-YYYY''))
';

    -- insert new mapping records i.e flagged P
    EXECUTE ' insert into encounter_mapping (encounter_ide,encounter_ide_source,encounter_ide_status,encounter_num,patient_ide,patient_ide_source,update_date,download_date,import_date,sourcesystem_cd,upload_id,project_id) 
    SELECT encounter_map_id,encounter_map_id_source,encounter_map_id_status,encounter_num,patient_map_id,patient_map_id_source,update_date,download_date,Now(),sourcesystem_cd,' || upload_id || ' , project_id
    FROM ' || tempEidTableName || '  
    WHERE process_status_flag = ''P'' ' ; 
    EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;
    end;
    $_$;


ALTER FUNCTION i2b2demodata.insert_eid_map_fromtemp(tempeidtablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_encountervisit_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_encountervisit_fromtemp(temptablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE

maxEncounterNum bigint; 

BEGIN 
    --Delete duplicate rows with same encounter and patient combination
    EXECUTE 'DELETE FROM ' || tempTableName || ' t1 WHERE oid > 
    (SELECT  min(oid) FROM ' || tempTableName || ' t2
        WHERE t1.encounter_id = t2.encounter_id 
        AND t1.encounter_id_source = t2.encounter_id_source
        AND coalesce(t1.patient_id,'''') = coalesce(t2.patient_id,'''')
        AND coalesce(t1.patient_id_source,'''') = coalesce(t2.patient_id_source,''''))';
    LOCK TABLE  encounter_mapping IN EXCLUSIVE MODE NOWAIT;
    -- select max(encounter_num) into maxEncounterNum from encounter_mapping ;
    --Create new patient(patient_mapping) if temp table patient_ide does not exists 
    -- in patient_mapping table.
    EXECUTE 'INSERT INTO encounter_mapping (
        encounter_ide
        , encounter_ide_source
        , encounter_num
        , patient_ide
        , patient_ide_source
        , encounter_ide_status
        , upload_id
        , project_id
    )
    (SELECT 
        distinctTemp.encounter_id
        , distinctTemp.encounter_id_source
        , CAST(distinctTemp.encounter_id AS numeric)
        , distinctTemp.patient_id
        , distinctTemp.patient_id_source
        , ''A''
        ,  '|| upload_id ||'
        , distinctTemp.project_id
        FROM 
        (SELECT 
            distinct encounter_id
            , encounter_id_source
            , patient_id
            , patient_id_source 
            , project_id
            FROM ' || tempTableName || '  temp
            WHERE 
            NOT EXISTS (SELECT encounter_ide 
                FROM encounter_mapping em 
                WHERE 
                em.encounter_ide = temp.encounter_id 
                AND em.encounter_ide_source = temp.encounter_id_source
            )
            AND encounter_id_source = ''HIVE'' 
    )   distinctTemp
) ' ;
    -- update patient_num for temp table
    EXECUTE ' UPDATE ' ||  tempTableName
    || ' SET encounter_num = (SELECT em.encounter_num
        FROM encounter_mapping em
        WHERE em.encounter_ide = '|| tempTableName ||'.encounter_id
        and em.encounter_ide_source = '|| tempTableName ||'.encounter_id_source 
        and coalesce(em.patient_ide_source,'''') = coalesce('|| tempTableName ||'.patient_id_source,'''')
        and coalesce(em.patient_ide,'''')= coalesce('|| tempTableName ||'.patient_id,'''')
    )
    WHERE EXISTS (SELECT em.encounter_num 
        FROM encounter_mapping em
        WHERE em.encounter_ide = '|| tempTableName ||'.encounter_id
        and em.encounter_ide_source = '||tempTableName||'.encounter_id_source
        and coalesce(em.patient_ide_source,'''') = coalesce('|| tempTableName ||'.patient_id_source,'''')
        and coalesce(em.patient_ide,'''')= coalesce('|| tempTableName ||'.patient_id,''''))';      

    EXECUTE ' UPDATE visit_dimension  SET  
    start_date =temp.start_date
    ,end_date=temp.end_date
    ,inout_cd=temp.inout_cd
    ,location_cd=temp.location_cd
    ,visit_blob=temp.visit_blob
    ,update_date=temp.update_date
    ,download_date=temp.download_date
    ,import_date=Now()
    ,sourcesystem_cd=temp.sourcesystem_cd
    , upload_id=' || UPLOAD_ID  || '
    FROM ' || tempTableName || '  temp       
    WHERE
    temp.encounter_num = visit_dimension.encounter_num 
    AND temp.update_date >= visit_dimension.update_date 
    AND exists (SELECT 1 
        FROM ' || tempTableName || ' temp 
        WHERE temp.encounter_num = visit_dimension.encounter_num 
        AND temp.update_date >= visit_dimension.update_date
    ) ';

    EXECUTE 'INSERT INTO visit_dimension  (encounter_num,patient_num,start_date,end_date,inout_cd,location_cd,visit_blob,update_date,download_date,import_date,sourcesystem_cd, upload_id)
    SELECT temp.encounter_num
    , pm.patient_num,
    temp.start_date,temp.end_date,temp.inout_cd,temp.location_cd,temp.visit_blob,
    temp.update_date,
    temp.download_date,
    Now(), 
    temp.sourcesystem_cd,
    '|| upload_id ||'
    FROM 
    ' || tempTableName || '  temp , patient_mapping pm 
    WHERE 
    (temp.encounter_num IS NOT NULL AND temp.encounter_num::text <> '''') and 
    NOT EXISTS (SELECT encounter_num 
        FROM visit_dimension vd 
        WHERE 
        vd.encounter_num = temp.encounter_num) 
    AND pm.patient_ide = temp.patient_id 
    AND pm.patient_ide_source = temp.patient_id_source
    ';
    EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.insert_encountervisit_fromtemp(temptablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_modifier_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_modifier_fromtemp(tempmodifiertablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    --Delete duplicate rows 
    EXECUTE 'DELETE FROM ' || tempModifierTableName || ' t1 WHERE oid > 
    (SELECT  min(oid) FROM ' || tempModifierTableName || ' t2
        WHERE t1.modifier_cd = t2.modifier_cd 
        AND t1.modifier_path = t2.modifier_path
    )';
    EXECUTE ' UPDATE modifier_dimension  SET  
        modifier_cd=temp.modifier_cd
        ,name_char=temp.name_char
        ,modifier_blob=temp.modifier_blob
        ,update_date=temp.update_date
        ,download_date=temp.download_date
        ,import_date=Now()
        ,sourcesystem_cd=temp.SOURCESYSTEM_CD
        ,upload_id=' || UPLOAD_ID  || ' 
        FROM ' || tempModifierTableName || '  temp
        WHERE 
        temp.modifier_path = modifier_dimension.modifier_path 
        AND temp.update_date >= modifier_dimension.update_date
        AND EXISTS (SELECT 1 
            FROM ' || tempModifierTableName || ' temp  
            WHERE temp.modifier_path = modifier_dimension.modifier_path 
            AND temp.update_date >= modifier_dimension.update_date)
        ';
        --Create new modifier if temp table modifier_path does not exists 
        -- in modifier dimension table.
        EXECUTE 'INSERT INTO modifier_dimension  (
            modifier_cd
            ,modifier_path
            ,name_char
            ,modifier_blob
            ,update_date
            ,download_date
            ,import_date
            ,sourcesystem_cd
            ,upload_id
        )
        SELECT  
        modifier_cd
        ,modifier_path
        ,name_char
        ,modifier_blob
        ,update_date
        ,download_date
        ,Now()
        ,sourcesystem_cd
        ,' || upload_id || '  
        FROM
        ' || tempModifierTableName || '  temp
        WHERE NOT EXISTs (SELECT modifier_cd 
            FROM modifier_dimension cd
            WHERE cd.modifier_path = temp.modifier_path
        )
        ';
        EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.insert_modifier_fromtemp(tempmodifiertablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_patient_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_patient_fromtemp(temptablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE

maxPatientNum bigint; 

BEGIN 
    LOCK TABLE  patient_mapping IN EXCLUSIVE MODE NOWAIT;
    --select max(patient_num) into maxPatientNum from patient_mapping ;
    --Create new patient(patient_mapping) if temp table patient_ide does not exists 
    -- in patient_mapping table.
    EXECUTE ' INSERT INTO patient_mapping (patient_ide,patient_ide_source,patient_num,patient_ide_status, upload_id)
    (SELECT distinctTemp.patient_id, distinctTemp.patient_id_source, CAST(distinctTemp.patient_id AS numeric), ''A'',   '|| upload_id ||'
        FROM 
        (SELECT distinct patient_id, patient_id_source from ' || tempTableName || '  temp
            where  not exists (SELECT patient_ide from patient_mapping pm where pm.patient_ide = temp.patient_id and pm.patient_ide_source = temp.patient_id_source)
            and patient_id_source = ''HIVE'' )   distinctTemp) ';

    -- update patient_num for temp table
    EXECUTE ' UPDATE ' ||  tempTableName
    || ' SET patient_num = (SELECT pm.patient_num
        FROM patient_mapping pm
        WHERE pm.patient_ide = '|| tempTableName ||'.patient_id
        AND pm.patient_ide_source = '|| tempTableName ||'.patient_id_source
    )
    WHERE EXISTS (SELECT pm.patient_num 
        FROM patient_mapping pm
        WHERE pm.patient_ide = '|| tempTableName ||'.patient_id
        AND pm.patient_ide_source = '||tempTableName||'.patient_id_source)';       

    EXECUTE ' UPDATE patient_dimension  SET  
    vital_status_cd = temp.vital_status_cd
    , birth_date = temp.birth_date
    , death_date = temp.death_date
    , sex_cd = temp.sex_cd
    , age_in_years_num = temp.age_in_years_num
    , language_cd = temp.language_cd
    , race_cd = temp.race_cd
    , marital_status_cd = temp.marital_status_cd
    , religion_cd = temp.religion_cd
    , zip_cd = temp.zip_cd
    , statecityzip_path = temp.statecityzip_path
    , patient_blob = temp.patient_blob
    , update_date = temp.update_date
    , download_date = temp.download_date
    , import_date = Now()
    , sourcesystem_cd = temp.sourcesystem_cd 
    , upload_id =  ' || UPLOAD_ID  || '
    FROM  ' || tempTableName || '  temp
    WHERE 
    temp.patient_num = patient_dimension.patient_num 
    AND temp.update_date >= patient_dimension.update_date
    AND EXISTS (select 1 
        FROM ' || tempTableName || ' temp  
        WHERE 
        temp.patient_num = patient_dimension.patient_num 
        AND temp.update_date >= patient_dimension.update_date
    )    ';

    --Create new patient(patient_dimension) for above inserted patient's.
    --If patient_dimension table's patient num does match temp table,
    --then new patient_dimension information is inserted.
    EXECUTE 'INSERT INTO patient_dimension  (patient_num,vital_status_cd, birth_date, death_date,
        sex_cd, age_in_years_num,language_cd,race_cd,marital_status_cd, religion_cd,
        zip_cd,statecityzip_path,patient_blob,update_date,download_date,import_date,sourcesystem_cd,
        upload_id)
    SELECT temp.patient_num,
    temp.vital_status_cd, temp.birth_date, temp.death_date,
    temp.sex_cd, temp.age_in_years_num,temp.language_cd,temp.race_cd,temp.marital_status_cd, temp.religion_cd,
    temp.zip_cd,temp.statecityzip_path,temp.patient_blob,
    temp.update_date,
    temp.download_date,
    Now(),
    temp.sourcesystem_cd,
    '|| upload_id ||'
    FROM 
    ' || tempTableName || '  temp 
    WHERE 
    NOT EXISTS (SELECT patient_num 
        FROM patient_dimension pd 
        WHERE pd.patient_num = temp.patient_num) 
    AND 
    (patient_num IS NOT NULL AND patient_num::text <> '''')
    ';
    EXCEPTION WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;

END;
$$;


ALTER FUNCTION i2b2demodata.insert_patient_fromtemp(temptablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_patient_map_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_patient_map_fromtemp(temppatienttablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
        --Create new patient mapping entry for HIVE patient's if they are not already mapped in mapping table
        EXECUTE 'insert into patient_mapping (
                PERFORM distinct temp.patient_id, temp.patient_id_source,''A'',temp.patient_id ,' || upload_id || '
                from ' || tempPatientTableName ||'  temp 
                where temp.patient_id_source = ''HIVE'' and 
                not exists (select patient_ide from patient_mapping pm where pm.patient_num = temp.patient_id and pm.patient_ide_source = temp.patient_id_source) 
                )'; 
    --Create new visit for above inserted encounter's
        --If Visit table's encounter and patient num does match temp table,
        --then new visit information is created.
        EXECUTE 'MERGE  INTO patient_dimension pd
                   USING ( select case when (ptemp.patient_id_source=''HIVE'') then  cast(ptemp.patient_id as int)
                                       else pmap.patient_num end patient_num,
                                  ptemp.VITAL_STATUS_CD, 
                                  ptemp.BIRTH_DATE,
                                  ptemp.DEATH_DATE, 
                                  ptemp.SEX_CD ,
                                  ptemp.AGE_IN_YEARS_NUM,
                                  ptemp.LANGUAGE_CD,
                                  ptemp.RACE_CD,
                                  ptemp.MARITAL_STATUS_CD,
                                  ptemp.RELIGION_CD,
                                  ptemp.ZIP_CD,
                                                                  ptemp.STATECITYZIP_PATH , 
                                                                  ptemp.PATIENT_BLOB, 
                                                                  ptemp.UPDATE_DATE, 
                                                                  ptemp.DOWNLOAD_DATE, 
                                                                  ptemp.IMPORT_DATE, 
                                                                  ptemp.SOURCESYSTEM_CD
                   from ' || tempPatientTableName || '  ptemp , patient_mapping pmap
                   where   ptemp.patient_id = pmap.patient_ide(+)
                   and ptemp.patient_id_source = pmap.patient_ide_source(+)
           ) temp
                   on (
                                pd.patient_num = temp.patient_num
                    )    
                        when matched then 
                                update  set 
                                        pd.VITAL_STATUS_CD= temp.VITAL_STATUS_CD,
                    pd.BIRTH_DATE= temp.BIRTH_DATE,
                    pd.DEATH_DATE= temp.DEATH_DATE,
                    pd.SEX_CD= temp.SEX_CD,
                    pd.AGE_IN_YEARS_NUM=temp.AGE_IN_YEARS_NUM,
                    pd.LANGUAGE_CD=temp.LANGUAGE_CD,
                    pd.RACE_CD=temp.RACE_CD,
                    pd.MARITAL_STATUS_CD=temp.MARITAL_STATUS_CD,
                    pd.RELIGION_CD=temp.RELIGION_CD,
                    pd.ZIP_CD=temp.ZIP_CD,
                                        pd.STATECITYZIP_PATH =temp.STATECITYZIP_PATH,
                                        pd.PATIENT_BLOB=temp.PATIENT_BLOB,
                                        pd.UPDATE_DATE=temp.UPDATE_DATE,
                                        pd.DOWNLOAD_DATE=temp.DOWNLOAD_DATE,
                                        pd.SOURCESYSTEM_CD=temp.SOURCESYSTEM_CD,
                                        pd.UPLOAD_ID = '||upload_id||'
                    where temp.update_date > pd.update_date
                         when not matched then 
                                insert (
                                        PATIENT_NUM,
                                        VITAL_STATUS_CD,
                    BIRTH_DATE,
                    DEATH_DATE,
                    SEX_CD,
                    AGE_IN_YEARS_NUM,
                    LANGUAGE_CD,
                    RACE_CD,
                    MARITAL_STATUS_CD,
                    RELIGION_CD,
                    ZIP_CD,
                                        STATECITYZIP_PATH,
                                        PATIENT_BLOB,
                                        UPDATE_DATE,
                                        DOWNLOAD_DATE,
                                        SOURCESYSTEM_CD,
                                        import_date,
                        upload_id
                                        ) 
                                values (
                                        temp.PATIENT_NUM,
                                        temp.VITAL_STATUS_CD,
                    temp.BIRTH_DATE,
                    temp.DEATH_DATE,
                    temp.SEX_CD,
                    temp.AGE_IN_YEARS_NUM,
                    temp.LANGUAGE_CD,
                    temp.RACE_CD,
                    temp.MARITAL_STATUS_CD,
                    temp.RELIGION_CD,
                    temp.ZIP_CD,
                                        temp.STATECITYZIP_PATH,
                                        temp.PATIENT_BLOB,
                                        temp.UPDATE_DATE,
                                        temp.DOWNLOAD_DATE,
                                        temp.SOURCESYSTEM_CD,
                                        LOCALTIMESTAMP,
                                '||upload_id||'
                                )';
EXCEPTION
        WHEN OTHERS THEN
                RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.insert_patient_map_fromtemp(temppatienttablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_pid_map_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_pid_map_fromtemp(temppidtablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
existingPatientNum varchar(32);
maxPatientNum bigint;
distinctPidCur REFCURSOR;
disPatientId varchar(100); 
disPatientIdSource varchar(100);
BEGIN
	--delete the doublons
	EXECUTE ' delete  from ' || tempPidTableName ||  ' t1  where 
	oid > (select min(oid) from ' || tempPidTableName || ' t2 
		where t1.patient_map_id = t2.patient_map_id
		and t1.patient_map_id_source = t2.patient_map_id_source) ';
	LOCK TABLE  patient_mapping IN EXCLUSIVE MODE NOWAIT;
	select max(patient_num) into STRICT  maxPatientNum from patient_mapping ; 
	-- set max patient num to zero of the value is null
	if coalesce(maxPatientNum::text, '') = '' then 
		maxPatientNum := 0;
	end if;
	open distinctPidCur for EXECUTE 'SELECT distinct patient_id,patient_id_source from ' || tempPidTableName || '' ;
	loop
		FETCH distinctPidCur INTO disPatientId, disPatientIdSource;
		IF NOT FOUND THEN EXIT; 
	END IF; -- apply on distinctPidCur
	-- dbms_output.put_line(disPatientId);
	if  disPatientIdSource = 'HIVE'  THEN 
		begin
			--check if hive number exist, if so assign that number to reset of map_id's within that pid
			select patient_num into existingPatientNum from patient_mapping where patient_num = CAST(disPatientId AS numeric) and patient_ide_source = 'HIVE';
			EXCEPTION  when NO_DATA_FOUND THEN
				existingPatientNum := null;
		end;
		if (existingPatientNum IS NOT NULL AND existingPatientNum::text <> '') then 
			EXECUTE ' update ' || tempPidTableName ||' set patient_num = CAST(patient_id AS numeric), process_status_flag = ''P''
			where patient_id = $1 and not exists (select 1 from patient_mapping pm where pm.patient_ide = patient_map_id
				and pm.patient_ide_source = patient_map_id_source)' using disPatientId;
		else 
			-- generate new patient_num i.e. take max(patient_num) + 1 
			if maxPatientNum < CAST(disPatientId AS numeric) then 
				maxPatientNum := disPatientId;
			end if ;
			EXECUTE ' update ' || tempPidTableName ||' set patient_num = CAST(patient_id AS numeric), process_status_flag = ''P'' where 
			patient_id = $1 and patient_id_source = ''HIVE'' and not exists (select 1 from patient_mapping pm where pm.patient_ide = patient_map_id
				and pm.patient_ide_source = patient_map_id_source)' using disPatientId;
		end if;    
		-- test if record fectched
		-- dbms_output.put_line(' HIVE ');
	else 
		begin
			select patient_num into STRICT  existingPatientNum from patient_mapping where patient_ide = disPatientId and 
			patient_ide_source = disPatientIdSource ; 
			-- test if record fetched. 
			EXCEPTION
	WHEN NO_DATA_FOUND THEN
		existingPatientNum := null;
		end;
		if (existingPatientNum IS NOT NULL AND existingPatientNum::text <> '') then 
			EXECUTE ' update ' || tempPidTableName ||' set patient_num = CAST($1 AS numeric) , process_status_flag = ''P''
			where patient_id = $2 and not exists (select 1 from patient_mapping pm where pm.patient_ide = patient_map_id
				and pm.patient_ide_source = patient_map_id_source)' using  existingPatientNum,disPatientId;
		else 
			maxPatientNum := maxPatientNum + 1 ; 
			EXECUTE 'insert into ' || tempPidTableName ||' (
				patient_map_id
				,patient_map_id_source
				,patient_id
				,patient_id_source
				,patient_num
				,process_status_flag
				,patient_map_id_status
				,update_date
				,download_date
				,import_date
				,sourcesystem_cd
				,project_id) 
			values(
				$1
				,''HIVE''
				,$2
				,''HIVE''
				,$3
				,''P''
				,''A''
				,Now()
				,Now()
				,Now()
				,''edu.harvard.i2b2.crc''
			,''HIVE''
			)' using maxPatientNum,maxPatientNum,maxPatientNum; 
			EXECUTE 'update ' || tempPidTableName ||' set patient_num =  $1 , process_status_flag = ''P'' 
			where patient_id = $2 and  not exists (select 1 from 
				patient_mapping pm where pm.patient_ide = patient_map_id
				and pm.patient_ide_source = patient_map_id_source)' using maxPatientNum, disPatientId  ;
		end if ;
		-- dbms_output.put_line(' NOT HIVE ');
	end if; 
	END LOOP;
	close distinctPidCur ;
	-- do the mapping update if the update date is old
EXECUTE ' UPDATE patient_mapping
SET 
patient_num = CAST(temp.patient_id AS numeric)
,patient_ide_status = temp.patient_map_id_status
,update_date = temp.update_date
,download_date  = temp.download_date
,import_date = Now()
,sourcesystem_cd  = temp.sourcesystem_cd
,upload_id = ' || upload_id ||'
FROM '|| tempPidTableName || '  temp
WHERE 
temp.patient_map_id = patient_mapping.patient_ide 
and temp.patient_map_id_source = patient_mapping.patient_ide_source
and temp.patient_id_source = ''HIVE''
and coalesce(temp.process_status_flag::text, '''') = ''''  
and coalesce(patient_mapping.update_date,to_date(''01-JAN-1900'',''DD-MON-YYYY'')) <= coalesce(temp.update_date,to_date(''01-JAN-1900'',''DD-MON-YYYY''))
';
	-- insert new mapping records i.e flagged P
	EXECUTE ' insert into patient_mapping (patient_ide,patient_ide_source,patient_ide_status,patient_num,update_date,download_date,import_date,sourcesystem_cd,upload_id,project_id)
	SELECT patient_map_id,patient_map_id_source,patient_map_id_status,patient_num,update_date,download_date,Now(),sourcesystem_cd,' || upload_id ||', project_id from '|| tempPidTableName || ' 
	where process_status_flag = ''P'' ' ; 
	EXCEPTION WHEN OTHERS THEN
		RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;
	END;
	$_$;


ALTER FUNCTION i2b2demodata.insert_pid_map_fromtemp(temppidtablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: insert_provider_fromtemp(text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.insert_provider_fromtemp(tempprovidertablename text, upload_id bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN 
    --Delete duplicate rows with same encounter and patient combination
    EXECUTE 'DELETE FROM ' || tempProviderTableName || ' t1 WHERE oid > 
    (SELECT  min(oid) FROM ' || tempProviderTableName || ' t2
        WHERE t1.provider_id = t2.provider_id 
        AND t1.provider_path = t2.provider_path
    )';
    EXECUTE ' UPDATE provider_dimension  SET  
        provider_id =temp.provider_id
        , name_char = temp.name_char
        , provider_blob = temp.provider_blob
        , update_date=temp.update_date
        , download_date=temp.download_date
        , import_date=Now()
        , sourcesystem_cd=temp.sourcesystem_cd
        , upload_id = ' || upload_id || '
        FROM ' || tempProviderTableName || '  temp 
        WHERE 
        temp.provider_path = provider_dimension.provider_path and temp.update_date >= provider_dimension.update_date 
    AND EXISTS (select 1 from ' || tempProviderTableName || ' temp  where temp.provider_path = provider_dimension.provider_path 
        and temp.update_date >= provider_dimension.update_date) ';

    --Create new patient(patient_mapping) if temp table patient_ide does not exists 
    -- in patient_mapping table.
    EXECUTE 'insert into provider_dimension  (provider_id,provider_path,name_char,provider_blob,update_date,download_date,import_date,sourcesystem_cd,upload_id)
    SELECT  provider_id,provider_path, 
    name_char,provider_blob,
    update_date,download_date,
    Now(),sourcesystem_cd, ' || upload_id || '
    FROM ' || tempProviderTableName || '  temp
    WHERE NOT EXISTS (SELECT provider_id 
        FROM provider_dimension pd 
        WHERE pd.provider_path = temp.provider_path 
    )';
    EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.insert_provider_fromtemp(tempprovidertablename text, upload_id bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: remove_temp_table(character varying); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.remove_temp_table(temptablename character varying, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$

DECLARE

BEGIN
    EXECUTE 'DROP TABLE ' || tempTableName|| ' CASCADE ';

EXCEPTION 
WHEN OTHERS THEN
    RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.remove_temp_table(temptablename character varying, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: sync_clear_concept_table(text, text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.sync_clear_concept_table(tempconcepttablename text, backupconcepttablename text, uploadid bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
 
interConceptTableName  varchar(400);

BEGIN 
        interConceptTableName := backupConceptTableName || '_inter';
                --Delete duplicate rows with same encounter and patient combination
        EXECUTE 'DELETE FROM ' || tempConceptTableName || ' t1 WHERE oid > 
                                           (SELECT  min(oid) FROM ' || tempConceptTableName || ' t2
                                             WHERE t1.concept_cd = t2.concept_cd 
                                            AND t1.concept_path = t2.concept_path
                                            )';
    EXECUTE 'create table ' ||  interConceptTableName || ' (
    CONCEPT_CD          varchar(50) NOT NULL,
        CONCEPT_PATH            varchar(700) NOT NULL,
        NAME_CHAR               varchar(2000) NULL,
        CONCEPT_BLOB        text NULL,
        UPDATE_DATE         timestamp NULL,
        DOWNLOAD_DATE       timestamp NULL,
        IMPORT_DATE         timestamp NULL,
        SOURCESYSTEM_CD     varchar(50) NULL,
        UPLOAD_ID               numeric(38,0) NULL,
    CONSTRAINT '|| interConceptTableName ||'_pk  PRIMARY KEY(CONCEPT_PATH)
         )';
    --Create new patient(patient_mapping) if temp table patient_ide does not exists 
        -- in patient_mapping table.
        EXECUTE 'insert into '|| interConceptTableName ||'  (concept_cd,concept_path,name_char,concept_blob,update_date,download_date,import_date,sourcesystem_cd,upload_id)
                            PERFORM  concept_cd, substring(concept_path from 1 for 700),
                        name_char,concept_blob,
                        update_date,download_date,
                        LOCALTIMESTAMP,sourcesystem_cd,
                         ' || uploadId || '  from ' || tempConceptTableName || '  temp ';
        --backup the concept_dimension table before creating a new one
        EXECUTE 'alter table concept_dimension rename to ' || backupConceptTableName  ||'' ;
        -- add index on upload_id 
    EXECUTE 'CREATE INDEX ' || interConceptTableName || '_uid_idx ON ' || interConceptTableName || '(UPLOAD_ID)';
    -- add index on upload_id 
    EXECUTE 'CREATE INDEX ' || interConceptTableName || '_cd_idx ON ' || interConceptTableName || '(concept_cd)';
    --backup the concept_dimension table before creating a new one
        EXECUTE 'alter table ' || interConceptTableName  || ' rename to concept_dimension' ;
EXCEPTION
        WHEN OTHERS THEN
                RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.sync_clear_concept_table(tempconcepttablename text, backupconcepttablename text, uploadid bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: sync_clear_modifier_table(text, text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.sync_clear_modifier_table(tempmodifiertablename text, backupmodifiertablename text, uploadid bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
 
interModifierTableName  varchar(400);

BEGIN 
        interModifierTableName := backupModifierTableName || '_inter';
        --Delete duplicate rows with same modifier_path and modifier cd
        EXECUTE 'DELETE FROM ' || tempModifierTableName || ' t1 WHERE oid > 
                                           (SELECT  min(oid) FROM ' || tempModifierTableName || ' t2
                                             WHERE t1.modifier_cd = t2.modifier_cd 
                                            AND t1.modifier_path = t2.modifier_path
                                            )';
    EXECUTE 'create table ' ||  interModifierTableName || ' (
        MODIFIER_CD          varchar(50) NOT NULL,
        MODIFIER_PATH           varchar(700) NOT NULL,
        NAME_CHAR               varchar(2000) NULL,
        MODIFIER_BLOB        text NULL,
        UPDATE_DATE         timestamp NULL,
        DOWNLOAD_DATE       timestamp NULL,
        IMPORT_DATE         timestamp NULL,
        SOURCESYSTEM_CD     varchar(50) NULL,
        UPLOAD_ID               numeric(38,0) NULL,
    CONSTRAINT '|| interModifierTableName ||'_pk  PRIMARY KEY(MODIFIER_PATH)
         )';
    --Create new patient(patient_mapping) if temp table patient_ide does not exists 
        -- in patient_mapping table.
        EXECUTE 'insert into '|| interModifierTableName ||'  (modifier_cd,modifier_path,name_char,modifier_blob,update_date,download_date,import_date,sourcesystem_cd,upload_id)
                            PERFORM  modifier_cd, substring(modifier_path from 1 for 700),
                        name_char,modifier_blob,
                        update_date,download_date,
                        LOCALTIMESTAMP,sourcesystem_cd,
                         ' || uploadId || '  from ' || tempModifierTableName || '  temp ';
        --backup the modifier_dimension table before creating a new one
        EXECUTE 'alter table modifier_dimension rename to ' || backupModifierTableName  ||'' ;
        -- add index on upload_id 
    EXECUTE 'CREATE INDEX ' || interModifierTableName || '_uid_idx ON ' || interModifierTableName || '(UPLOAD_ID)';
    -- add index on upload_id 
    EXECUTE 'CREATE INDEX ' || interModifierTableName || '_cd_idx ON ' || interModifierTableName || '(modifier_cd)';
       --backup the modifier_dimension table before creating a new one
        EXECUTE 'alter table ' || interModifierTableName  || ' rename to modifier_dimension' ;
EXCEPTION
        WHEN OTHERS THEN
                RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.sync_clear_modifier_table(tempmodifiertablename text, backupmodifiertablename text, uploadid bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: sync_clear_provider_table(text, text, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.sync_clear_provider_table(tempprovidertablename text, backupprovidertablename text, uploadid bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
 
interProviderTableName  varchar(400);

BEGIN 
        interProviderTableName := backupProviderTableName || '_inter';
                --Delete duplicate rows with same encounter and patient combination
        EXECUTE 'DELETE FROM ' || tempProviderTableName || ' t1 WHERE oid > 
                                           (SELECT  min(oid) FROM ' || tempProviderTableName || ' t2
                                             WHERE t1.provider_id = t2.provider_id 
                                            AND t1.provider_path = t2.provider_path
                                            )';
    EXECUTE 'create table ' ||  interProviderTableName || ' (
    PROVIDER_ID         varchar(50) NOT NULL,
        PROVIDER_PATH       varchar(700) NOT NULL,
        NAME_CHAR               varchar(850) NULL,
        PROVIDER_BLOB       text NULL,
        UPDATE_DATE             timestamp NULL,
        DOWNLOAD_DATE       timestamp NULL,
        IMPORT_DATE         timestamp NULL,
        SOURCESYSTEM_CD     varchar(50) NULL,
        UPLOAD_ID               numeric(38,0) NULL ,
    CONSTRAINT  ' || interProviderTableName || '_pk PRIMARY KEY(PROVIDER_PATH,provider_id)
         )';
    --Create new patient(patient_mapping) if temp table patient_ide does not exists 
        -- in patient_mapping table.
        EXECUTE 'insert into ' ||  interProviderTableName || ' (provider_id,provider_path,name_char,provider_blob,update_date,download_date,import_date,sourcesystem_cd,upload_id)
                            PERFORM  provider_id,provider_path, 
                        name_char,provider_blob,
                        update_date,download_date,
                        LOCALTIMESTAMP,sourcesystem_cd, ' || uploadId || '
                             from ' || tempProviderTableName || '  temp ';
        --backup the concept_dimension table before creating a new one
        EXECUTE 'alter table provider_dimension rename to ' || backupProviderTableName  ||'' ;
        -- add index on provider_id, name_char 
    EXECUTE 'CREATE INDEX ' || interProviderTableName || '_id_idx ON ' || interProviderTableName  || '(Provider_Id,name_char)';
    EXECUTE 'CREATE INDEX ' || interProviderTableName || '_uid_idx ON ' || interProviderTableName  || '(UPLOAD_ID)';
        --backup the concept_dimension table before creating a new one
        EXECUTE 'alter table ' || interProviderTableName  || ' rename to provider_dimension' ;
EXCEPTION
        WHEN OTHERS THEN
                RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.sync_clear_provider_table(tempprovidertablename text, backupprovidertablename text, uploadid bigint, OUT errormsg text) OWNER TO i2b2demodata;

--
-- Name: update_observation_fact(text, bigint, bigint); Type: FUNCTION; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE FUNCTION i2b2demodata.update_observation_fact(upload_temptable_name text, upload_id bigint, appendflag bigint, OUT errormsg text) RETURNS text
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- appendFlag = 0 -> remove all and then insert
    -- appendFlag <> 0 -> do update, then insert what have not been updated    

    --Delete duplicate records(encounter_ide,patient_ide,concept_cd,start_date,modifier_cd,provider_id)
    EXECUTE 'DELETE FROM ' || upload_temptable_name ||'  t1 
    WHERE oid > (select min(oid) FROM ' || upload_temptable_name ||' t2 
        WHERE t1.encounter_id = t2.encounter_id  
        AND
        t1.encounter_id_source = t2.encounter_id_source
        AND
        t1.patient_id = t2.patient_id 
        AND 
        t1.patient_id_source = t2.patient_id_source
        AND 
        t1.concept_cd = t2.concept_cd
        AND 
        t1.start_date = t2.start_date
        AND 
        coalesce(t1.modifier_cd,''xyz'') = coalesce(t2.modifier_cd,''xyz'')
        AND 
        t1.instance_num = t2.instance_num
        AND 
        t1.provider_id = t2.provider_id)';
    --Delete records having null in start_date
    EXECUTE 'DELETE FROM ' || upload_temptable_name ||'  t1           
    WHERE coalesce(t1.start_date::text, '''') = '''' 
    ';
    --One time lookup on encounter_ide to get encounter_num 
    EXECUTE 'UPDATE ' ||  upload_temptable_name
    || ' SET encounter_num = (SELECT distinct em.encounter_num
        FROM encounter_mapping em
        WHERE em.encounter_ide = ' || upload_temptable_name||'.encounter_id
        AND em.encounter_ide_source = '|| upload_temptable_name||'.encounter_id_source
    )
    WHERE EXISTS (SELECT distinct em.encounter_num
        FROM encounter_mapping em
        WHERE em.encounter_ide = '|| upload_temptable_name||'.encounter_id
        AND em.encounter_ide_source = '||upload_temptable_name||'.encounter_id_source)';                
    --One time lookup on patient_ide to get patient_num 
    EXECUTE 'UPDATE ' ||  upload_temptable_name
    || ' SET patient_num = (SELECT distinct pm.patient_num
        FROM patient_mapping pm
        WHERE pm.patient_ide = '|| upload_temptable_name||'.patient_id
        AND pm.patient_ide_source = '|| upload_temptable_name||'.patient_id_source
    )
    WHERE EXISTS (SELECT distinct pm.patient_num 
        FROM patient_mapping pm
        WHERE pm.patient_ide = '|| upload_temptable_name||'.patient_id
        AND pm.patient_ide_source = '||upload_temptable_name||'.patient_id_source)';                    
    IF (appendFlag = 0) THEN
        --Archive records which are to be deleted in observation_fact table
        EXECUTE 'INSERT INTO  archive_observation_fact 
        SELECT obsfact.*, ' || upload_id ||'
        FROM observation_fact obsfact
        WHERE obsfact.encounter_num IN 
        (SELECT temp_obsfact.encounter_num
            FROM  ' ||upload_temptable_name ||' temp_obsfact
            GROUP BY temp_obsfact.encounter_num  
        )';
        --Delete above archived row FROM observation_fact
        EXECUTE 'DELETE  
        FROM observation_fact 
        WHERE EXISTS (
            SELECT archive.encounter_num
            FROM archive_observation_fact  archive
            WHERE archive.archive_upload_id = '||upload_id ||'
            AND archive.encounter_num=observation_fact.encounter_num
            AND archive.concept_cd = observation_fact.concept_cd
            AND archive.start_date = observation_fact.start_date
        )';
END IF;
-- if the append is true, then do the update else do insert all
IF (appendFlag <> 0) THEN -- update
    EXECUTE ' 
    UPDATE observation_fact f    
    SET valtype_cd = temp.valtype_cd ,
    tval_char=temp.tval_char, 
    nval_num = temp.nval_num,
    valueflag_cd=temp.valueflag_cd,
    quantity_num=temp.quantity_num,
    confidence_num=temp.confidence_num,
    observation_blob =temp.observation_blob,
    units_cd=temp.units_cd,
    end_date=temp.end_date,
    location_cd =temp.location_cd,
    update_date=temp.update_date ,
    download_date =temp.download_date,
    import_date=temp.import_date,
    sourcesystem_cd =temp.sourcesystem_cd,
    upload_id = temp.upload_id 
    FROM ' || upload_temptable_name ||' temp
    WHERE 
    temp.patient_num is not null 
    and temp.encounter_num is not null 
    and temp.encounter_num = f.encounter_num 
    and temp.patient_num = f.patient_num
    and temp.concept_cd = f.concept_cd
    and temp.start_date = f.start_date
    and temp.provider_id = f.provider_id
    and temp.modifier_cd = f.modifier_cd 
    and temp.instance_num = f.instance_num
    and coalesce(f.update_date,to_date(''01-JAN-1900'',''DD-MON-YYYY'')) <= coalesce(temp.update_date,to_date(''01-JAN-1900'',''DD-MON-YYYY''))';

    EXECUTE  'DELETE FROM ' || upload_temptable_name ||' temp WHERE EXISTS (SELECT 1 
        FROM observation_fact f 
        WHERE temp.patient_num is not null 
        and temp.encounter_num is not null 
        and temp.encounter_num = f.encounter_num 
        and temp.patient_num = f.patient_num
        and temp.concept_cd = f.concept_cd
        and temp.start_date = f.start_date
        and temp.provider_id = f.provider_id
        and temp.modifier_cd = f.modifier_cd 
        and temp.instance_num = f.instance_num
    )';

END IF;
--Transfer all rows FROM temp_obsfact to observation_fact
EXECUTE 'INSERT INTO observation_fact(
    encounter_num
    ,concept_cd
    , patient_num
    ,provider_id
    , start_date
    ,modifier_cd
    ,instance_num
    ,valtype_cd
    ,tval_char
    ,nval_num
    ,valueflag_cd
    ,quantity_num
    ,confidence_num
    ,observation_blob
    ,units_cd
    ,end_date
    ,location_cd
    , update_date
    ,download_date
    ,import_date
    ,sourcesystem_cd
    ,upload_id)
SELECT encounter_num
,concept_cd
, patient_num
,provider_id
, start_date
,modifier_cd
,instance_num
,valtype_cd
,tval_char
,nval_num
,valueflag_cd
,quantity_num
,confidence_num
,observation_blob
,units_cd
,end_date
,location_cd
, update_date
,download_date
,Now()
,sourcesystem_cd
,temp.upload_id 
FROM ' || upload_temptable_name ||' temp
WHERE (temp.patient_num IS NOT NULL AND temp.patient_num::text <> '''') AND  (temp.encounter_num IS NOT NULL AND temp.encounter_num::text <> '''')';


EXCEPTION
    WHEN OTHERS THEN
        RAISE EXCEPTION 'An error was encountered - % -ERROR- %',SQLSTATE,SQLERRM;      
END;
$$;


ALTER FUNCTION i2b2demodata.update_observation_fact(upload_temptable_name text, upload_id bigint, appendflag bigint, OUT errormsg text) OWNER TO i2b2demodata;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: archive_observation_fact; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.archive_observation_fact (
    encounter_num integer,
    patient_num integer,
    concept_cd character varying(50),
    provider_id character varying(50),
    start_date timestamp without time zone,
    modifier_cd character varying(100),
    instance_num integer,
    valtype_cd character varying(50),
    tval_char character varying(255),
    nval_num numeric(18,5),
    valueflag_cd character varying(50),
    quantity_num numeric(18,5),
    units_cd character varying(50),
    end_date timestamp without time zone,
    location_cd character varying(50),
    observation_blob text,
    confidence_num numeric(18,5),
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer,
    text_search_index integer,
    archive_upload_id integer
);


ALTER TABLE i2b2demodata.archive_observation_fact OWNER TO i2b2demodata;

--
-- Name: code_lookup; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.code_lookup (
    table_cd character varying(100) NOT NULL,
    column_cd character varying(100) NOT NULL,
    code_cd character varying(50) NOT NULL,
    name_char character varying(650),
    lookup_blob text,
    upload_date timestamp without time zone,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.code_lookup OWNER TO i2b2demodata;

--
-- Name: concept_dimension; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.concept_dimension (
    concept_path character varying(700) NOT NULL,
    concept_cd character varying(50),
    name_char character varying(2000),
    concept_blob text,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.concept_dimension OWNER TO i2b2demodata;

--
-- Name: datamart_report; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.datamart_report (
    total_patient integer,
    total_observationfact integer,
    total_event integer,
    report_date timestamp without time zone
);


ALTER TABLE i2b2demodata.datamart_report OWNER TO i2b2demodata;

--
-- Name: encounter_mapping; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.encounter_mapping (
    encounter_ide character varying(200) NOT NULL,
    encounter_ide_source character varying(50) NOT NULL,
    project_id character varying(50) NOT NULL,
    encounter_num integer NOT NULL,
    patient_ide character varying(200) NOT NULL,
    patient_ide_source character varying(50) NOT NULL,
    encounter_ide_status character varying(50),
    upload_date timestamp without time zone,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.encounter_mapping OWNER TO i2b2demodata;

--
-- Name: modifier_dimension; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.modifier_dimension (
    modifier_path character varying(700) NOT NULL,
    modifier_cd character varying(50),
    name_char character varying(2000),
    modifier_blob text,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.modifier_dimension OWNER TO i2b2demodata;

--
-- Name: observation_fact; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.observation_fact (
    encounter_num integer NOT NULL,
    patient_num integer NOT NULL,
    concept_cd character varying(50) NOT NULL,
    provider_id character varying(50) NOT NULL,
    start_date timestamp without time zone NOT NULL,
    modifier_cd character varying(100) DEFAULT '@'::character varying NOT NULL,
    instance_num integer DEFAULT 1 NOT NULL,
    valtype_cd character varying(50),
    tval_char character varying(255),
    nval_num numeric(18,5),
    valueflag_cd character varying(50),
    quantity_num numeric(18,5),
    units_cd character varying(50),
    end_date timestamp without time zone,
    location_cd character varying(50),
    observation_blob text,
    confidence_num numeric(18,5),
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer,
    text_search_index integer NOT NULL
);


ALTER TABLE i2b2demodata.observation_fact OWNER TO i2b2demodata;

--
-- Name: observation_fact_text_search_index_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.observation_fact_text_search_index_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.observation_fact_text_search_index_seq OWNER TO i2b2demodata;

--
-- Name: observation_fact_text_search_index_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.observation_fact_text_search_index_seq OWNED BY i2b2demodata.observation_fact.text_search_index;


--
-- Name: patient_dimension; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.patient_dimension (
    patient_num integer NOT NULL,
    vital_status_cd character varying(50),
    birth_date timestamp without time zone,
    death_date timestamp without time zone,
    sex_cd character varying(50),
    age_in_years_num integer,
    language_cd character varying(50),
    race_cd character varying(50),
    marital_status_cd character varying(50),
    religion_cd character varying(50),
    zip_cd character varying(10),
    statecityzip_path character varying(700),
    income_cd character varying(50),
    patient_blob text,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.patient_dimension OWNER TO i2b2demodata;

--
-- Name: patient_mapping; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.patient_mapping (
    patient_ide character varying(200) NOT NULL,
    patient_ide_source character varying(50) NOT NULL,
    patient_num integer NOT NULL,
    patient_ide_status character varying(50),
    project_id character varying(50) NOT NULL,
    upload_date timestamp without time zone,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.patient_mapping OWNER TO i2b2demodata;

--
-- Name: provider_dimension; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.provider_dimension (
    provider_id character varying(50) NOT NULL,
    provider_path character varying(700) NOT NULL,
    name_char character varying(850),
    provider_blob text,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.provider_dimension OWNER TO i2b2demodata;

--
-- Name: qt_analysis_plugin; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_analysis_plugin (
    plugin_id integer NOT NULL,
    plugin_name character varying(2000),
    description character varying(2000),
    version_cd character varying(50),
    parameter_info text,
    parameter_info_xsd text,
    command_line text,
    working_folder text,
    commandoption_cd text,
    plugin_icon text,
    status_cd character varying(50),
    user_id character varying(50),
    group_id character varying(50),
    create_date timestamp without time zone,
    update_date timestamp without time zone
);


ALTER TABLE i2b2demodata.qt_analysis_plugin OWNER TO i2b2demodata;

--
-- Name: qt_analysis_plugin_result_type; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_analysis_plugin_result_type (
    plugin_id integer NOT NULL,
    result_type_id integer NOT NULL
);


ALTER TABLE i2b2demodata.qt_analysis_plugin_result_type OWNER TO i2b2demodata;

--
-- Name: qt_breakdown_path; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_breakdown_path (
    name character varying(100),
    value character varying(2000),
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    user_id character varying(50)
);


ALTER TABLE i2b2demodata.qt_breakdown_path OWNER TO i2b2demodata;

--
-- Name: qt_patient_enc_collection; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_patient_enc_collection (
    patient_enc_coll_id integer NOT NULL,
    result_instance_id integer,
    set_index integer,
    patient_num integer,
    encounter_num integer
);


ALTER TABLE i2b2demodata.qt_patient_enc_collection OWNER TO i2b2demodata;

--
-- Name: qt_patient_enc_collection_patient_enc_coll_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.qt_patient_enc_collection_patient_enc_coll_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.qt_patient_enc_collection_patient_enc_coll_id_seq OWNER TO i2b2demodata;

--
-- Name: qt_patient_enc_collection_patient_enc_coll_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.qt_patient_enc_collection_patient_enc_coll_id_seq OWNED BY i2b2demodata.qt_patient_enc_collection.patient_enc_coll_id;


--
-- Name: qt_patient_set_collection; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_patient_set_collection (
    patient_set_coll_id bigint NOT NULL,
    result_instance_id integer,
    set_index integer,
    patient_num integer
);


ALTER TABLE i2b2demodata.qt_patient_set_collection OWNER TO i2b2demodata;

--
-- Name: qt_patient_set_collection_patient_set_coll_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.qt_patient_set_collection_patient_set_coll_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.qt_patient_set_collection_patient_set_coll_id_seq OWNER TO i2b2demodata;

--
-- Name: qt_patient_set_collection_patient_set_coll_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.qt_patient_set_collection_patient_set_coll_id_seq OWNED BY i2b2demodata.qt_patient_set_collection.patient_set_coll_id;


--
-- Name: qt_pdo_query_master; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_pdo_query_master (
    query_master_id integer NOT NULL,
    user_id character varying(50) NOT NULL,
    group_id character varying(50) NOT NULL,
    create_date timestamp without time zone NOT NULL,
    request_xml text,
    i2b2_request_xml text
);


ALTER TABLE i2b2demodata.qt_pdo_query_master OWNER TO i2b2demodata;

--
-- Name: qt_pdo_query_master_query_master_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.qt_pdo_query_master_query_master_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.qt_pdo_query_master_query_master_id_seq OWNER TO i2b2demodata;

--
-- Name: qt_pdo_query_master_query_master_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.qt_pdo_query_master_query_master_id_seq OWNED BY i2b2demodata.qt_pdo_query_master.query_master_id;


--
-- Name: qt_privilege; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_privilege (
    protection_label_cd character varying(1500) NOT NULL,
    dataprot_cd character varying(1000),
    hivemgmt_cd character varying(1000),
    plugin_id integer
);


ALTER TABLE i2b2demodata.qt_privilege OWNER TO i2b2demodata;

--
-- Name: qt_query_instance; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_query_instance (
    query_instance_id integer NOT NULL,
    query_master_id integer,
    user_id character varying(50) NOT NULL,
    group_id character varying(50) NOT NULL,
    batch_mode character varying(50),
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    delete_flag character varying(3),
    status_type_id integer,
    message text
);


ALTER TABLE i2b2demodata.qt_query_instance OWNER TO i2b2demodata;

--
-- Name: qt_query_instance_query_instance_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.qt_query_instance_query_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.qt_query_instance_query_instance_id_seq OWNER TO i2b2demodata;

--
-- Name: qt_query_instance_query_instance_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.qt_query_instance_query_instance_id_seq OWNED BY i2b2demodata.qt_query_instance.query_instance_id;


--
-- Name: qt_query_master; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_query_master (
    query_master_id integer NOT NULL,
    name character varying(250) NOT NULL,
    user_id character varying(50) NOT NULL,
    group_id character varying(50) NOT NULL,
    master_type_cd character varying(2000),
    plugin_id integer,
    create_date timestamp without time zone NOT NULL,
    delete_date timestamp without time zone,
    delete_flag character varying(3),
    request_xml text,
    generated_sql text,
    i2b2_request_xml text,
    pm_xml text
);


ALTER TABLE i2b2demodata.qt_query_master OWNER TO i2b2demodata;

--
-- Name: qt_query_master_query_master_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.qt_query_master_query_master_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.qt_query_master_query_master_id_seq OWNER TO i2b2demodata;

--
-- Name: qt_query_master_query_master_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.qt_query_master_query_master_id_seq OWNED BY i2b2demodata.qt_query_master.query_master_id;


--
-- Name: qt_query_result_instance; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_query_result_instance (
    result_instance_id integer NOT NULL,
    query_instance_id integer,
    result_type_id integer NOT NULL,
    set_size integer,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    status_type_id integer NOT NULL,
    delete_flag character varying(3),
    message text,
    description character varying(200),
    real_set_size integer,
    obfusc_method character varying(500)
);


ALTER TABLE i2b2demodata.qt_query_result_instance OWNER TO i2b2demodata;

--
-- Name: qt_query_result_instance_result_instance_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.qt_query_result_instance_result_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.qt_query_result_instance_result_instance_id_seq OWNER TO i2b2demodata;

--
-- Name: qt_query_result_instance_result_instance_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.qt_query_result_instance_result_instance_id_seq OWNED BY i2b2demodata.qt_query_result_instance.result_instance_id;


--
-- Name: qt_query_result_type; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_query_result_type (
    result_type_id integer NOT NULL,
    name character varying(100),
    description character varying(200),
    display_type_id character varying(500),
    visual_attribute_type_id character varying(3)
);


ALTER TABLE i2b2demodata.qt_query_result_type OWNER TO i2b2demodata;

--
-- Name: qt_query_status_type; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_query_status_type (
    status_type_id integer NOT NULL,
    name character varying(100),
    description character varying(200)
);


ALTER TABLE i2b2demodata.qt_query_status_type OWNER TO i2b2demodata;

--
-- Name: qt_xml_result; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.qt_xml_result (
    xml_result_id integer NOT NULL,
    result_instance_id integer,
    xml_value text
);


ALTER TABLE i2b2demodata.qt_xml_result OWNER TO i2b2demodata;

--
-- Name: qt_xml_result_xml_result_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.qt_xml_result_xml_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.qt_xml_result_xml_result_id_seq OWNER TO i2b2demodata;

--
-- Name: qt_xml_result_xml_result_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.qt_xml_result_xml_result_id_seq OWNED BY i2b2demodata.qt_xml_result.xml_result_id;


--
-- Name: seq_encounter_num; Type: SEQUENCE; Schema: i2b2demodata; Owner: postgres
--

CREATE SEQUENCE i2b2demodata.seq_encounter_num
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.seq_encounter_num OWNER TO postgres;

--
-- Name: seq_patient_num; Type: SEQUENCE; Schema: i2b2demodata; Owner: postgres
--

CREATE SEQUENCE i2b2demodata.seq_patient_num
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.seq_patient_num OWNER TO postgres;

--
-- Name: set_type; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.set_type (
    id integer NOT NULL,
    name character varying(500),
    create_date timestamp without time zone
);


ALTER TABLE i2b2demodata.set_type OWNER TO i2b2demodata;

--
-- Name: set_upload_status; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.set_upload_status (
    upload_id integer NOT NULL,
    set_type_id integer NOT NULL,
    source_cd character varying(50) NOT NULL,
    no_of_record bigint,
    loaded_record bigint,
    deleted_record bigint,
    load_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    load_status character varying(100),
    message text,
    input_file_name text,
    log_file_name text,
    transform_name character varying(500)
);


ALTER TABLE i2b2demodata.set_upload_status OWNER TO i2b2demodata;

--
-- Name: source_master; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.source_master (
    source_cd character varying(50) NOT NULL,
    description character varying(300),
    create_date timestamp without time zone
);


ALTER TABLE i2b2demodata.source_master OWNER TO i2b2demodata;

--
-- Name: upload_status; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.upload_status (
    upload_id integer NOT NULL,
    upload_label character varying(500) NOT NULL,
    user_id character varying(100) NOT NULL,
    source_cd character varying(50) NOT NULL,
    no_of_record bigint,
    loaded_record bigint,
    deleted_record bigint,
    load_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    load_status character varying(100),
    message text,
    input_file_name text,
    log_file_name text,
    transform_name character varying(500)
);


ALTER TABLE i2b2demodata.upload_status OWNER TO i2b2demodata;

--
-- Name: upload_status_upload_id_seq; Type: SEQUENCE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE SEQUENCE i2b2demodata.upload_status_upload_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2demodata.upload_status_upload_id_seq OWNER TO i2b2demodata;

--
-- Name: upload_status_upload_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER SEQUENCE i2b2demodata.upload_status_upload_id_seq OWNED BY i2b2demodata.upload_status.upload_id;


--
-- Name: visit_dimension; Type: TABLE; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE TABLE i2b2demodata.visit_dimension (
    encounter_num integer NOT NULL,
    patient_num integer NOT NULL,
    active_status_cd character varying(50),
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    inout_cd character varying(50),
    location_cd character varying(50),
    location_path character varying(900),
    length_of_stay integer,
    visit_blob text,
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2demodata.visit_dimension OWNER TO i2b2demodata;

--
-- Name: crc_analysis_job; Type: TABLE; Schema: i2b2hive; Owner: i2b2hive
--

CREATE TABLE i2b2hive.crc_analysis_job (
    job_id character varying(10) NOT NULL,
    queue_name character varying(50),
    status_type_id integer,
    domain_id character varying(255),
    project_id character varying(500),
    user_id character varying(255),
    request_xml text,
    create_date timestamp without time zone,
    update_date timestamp without time zone
);


ALTER TABLE i2b2hive.crc_analysis_job OWNER TO i2b2hive;

--
-- Name: crc_db_lookup; Type: TABLE; Schema: i2b2hive; Owner: i2b2hive
--

CREATE TABLE i2b2hive.crc_db_lookup (
    c_domain_id character varying(255) NOT NULL,
    c_project_path character varying(255) NOT NULL,
    c_owner_id character varying(255) NOT NULL,
    c_db_fullschema character varying(255) NOT NULL,
    c_db_datasource character varying(255) NOT NULL,
    c_db_servertype character varying(255) NOT NULL,
    c_db_nicename character varying(255),
    c_db_tooltip character varying(255),
    c_comment text,
    c_entry_date timestamp without time zone,
    c_change_date timestamp without time zone,
    c_status_cd character(1)
);


ALTER TABLE i2b2hive.crc_db_lookup OWNER TO i2b2hive;

--
-- Name: im_db_lookup; Type: TABLE; Schema: i2b2hive; Owner: i2b2hive
--

CREATE TABLE i2b2hive.im_db_lookup (
    c_domain_id character varying(255) NOT NULL,
    c_project_path character varying(255) NOT NULL,
    c_owner_id character varying(255) NOT NULL,
    c_db_fullschema character varying(255) NOT NULL,
    c_db_datasource character varying(255) NOT NULL,
    c_db_servertype character varying(255) NOT NULL,
    c_db_nicename character varying(255),
    c_db_tooltip character varying(255),
    c_comment text,
    c_entry_date timestamp without time zone,
    c_change_date timestamp without time zone,
    c_status_cd character(1)
);


ALTER TABLE i2b2hive.im_db_lookup OWNER TO i2b2hive;

--
-- Name: ont_db_lookup; Type: TABLE; Schema: i2b2hive; Owner: i2b2hive
--

CREATE TABLE i2b2hive.ont_db_lookup (
    c_domain_id character varying(255) NOT NULL,
    c_project_path character varying(255) NOT NULL,
    c_owner_id character varying(255) NOT NULL,
    c_db_fullschema character varying(255) NOT NULL,
    c_db_datasource character varying(255) NOT NULL,
    c_db_servertype character varying(255) NOT NULL,
    c_db_nicename character varying(255),
    c_db_tooltip character varying(255),
    c_comment text,
    c_entry_date timestamp without time zone,
    c_change_date timestamp without time zone,
    c_status_cd character(1)
);


ALTER TABLE i2b2hive.ont_db_lookup OWNER TO i2b2hive;

--
-- Name: work_db_lookup; Type: TABLE; Schema: i2b2hive; Owner: i2b2hive
--

CREATE TABLE i2b2hive.work_db_lookup (
    c_domain_id character varying(255) NOT NULL,
    c_project_path character varying(255) NOT NULL,
    c_owner_id character varying(255) NOT NULL,
    c_db_fullschema character varying(255) NOT NULL,
    c_db_datasource character varying(255) NOT NULL,
    c_db_servertype character varying(255) NOT NULL,
    c_db_nicename character varying(255),
    c_db_tooltip character varying(255),
    c_comment text,
    c_entry_date timestamp without time zone,
    c_change_date timestamp without time zone,
    c_status_cd character(1)
);


ALTER TABLE i2b2hive.work_db_lookup OWNER TO i2b2hive;

--
-- Name: im_audit; Type: TABLE; Schema: i2b2imdata; Owner: i2b2imdata
--

CREATE TABLE i2b2imdata.im_audit (
    query_date timestamp without time zone DEFAULT now() NOT NULL,
    lcl_site character varying(50) NOT NULL,
    lcl_id character varying(200) NOT NULL,
    user_id character varying(50) NOT NULL,
    project_id character varying(50) NOT NULL,
    comments text
);


ALTER TABLE i2b2imdata.im_audit OWNER TO i2b2imdata;

--
-- Name: im_mpi_demographics; Type: TABLE; Schema: i2b2imdata; Owner: i2b2imdata
--

CREATE TABLE i2b2imdata.im_mpi_demographics (
    global_id character varying(200) NOT NULL,
    global_status character varying(50),
    demographics character varying(400),
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2imdata.im_mpi_demographics OWNER TO i2b2imdata;

--
-- Name: im_mpi_mapping; Type: TABLE; Schema: i2b2imdata; Owner: i2b2imdata
--

CREATE TABLE i2b2imdata.im_mpi_mapping (
    global_id character varying(200) NOT NULL,
    lcl_site character varying(50) NOT NULL,
    lcl_id character varying(200) NOT NULL,
    lcl_status character varying(50),
    update_date timestamp without time zone NOT NULL,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2imdata.im_mpi_mapping OWNER TO i2b2imdata;

--
-- Name: im_project_patients; Type: TABLE; Schema: i2b2imdata; Owner: i2b2imdata
--

CREATE TABLE i2b2imdata.im_project_patients (
    project_id character varying(50) NOT NULL,
    global_id character varying(200) NOT NULL,
    patient_project_status character varying(50),
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2imdata.im_project_patients OWNER TO i2b2imdata;

--
-- Name: im_project_sites; Type: TABLE; Schema: i2b2imdata; Owner: i2b2imdata
--

CREATE TABLE i2b2imdata.im_project_sites (
    project_id character varying(50) NOT NULL,
    lcl_site character varying(50) NOT NULL,
    project_status character varying(50),
    update_date timestamp without time zone,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    upload_id integer
);


ALTER TABLE i2b2imdata.im_project_sites OWNER TO i2b2imdata;

--
-- Name: birn; Type: TABLE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE TABLE i2b2metadata.birn (
    c_hlevel integer NOT NULL,
    c_fullname character varying(700) NOT NULL,
    c_name character varying(2000) NOT NULL,
    c_synonym_cd character(1) NOT NULL,
    c_visualattributes character(3) NOT NULL,
    c_totalnum integer,
    c_basecode character varying(50),
    c_metadataxml text,
    c_facttablecolumn character varying(50) NOT NULL,
    c_tablename character varying(50) NOT NULL,
    c_columnname character varying(50) NOT NULL,
    c_columndatatype character varying(50) NOT NULL,
    c_operator character varying(10) NOT NULL,
    c_dimcode character varying(700) NOT NULL,
    c_comment text,
    c_tooltip character varying(900),
    m_applied_path character varying(700) NOT NULL,
    update_date timestamp without time zone NOT NULL,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    valuetype_cd character varying(50),
    m_exclusion_cd character varying(25),
    c_path character varying(700),
    c_symbol character varying(50)
);


ALTER TABLE i2b2metadata.birn OWNER TO i2b2metadata;

--
-- Name: custom_meta; Type: TABLE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE TABLE i2b2metadata.custom_meta (
    c_hlevel integer NOT NULL,
    c_fullname character varying(700) NOT NULL,
    c_name character varying(2000) NOT NULL,
    c_synonym_cd character(1) NOT NULL,
    c_visualattributes character(3) NOT NULL,
    c_totalnum integer,
    c_basecode character varying(50),
    c_metadataxml text,
    c_facttablecolumn character varying(50) NOT NULL,
    c_tablename character varying(50) NOT NULL,
    c_columnname character varying(50) NOT NULL,
    c_columndatatype character varying(50) NOT NULL,
    c_operator character varying(10) NOT NULL,
    c_dimcode character varying(700) NOT NULL,
    c_comment text,
    c_tooltip character varying(900),
    m_applied_path character varying(700) NOT NULL,
    update_date timestamp without time zone NOT NULL,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    valuetype_cd character varying(50),
    m_exclusion_cd character varying(25),
    c_path character varying(700),
    c_symbol character varying(50)
);


ALTER TABLE i2b2metadata.custom_meta OWNER TO i2b2metadata;

--
-- Name: i2b2; Type: TABLE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE TABLE i2b2metadata.i2b2 (
    c_hlevel integer NOT NULL,
    c_fullname character varying(700) NOT NULL,
    c_name character varying(2000) NOT NULL,
    c_synonym_cd character(1) NOT NULL,
    c_visualattributes character(3) NOT NULL,
    c_totalnum integer,
    c_basecode character varying(50),
    c_metadataxml text,
    c_facttablecolumn character varying(50) NOT NULL,
    c_tablename character varying(50) NOT NULL,
    c_columnname character varying(50) NOT NULL,
    c_columndatatype character varying(50) NOT NULL,
    c_operator character varying(10) NOT NULL,
    c_dimcode character varying(700) NOT NULL,
    c_comment text,
    c_tooltip character varying(900),
    m_applied_path character varying(700) NOT NULL,
    update_date timestamp without time zone NOT NULL,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    valuetype_cd character varying(50),
    m_exclusion_cd character varying(25),
    c_path character varying(700),
    c_symbol character varying(50)
);


ALTER TABLE i2b2metadata.i2b2 OWNER TO i2b2metadata;

--
-- Name: icd10_icd9; Type: TABLE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE TABLE i2b2metadata.icd10_icd9 (
    c_hlevel integer NOT NULL,
    c_fullname character varying(700) NOT NULL,
    c_name character varying(2000) NOT NULL,
    c_synonym_cd character(1) NOT NULL,
    c_visualattributes character(3) NOT NULL,
    c_totalnum integer,
    c_basecode character varying(50),
    c_metadataxml text,
    c_facttablecolumn character varying(50) NOT NULL,
    c_tablename character varying(50) NOT NULL,
    c_columnname character varying(50) NOT NULL,
    c_columndatatype character varying(50) NOT NULL,
    c_operator character varying(10) NOT NULL,
    c_dimcode character varying(700) NOT NULL,
    c_comment text,
    c_tooltip character varying(900),
    m_applied_path character varying(700) NOT NULL,
    update_date timestamp without time zone NOT NULL,
    download_date timestamp without time zone,
    import_date timestamp without time zone,
    sourcesystem_cd character varying(50),
    valuetype_cd character varying(50),
    m_exclusion_cd character varying(25),
    c_path character varying(700),
    c_symbol character varying(50),
    plain_code character varying(25)
);


ALTER TABLE i2b2metadata.icd10_icd9 OWNER TO i2b2metadata;

--
-- Name: ont_process_status; Type: TABLE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE TABLE i2b2metadata.ont_process_status (
    process_id integer NOT NULL,
    process_type_cd character varying(50),
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    process_step_cd character varying(50),
    process_status_cd character varying(50),
    crc_upload_id integer,
    status_cd character varying(50),
    message text,
    entry_date timestamp without time zone,
    change_date timestamp without time zone,
    changedby_char character(50)
);


ALTER TABLE i2b2metadata.ont_process_status OWNER TO i2b2metadata;

--
-- Name: ont_process_status_process_id_seq; Type: SEQUENCE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE SEQUENCE i2b2metadata.ont_process_status_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2metadata.ont_process_status_process_id_seq OWNER TO i2b2metadata;

--
-- Name: ont_process_status_process_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2metadata; Owner: i2b2metadata
--

ALTER SEQUENCE i2b2metadata.ont_process_status_process_id_seq OWNED BY i2b2metadata.ont_process_status.process_id;


--
-- Name: schemes; Type: TABLE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE TABLE i2b2metadata.schemes (
    c_key character varying(50) NOT NULL,
    c_name character varying(50) NOT NULL,
    c_description character varying(100)
);


ALTER TABLE i2b2metadata.schemes OWNER TO i2b2metadata;

--
-- Name: table_access; Type: TABLE; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE TABLE i2b2metadata.table_access (
    c_table_cd character varying(50) NOT NULL,
    c_table_name character varying(50) NOT NULL,
    c_protected_access character(1),
    c_hlevel integer NOT NULL,
    c_fullname character varying(700) NOT NULL,
    c_name character varying(2000) NOT NULL,
    c_synonym_cd character(1) NOT NULL,
    c_visualattributes character(3) NOT NULL,
    c_totalnum integer,
    c_basecode character varying(50),
    c_metadataxml text,
    c_facttablecolumn character varying(50) NOT NULL,
    c_dimtablename character varying(50) NOT NULL,
    c_columnname character varying(50) NOT NULL,
    c_columndatatype character varying(50) NOT NULL,
    c_operator character varying(10) NOT NULL,
    c_dimcode character varying(700) NOT NULL,
    c_comment text,
    c_tooltip character varying(900),
    c_entry_date timestamp without time zone,
    c_change_date timestamp without time zone,
    c_status_cd character(1),
    valuetype_cd character varying(50)
);


ALTER TABLE i2b2metadata.table_access OWNER TO i2b2metadata;

--
-- Name: pm_approvals; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_approvals (
    approval_id character varying(50) NOT NULL,
    approval_name character varying(255),
    approval_description character varying(2000),
    approval_activation_date timestamp without time zone,
    approval_expiration_date timestamp without time zone,
    object_cd character varying(50),
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_approvals OWNER TO i2b2pm;

--
-- Name: pm_approvals_params; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_approvals_params (
    id integer NOT NULL,
    approval_id character varying(50) NOT NULL,
    param_name_cd character varying(50) NOT NULL,
    value text,
    activation_date timestamp without time zone,
    expiration_date timestamp without time zone,
    datatype_cd character varying(50),
    object_cd character varying(50),
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_approvals_params OWNER TO i2b2pm;

--
-- Name: pm_approvals_params_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_approvals_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_approvals_params_id_seq OWNER TO i2b2pm;

--
-- Name: pm_approvals_params_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_approvals_params_id_seq OWNED BY i2b2pm.pm_approvals_params.id;


--
-- Name: pm_cell_data; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_cell_data (
    cell_id character varying(50) NOT NULL,
    project_path character varying(255) NOT NULL,
    name character varying(255),
    method_cd character varying(255),
    url character varying(255),
    can_override integer,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_cell_data OWNER TO i2b2pm;

--
-- Name: pm_cell_params; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_cell_params (
    id integer NOT NULL,
    datatype_cd character varying(50),
    cell_id character varying(50) NOT NULL,
    project_path character varying(255) NOT NULL,
    param_name_cd character varying(50) NOT NULL,
    value text,
    can_override integer,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_cell_params OWNER TO i2b2pm;

--
-- Name: pm_cell_params_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_cell_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_cell_params_id_seq OWNER TO i2b2pm;

--
-- Name: pm_cell_params_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_cell_params_id_seq OWNED BY i2b2pm.pm_cell_params.id;


--
-- Name: pm_global_params; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_global_params (
    id integer NOT NULL,
    datatype_cd character varying(50),
    param_name_cd character varying(50) NOT NULL,
    project_path character varying(255) NOT NULL,
    value text,
    can_override integer,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_global_params OWNER TO i2b2pm;

--
-- Name: pm_global_params_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_global_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_global_params_id_seq OWNER TO i2b2pm;

--
-- Name: pm_global_params_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_global_params_id_seq OWNED BY i2b2pm.pm_global_params.id;


--
-- Name: pm_hive_data; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_hive_data (
    domain_id character varying(50) NOT NULL,
    helpurl character varying(255),
    domain_name character varying(255),
    environment_cd character varying(255),
    active integer,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_hive_data OWNER TO i2b2pm;

--
-- Name: pm_hive_params; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_hive_params (
    id integer NOT NULL,
    datatype_cd character varying(50),
    domain_id character varying(50) NOT NULL,
    param_name_cd character varying(50) NOT NULL,
    value text,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_hive_params OWNER TO i2b2pm;

--
-- Name: pm_hive_params_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_hive_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_hive_params_id_seq OWNER TO i2b2pm;

--
-- Name: pm_hive_params_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_hive_params_id_seq OWNED BY i2b2pm.pm_hive_params.id;


--
-- Name: pm_project_data; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_project_data (
    project_id character varying(50) NOT NULL,
    project_name character varying(255),
    project_wiki character varying(255),
    project_key character varying(255),
    project_path character varying(255),
    project_description character varying(2000),
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_project_data OWNER TO i2b2pm;

--
-- Name: pm_project_params; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_project_params (
    id integer NOT NULL,
    datatype_cd character varying(50),
    project_id character varying(50) NOT NULL,
    param_name_cd character varying(50) NOT NULL,
    value text,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_project_params OWNER TO i2b2pm;

--
-- Name: pm_project_params_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_project_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_project_params_id_seq OWNER TO i2b2pm;

--
-- Name: pm_project_params_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_project_params_id_seq OWNED BY i2b2pm.pm_project_params.id;


--
-- Name: pm_project_request; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_project_request (
    id integer NOT NULL,
    title character varying(255),
    request_xml text NOT NULL,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50),
    project_id character varying(50),
    submit_char character varying(50)
);


ALTER TABLE i2b2pm.pm_project_request OWNER TO i2b2pm;

--
-- Name: pm_project_request_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_project_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_project_request_id_seq OWNER TO i2b2pm;

--
-- Name: pm_project_request_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_project_request_id_seq OWNED BY i2b2pm.pm_project_request.id;


--
-- Name: pm_project_user_params; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_project_user_params (
    id integer NOT NULL,
    datatype_cd character varying(50),
    project_id character varying(50) NOT NULL,
    user_id character varying(50) NOT NULL,
    param_name_cd character varying(50) NOT NULL,
    value text,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_project_user_params OWNER TO i2b2pm;

--
-- Name: pm_project_user_params_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_project_user_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_project_user_params_id_seq OWNER TO i2b2pm;

--
-- Name: pm_project_user_params_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_project_user_params_id_seq OWNED BY i2b2pm.pm_project_user_params.id;


--
-- Name: pm_project_user_roles; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_project_user_roles (
    project_id character varying(50) NOT NULL,
    user_id character varying(50) NOT NULL,
    user_role_cd character varying(255) NOT NULL,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_project_user_roles OWNER TO i2b2pm;

--
-- Name: pm_role_requirement; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_role_requirement (
    table_cd character varying(50) NOT NULL,
    column_cd character varying(50) NOT NULL,
    read_hivemgmt_cd character varying(50) NOT NULL,
    write_hivemgmt_cd character varying(50) NOT NULL,
    name_char character varying(2000),
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_role_requirement OWNER TO i2b2pm;

--
-- Name: pm_user_data; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_user_data (
    user_id character varying(50) NOT NULL,
    full_name character varying(255),
    password character varying(255),
    email character varying(255),
    project_path character varying(255),
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_user_data OWNER TO i2b2pm;

--
-- Name: pm_user_login; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_user_login (
    user_id character varying(50) NOT NULL,
    attempt_cd character varying(50) NOT NULL,
    entry_date timestamp without time zone NOT NULL,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_user_login OWNER TO i2b2pm;

--
-- Name: pm_user_params; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_user_params (
    id integer NOT NULL,
    datatype_cd character varying(50),
    user_id character varying(50) NOT NULL,
    param_name_cd character varying(50) NOT NULL,
    value text,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_user_params OWNER TO i2b2pm;

--
-- Name: pm_user_params_id_seq; Type: SEQUENCE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE SEQUENCE i2b2pm.pm_user_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE i2b2pm.pm_user_params_id_seq OWNER TO i2b2pm;

--
-- Name: pm_user_params_id_seq; Type: SEQUENCE OWNED BY; Schema: i2b2pm; Owner: i2b2pm
--

ALTER SEQUENCE i2b2pm.pm_user_params_id_seq OWNED BY i2b2pm.pm_user_params.id;


--
-- Name: pm_user_session; Type: TABLE; Schema: i2b2pm; Owner: i2b2pm
--

CREATE TABLE i2b2pm.pm_user_session (
    user_id character varying(50) NOT NULL,
    session_id character varying(50) NOT NULL,
    expired_date timestamp without time zone,
    change_date timestamp without time zone,
    entry_date timestamp without time zone,
    changeby_char character varying(50),
    status_cd character varying(50)
);


ALTER TABLE i2b2pm.pm_user_session OWNER TO i2b2pm;

--
-- Name: workplace; Type: TABLE; Schema: i2b2workdata; Owner: i2b2workdata
--

CREATE TABLE i2b2workdata.workplace (
    c_name character varying(255) NOT NULL,
    c_user_id character varying(255) NOT NULL,
    c_group_id character varying(255) NOT NULL,
    c_share_id character varying(255),
    c_index character varying(255) NOT NULL,
    c_parent_index character varying(255),
    c_visualattributes character(3) NOT NULL,
    c_protected_access character(1),
    c_tooltip character varying(255),
    c_work_xml text,
    c_work_xml_schema text,
    c_work_xml_i2b2_type character varying(255),
    c_entry_date timestamp without time zone,
    c_change_date timestamp without time zone,
    c_status_cd character(1)
);


ALTER TABLE i2b2workdata.workplace OWNER TO i2b2workdata;

--
-- Name: workplace_access; Type: TABLE; Schema: i2b2workdata; Owner: i2b2workdata
--

CREATE TABLE i2b2workdata.workplace_access (
    c_table_cd character varying(255) NOT NULL,
    c_table_name character varying(255) NOT NULL,
    c_protected_access character(1),
    c_hlevel integer NOT NULL,
    c_name character varying(255) NOT NULL,
    c_user_id character varying(255) NOT NULL,
    c_group_id character varying(255) NOT NULL,
    c_share_id character varying(255),
    c_index character varying(255) NOT NULL,
    c_parent_index character varying(255),
    c_visualattributes character(3) NOT NULL,
    c_tooltip character varying(255),
    c_entry_date timestamp without time zone,
    c_change_date timestamp without time zone,
    c_status_cd character(1)
);


ALTER TABLE i2b2workdata.workplace_access OWNER TO i2b2workdata;

--
-- Name: batch_job_execution; Type: TABLE; Schema: ts_batch; Owner: postgres
--

CREATE TABLE ts_batch.batch_job_execution (
    job_execution_id bigint NOT NULL,
    version bigint,
    job_instance_id bigint NOT NULL,
    create_time timestamp without time zone NOT NULL,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    status character varying(10),
    exit_code character varying(2500),
    exit_message character varying(2500),
    last_updated timestamp without time zone,
    job_configuration_location character varying(2500)
);


ALTER TABLE ts_batch.batch_job_execution OWNER TO postgres;

--
-- Name: batch_job_execution_context; Type: TABLE; Schema: ts_batch; Owner: postgres
--

CREATE TABLE ts_batch.batch_job_execution_context (
    job_execution_id bigint NOT NULL,
    short_context character varying(2500) NOT NULL,
    serialized_context text
);


ALTER TABLE ts_batch.batch_job_execution_context OWNER TO postgres;

--
-- Name: batch_job_execution_params; Type: TABLE; Schema: ts_batch; Owner: postgres
--

CREATE TABLE ts_batch.batch_job_execution_params (
    job_execution_id bigint NOT NULL,
    type_cd character varying(6) NOT NULL,
    key_name character varying(100) NOT NULL,
    string_val character varying(250),
    date_val timestamp without time zone,
    long_val bigint,
    double_val double precision,
    identifying character(1) NOT NULL
);


ALTER TABLE ts_batch.batch_job_execution_params OWNER TO postgres;

--
-- Name: batch_job_execution_seq; Type: SEQUENCE; Schema: ts_batch; Owner: postgres
--

CREATE SEQUENCE ts_batch.batch_job_execution_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ts_batch.batch_job_execution_seq OWNER TO postgres;

--
-- Name: batch_job_instance; Type: TABLE; Schema: ts_batch; Owner: postgres
--

CREATE TABLE ts_batch.batch_job_instance (
    job_instance_id bigint NOT NULL,
    version bigint,
    job_name character varying(100) NOT NULL,
    job_key character varying(32) NOT NULL
);


ALTER TABLE ts_batch.batch_job_instance OWNER TO postgres;

--
-- Name: batch_job_seq; Type: SEQUENCE; Schema: ts_batch; Owner: postgres
--

CREATE SEQUENCE ts_batch.batch_job_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ts_batch.batch_job_seq OWNER TO postgres;

--
-- Name: batch_step_execution; Type: TABLE; Schema: ts_batch; Owner: postgres
--

CREATE TABLE ts_batch.batch_step_execution (
    step_execution_id bigint NOT NULL,
    version bigint NOT NULL,
    step_name character varying(100) NOT NULL,
    job_execution_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone,
    status character varying(10),
    commit_count bigint,
    read_count bigint,
    filter_count bigint,
    write_count bigint,
    read_skip_count bigint,
    write_skip_count bigint,
    process_skip_count bigint,
    rollback_count bigint,
    exit_code character varying(2500),
    exit_message character varying(2500),
    last_updated timestamp without time zone
);


ALTER TABLE ts_batch.batch_step_execution OWNER TO postgres;

--
-- Name: batch_step_execution_context; Type: TABLE; Schema: ts_batch; Owner: postgres
--

CREATE TABLE ts_batch.batch_step_execution_context (
    step_execution_id bigint NOT NULL,
    short_context character varying(2500) NOT NULL,
    serialized_context text
);


ALTER TABLE ts_batch.batch_step_execution_context OWNER TO postgres;

--
-- Name: batch_step_execution_seq; Type: SEQUENCE; Schema: ts_batch; Owner: postgres
--

CREATE SEQUENCE ts_batch.batch_step_execution_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ts_batch.batch_step_execution_seq OWNER TO postgres;

--
-- Name: text_search_index; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.observation_fact ALTER COLUMN text_search_index SET DEFAULT nextval('i2b2demodata.observation_fact_text_search_index_seq'::regclass);


--
-- Name: patient_enc_coll_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_patient_enc_collection ALTER COLUMN patient_enc_coll_id SET DEFAULT nextval('i2b2demodata.qt_patient_enc_collection_patient_enc_coll_id_seq'::regclass);


--
-- Name: patient_set_coll_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_patient_set_collection ALTER COLUMN patient_set_coll_id SET DEFAULT nextval('i2b2demodata.qt_patient_set_collection_patient_set_coll_id_seq'::regclass);


--
-- Name: query_master_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_pdo_query_master ALTER COLUMN query_master_id SET DEFAULT nextval('i2b2demodata.qt_pdo_query_master_query_master_id_seq'::regclass);


--
-- Name: query_instance_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_instance ALTER COLUMN query_instance_id SET DEFAULT nextval('i2b2demodata.qt_query_instance_query_instance_id_seq'::regclass);


--
-- Name: query_master_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_master ALTER COLUMN query_master_id SET DEFAULT nextval('i2b2demodata.qt_query_master_query_master_id_seq'::regclass);


--
-- Name: result_instance_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_result_instance ALTER COLUMN result_instance_id SET DEFAULT nextval('i2b2demodata.qt_query_result_instance_result_instance_id_seq'::regclass);


--
-- Name: xml_result_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_xml_result ALTER COLUMN xml_result_id SET DEFAULT nextval('i2b2demodata.qt_xml_result_xml_result_id_seq'::regclass);


--
-- Name: upload_id; Type: DEFAULT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.upload_status ALTER COLUMN upload_id SET DEFAULT nextval('i2b2demodata.upload_status_upload_id_seq'::regclass);


--
-- Name: process_id; Type: DEFAULT; Schema: i2b2metadata; Owner: i2b2metadata
--

ALTER TABLE ONLY i2b2metadata.ont_process_status ALTER COLUMN process_id SET DEFAULT nextval('i2b2metadata.ont_process_status_process_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_approvals_params ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_approvals_params_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_cell_params ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_cell_params_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_global_params ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_global_params_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_hive_params ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_hive_params_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_params ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_project_params_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_request ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_project_request_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_user_params ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_project_user_params_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_user_params ALTER COLUMN id SET DEFAULT nextval('i2b2pm.pm_user_params_id_seq'::regclass);


--
-- Data for Name: archive_observation_fact; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.archive_observation_fact (encounter_num, patient_num, concept_cd, provider_id, start_date, modifier_cd, instance_num, valtype_cd, tval_char, nval_num, valueflag_cd, quantity_num, units_cd, end_date, location_cd, observation_blob, confidence_num, update_date, download_date, import_date, sourcesystem_cd, upload_id, text_search_index, archive_upload_id) FROM stdin;
\.


--
-- Data for Name: code_lookup; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.code_lookup (table_cd, column_cd, code_cd, name_char, lookup_blob, upload_date, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:yugo	Yugoslavian-YUGO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:french	French-FRENCH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:haitian	Haitian-HAITIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:soma	Somalian-SOMA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:dane	Danish-DANE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:danish	Danish-DANISH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:latvian	Latvian-LATVIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:navajo	Navajo-NAVAJO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:urdu	Urdu	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:portuguese	Portuguese-PORTUGUESE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:nez	Nez Perce-NEZ	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:english	English-ENGLISH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hebr	Hebrew-HEBR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:spanish	Spanish-SPANISH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:greek	Greek-GREEK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:taga	Tagalag-TAGA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:bulgarian	Bulgarian-BULGARIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hndi	Hindi-HNDI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:aborigines	Aborigines-ABORIGINES	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:russian	Russian-RUSSIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tamil	Tamil-TAMIL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:swed	Swedish-SWED	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:korean	Korean-KOREAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:laot	Laotian-LAOT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:arab	Arabic-ARAB	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:creole	Creole	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:italian	Italian-ITALIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:bulg	Bulgarian-BULG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:mute	Mute / Illiterate	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:albanian	Albanian-ALBANIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:bosnian	Bosnian-BOSNIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:amha	Ethiopian-Amharic-AMHA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:islandic	Islandic-ISLANDIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:laotian	Laotian-LAOTIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:german	German-GERMAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:cape verdean	Cape Verdean-CAPE VERDEAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hungarian	Hungarian-HUNGARIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hmong	Hmong-HMONG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:liberian	Liberian-LIBERIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:u	Not recorded-U	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:rom	Romanian-ROM	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tigr	Ethiopian-Tigrinia-TIGR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:isle	Islandic-ISLE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:cambodian	Cambodian-CAMBODIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:sign	Sign Language-SIGN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:latv	Latvian-LATV	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hebrew	Hebrew-HEBREW	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:indian-native	Native Indian-INDIAN-NATIVE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:bengali	Bengali-BENGALI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:yiddish	Yiddish-YIDDISH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:lith	Lithuanian-LITH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:shnd	Shonal Ndebele-SHND	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:sign lang	Sign Language-SIGN LANG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tami	Tamil-TAMI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:chinese-mand.	Chinese-Mandarin-CHINESE-MAND.	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:norw	Norwegian-NORW	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:spen	Spanish English-SPEN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tagalog	Tagalog	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:dutch	Dutch-DUTCH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:dutc	Dutch-DUTC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:norwegian	Norwegian-NORWEGIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:span-engl	Spanish English-SPAN-ENGL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:mong	Hmong-MONG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:czech	Czech-CZECH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ethiopian-amharic	Ethiopian-Amharic-ETHIOPIAN-AMHARIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:nige	Nigerian-NIGE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:burm	Burmese-BURM	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ethiopian-tigrinya	Ethiopian-Tigrinia-ETHIOPIAN-TIGRINYA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:grek	Greek-GREK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:serbo-croatian	Serbo-Croation-SERBO-CROATIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:vietnamese	Vietnamese-VIETNAMESE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:cape	Cape Verdean-CAPE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:prci	Persian Farsi	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:kn	KN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:capp	Cape Verdean Portugese-CAPP	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tois	Chinese-Toisan-TOIS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:@	Not recorded-@	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:russ	Russian-RUSS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:eritrean	Eritrean-ERITREAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:cape verd-creole	Cape Verdean Creole-CAPE VERD-CREOLE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:lithuanian	Lithuanian-LITHUANIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tibe	Tibetan-TIBE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ccv	Cape Verdean Creole-CCV	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:chinese	Chinese-CHINESE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:frc	French Canadian	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:natv	Native Indian-NATV	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tagalag	Tagalag-TAGALAG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:gugarati	Gujarati-GUGARATI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:camb	Cambodian-CAMB	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:fren	French-FREN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hc	Haitian French Creole-HC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hung	Hungarian-HUNG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:swah	Swahil-SWAH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:nav	Navajo-NAV	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:other	Other-OTHER	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:swedish	Swedish-SWEDISH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:taiwanese	Taiwanese-TAIWANESE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:arme	Armenian-ARME	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:turk	Turkish-TURK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:port	Portuguese-PORT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:punj	Punjabi-PUNJ	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:libe	Liberian-LIBE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ydsh	Yiddish-YDSH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:thai	Thai	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:arabic	Arabic-ARABIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:germ	German-GERM	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:kore	Korean-KORE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:erit	Eritrean-ERIT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ital	Italian-ITAL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:shonal ndebele	Shonal Ndebele-SHONAL NDEBELE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:viet	Vietnamese-VIET	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:test	Not recorded-TEST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:polish	Polish-POLISH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:haitian-fr.creole	Haitian French Creole-HAITIAN-FR.CREOLE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:chinese-toisan	Chinese-Toisan-CHINESE-TOISAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:punjabi	Punjabi-PUNJABI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:othe	Other-OTHE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:pol	Polish-POL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:alba	Albanian-ALBA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:cant	Chinese-Cantonese-CANT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:farsi-persian	Farsi-FARSI-PERSIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:french creole	French Creole-FRENCH CREOLE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:cape verd-portuguese	Cape Verdean Portugese-CAPE VERD-PORTUGUESE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:chinese-cant.	Chinese-Cantonese-CHINESE-CANT.	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hindi	Hindi-HINDI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:por	Portuguese-POR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:frcr	French Creole-FRCR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:nigerian	Nigerian-NIGERIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:amer. sign lang	American Sign Language-AMER. SIGN LANG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:fars	Farsi-FARS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:na	Native Indian-NA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ukra	Ukranian\\UKRA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ukrainian	Ukranian\\UKRAINIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:beng	Bengali-BENG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:hati	Haitian-HATI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:ser	Serbo-Croation-SER	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:swahili	Swahil-SWAHILI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:guga	Gujarat-GUGA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:mand	Chinese-Mandarin-MAND	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:span	Spanish-SPAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:chin	Chinese-CHIN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:abo	Aborigines-ABO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:unknown	Unknown-UNKNOWN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:tibetan	Tibetan-TIBETAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:nez perce	Nez Perce-NEZ PERCE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:taiw	Taiwanese-TAIW	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:creo	Creole French	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:turkish	Turkish-TURKISH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:czek	Czech-CZEK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:engl	English-ENGL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:asl	American Sign Language-ASL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:armenian	Armenian-ARMENIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:japa	Japanese-JAPA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:burmese	Burmese-BURMESE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:romanian	Romanian-ROMANIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:somalian	Somalian-SOMALIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:japanese	Japanese-JAPANESE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:yugoslavian	Yugoslavian-YUGOSLAVIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	LANGUAGE_CD	DEM|LANGUAGE:bosn	Bosnian-BOSN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	VITAL_STATUS_CD	DEM|VITAL:x	Deferred	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	VITAL_STATUS_CD	DEM|VITAL:y	Deceased	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	VITAL_STATUS_CD	DEM|VITAL:@	Not recorded	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	VITAL_STATUS_CD	DEM|VITAL:n	Living	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:m	Middle Eastern-M	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:na	Native American-NA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:o	Other-O	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:i	American Indian-I	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:black	Black-BLACK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:oriental	Oriental-ORIENTAL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:al	Aleutian-AL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:u	Not Recorded-U	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:indian	Indian-INDIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:asian/pac. isl	Asian Pacific Islander-ASIAN PAC ISL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:hispanic	Hispanic-HISPANIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:w	White-W	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:es	Eskimo-ES	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:hib	Hispanic Black-HIB	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:his/black	Hispanic Black-HIS BLACK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:r	Not Recorded-R	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:amer. indian	American Indian-AMER. INDIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:asian	Asian-ASIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:nat. am.	Native American-NAT AM	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:ni	Native American-NI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:white	White-WHITE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:h	Hispanic-H	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:a	Asian-A	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:b	Black-B	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:refused	Not Recorded-REFUSED	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:c	White-C	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:hiw	Hispanic White-HIW	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:d	Not Recorded-D	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:his/white	Hispanic White-HIS WHITE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:api	Asian Pacific Islander-API	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:multi	Multiracial-MULTI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:eskimo	Eskimo-ESKIMO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:other	Other-OTHER	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:na.esk	Eskimo-NA ESK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:deferred	Not Recorded-DEFERRED	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:nv	Navajo-NV	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:navajo	Navajo-NAVAJO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:or	Oriental-OR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:mid.eastern	Middle Eastern-MID EASTERN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:in	Indian-IN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:mr	Multiracial-MR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:@	Not Recorded-@	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:unk	Not Recorded-UNK	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RACE_CD	DEM|RACE:aleutian	Aleutian-ALEUTIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ai	American Indian-AI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:church of god	Church of God	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:is	Islamic-IS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:pentecostal	Pentecostal-PENTECOSTAL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:an	Anglican-AN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:as	Assembly of God-AS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:christian	Christian-CHRISTIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:rc	Roman Catholic-RC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:at	Atheist-AT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:hindu	Hindu-HINDU	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:jh	Jewish	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ro	Russian Orthodox-RO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:zor	Zoroastrian-ZOR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ba	Bahai-BA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:christian science	Christian Scientist-CHRISTIAN SCIENCE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:presbyterian	Presbyterian-PRESBYTERIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:rs	Religious Science-RELIGIOUS SCIENCE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:car	Charismatic-CAR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:7th day advent	Seventh Day Adventist-7TH DAY ADVENT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:amer indian	American Indian-AMER INDIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:jv	Jehovah's Witness-JV	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:religious science	Religious Science-RS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:bp	Baptist-BP	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:sa	Salvation Army-SA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:jw	Jehovah's Witness-JW	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:or	Orthodox-OR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:sb	Southern Baptist-SB	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:sd	Seventh Day Adventist-SD	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:acat	Armenian Catholic-ACAT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:nazarene	Nazarene-NAZARENE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:bu	Buddhist-BU	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:si	Sikh-SI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:disciples of christ	Disciples Of Christ-DISCIPLES OF CHRIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ca	Catholic-CA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:sp	Spiritualist-SP	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:lutheran	Lutheran-LUTHERAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:unitarian univers.	Unitarian Universal-UNITARIAN UNIVERS.	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:melkite catholic	Melkite Catholic-MELKITE CATHOLIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:unknown	Unknown-UNKNOWN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ch	Christian-CH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:russian orth.	Russian Orthodox-RUSSIAN ORTH.	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:co	Congregational-CO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:other	Other-OTHER	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:cs	Christian Scientist-CS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:spiritualist	Spiritualist-SPIRITUALIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:mennonite	Mennonite	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:maronite catholic	Maronite Catholic-MC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:charismatic	Charismatic-CHARISMATIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:dc	Disciples Of Christ-DC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:de	Deferred-DE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:sci	Scientology-SCI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:roman catholic	Roman Catholic-ROMAN CATHOLIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:jewish	Jewish	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:lu	Lutheran-LU	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ua	Unaffiliated	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:united church of christ	United Church of Christ-UNITED CHURCH  OF CHRIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:naf	Not Affiliated	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:bahai	Bahai-BAHAI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:anglican	Anglican-ANGLICAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:mc	Maronite Catholic-MARONITE CATHOLIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:orthodox	Orthodox-ORTHODOX	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:me	Methodist-ME	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:quaker	Quaker-QUAKER	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:greek orthodox	Greek Orthodox-GREEK ORTHODOX	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:un	Unknown-UN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ec	Evangelical Christian-EC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:uu	Unitarian-UU	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:methodist	Methodist-METHODIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:mo	Mormon-MO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:uv	Unitarian Universal-UV	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:islamic	Islamic-ISLAMIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:@	Not Recorded-@	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ms	Moslem	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:non-denom	Non-denominational-NON-DENOM	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:mu	Muslim-MU	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:wiccan	Wiccan-WICCAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:eo	Eastern Orthodox-EO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ndn	Non-denominational-NDN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ep	Episcopal-EP	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:sikh	Sikh-SIKH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:agnostic	Agnostic-AGNOSTIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:orthodox christian	Orthodox Christian-ORTHODOX CHRISTIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:mormon (lds)	Mormon-MORMON (LDS)	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:na	Nazarene-NAZ	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:nd	Non-denominational-ND	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:scientology	Scientology-SCIENTOLOGY	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:advent christian	Advent Christian-ADVENT CHRISTIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:none	None-NONE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:np	Not Recorded-NP	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:no	None-NO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:no pref	No Preference-NO PREF	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:congragational	Congregational-CONGRAGATIONAL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:united church christ	United Church of Christ-UNITED CHURCH CHRIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:wi	Wiccan-WI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:oc	Orthodox Christian-OC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:deferred	Deferred-DEFERRED	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:nsp	No Preference-NSP	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:buddhist	Buddhist-BUDDHIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:southern baptist	Southern Baptist-SOUTHERN BAPTIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:zoroastrian	Zoroastrian-ZOROASTRIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:congregational	Congregational-CONGREGATIONAL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ot	Other-OT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:go	Greek Orthodox-GO	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:eastern orthodox	Eastern Orthodox-EASTERN ORTHODOX	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:unitarian	Unitarian-UNITARIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:catholic	Catholic-CATHOLIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:pe	Pentecostal-PE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:jehov. witness	Jehovah's Witness-JEHOV. WITNESS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:jehov.witness	Jehovah's Witness-JEHOV.WITNESS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:protestant	Protestant-PROTESTANT	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:church of england	Church of England	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:pr	Protestant-PR	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:hi	Hindu-HI	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:no visit	No Visit	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ps	Presbyterian-PS	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:baptist	Baptist-BAPTIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:armenian catholic	Armenian Catholic-ARMENIAN CATHOLIC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:atheist	Atheist-ATHEIST	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ucc	United Church of Christ-UCC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:7a	Seventh Day Adventist-7A	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ref	Not Recorded-REF	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ath	Atheist-ATH	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:muslim	Muslim-MUSLIM	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:assemb of god	Assembly of God-ASSEMB OF GOD	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:episcopal	Episcopal-EPISCOPAL	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:mkc	Melkite Catholic-MKC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:swe	Swedenborgia-SWE	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:cor	Christian Orth	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:evang. christian	Evangelical Christian-EVANG. CHRISTIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ac	Advent Christian-AC	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:salv. army	Salvation Army-SALV. ARMY	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:qu	Quaker-QU	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:aca	Armenian Catholic-ACA	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:ag	Agnostic-AG	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	RELIGION_CD	DEM|RELIGION:swedenborgian	Swedenborgian-SWEDENBORGIAN	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	SEX_CD	DEM|SEX:m	Male	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	SEX_CD	DEM|SEX:nb	NonBinary	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	SEX_CD	DEM|SEX:@	Not Recorded	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	SEX_CD	DEM|SEX:u	Unknown-U	\N	\N	\N	\N	\N	\N	\N
PATIENT_DIMENSION	SEX_CD	DEM|SEX:f	Female	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: concept_dimension; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.concept_dimension (concept_path, concept_cd, name_char, concept_blob, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
\\DRG\\	DRG:0	DRG	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ZUSCHLAGTAGE\\	ZUSCHLAGTA:0	ZUSCHLAGTAGE:ZUSCHLAGTA:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ZUSCHLAGKOOPBETRAG\\	ZUSCHLAGKO:0	ZUSCHLAGKOOPBETRAG:ZUSCHLAGKO:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ZUSCHLAGGEW\\	ZUSCHLAGGE:0	ZUSCHLAGGEW:ZUSCHLAGGE:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\VWDVERWENDET\\	VWDVERWEND:0	VWDVERWENDET:VWDVERWEND:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\	RELGEW:0	RELGEW	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\2,723\\	RELGEW:7	2,723:RELGEW:7	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\1,031\\	RELGEW:4	1,031:RELGEW:4	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\0,9\\	RELGEW:5	0,9:RELGEW:5	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\0,843\\	RELGEW:3	0,843:RELGEW:3	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\0,624\\	RELGEW:1	0,624:RELGEW:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\0,478\\	RELGEW:6	0,478:RELGEW:6	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\RELGEW\\0,295\\	RELGEW:2	0,295:RELGEW:2	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\PCCL\\	PCCL:0	PCCL:PCCL:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\PATIENTSTATUS\\	PATIENTSTA:0	PATIENTSTATUS:PATIENTSTA:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\MDC\\	MDC:0	MDC:MDC:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\GST\\	GST:0	GST:GST:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGVWD\\	FLAGVWD:0	FLAGVWD:FLAGVWD:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGTAGESFALL\\	FLAGTAGESF:0	FLAGTAGESFALL:FLAGTAGESF:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGGESCHLECHT\\	FLAGGESCHL:0	FLAGGESCHLECHT:FLAGGESCHL:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGENTLGRUND\\	FLAGENTLGR:0	FLAGENTLGRUND:FLAGENTLGR:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGEINWEISUNG\\	FLAGEINWEI:0	FLAGEINWEISUNG:FLAGEINWEI:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGAUFNGEWICHT2\\	FLAGAUFNGE:0	FLAGAUFNGEWICHT2:FLAGAUFNGE:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGAUFNGEWICHT1\\	FLAGAUFNGE:1	FLAGAUFNGEWICHT1:FLAGAUFNGE:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGANZBEATMUNGSTD\\	FLAGANZBEA:0	FLAGANZBEATMUNGSTD:FLAGANZBEA:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\FLAGALTER\\	FLAGALTER:0	FLAGALTER:FLAGALTER:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ENTGSTATUS\\	ENTGSTATUS:0	ENTGSTATUS:ENTGSTATUS:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\	EFFGEW:0	EFFGEW	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\2,723\\	EFFGEW:7	2,723:EFFGEW:7	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\1,031\\	EFFGEW:4	1,031:EFFGEW:4	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\0,843\\	EFFGEW:3	0,843:EFFGEW:3	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\0,624\\	EFFGEW:1	0,624:EFFGEW:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\0,564\\	EFFGEW:5	0,564:EFFGEW:5	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\0,478\\	EFFGEW:6	0,478:EFFGEW:6	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\EFFGEW\\0,295\\	EFFGEW:2	0,295:EFFGEW:2	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGVERSION\\	DRGVERSION:0	DRGVERSION	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGVERSION\\G-DRG V2009\\	DRGVERSION:1	G-DRG V2009:DRGVERSION:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\	DRGCODE:0	DRGCODE	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\J11C\\	DRGCODE:1	J11C:DRGCODE:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\F62C\\	DRGCODE:3	F62C:DRGCODE:3	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\F59B\\	DRGCODE:5	F59B:DRGCODE:5	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\D24B\\	DRGCODE:7	D24B:DRGCODE:7	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\C08B\\	DRGCODE:6	C08B:DRGCODE:6	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\C02B\\	DRGCODE:4	C02B:DRGCODE:4	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\DRGCODE\\B80Z\\	DRGCODE:2	B80Z:DRGCODE:2	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\BASISRATE\\	BASISRATE:0	BASISRATE:BASISRATE:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ABTTYP\\	ABTTYP:0	ABTTYP:ABTTYP:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ABSCHLAGVERLTAGE\\	ABSCHLAGVE:0	ABSCHLAGVERLTAGE:ABSCHLAGVE:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ABSCHLAGVERLGEW\\	ABSCHLAGVE:1	ABSCHLAGVERLGEW:ABSCHLAGVE:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\DRG\\ABSCHLAGTAGE\\	ABSCHLAGTA:0	ABSCHLAGTAGE:ABSCHLAGTA:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\	FALL:0	Fall	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\ENTLASSUNGSGRUND\\	ENTLASSUNG:0	ENTLASSUNGSGRUND:ENTLASSUNG:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\ENTLASSUNGSDATUM\\	ENTLASSUNG:1	ENTLASSUNGSDATUM:ENTLASSUNG:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\AUFNAHMEGRUND\\	AUFNAHMEGR:0	AUFNAHMEGRUND:AUFNAHMEGR:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\AUFNAHMEDATUM\\	AUFNAHMEDA:0	AUFNAHMEDATUM:AUFNAHMEDA:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\AUFNAHMEANLASS\\	AUFNAHMEAN:0	AUFNAHMEANLASS	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\AUFNAHMEANLASS\\Notfall\\	AUFNAHMEAN:2	Notfall:AUFNAHMEAN:2	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Fall\\AUFNAHMEANLASS\\Einweisung durch einen Arzt\\	AUFNAHMEAN:1	Einweisung durch einen Arzt:AUFNAHMEAN:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\	ICD-10:0	ICD-10	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\	ICD_KODE:0	ICD_KODE	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\Z96.1\\	ICD_KODE:19	Z96.1:ICD_KODE:19	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\Z95.1\\	ICD_KODE:26	Z95.1:ICD_KODE:26	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\Z86.4\\	ICD_KODE:36	Z86.4:ICD_KODE:36	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\Z03.4\\	ICD_KODE:11	Z03.4:ICD_KODE:11	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\T88.8\\	ICD_KODE:14	T88.8:ICD_KODE:14	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\T82.5\\	ICD_KODE:22	T82.5:ICD_KODE:22	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\S13.4\\	ICD_KODE:5	S13.4:ICD_KODE:5	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\S06.0\\	ICD_KODE:6	S06.0:ICD_KODE:6	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\S05.8\\	ICD_KODE:27	S05.8:ICD_KODE:27	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\S00.85\\	ICD_KODE:4	S00.85:ICD_KODE:4	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\R59.0\\	ICD_KODE:33	R59.0:ICD_KODE:33	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\N25.8\\	ICD_KODE:23	N25.8:ICD_KODE:23	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\N18.0\\	ICD_KODE:20	N18.0:ICD_KODE:20	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\M54.16\\	ICD_KODE:2	M54.16:ICD_KODE:2	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\J34.3\\	ICD_KODE:30	J34.3:ICD_KODE:30	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\J34.2\\	ICD_KODE:32	J34.2:ICD_KODE:32	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I97.8\\	ICD_KODE:29	I97.8:ICD_KODE:29	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I71.2\\	ICD_KODE:9	I71.2:ICD_KODE:9	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I50.13\\	ICD_KODE:7	I50.13:ICD_KODE:7	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I48.10\\	ICD_KODE:37	I48.10:ICD_KODE:37	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I44.3\\	ICD_KODE:31	I44.3:ICD_KODE:31	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I31.3\\	ICD_KODE:15	I31.3:ICD_KODE:15	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I27.28\\	ICD_KODE:10	I27.28:ICD_KODE:10	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I10.90\\	ICD_KODE:3	I10.90:ICD_KODE:3	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\I10.00\\	ICD_KODE:21	I10.00:ICD_KODE:21	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\H52.1\\	ICD_KODE:28	H52.1:ICD_KODE:28	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\H25.1\\	ICD_KODE:25	H25.1:ICD_KODE:25	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\H02.4\\	ICD_KODE:16	H02.4:ICD_KODE:16	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\H02.3\\	ICD_KODE:18	H02.3:ICD_KODE:18	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\F40.00\\	ICD_KODE:34	F40.00:ICD_KODE:34	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\F17.2\\	ICD_KODE:12	F17.2:ICD_KODE:12	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\F17.1\\	ICD_KODE:35	F17.1:ICD_KODE:35	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\F10.1\\	ICD_KODE:39	F10.1:ICD_KODE:39	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\E79.0\\	ICD_KODE:17	E79.0:ICD_KODE:17	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\E78.5\\	ICD_KODE:8	E78.5:ICD_KODE:8	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\E78.2\\	ICD_KODE:24	E78.2:ICD_KODE:24	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\E66.92\\	ICD_KODE:1	E66.92:ICD_KODE:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\E11.20+\\	ICD_KODE:13	E11.20+:ICD_KODE:13	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\C32.9\\	ICD_KODE:38	C32.9:ICD_KODE:38	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\	DEMOGRAFIS:0	Demografische Daten	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Race\\	RACE:0	Race	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Race\\Not recorded\\	DEM|RACE:@	Not recorded:DEM|RACE:@	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Vital status\\	VITAL STAT:0	Vital status	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Vital status\\Not recorded\\	DEM|VITAL:@	Not recorded:DEM|VITAL:@	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\	AGE:0	Age	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\Not recorded\\	DEM|AGE:@	Not recorded:DEM|AGE:@	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\Unknown\\	DEM|AGE:u	Unknown:DEM|AGE:u	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\	DEM|AGE:GE_85	>= 85 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\125 years old\\	DEM|AGE:125	125 years old:DEM|AGE:125	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\124 years old\\	DEM|AGE:124	124 years old:DEM|AGE:124	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\123 years old\\	DEM|AGE:123	123 years old:DEM|AGE:123	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\122 years old\\	DEM|AGE:122	122 years old:DEM|AGE:122	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\121 years old\\	DEM|AGE:121	121 years old:DEM|AGE:121	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\120 years old\\	DEM|AGE:120	120 years old:DEM|AGE:120	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\119 years old\\	DEM|AGE:119	119 years old:DEM|AGE:119	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\118 years old\\	DEM|AGE:118	118 years old:DEM|AGE:118	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\117 years old\\	DEM|AGE:117	117 years old:DEM|AGE:117	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\116 years old\\	DEM|AGE:116	116 years old:DEM|AGE:116	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\115 years old\\	DEM|AGE:115	115 years old:DEM|AGE:115	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\114 years old\\	DEM|AGE:114	114 years old:DEM|AGE:114	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\113 years old\\	DEM|AGE:113	113 years old:DEM|AGE:113	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\112 years old\\	DEM|AGE:112	112 years old:DEM|AGE:112	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\111 years old\\	DEM|AGE:111	111 years old:DEM|AGE:111	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\110 years old\\	DEM|AGE:110	110 years old:DEM|AGE:110	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\109 years old\\	DEM|AGE:109	109 years old:DEM|AGE:109	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\108 years old\\	DEM|AGE:108	108 years old:DEM|AGE:108	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\107 years old\\	DEM|AGE:107	107 years old:DEM|AGE:107	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\106 years old\\	DEM|AGE:106	106 years old:DEM|AGE:106	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\105 years old\\	DEM|AGE:105	105 years old:DEM|AGE:105	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\104 years old\\	DEM|AGE:104	104 years old:DEM|AGE:104	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\103 years old\\	DEM|AGE:103	103 years old:DEM|AGE:103	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\102 years old\\	DEM|AGE:102	102 years old:DEM|AGE:102	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\101 years old\\	DEM|AGE:101	101 years old:DEM|AGE:101	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\100 years old\\	DEM|AGE:100	100 years old:DEM|AGE:100	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\99 years old\\	DEM|AGE:99	99 years old:DEM|AGE:99	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\98 years old\\	DEM|AGE:98	98 years old:DEM|AGE:98	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\97 years old\\	DEM|AGE:97	97 years old:DEM|AGE:97	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\96 years old\\	DEM|AGE:96	96 years old:DEM|AGE:96	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\95 years old\\	DEM|AGE:95	95 years old:DEM|AGE:95	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\94 years old\\	DEM|AGE:94	94 years old:DEM|AGE:94	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\93 years old\\	DEM|AGE:93	93 years old:DEM|AGE:93	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\92 years old\\	DEM|AGE:92	92 years old:DEM|AGE:92	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\91 years old\\	DEM|AGE:91	91 years old:DEM|AGE:91	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\90 years old\\	DEM|AGE:90	90 years old:DEM|AGE:90	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\89 years old\\	DEM|AGE:89	89 years old:DEM|AGE:89	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\88 years old\\	DEM|AGE:88	88 years old:DEM|AGE:88	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\87 years old\\	DEM|AGE:87	87 years old:DEM|AGE:87	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\86 years old\\	DEM|AGE:86	86 years old:DEM|AGE:86	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\>= 85 years old\\85 years old\\	DEM|AGE:85	85 years old:DEM|AGE:85	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\	DEM|AGE:75_84	75-84 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\84 years old\\	DEM|AGE:84	84 years old:DEM|AGE:84	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\83 years old\\	DEM|AGE:83	83 years old:DEM|AGE:83	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\82 years old\\	DEM|AGE:82	82 years old:DEM|AGE:82	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\81 years old\\	DEM|AGE:81	81 years old:DEM|AGE:81	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\80 years old\\	DEM|AGE:80	80 years old:DEM|AGE:80	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\79 years old\\	DEM|AGE:79	79 years old:DEM|AGE:79	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\78 years old\\	DEM|AGE:78	78 years old:DEM|AGE:78	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\77 years old\\	DEM|AGE:77	77 years old:DEM|AGE:77	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\76 years old\\	DEM|AGE:76	76 years old:DEM|AGE:76	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\75-84 years old\\75 years old\\	DEM|AGE:75	75 years old:DEM|AGE:75	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\	DEM|AGE:65_74	65-74 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\74 years old\\	DEM|AGE:74	74 years old:DEM|AGE:74	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\73 years old\\	DEM|AGE:73	73 years old:DEM|AGE:73	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\72 years old\\	DEM|AGE:72	72 years old:DEM|AGE:72	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\71 years old\\	DEM|AGE:71	71 years old:DEM|AGE:71	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\70 years old\\	DEM|AGE:70	70 years old:DEM|AGE:70	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\69 years old\\	DEM|AGE:69	69 years old:DEM|AGE:69	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\68 years old\\	DEM|AGE:68	68 years old:DEM|AGE:68	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\67 years old\\	DEM|AGE:67	67 years old:DEM|AGE:67	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\66 years old\\	DEM|AGE:66	66 years old:DEM|AGE:66	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\65-74 years old\\65 years old\\	DEM|AGE:65	65 years old:DEM|AGE:65	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\	DEM|AGE:55_64	55-64 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\64 years old\\	DEM|AGE:64	64 years old:DEM|AGE:64	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\63 years old\\	DEM|AGE:63	63 years old:DEM|AGE:63	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\62 years old\\	DEM|AGE:62	62 years old:DEM|AGE:62	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\61 years old\\	DEM|AGE:61	61 years old:DEM|AGE:61	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\60 years old\\	DEM|AGE:60	60 years old:DEM|AGE:60	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\59 years old\\	DEM|AGE:59	59 years old:DEM|AGE:59	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\58 years old\\	DEM|AGE:58	58 years old:DEM|AGE:58	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\57 years old\\	DEM|AGE:57	57 years old:DEM|AGE:57	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\56 years old\\	DEM|AGE:56	56 years old:DEM|AGE:56	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\55-64 years old\\55 years old\\	DEM|AGE:55	55 years old:DEM|AGE:55	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\	DEM|AGE:45_54	45-54 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\54 years old\\	DEM|AGE:54	54 years old:DEM|AGE:54	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\53 years old\\	DEM|AGE:53	53 years old:DEM|AGE:53	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\52 years old\\	DEM|AGE:52	52 years old:DEM|AGE:52	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\51 years old\\	DEM|AGE:51	51 years old:DEM|AGE:51	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\50 years old\\	DEM|AGE:50	50 years old:DEM|AGE:50	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\49 years old\\	DEM|AGE:49	49 years old:DEM|AGE:49	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\48 years old\\	DEM|AGE:48	48 years old:DEM|AGE:48	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\47 years old\\	DEM|AGE:47	47 years old:DEM|AGE:47	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\46 years old\\	DEM|AGE:46	46 years old:DEM|AGE:46	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\45-54 years old\\45 years old\\	DEM|AGE:45	45 years old:DEM|AGE:45	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\	DEM|AGE:35_44	35-44 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\44 years old\\	DEM|AGE:44	44 years old:DEM|AGE:44	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\43 years old\\	DEM|AGE:43	43 years old:DEM|AGE:43	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\42 years old\\	DEM|AGE:42	42 years old:DEM|AGE:42	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\41 years old\\	DEM|AGE:41	41 years old:DEM|AGE:41	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\40 years old\\	DEM|AGE:40	40 years old:DEM|AGE:40	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\39 years old\\	DEM|AGE:39	39 years old:DEM|AGE:39	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\38 years old\\	DEM|AGE:38	38 years old:DEM|AGE:38	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\37 years old\\	DEM|AGE:37	37 years old:DEM|AGE:37	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\36 years old\\	DEM|AGE:36	36 years old:DEM|AGE:36	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\35-44 years old\\35 years old\\	DEM|AGE:35	35 years old:DEM|AGE:35	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\	DEM|AGE:18_34	18-34 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\34 years old\\	DEM|AGE:34	34 years old:DEM|AGE:34	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\33 years old\\	DEM|AGE:33	33 years old:DEM|AGE:33	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\32 years old\\	DEM|AGE:32	32 years old:DEM|AGE:32	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\31 years old\\	DEM|AGE:31	31 years old:DEM|AGE:31	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\30 years old\\	DEM|AGE:30	30 years old:DEM|AGE:30	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\29 years old\\	DEM|AGE:29	29 years old:DEM|AGE:29	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\28 years old\\	DEM|AGE:28	28 years old:DEM|AGE:28	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\27 years old\\	DEM|AGE:27	27 years old:DEM|AGE:27	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\26 years old\\	DEM|AGE:26	26 years old:DEM|AGE:26	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\25 years old\\	DEM|AGE:25	25 years old:DEM|AGE:25	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\24 years old\\	DEM|AGE:24	24 years old:DEM|AGE:24	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\23 years old\\	DEM|AGE:23	23 years old:DEM|AGE:23	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\22 years old\\	DEM|AGE:22	22 years old:DEM|AGE:22	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\21 years old\\	DEM|AGE:21	21 years old:DEM|AGE:21	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\20 years old\\	DEM|AGE:20	20 years old:DEM|AGE:20	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\19 years old\\	DEM|AGE:19	19 years old:DEM|AGE:19	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\18-34 years old\\18 years old\\	DEM|AGE:18	18 years old:DEM|AGE:18	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\	DEM|AGE:10_17	10-17 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\17 years old\\	DEM|AGE:17	17 years old:DEM|AGE:17	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\16 years old\\	DEM|AGE:16	16 years old:DEM|AGE:16	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\15 years old\\	DEM|AGE:15	15 years old:DEM|AGE:15	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\14 years old\\	DEM|AGE:14	14 years old:DEM|AGE:14	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\13 years old\\	DEM|AGE:13	13 years old:DEM|AGE:13	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\12 years old\\	DEM|AGE:12	12 years old:DEM|AGE:12	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\11 years old\\	DEM|AGE:11	11 years old:DEM|AGE:11	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\10-17 years old\\10 years old\\	DEM|AGE:10	10 years old:DEM|AGE:10	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\	DEM|AGE:0_9	0-9 years old	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\9 years old\\	DEM|AGE:9	9 years old:DEM|AGE:9	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\8 years old\\	DEM|AGE:8	8 years old:DEM|AGE:8	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\7 years old\\	DEM|AGE:7	7 years old:DEM|AGE:7	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\6 years old\\	DEM|AGE:6	6 years old:DEM|AGE:6	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\5 years old\\	DEM|AGE:5	5 years old:DEM|AGE:5	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\4 years old\\	DEM|AGE:4	4 years old:DEM|AGE:4	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\3 years old\\	DEM|AGE:3	3 years old:DEM|AGE:3	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\2 years old\\	DEM|AGE:2	2 years old:DEM|AGE:2	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\1 years old\\	DEM|AGE:1	1 years old:DEM|AGE:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Age\\0-9 years old\\0 years old\\	DEM|AGE:0	0 years old:DEM|AGE:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Zip code\\	ZIP CODE:0	Zip code:ZIP CODE:0	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Gender\\	GENDER:0	Gender	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Gender\\Not Recorded\\	DEM|SEX:@	Not Recorded:DEM|SEX:@	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Gender\\Unknown\\	DEM|SEX:u	Unknown:DEM|SEX:u	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Gender\\NonBinary\\	DEM|SEX:nb	NonBinary:DEM|SEX:nb	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Gender\\Female\\	DEM|SEX:f	Female:DEM|SEX:f	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\Demografische Daten\\Gender\\Male\\	DEM|SEX:m	Male:DEM|SEX:m	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\.


--
-- Data for Name: datamart_report; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.datamart_report (total_patient, total_observationfact, total_event, report_date) FROM stdin;
\.


--
-- Data for Name: encounter_mapping; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.encounter_mapping (encounter_ide, encounter_ide_source, project_id, encounter_num, patient_ide, patient_ide_source, encounter_ide_status, upload_date, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
1@2009-01-15	UNSPECIFIED	default	10	1	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
20393	UNSPECIFIED	default	9	2	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
26284	UNSPECIFIED	default	8	2	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
29340	UNSPECIFIED	default	7	1	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
29595	UNSPECIFIED	default	6	1	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
2@2009-03-10	UNSPECIFIED	default	5	2	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
31176	UNSPECIFIED	default	4	3	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
3@2009-03-03	UNSPECIFIED	default	3	3	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
43014	UNSPECIFIED	default	2	3	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
46548	UNSPECIFIED	default	1	2	UNSPECIFIED	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
\.


--
-- Data for Name: modifier_dimension; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.modifier_dimension (modifier_path, modifier_cd, name_char, modifier_blob, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
\\ICD-10\\ICD_KODE\\DIAGNOSEART\\	ICD-10_ICD:0	DIAGNOSEART	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\DIAGNOSEART\\ND\\	ICD-10_ICD:1	ND:ICD-10_ICD:1	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\\ICD-10\\ICD_KODE\\DIAGNOSEART\\HD\\	ICD-10_ICD:2	HD:ICD-10_ICD:2	\N	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	2020-03-14 14:54:49.288	P21	\N
\.


--
-- Data for Name: observation_fact; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.observation_fact (encounter_num, patient_num, concept_cd, provider_id, start_date, modifier_cd, instance_num, valtype_cd, tval_char, nval_num, valueflag_cd, quantity_num, units_cd, end_date, location_cd, observation_blob, confidence_num, update_date, download_date, import_date, sourcesystem_cd, upload_id, text_search_index) FROM stdin;
10	19	DEM|AGE:65	Provider for P21	2009-01-15 08:00:00	@	1	T	65	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	1
5	18	DEM|AGE:81	Provider for P21	2009-03-10 08:00:00	@	1	T	81	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	2
3	17	DEM|AGE:80	Provider for P21	2009-03-03 08:00:00	@	1	T	80	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	3
10	19	DEM|SEX:m	Provider for P21	2009-01-15 08:00:00	@	1	T	DEM|SEX:m	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	4
5	18	DEM|SEX:f	Provider for P21	2009-03-10 08:00:00	@	1	T	DEM|SEX:f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	5
3	17	DEM|SEX:f	Provider for P21	2009-03-03 08:00:00	@	1	T	DEM|SEX:f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	6
10	19	DEM|RACE:@	Provider for P21	2009-01-15 08:00:00	@	1	T	DEM|RACE:@	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	7
5	18	DEM|RACE:@	Provider for P21	2009-03-10 08:00:00	@	1	T	DEM|RACE:@	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	8
3	17	DEM|RACE:@	Provider for P21	2009-03-03 08:00:00	@	1	T	DEM|RACE:@	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	9
10	19	DEM|VITAL:@	Provider for P21	2009-01-15 08:00:00	@	1	T	DEM|VITAL:@	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	10
5	18	DEM|VITAL:@	Provider for P21	2009-03-10 08:00:00	@	1	T	DEM|VITAL:@	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	11
3	17	DEM|VITAL:@	Provider for P21	2009-03-03 08:00:00	@	1	T	DEM|VITAL:@	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	12
6	19	ABSCHLAGTA:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	13
7	19	ABSCHLAGTA:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	14
1	18	ABSCHLAGTA:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	15
8	18	ABSCHLAGTA:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	16
9	18	ABSCHLAGTA:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	17
4	17	ABSCHLAGTA:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	18
2	17	ABSCHLAGTA:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	19
6	19	ABSCHLAGVE:1	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	20
7	19	ABSCHLAGVE:1	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	21
1	18	ABSCHLAGVE:1	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	22
8	18	ABSCHLAGVE:1	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	23
9	18	ABSCHLAGVE:1	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	24
4	17	ABSCHLAGVE:1	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	25
2	17	ABSCHLAGVE:1	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	26
6	19	ABSCHLAGVE:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	27
7	19	ABSCHLAGVE:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	28
1	18	ABSCHLAGVE:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	29
8	18	ABSCHLAGVE:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	30
9	18	ABSCHLAGVE:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	31
4	17	ABSCHLAGVE:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	32
2	17	ABSCHLAGVE:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	33
6	19	ABTTYP:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	34
7	19	ABTTYP:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	35
1	18	ABTTYP:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	36
8	18	ABTTYP:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	37
9	18	ABTTYP:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	38
4	17	ABTTYP:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	39
2	17	ABTTYP:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	40
6	19	BASISRATE:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	2800.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	41
7	19	BASISRATE:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	2800.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	42
1	18	BASISRATE:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	2800.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	43
8	18	BASISRATE:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	2800.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	44
9	18	BASISRATE:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	2800.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	45
4	17	BASISRATE:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	2800.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	46
2	17	BASISRATE:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	2800.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	47
6	19	DRGCODE:1	Provider for P21	2009-01-15 08:00:00	@	1	T	J11C	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	48
7	19	DRGCODE:2	Provider for P21	2009-11-21 08:00:00	@	1	T	B80Z	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	49
1	18	DRGCODE:3	Provider for P21	2009-03-10 08:00:00	@	1	T	F62C	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	50
8	18	DRGCODE:4	Provider for P21	2009-03-25 08:00:00	@	1	T	C02B	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	51
9	18	DRGCODE:5	Provider for P21	2009-07-09 08:00:00	@	1	T	F59B	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	52
4	17	DRGCODE:6	Provider for P21	2009-07-29 08:00:00	@	1	T	C08B	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	53
2	17	DRGCODE:7	Provider for P21	2009-03-03 08:00:00	@	1	T	D24B	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	54
6	19	DRGVERSION:1	Provider for P21	2009-01-15 08:00:00	@	1	T	G-DRG V2009	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	55
7	19	DRGVERSION:1	Provider for P21	2009-11-21 08:00:00	@	1	T	G-DRG V2009	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	56
1	18	DRGVERSION:1	Provider for P21	2009-03-10 08:00:00	@	1	T	G-DRG V2009	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	57
8	18	DRGVERSION:1	Provider for P21	2009-03-25 08:00:00	@	1	T	G-DRG V2009	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	58
9	18	DRGVERSION:1	Provider for P21	2009-07-09 08:00:00	@	1	T	G-DRG V2009	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	59
4	17	DRGVERSION:1	Provider for P21	2009-07-29 08:00:00	@	1	T	G-DRG V2009	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	60
2	17	DRGVERSION:1	Provider for P21	2009-03-03 08:00:00	@	1	T	G-DRG V2009	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	61
6	19	EFFGEW:1	Provider for P21	2009-01-15 08:00:00	@	1	T	0,624	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	62
7	19	EFFGEW:2	Provider for P21	2009-11-21 08:00:00	@	1	T	0,295	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	63
1	18	EFFGEW:3	Provider for P21	2009-03-10 08:00:00	@	1	T	0,843	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	64
8	18	EFFGEW:4	Provider for P21	2009-03-25 08:00:00	@	1	T	1,031	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	65
9	18	EFFGEW:5	Provider for P21	2009-07-09 08:00:00	@	1	T	0,564	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	66
4	17	EFFGEW:6	Provider for P21	2009-07-29 08:00:00	@	1	T	0,478	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	67
2	17	EFFGEW:7	Provider for P21	2009-03-03 08:00:00	@	1	T	2,723	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	68
6	19	ENTGSTATUS:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	69
7	19	ENTGSTATUS:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	70
1	18	ENTGSTATUS:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	71
8	18	ENTGSTATUS:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	72
9	18	ENTGSTATUS:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	73
4	17	ENTGSTATUS:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	74
2	17	ENTGSTATUS:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	75
6	19	FLAGALTER:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	76
7	19	FLAGALTER:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	77
1	18	FLAGALTER:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	78
8	18	FLAGALTER:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	79
9	18	FLAGALTER:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	80
4	17	FLAGALTER:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	81
2	17	FLAGALTER:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	82
6	19	FLAGANZBEA:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	83
7	19	FLAGANZBEA:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	84
1	18	FLAGANZBEA:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	85
8	18	FLAGANZBEA:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	86
9	18	FLAGANZBEA:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	87
4	17	FLAGANZBEA:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	88
2	17	FLAGANZBEA:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	89
6	19	FLAGAUFNGE:1	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	90
7	19	FLAGAUFNGE:1	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	91
1	18	FLAGAUFNGE:1	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	92
8	18	FLAGAUFNGE:1	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	93
9	18	FLAGAUFNGE:1	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	94
4	17	FLAGAUFNGE:1	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	95
2	17	FLAGAUFNGE:1	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	96
6	19	FLAGAUFNGE:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	97
7	19	FLAGAUFNGE:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	98
1	18	FLAGAUFNGE:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	99
8	18	FLAGAUFNGE:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	100
9	18	FLAGAUFNGE:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	101
4	17	FLAGAUFNGE:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	102
2	17	FLAGAUFNGE:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	103
6	19	FLAGEINWEI:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	104
7	19	FLAGEINWEI:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	105
1	18	FLAGEINWEI:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	106
8	18	FLAGEINWEI:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	107
9	18	FLAGEINWEI:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	108
4	17	FLAGEINWEI:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	109
2	17	FLAGEINWEI:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	110
6	19	FLAGENTLGR:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	111
7	19	FLAGENTLGR:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	112
1	18	FLAGENTLGR:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	113
8	18	FLAGENTLGR:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	114
9	18	FLAGENTLGR:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	115
4	17	FLAGENTLGR:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	116
2	17	FLAGENTLGR:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	117
6	19	FLAGGESCHL:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	118
7	19	FLAGGESCHL:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	119
1	18	FLAGGESCHL:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	120
8	18	FLAGGESCHL:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	121
9	18	FLAGGESCHL:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	122
4	17	FLAGGESCHL:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	123
2	17	FLAGGESCHL:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	124
6	19	FLAGTAGESF:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	125
7	19	FLAGTAGESF:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	126
1	18	FLAGTAGESF:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	127
8	18	FLAGTAGESF:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	128
9	18	FLAGTAGESF:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	129
4	17	FLAGTAGESF:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	130
2	17	FLAGTAGESF:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	131
6	19	FLAGVWD:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	132
7	19	FLAGVWD:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	133
1	18	FLAGVWD:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	134
8	18	FLAGVWD:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	135
9	18	FLAGVWD:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	136
4	17	FLAGVWD:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	137
2	17	FLAGVWD:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	138
6	19	GST:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	139
7	19	GST:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	140
1	18	GST:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	141
8	18	GST:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	142
9	18	GST:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	143
4	17	GST:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	144
2	17	GST:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	145
6	19	MDC:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	9.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	146
7	19	MDC:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	147
1	18	MDC:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	5.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	148
8	18	MDC:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	2.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	149
9	18	MDC:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	5.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	150
4	17	MDC:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	2.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	151
2	17	MDC:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	3.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	152
6	19	PATIENTSTA:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	153
7	19	PATIENTSTA:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	154
1	18	PATIENTSTA:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	155
8	18	PATIENTSTA:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	156
9	18	PATIENTSTA:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	2.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	157
4	17	PATIENTSTA:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	158
2	17	PATIENTSTA:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	159
6	19	PCCL:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	160
7	19	PCCL:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	161
1	18	PCCL:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	3.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	162
8	18	PCCL:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	163
9	18	PCCL:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	3.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	164
4	17	PCCL:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	2.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	165
2	17	PCCL:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	3.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	166
6	19	RELGEW:1	Provider for P21	2009-01-15 08:00:00	@	1	T	0,624	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	167
7	19	RELGEW:2	Provider for P21	2009-11-21 08:00:00	@	1	T	0,295	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	168
1	18	RELGEW:3	Provider for P21	2009-03-10 08:00:00	@	1	T	0,843	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	169
8	18	RELGEW:4	Provider for P21	2009-03-25 08:00:00	@	1	T	1,031	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	170
9	18	RELGEW:5	Provider for P21	2009-07-09 08:00:00	@	1	T	0,9	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	171
4	17	RELGEW:6	Provider for P21	2009-07-29 08:00:00	@	1	T	0,478	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	172
2	17	RELGEW:7	Provider for P21	2009-03-03 08:00:00	@	1	T	2,723	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	173
6	19	VWDVERWEND:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	2.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	174
7	19	VWDVERWEND:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	4.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	175
1	18	VWDVERWEND:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	4.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	176
8	18	VWDVERWEND:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	3.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	177
9	18	VWDVERWEND:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	1.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	178
4	17	VWDVERWEND:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	2.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	179
2	17	VWDVERWEND:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	6.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	180
6	19	ZUSCHLAGGE:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	181
7	19	ZUSCHLAGGE:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	182
1	18	ZUSCHLAGGE:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	183
8	18	ZUSCHLAGGE:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	184
9	18	ZUSCHLAGGE:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	185
4	17	ZUSCHLAGGE:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	186
2	17	ZUSCHLAGGE:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	187
6	19	ZUSCHLAGKO:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	188
7	19	ZUSCHLAGKO:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	189
1	18	ZUSCHLAGKO:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	190
8	18	ZUSCHLAGKO:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	191
9	18	ZUSCHLAGKO:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	192
4	17	ZUSCHLAGKO:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	193
2	17	ZUSCHLAGKO:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	194
6	19	ZUSCHLAGTA:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	195
7	19	ZUSCHLAGTA:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	196
1	18	ZUSCHLAGTA:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	197
8	18	ZUSCHLAGTA:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	198
9	18	ZUSCHLAGTA:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	199
4	17	ZUSCHLAGTA:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	200
2	17	ZUSCHLAGTA:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	0.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	201
6	19	AUFNAHMEAN:1	Provider for P21	2009-01-15 08:00:00	@	1	T	Einweisung durch einen Arzt	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	202
7	19	AUFNAHMEAN:2	Provider for P21	2009-11-21 08:00:00	@	1	T	Notfall	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	203
1	18	AUFNAHMEAN:2	Provider for P21	2009-03-10 08:00:00	@	1	T	Notfall	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	204
8	18	AUFNAHMEAN:1	Provider for P21	2009-03-25 08:00:00	@	1	T	Einweisung durch einen Arzt	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	205
9	18	AUFNAHMEAN:1	Provider for P21	2009-07-09 08:00:00	@	1	T	Einweisung durch einen Arzt	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	206
4	17	AUFNAHMEAN:1	Provider for P21	2009-07-29 08:00:00	@	1	T	Einweisung durch einen Arzt	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	207
2	17	AUFNAHMEAN:1	Provider for P21	2009-03-03 08:00:00	@	1	T	Einweisung durch einen Arzt	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	208
6	19	AUFNAHMEDA:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	200901150800.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	209
7	19	AUFNAHMEDA:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	200911210800.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	210
1	18	AUFNAHMEDA:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	200903100800.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	211
8	18	AUFNAHMEDA:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	200903250800.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	212
9	18	AUFNAHMEDA:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	200907090800.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	213
4	17	AUFNAHMEDA:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	200907290800.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	214
2	17	AUFNAHMEDA:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	200903030800.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	215
6	19	AUFNAHMEGR:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	101.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	216
7	19	AUFNAHMEGR:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	107.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	217
1	18	AUFNAHMEGR:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	107.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	218
8	18	AUFNAHMEGR:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	101.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	219
9	18	AUFNAHMEGR:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	101.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	220
4	17	AUFNAHMEGR:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	101.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	221
2	17	AUFNAHMEGR:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	101.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	222
6	19	ENTLASSUNG:1	Provider for P21	2009-01-15 08:00:00	@	1	N	E	200901171600.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	223
7	19	ENTLASSUNG:1	Provider for P21	2009-11-21 08:00:00	@	1	N	E	200911251600.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	224
1	18	ENTLASSUNG:1	Provider for P21	2009-03-10 08:00:00	@	1	N	E	200903141600.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	225
8	18	ENTLASSUNG:1	Provider for P21	2009-03-25 08:00:00	@	1	N	E	200903281600.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	226
9	18	ENTLASSUNG:1	Provider for P21	2009-07-09 08:00:00	@	1	N	E	200907101600.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	227
4	17	ENTLASSUNG:1	Provider for P21	2009-07-29 08:00:00	@	1	N	E	200907311600.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	228
2	17	ENTLASSUNG:1	Provider for P21	2009-03-03 08:00:00	@	1	N	E	200903091600.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	229
6	19	ENTLASSUNG:0	Provider for P21	2009-01-15 08:00:00	@	1	N	E	12.00000	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	230
7	19	ENTLASSUNG:0	Provider for P21	2009-11-21 08:00:00	@	1	N	E	19.00000	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	231
1	18	ENTLASSUNG:0	Provider for P21	2009-03-10 08:00:00	@	1	N	E	19.00000	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	232
8	18	ENTLASSUNG:0	Provider for P21	2009-03-25 08:00:00	@	1	N	E	19.00000	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	233
9	18	ENTLASSUNG:0	Provider for P21	2009-07-09 08:00:00	@	1	N	E	19.00000	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	234
4	17	ENTLASSUNG:0	Provider for P21	2009-07-29 08:00:00	@	1	N	E	19.00000	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	235
2	17	ENTLASSUNG:0	Provider for P21	2009-03-03 08:00:00	@	1	N	E	19.00000	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	236
6	19	ICD_KODE:1	Provider for P21	2009-01-15 08:00:00	@	1	T	E66.92	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	237
6	19	ICD_KODE:1	Provider for P21	2009-01-15 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	238
6	19	ICD_KODE:2	Provider for P21	2009-01-15 08:00:00	@	1	T	M54.16	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	239
6	19	ICD_KODE:2	Provider for P21	2009-01-15 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	240
6	19	ICD_KODE:3	Provider for P21	2009-01-15 08:00:00	@	1	T	I10.90	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	241
6	19	ICD_KODE:3	Provider for P21	2009-01-15 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-01-17 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	242
7	19	ICD_KODE:4	Provider for P21	2009-11-21 08:00:00	@	1	T	S00.85	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	243
7	19	ICD_KODE:4	Provider for P21	2009-11-21 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	244
7	19	ICD_KODE:5	Provider for P21	2009-11-21 08:00:00	@	1	T	S13.4	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	245
7	19	ICD_KODE:5	Provider for P21	2009-11-21 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	246
7	19	ICD_KODE:6	Provider for P21	2009-11-21 08:00:00	@	1	T	S06.0	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	247
7	19	ICD_KODE:6	Provider for P21	2009-11-21 08:00:00	ICD-10_ICD:2	1	T	HD	\N	\N	\N	\N	2009-11-25 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	248
1	18	ICD_KODE:7	Provider for P21	2009-03-10 08:00:00	@	1	T	I50.13	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	249
1	18	ICD_KODE:7	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:2	1	T	HD	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	250
1	18	ICD_KODE:8	Provider for P21	2009-03-10 08:00:00	@	1	T	E78.5	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	251
1	18	ICD_KODE:8	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	252
1	18	ICD_KODE:9	Provider for P21	2009-03-10 08:00:00	@	1	T	I71.2	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	253
1	18	ICD_KODE:9	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	254
1	18	ICD_KODE:10	Provider for P21	2009-03-10 08:00:00	@	1	T	I27.28	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	255
1	18	ICD_KODE:10	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	256
1	18	ICD_KODE:11	Provider for P21	2009-03-10 08:00:00	@	1	T	Z03.4	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	257
1	18	ICD_KODE:11	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	258
1	18	ICD_KODE:12	Provider for P21	2009-03-10 08:00:00	@	1	T	F17.2	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	259
1	18	ICD_KODE:12	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	260
1	18	ICD_KODE:13	Provider for P21	2009-03-10 08:00:00	@	1	T	E11.20+	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	261
1	18	ICD_KODE:13	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	262
1	18	ICD_KODE:14	Provider for P21	2009-03-10 08:00:00	@	1	T	T88.8	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	263
1	18	ICD_KODE:14	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	264
1	18	ICD_KODE:15	Provider for P21	2009-03-10 08:00:00	@	1	T	I31.3	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	265
1	18	ICD_KODE:15	Provider for P21	2009-03-10 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-14 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	266
8	18	ICD_KODE:16	Provider for P21	2009-03-25 08:00:00	@	1	T	H02.4	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	267
8	18	ICD_KODE:16	Provider for P21	2009-03-25 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	268
8	18	ICD_KODE:17	Provider for P21	2009-03-25 08:00:00	@	1	T	E79.0	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	269
8	18	ICD_KODE:17	Provider for P21	2009-03-25 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	270
8	18	ICD_KODE:18	Provider for P21	2009-03-25 08:00:00	@	1	T	H02.3	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	271
8	18	ICD_KODE:18	Provider for P21	2009-03-25 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	272
8	18	ICD_KODE:19	Provider for P21	2009-03-25 08:00:00	@	1	T	Z96.1	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	273
8	18	ICD_KODE:19	Provider for P21	2009-03-25 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-28 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	274
9	18	ICD_KODE:20	Provider for P21	2009-07-09 08:00:00	@	1	T	N18.0	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	275
9	18	ICD_KODE:20	Provider for P21	2009-07-09 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	276
9	18	ICD_KODE:21	Provider for P21	2009-07-09 08:00:00	@	1	T	I10.00	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	277
9	18	ICD_KODE:21	Provider for P21	2009-07-09 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	278
9	18	ICD_KODE:22	Provider for P21	2009-07-09 08:00:00	@	1	T	T82.5	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	279
9	18	ICD_KODE:22	Provider for P21	2009-07-09 08:00:00	ICD-10_ICD:2	1	T	HD	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	280
9	18	ICD_KODE:23	Provider for P21	2009-07-09 08:00:00	@	1	T	N25.8	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	281
9	18	ICD_KODE:23	Provider for P21	2009-07-09 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	282
9	18	ICD_KODE:24	Provider for P21	2009-07-09 08:00:00	@	1	T	E78.2	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	283
9	18	ICD_KODE:24	Provider for P21	2009-07-09 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-10 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	284
4	17	ICD_KODE:25	Provider for P21	2009-07-29 08:00:00	@	1	T	H25.1	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	285
4	17	ICD_KODE:25	Provider for P21	2009-07-29 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	286
4	17	ICD_KODE:3	Provider for P21	2009-07-29 08:00:00	@	1	T	I10.90	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	287
4	17	ICD_KODE:3	Provider for P21	2009-07-29 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	288
4	17	ICD_KODE:26	Provider for P21	2009-07-29 08:00:00	@	1	T	Z95.1	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	289
4	17	ICD_KODE:26	Provider for P21	2009-07-29 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	290
4	17	ICD_KODE:27	Provider for P21	2009-07-29 08:00:00	@	1	T	S05.8	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	291
4	17	ICD_KODE:27	Provider for P21	2009-07-29 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	292
4	17	ICD_KODE:28	Provider for P21	2009-07-29 08:00:00	@	1	T	H52.1	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	293
4	17	ICD_KODE:28	Provider for P21	2009-07-29 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-07-31 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	294
2	17	ICD_KODE:29	Provider for P21	2009-03-03 08:00:00	@	1	T	I97.8	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	295
2	17	ICD_KODE:29	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	296
2	17	ICD_KODE:30	Provider for P21	2009-03-03 08:00:00	@	1	T	J34.3	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	297
2	17	ICD_KODE:30	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	298
2	17	ICD_KODE:17	Provider for P21	2009-03-03 08:00:00	@	1	T	E79.0	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	299
2	17	ICD_KODE:17	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	300
2	17	ICD_KODE:31	Provider for P21	2009-03-03 08:00:00	@	1	T	I44.3	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	301
2	17	ICD_KODE:31	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	302
2	17	ICD_KODE:32	Provider for P21	2009-03-03 08:00:00	@	1	T	J34.2	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	303
2	17	ICD_KODE:32	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	304
2	17	ICD_KODE:33	Provider for P21	2009-03-03 08:00:00	@	1	T	R59.0	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	305
2	17	ICD_KODE:33	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	306
2	17	ICD_KODE:34	Provider for P21	2009-03-03 08:00:00	@	1	T	F40.00	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	307
2	17	ICD_KODE:34	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	308
2	17	ICD_KODE:35	Provider for P21	2009-03-03 08:00:00	@	1	T	F17.1	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	309
2	17	ICD_KODE:35	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	310
2	17	ICD_KODE:36	Provider for P21	2009-03-03 08:00:00	@	1	T	Z86.4	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	311
2	17	ICD_KODE:36	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	312
2	17	ICD_KODE:37	Provider for P21	2009-03-03 08:00:00	@	1	T	I48.10	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	313
2	17	ICD_KODE:37	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	314
2	17	ICD_KODE:38	Provider for P21	2009-03-03 08:00:00	@	1	T	C32.9	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	315
2	17	ICD_KODE:38	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:2	1	T	HD	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	316
2	17	ICD_KODE:39	Provider for P21	2009-03-03 08:00:00	@	1	T	F10.1	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	317
2	17	ICD_KODE:39	Provider for P21	2009-03-03 08:00:00	ICD-10_ICD:1	1	T	ND	\N	\N	\N	\N	2009-03-09 16:00:00	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1	318
\.


--
-- Name: observation_fact_text_search_index_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.observation_fact_text_search_index_seq', 318, true);


--
-- Data for Name: patient_dimension; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.patient_dimension (patient_num, vital_status_cd, birth_date, death_date, sex_cd, age_in_years_num, language_cd, race_cd, marital_status_cd, religion_cd, zip_cd, statecityzip_path, income_cd, patient_blob, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
19	\N	1944-01-01 00:00:00	\N	DEM|SEX:m	65	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
18	\N	1928-01-01 00:00:00	\N	DEM|SEX:f	81	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
17	\N	1929-01-01 00:00:00	\N	DEM|SEX:f	80	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
\.


--
-- Data for Name: patient_mapping; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.patient_mapping (patient_ide, patient_ide_source, patient_num, patient_ide_status, project_id, upload_date, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
1	UNSPECIFIED	19	\N	default	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
2	UNSPECIFIED	18	\N	default	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
3	UNSPECIFIED	17	\N	default	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
\.


--
-- Data for Name: provider_dimension; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.provider_dimension (provider_id, provider_path, name_char, provider_blob, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
Provider for P21	/	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
\.


--
-- Data for Name: qt_analysis_plugin; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_analysis_plugin (plugin_id, plugin_name, description, version_cd, parameter_info, parameter_info_xsd, command_line, working_folder, commandoption_cd, plugin_icon, status_cd, user_id, group_id, create_date, update_date) FROM stdin;
\.


--
-- Data for Name: qt_analysis_plugin_result_type; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_analysis_plugin_result_type (plugin_id, result_type_id) FROM stdin;
\.


--
-- Data for Name: qt_breakdown_path; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_breakdown_path (name, value, create_date, update_date, user_id) FROM stdin;
PATIENT_RACE_COUNT_XML	\\\\i2b2\\Demografische Daten\\Race\\	\N	\N	\N
PATIENT_VITALSTATUS_COUNT_XML	\\\\i2b2\\Demografische Daten\\Vital status\\	\N	\N	\N
PATIENT_AGE_COUNT_XML	\\\\i2b2\\Demografische Daten\\Age\\	\N	\N	\N
PATIENT_GENDER_COUNT_XML	\\\\i2b2\\Demografische Daten\\Gender\\	\N	\N	\N
\.


--
-- Data for Name: qt_patient_enc_collection; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_patient_enc_collection (patient_enc_coll_id, result_instance_id, set_index, patient_num, encounter_num) FROM stdin;
\.


--
-- Name: qt_patient_enc_collection_patient_enc_coll_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.qt_patient_enc_collection_patient_enc_coll_id_seq', 1, false);


--
-- Data for Name: qt_patient_set_collection; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_patient_set_collection (patient_set_coll_id, result_instance_id, set_index, patient_num) FROM stdin;
\.


--
-- Name: qt_patient_set_collection_patient_set_coll_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.qt_patient_set_collection_patient_set_coll_id_seq', 1, false);


--
-- Data for Name: qt_pdo_query_master; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_pdo_query_master (query_master_id, user_id, group_id, create_date, request_xml, i2b2_request_xml) FROM stdin;
\.


--
-- Name: qt_pdo_query_master_query_master_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.qt_pdo_query_master_query_master_id_seq', 1, false);


--
-- Data for Name: qt_privilege; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_privilege (protection_label_cd, dataprot_cd, hivemgmt_cd, plugin_id) FROM stdin;
PDO_WITHOUT_BLOB	DATA_LDS	USER	\N
PDO_WITH_BLOB	DATA_DEID	USER	\N
SETFINDER_QRY_WITH_DATAOBFSC	DATA_OBFSC	USER	\N
SETFINDER_QRY_WITHOUT_DATAOBFSC	DATA_AGG	USER	\N
UPLOAD	DATA_OBFSC	MANAGER	\N
SETFINDER_QRY_WITH_LGTEXT	DATA_DEID	USER	\N
\.


--
-- Data for Name: qt_query_instance; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_query_instance (query_instance_id, query_master_id, user_id, group_id, batch_mode, start_date, end_date, delete_flag, status_type_id, message) FROM stdin;
\.


--
-- Name: qt_query_instance_query_instance_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.qt_query_instance_query_instance_id_seq', 1, false);


--
-- Data for Name: qt_query_master; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_query_master (query_master_id, name, user_id, group_id, master_type_cd, plugin_id, create_date, delete_date, delete_flag, request_xml, generated_sql, i2b2_request_xml, pm_xml) FROM stdin;
\.


--
-- Name: qt_query_master_query_master_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.qt_query_master_query_master_id_seq', 1, false);


--
-- Data for Name: qt_query_result_instance; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_query_result_instance (result_instance_id, query_instance_id, result_type_id, set_size, start_date, end_date, status_type_id, delete_flag, message, description, real_set_size, obfusc_method) FROM stdin;
\.


--
-- Name: qt_query_result_instance_result_instance_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.qt_query_result_instance_result_instance_id_seq', 1, false);


--
-- Data for Name: qt_query_result_type; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_query_result_type (result_type_id, name, description, display_type_id, visual_attribute_type_id) FROM stdin;
1	PATIENTSET	Patient set	LIST	LA
2	PATIENT_ENCOUNTER_SET	Encounter set	LIST	LA
3	XML	Generic query result	CATNUM	LH
4	PATIENT_COUNT_XML	Number of patients	CATNUM	LA
5	PATIENT_GENDER_COUNT_XML	Gender patient breakdown	CATNUM	LA
6	PATIENT_VITALSTATUS_COUNT_XML	Vital Status patient breakdown	CATNUM	LA
7	PATIENT_RACE_COUNT_XML	Race patient breakdown	CATNUM	LA
8	PATIENT_AGE_COUNT_XML	Age patient breakdown	CATNUM	LA
9	PATIENTSET	Timeline	LIST	LA
\.


--
-- Data for Name: qt_query_status_type; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_query_status_type (status_type_id, name, description) FROM stdin;
1	QUEUED	 WAITING IN QUEUE TO START PROCESS
2	PROCESSING	PROCESSING
3	FINISHED	FINISHED
4	ERROR	ERROR
5	INCOMPLETE	INCOMPLETE
6	COMPLETED	COMPLETED
7	MEDIUM_QUEUE	MEDIUM QUEUE
8	LARGE_QUEUE	LARGE QUEUE
9	CANCELLED	CANCELLED
10	TIMEDOUT	TIMEDOUT
\.


--
-- Data for Name: qt_xml_result; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.qt_xml_result (xml_result_id, result_instance_id, xml_value) FROM stdin;
\.


--
-- Name: qt_xml_result_xml_result_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.qt_xml_result_xml_result_id_seq', 1, false);


--
-- Name: seq_encounter_num; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: postgres
--

SELECT pg_catalog.setval('i2b2demodata.seq_encounter_num', 10, true);


--
-- Name: seq_patient_num; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: postgres
--

SELECT pg_catalog.setval('i2b2demodata.seq_patient_num', 19, true);


--
-- Data for Name: set_type; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.set_type (id, name, create_date) FROM stdin;
1	event_set	2020-03-13 14:06:12.049067
2	patient_set	2020-03-13 14:06:12.051135
3	concept_set	2020-03-13 14:06:12.05295
4	observer_set	2020-03-13 14:06:12.054654
5	observation_set	2020-03-13 14:06:12.057058
6	pid_set	2020-03-13 14:06:12.059106
7	eid_set	2020-03-13 14:06:12.061233
8	modifier_set	2020-03-13 14:06:12.06324
\.


--
-- Data for Name: set_upload_status; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.set_upload_status (upload_id, set_type_id, source_cd, no_of_record, loaded_record, deleted_record, load_date, end_date, load_status, message, input_file_name, log_file_name, transform_name) FROM stdin;
\.


--
-- Data for Name: source_master; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.source_master (source_cd, description, create_date) FROM stdin;
\.


--
-- Data for Name: upload_status; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.upload_status (upload_id, upload_label, user_id, source_cd, no_of_record, loaded_record, deleted_record, load_date, end_date, load_status, message, input_file_name, log_file_name, transform_name) FROM stdin;
\.


--
-- Name: upload_status_upload_id_seq; Type: SEQUENCE SET; Schema: i2b2demodata; Owner: i2b2demodata
--

SELECT pg_catalog.setval('i2b2demodata.upload_status_upload_id_seq', 1, false);


--
-- Data for Name: visit_dimension; Type: TABLE DATA; Schema: i2b2demodata; Owner: i2b2demodata
--

COPY i2b2demodata.visit_dimension (encounter_num, patient_num, active_status_cd, start_date, end_date, inout_cd, location_cd, location_path, length_of_stay, visit_blob, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
10	19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
5	18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
3	17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
1	18	\N	2009-03-10 08:00:00	2009-03-14 16:00:00	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
8	18	\N	2009-03-25 08:00:00	2009-03-28 16:00:00	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
6	19	\N	2009-01-15 08:00:00	2009-01-17 16:00:00	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
4	17	\N	2009-07-29 08:00:00	2009-07-31 16:00:00	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
2	17	\N	2009-03-03 08:00:00	2009-03-09 16:00:00	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
9	18	\N	2009-07-09 08:00:00	2009-07-10 16:00:00	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
7	19	\N	2009-11-21 08:00:00	2009-11-25 16:00:00	\N	\N	\N	\N	\N	\N	\N	2020-03-14 14:54:59.041	P21	1
\.


--
-- Data for Name: crc_analysis_job; Type: TABLE DATA; Schema: i2b2hive; Owner: i2b2hive
--

COPY i2b2hive.crc_analysis_job (job_id, queue_name, status_type_id, domain_id, project_id, user_id, request_xml, create_date, update_date) FROM stdin;
\.


--
-- Data for Name: crc_db_lookup; Type: TABLE DATA; Schema: i2b2hive; Owner: i2b2hive
--

COPY i2b2hive.crc_db_lookup (c_domain_id, c_project_path, c_owner_id, c_db_fullschema, c_db_datasource, c_db_servertype, c_db_nicename, c_db_tooltip, c_comment, c_entry_date, c_change_date, c_status_cd) FROM stdin;
i2b2demo	/Demo/	@	i2b2demodata	java:/QueryToolDemoDS	POSTGRESQL	Demo	\N	\N	\N	\N	\N
\.


--
-- Data for Name: im_db_lookup; Type: TABLE DATA; Schema: i2b2hive; Owner: i2b2hive
--

COPY i2b2hive.im_db_lookup (c_domain_id, c_project_path, c_owner_id, c_db_fullschema, c_db_datasource, c_db_servertype, c_db_nicename, c_db_tooltip, c_comment, c_entry_date, c_change_date, c_status_cd) FROM stdin;
i2b2demo	Demo/	@	i2b2imdata	java:/IMDemoDS	POSTGRESQL	IM	\N	\N	\N	\N	\N
\.


--
-- Data for Name: ont_db_lookup; Type: TABLE DATA; Schema: i2b2hive; Owner: i2b2hive
--

COPY i2b2hive.ont_db_lookup (c_domain_id, c_project_path, c_owner_id, c_db_fullschema, c_db_datasource, c_db_servertype, c_db_nicename, c_db_tooltip, c_comment, c_entry_date, c_change_date, c_status_cd) FROM stdin;
i2b2demo	Demo/	@	i2b2metadata	java:/OntologyDemoDS	POSTGRESQL	Metadata	\N	\N	\N	\N	\N
\.


--
-- Data for Name: work_db_lookup; Type: TABLE DATA; Schema: i2b2hive; Owner: i2b2hive
--

COPY i2b2hive.work_db_lookup (c_domain_id, c_project_path, c_owner_id, c_db_fullschema, c_db_datasource, c_db_servertype, c_db_nicename, c_db_tooltip, c_comment, c_entry_date, c_change_date, c_status_cd) FROM stdin;
i2b2demo	Demo/	@	i2b2workdata	java:/WorkplaceDemoDS	POSTGRESQL	Workplace	\N	\N	\N	\N	\N
\.


--
-- Data for Name: im_audit; Type: TABLE DATA; Schema: i2b2imdata; Owner: i2b2imdata
--

COPY i2b2imdata.im_audit (query_date, lcl_site, lcl_id, user_id, project_id, comments) FROM stdin;
\.


--
-- Data for Name: im_mpi_demographics; Type: TABLE DATA; Schema: i2b2imdata; Owner: i2b2imdata
--

COPY i2b2imdata.im_mpi_demographics (global_id, global_status, demographics, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
100790915	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
100790926	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
100791247	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
101164949	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
101809330	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344360	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344362	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344364	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344367	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344369	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344370	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344373	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344376	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344379	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102344381	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102637795	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102785439	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
102788263	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
103593382	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
103703039	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
103703072	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
103943507	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
103943509	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
104308528	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
104334898	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105541340	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105541343	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105541501	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105560546	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105560548	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105560549	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105802853	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105807520	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105810956	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
105893324	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
106003404	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
990056789	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056790	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056791	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056792	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056793	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056794	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056795	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056796	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056797	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056798	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056799	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056800	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056801	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056802	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056803	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056804	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056805	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056806	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056807	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056808	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056809	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056810	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056811	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056812	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056813	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056814	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056815	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056816	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056817	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056818	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056819	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056820	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056821	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056822	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056823	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056824	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056825	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056826	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056827	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056828	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056829	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056830	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056831	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056832	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056833	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056834	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056835	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056836	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056837	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056838	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056839	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056840	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056841	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056842	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056843	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056844	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056845	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056846	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056847	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056848	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056849	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056850	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056851	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056852	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056853	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056854	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056855	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056856	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056857	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056858	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056859	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056860	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056861	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056862	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056863	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056864	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056865	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056866	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056867	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056868	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056869	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056870	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056871	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056872	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056873	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056874	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056875	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056876	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056877	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056878	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056879	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056880	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056881	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056882	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056883	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056884	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056885	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056886	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056887	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056888	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056889	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056890	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056891	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056892	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056893	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056894	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056895	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056896	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056897	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056898	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056899	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056900	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056901	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056902	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056903	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056904	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056905	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056906	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056907	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056908	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056909	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056910	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056911	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056912	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056913	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056914	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056915	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056916	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056917	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056918	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056919	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056920	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056921	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056922	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056923	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056924	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056925	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056926	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056927	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056928	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056929	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056930	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056931	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056932	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056933	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056934	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056935	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056936	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056937	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056938	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056939	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056940	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056941	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056942	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056943	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056944	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056945	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056946	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056947	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056948	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056949	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056950	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056951	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056952	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056953	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056954	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056955	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056956	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056957	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056958	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056959	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056960	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056961	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056962	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056963	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056964	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056965	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056966	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056967	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056968	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056969	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056970	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056971	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056972	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056973	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056974	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056975	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056976	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056977	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056978	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056979	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056980	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056981	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056982	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056983	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056984	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056985	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056986	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056987	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056988	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056989	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056990	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056991	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056992	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056993	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056994	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056995	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056996	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056997	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056998	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056999	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057000	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057001	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057002	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057003	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057004	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057005	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057006	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057007	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057008	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057009	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057010	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057011	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057012	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057013	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057014	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057015	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057016	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057017	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057018	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057019	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057020	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057021	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057022	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057023	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057024	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057025	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057026	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057027	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057028	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057029	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057030	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057031	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057032	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057033	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057034	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057035	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057036	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057037	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057038	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057039	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057040	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057041	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057042	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057043	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057044	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057045	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057046	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057047	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057048	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057049	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057050	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057051	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057052	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057053	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057054	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057055	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057056	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057057	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057058	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057059	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057060	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057061	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057062	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057063	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057064	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057065	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057066	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057067	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057068	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057069	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057070	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057071	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057072	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057073	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057074	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057075	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057076	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057077	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057078	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057079	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057080	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057081	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057082	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057083	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057084	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057085	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057086	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057087	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057088	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057089	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057090	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057091	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057092	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057093	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057094	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057095	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057096	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057097	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057098	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057099	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057100	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057101	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057102	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057103	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057104	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057105	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057106	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057107	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057108	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057109	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057110	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057111	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057112	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057113	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057114	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057115	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057116	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057117	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057118	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057119	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057120	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057121	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057122	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057123	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057124	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057125	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057126	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057127	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057128	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057129	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057130	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057131	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057132	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057133	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057134	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057135	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057136	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057137	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057138	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057139	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057140	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057141	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057142	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057143	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057144	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057145	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057146	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057147	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057148	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057149	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057150	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057151	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057152	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057153	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057154	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057155	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057156	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057157	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057158	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057159	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057160	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057161	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057162	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057163	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057164	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057165	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057166	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057167	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057168	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057169	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057170	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057171	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057172	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057173	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057174	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057175	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057176	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057177	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057178	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057179	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057180	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057181	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057182	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057183	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057184	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057185	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057186	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057187	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057188	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057189	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057190	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057191	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057192	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057193	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057194	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057195	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057196	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057197	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057198	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057199	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057200	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057201	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057202	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057203	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057204	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057205	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057206	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057207	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057208	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057209	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057210	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057211	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057212	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057213	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057214	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057215	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057216	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057217	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057218	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057219	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057220	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057221	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057222	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057223	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057224	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057225	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057226	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057227	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057228	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057229	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057230	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057231	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057232	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057233	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057234	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057235	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057236	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057237	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057238	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057239	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057240	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057241	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057242	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057243	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057244	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057245	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057246	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057247	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057248	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057249	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057250	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057251	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057252	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057253	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057254	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057255	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057256	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057257	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057258	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057259	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057260	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057261	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057262	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057263	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057264	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057265	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057266	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057267	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057268	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057269	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057270	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057271	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057272	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057273	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057274	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057275	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057276	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057277	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057278	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057279	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057280	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057281	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057282	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057283	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057284	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057285	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057286	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057287	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057288	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057289	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057290	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057291	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057292	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057293	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057294	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057295	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057296	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057297	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057298	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057299	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057300	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057301	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057302	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057303	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057304	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057305	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057306	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057307	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057308	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057309	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057310	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057311	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057312	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057313	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057314	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057315	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057316	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057317	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057318	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057319	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057320	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057321	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057322	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057323	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057324	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057325	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057326	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057327	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057328	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057329	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057330	A	\N	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
\.


--
-- Data for Name: im_mpi_mapping; Type: TABLE DATA; Schema: i2b2imdata; Owner: i2b2imdata
--

COPY i2b2imdata.im_mpi_mapping (global_id, lcl_site, lcl_id, lcl_status, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
100790915	Hospital-1_E	BVFFC8U395LIG5A20IQGF5VPBQ	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790915	Hospital-2_E	2ITNSBR835A6TBT5UH4RQOIV09	A	2008-07-24 11:03:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
100790915	Master_Index	100790915	A	2008-07-24 11:03:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
100790915	Hospital-5	3000001831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790915	Hospital-7	S500003061	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790915	Hospital-8	U500004021	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790915	Hospital-6	4000002011	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790926	Hospital-1	11489986	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790926	Hospital-2	01164897	A	2012-03-10 09:19:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
100790926	Master_Index	100790926	A	2012-03-10 09:19:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
100790926	Hospital-5	3000001832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790926	Hospital-7	S500003062	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790926	Hospital-8	U500004022	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100790926	Hospital-6	4000002012	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100791247	Hospital-1	11490505	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100791247	Hospital-2	01165476	A	2012-04-10 14:41:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
100791247	Master_Index	100791247	A	2012-04-10 14:41:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
100791247	Hospital-5	3000001834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100791247	Hospital-7	S500003064	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
100791247	Hospital-6	4000002014	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101164949	Hospital-1	00000117	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101164949	Hospital-2_E	AJ10DO3JG0L3M3KLPT1SGD79JD	A	2006-04-10 14:31:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
101164949	Master_Index	101164949	A	2006-04-10 14:31:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
101164949	Hospital-5	3000001838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101164949	Hospital-7	S500003068	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101164949	Hospital-8	U500004028	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101164949	Hospital-6	4000002018	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101809330	Hospital-1_E	A2HB8LGT048UV9JOCJ92D5JOOO	A	2011-08-08 14:27:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
101809330	Hospital-2	01800290	A	2011-08-08 14:27:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
101809330	Master_Index	101809330	A	2011-08-08 14:27:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
101809330	Hospital-5	3000001845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101809330	Hospital-7	S500003075	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101809330	Hospital-8	U500004035	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
101809330	Hospital-6	4000002025	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344360	Hospital-1	17028580	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344360	Hospital-2	01954309	A	2010-05-17 09:35:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344360	Hospital-3_E	3VED4MPC6VM7!8GANM9E9RE5VM	A	2010-05-17 09:35:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344360	Master_Index	102344360	A	2010-05-17 09:35:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344360	Hospital-4	00001003	A	2010-05-17 09:35:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344360	Hospital-7	S500003076	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344360	Hospital-8	U500004036	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344360	Hospital-6	4000002026	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344362	Hospital-1	17028598	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344362	Hospital-2_E	643LL7U2ETFUDJ7P1D7BLJCGA!	A	2007-10-01 13:51:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344362	Hospital-3_E	59C60C8U4SHIEB3D416ISKUVJ8	A	2007-10-01 13:51:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344362	Master_Index	102344362	A	2007-10-01 13:51:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344362	Hospital-5	3000001848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344362	Hospital-7	S500003078	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344362	Hospital-8	U500004038	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344362	Hospital-6	4000002028	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344364	Hospital-1	17028630	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344364	Hospital-2	01954385	A	2012-07-25 15:33:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344364	Hospital-3	252307	A	2012-07-25 15:33:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344364	Master_Index	102344364	A	2012-07-25 15:33:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344364	Hospital-5	3000001853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344364	Hospital-8	U500004043	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344364	Hospital-6	4000002033	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344367	Hospital-1	17028655	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344367	Hospital-2	01954387	A	2006-04-11 11:13:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344367	Hospital-3_E	EC03V1RTVE6B!88BHA8LEM0CVV	A	2006-04-11 11:13:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344367	Master_Index	102344367	A	2006-04-11 11:13:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344367	Hospital-5	3000001857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344367	Hospital-7	S500003087	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344367	Hospital-8	U500004047	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344369	Hospital-1	17028473	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344369	Hospital-2	01954124	A	2006-04-11 11:14:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344369	Hospital-3_E	48H0V6FFMLSUNFU1TJT6CT3R76	A	2006-04-11 11:14:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344369	Master_Index	102344369	A	2006-04-11 11:14:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344369	Hospital-5	3000001859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344369	Hospital-7	S500003089	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344369	Hospital-8	U500004049	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344369	Hospital-6	4000002039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344370	Hospital-1_E	C7UNJSOKKAIBG7LHJMVCSS3C5!	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344370	Hospital-2	01954388	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344370	Hospital-3_E	4VTSH27PC1SS2DISDJNH8LNTJV	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344370	Master_Index	102344370	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344370	Hospital-4	00001005	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344370	Hospital-5	3000001863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344370	Hospital-8	U500004053	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344370	Hospital-6	4000002043	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344373	Hospital-1	17028671	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344373	Hospital-2_E	C8JTCV4683RJC5GP4FUPN28R3S	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344373	Hospital-3	252312	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344373	Master_Index	102344373	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344373	Hospital-5	3000001865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344373	Hospital-7	S500003095	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344373	Hospital-8	U500004055	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344373	Hospital-6	4000002045	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344376	Hospital-1	17028705	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344376	Hospital-2_E	B5Q0ULMUEQN2DCB7Q2QTGIKDU2	A	2006-04-11 11:16:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344376	Hospital-3	252313	A	2006-04-11 11:16:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344376	Master_Index	102344376	A	2006-04-11 11:16:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344376	Hospital-5	3000001867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344376	Hospital-7	S500003097	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344376	Hospital-8	U500004057	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344379	Hospital-1	17028481	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344379	Hospital-2	01954285	A	2006-04-11 11:16:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344379	Hospital-3	252308	A	2006-04-11 11:16:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344379	Master_Index	102344379	A	2006-04-11 11:16:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344379	Hospital-5	3000001870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344379	Hospital-7	S500003100	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344379	Hospital-8	U500004060	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344379	Hospital-6	4000002050	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344381	Hospital-1	17028747	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344381	Hospital-2	01954550	A	2008-12-02 16:36:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344381	Hospital-3_E	1C383QGJ5RDHT9FEK7UJT3V0ND	A	2008-12-02 16:36:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344381	Master_Index	102344381	A	2008-12-02 16:36:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344381	Hospital-4	00001006	A	2008-12-02 16:36:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102344381	Hospital-5	3000001872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344381	Hospital-7	S500003102	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344381	Hospital-8	U500004062	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102344381	Hospital-6	4000002052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102637795	Hospital-1	18092957	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102637795	Hospital-2	01722840	A	2008-07-28 15:07:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102637795	Master_Index	102637795	A	2008-07-28 15:07:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102637795	Hospital-5	3000001874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102637795	Hospital-7	S500003104	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102637795	Hospital-6	4000002054	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102785439	Hospital-1_E	69RI1ABH0Q6LO3HTML79HQF98M	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102785439	Hospital-2	02160313	A	2006-04-10 14:38:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102785439	Master_Index	102785439	A	2006-04-10 14:38:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102785439	Hospital-7	S500003116	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102785439	Hospital-8	U500004076	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102785439	Hospital-6_E	3O4UBEPK5RKJR68JL97GPG8BC5	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102788263	Hospital-1	18658583	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102788263	Hospital-2_E	B0T96NJTTVCECAEDTB09I37G67	A	2009-05-15 14:30:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102788263	Master_Index	102788263	A	2009-05-15 14:30:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
102788263	Hospital-5	3000001891	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102788263	Hospital-7	S500003121	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102788263	Hospital-8	U500004081	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
102788263	Hospital-6	4000002071	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103593382	Hospital-1	11489960	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103593382	Hospital-2	01164891	A	2009-04-20 15:11:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103593382	Hospital-3_E	29LPSF4019FJR2CKFOVV23QOPA	A	2009-04-20 15:11:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103593382	Master_Index	103593382	A	2009-04-20 15:11:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103593382	Hospital-4	01055419	A	2009-04-20 15:11:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103593382	Hospital-5	3000001894	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103593382	Hospital-7	S500003124	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103593382	Hospital-6	4000002074	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703039	Hospital-1	20722294	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703039	Hospital-2	02408012	A	2011-03-21 15:30:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103703039	Hospital-3	346657	A	2011-03-21 15:30:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103703039	Master_Index	103703039	A	2011-03-21 15:30:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103703039	Hospital-5	3000001903	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703039	Hospital-8	U500004093	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703039	Hospital-6	4000002083	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703072	Hospital-1	20722302	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703072	Hospital-2	02408022	A	2012-11-12 15:19:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103703072	Hospital-3	345830	A	2012-11-12 15:19:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103703072	Master_Index	103703072	A	2012-11-12 15:19:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103703072	Hospital-7	S500003136	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703072	Hospital-8	U500004096	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103703072	Hospital-6	4000002086	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943507	Hospital-1	2000001987	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943507	Master_Index	103943507	A	2009-10-23 15:35:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103943507	Hospital-5	3000001847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943507	Hospital-7	S500003077	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943507	Hospital-8	U500004037	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943509	Hospital-1	2000002090	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943509	Master_Index	103943509	A	2009-10-23 15:35:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
103943509	Hospital-5	3000001950	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943509	Hospital-7	S500003180	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943509	Hospital-8	U500004140	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
103943509	Hospital-6	4000002130	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104308528	Hospital-1	00000091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104308528	Hospital-2_E	4GTLSO9CP9DJ86FCV0QJ2E25H5	A	2010-03-16 08:39:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
104308528	Master_Index	104308528	A	2010-03-16 08:39:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
104308528	Hospital-5	3000001907	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104308528	Hospital-7	S500003137	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104308528	Hospital-8	U500004097	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104334898	Hospital-1_E	55NI0PG4FE1K45FTFS9HA983IG	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104334898	Hospital-2	01164890	A	2012-03-16 14:01:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
104334898	Hospital-3	299566	A	2012-03-16 14:01:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
104334898	Master_Index	104334898	A	2012-03-16 14:01:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
104334898	Hospital-5	3000001909	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104334898	Hospital-7	S500003139	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104334898	Hospital-8	U500004099	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
104334898	Hospital-6	4000002089	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541340	Hospital-1	2000002054	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541340	Master_Index	105541340	A	2009-10-23 14:57:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105541340	Hospital-5	4745300	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541340	Hospital-7	S500003144	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541340	Hospital-6	4000002094	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541343	Hospital-1	2000002056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541343	Master_Index	105541343	A	2009-10-29 22:49:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105541343	Hospital-5_E	AUFRP61U0FK6TFILC8I0CHA957	A	2009-10-29 22:49:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105541343	Hospital-7	S500003146	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541343	Hospital-8	U500004106	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541343	Hospital-6	4000002096	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541501	Hospital-1	2000002059	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541501	Master_Index	105541501	A	2009-10-28 22:44:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105541501	Hospital-5	4745303	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541501	Hospital-7_E	C6KC9GBNAVJED24N3SL3QHF7HQ	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541501	Hospital-8	U500004109	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105541501	Hospital-6	4000002099	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560546	Master_Index	105560546	A	2009-08-19 11:49:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105560546	Hospital-5_E	DNUTM3P012I4K1RIMJTV325M5G	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560546	Hospital-7	S500003155	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560546	Hospital-8	U500004115	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560546	Hospital-6	4000002105	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560548	Hospital-1	2000002067	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560548	Master_Index	105560548	A	2009-08-19 11:51:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105560548	Hospital-5	4745321	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560548	Hospital-7	S500003157	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560548	Hospital-8	U500004117	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560549	Hospital-1	2000002068	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560549	Master_Index	105560549	A	2009-08-19 11:52:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105560549	Hospital-5_E	59TLK285NQ49!D76D6DQO8IME8	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560549	Hospital-7	S500003158	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560549	Hospital-8	U500004118	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105560549	Hospital-6	4000002108	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105802853	Hospital-1	24492902	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105802853	Hospital-2	02847863	A	2010-01-07 13:04:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105802853	Hospital-3_E	82NGIU1EMUVKEED33Q4726OVAG	A	2010-01-07 13:04:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105802853	Master_Index	105802853	A	2010-01-07 13:04:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105802853	Hospital-5	3000001938	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105802853	Hospital-7	S500003168	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105802853	Hospital-8	U500004128	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105802853	Hospital-6	4000002118	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105807520	Hospital-1	24528291	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105807520	Hospital-2	02848903	A	2012-10-18 07:26:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105807520	Hospital-3	477778	A	2012-10-18 07:26:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105807520	Master_Index	105807520	A	2012-10-18 07:26:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105807520	Hospital-5	3000001941	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105807520	Hospital-7	S500003171	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105807520	Hospital-8	U500004131	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105807520	Hospital-6	4000002121	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105810956	Hospital-1	24528580	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105810956	Hospital-2	02849661	A	2010-01-11 10:39:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105810956	Hospital-3	477779	A	2010-01-11 10:39:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105810956	Master_Index	105810956	A	2010-01-11 10:39:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105810956	Hospital-5	3000001943	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105810956	Hospital-8	U500004133	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105810956	Hospital-6	4000002123	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105893324	Hospital-1_E	AS6RQRP6604HMDJSLT1GT3JLSP	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105893324	Hospital-2	02867887	A	2013-01-03 14:32:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105893324	Hospital-3_E	5JSV828EKSUQK6GOD60HS2JL4F	A	2013-01-03 14:32:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105893324	Master_Index	105893324	A	2013-01-03 14:32:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
105893324	Hospital-5	3000001948	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105893324	Hospital-7	S500003178	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105893324	Hospital-8	U500004138	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
105893324	Hospital-6	4000002128	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
106003404	Hospital-1	17028606	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
106003404	Hospital-2	01954311	A	2010-06-03 07:45:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
106003404	Hospital-3	521660	A	2010-06-03 07:45:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
106003404	Master_Index	106003404	A	2010-06-03 07:45:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
106003404	Hospital-4	00001004	A	2010-06-03 07:45:00	2013-01-22 10:15:00	\N	DEMO_I2B2	\N
106003404	Hospital-5	3000001949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
106003404	Hospital-7	S500003179	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
106003404	Hospital-8	U500004139	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
106003404	Hospital-6	4000002129	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_I2B2	\N
990056887	Hospital-8_E	1RRSFOJ3LFQ6EAER08G8TI57CR	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056888	Hospital-1	2000001961	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056889	Hospital-1	2000001962	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056890	Hospital-1	2000001963	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056891	Hospital-1	2000001964	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056892	Hospital-1	2000001965	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056893	Hospital-1	2000001966	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056894	Hospital-1	2000001967	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056895	Hospital-1	2000001968	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056896	Hospital-1	2000001969	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056897	Hospital-1	2000001970	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056898	Hospital-1	2000001973	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056899	Hospital-1	2000001977	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056900	Hospital-1	2000001979	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056901	Hospital-1	2000001980	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056902	Hospital-1	2000001981	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056903	Hospital-1	2000001982	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056904	Hospital-1	2000001983	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056905	Hospital-1	2000001984	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056906	Hospital-1	2000001989	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056907	Hospital-1	2000001990	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056908	Hospital-1	2000001991	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056909	Hospital-1	2000001994	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056910	Hospital-1	2000001996	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056911	Hospital-1	2000001998	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056912	Hospital-1	2000002000	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056913	Hospital-1	2000002001	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056914	Hospital-1	2000002002	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056915	Hospital-1	2000002004	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056916	Hospital-1	2000002006	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056917	Hospital-1	2000002008	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056918	Hospital-1	2000002009	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056919	Hospital-1	2000002011	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056920	Hospital-1	2000002013	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056921	Hospital-1	2000002016	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056922	Hospital-1	2000002017	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056923	Hospital-1	2000002018	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056924	Hospital-1	2000002019	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056925	Hospital-1	2000002021	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056926	Hospital-1	2000002022	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056927	Hospital-1	2000002023	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056928	Hospital-1	2000002024	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056929	Hospital-1	2000002027	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056930	Hospital-1	2000002028	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056931	Hospital-1	2000002029	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056932	Hospital-1	2000002030	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056933	Hospital-1	2000002032	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056934	Hospital-1	2000002033	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056935	Hospital-1	2000002036	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056936	Hospital-1	2000002037	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056937	Hospital-1	2000002038	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056938	Hospital-1	2000002039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056939	Hospital-1	2000002040	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056940	Hospital-1	2000002041	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056941	Hospital-1	2000002042	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056942	Hospital-1	2000002044	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056943	Hospital-1	2000002050	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056944	Hospital-1	2000002051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056945	Hospital-1	2000002052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056946	Hospital-1	2000002053	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056947	Hospital-1	2000002057	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056948	Hospital-1	2000002058	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056949	Hospital-1	2000002060	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056950	Hospital-1	2000002061	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056951	Hospital-1	2000002062	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056952	Hospital-1	2000002063	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056953	Hospital-1	2000002064	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056954	Hospital-1	2000002066	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056955	Hospital-1	2000002069	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056956	Hospital-1	2000002070	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056957	Hospital-1	2000002071	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056958	Hospital-1	2000002072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056959	Hospital-1	2000002073	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056960	Hospital-1	2000002074	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056961	Hospital-1	2000002076	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056962	Hospital-1	2000002077	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056963	Hospital-1	2000002079	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056964	Hospital-1	2000002080	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056965	Hospital-1	2000002082	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056966	Hospital-1	2000002084	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056967	Hospital-1	2000002086	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056968	Hospital-1	2000002087	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056969	Hospital-1	2000002091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056970	Hospital-1	2000002092	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056971	Hospital-1	2000002093	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056972	Hospital-1	2000002094	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056973	Hospital-5	3000001821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056974	Hospital-5	3000001822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056975	Hospital-5	3000001823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056976	Hospital-5	3000001824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056977	Hospital-5	3000001825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056978	Hospital-5	3000001827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056979	Hospital-5	3000001828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056980	Hospital-5	3000001829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056981	Hospital-5	3000001830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056982	Hospital-5	3000001833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056983	Hospital-5	3000001835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056984	Hospital-5	3000001837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056985	Hospital-5	3000001839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056986	Hospital-5	3000001841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056987	Hospital-5	3000001842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056988	Hospital-5	3000001843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056989	Hospital-5	3000001844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056990	Hospital-5	3000001849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056991	Hospital-5	3000001850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056992	Hospital-5	3000001851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056993	Hospital-5	3000001852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056994	Hospital-5	3000001855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056995	Hospital-5	3000001858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056996	Hospital-5	3000001860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056997	Hospital-5	3000001861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056998	Hospital-5	3000001862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990056999	Hospital-5	3000001864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057000	Hospital-5	3000001868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057001	Hospital-5	3000001869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057002	Hospital-5	3000001871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057003	Hospital-5	3000001873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057004	Hospital-5	3000001875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057005	Hospital-5	3000001877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057006	Hospital-5	3000001878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057007	Hospital-5	3000001879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057008	Hospital-5	3000001881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057009	Hospital-5	3000001882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057010	Hospital-5	3000001883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057011	Hospital-5	3000001884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057012	Hospital-5	3000001885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057013	Hospital-5	3000001887	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057014	Hospital-5	3000001888	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057015	Hospital-5	3000001889	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057016	Hospital-5	3000001890	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057017	Hospital-5	3000001892	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057018	Hospital-5	3000001893	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057019	Hospital-5	3000001895	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057020	Hospital-5	3000001897	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057021	Hospital-5	3000001898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057022	Hospital-5	3000001899	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057023	Hospital-5	3000001900	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057024	Hospital-5	3000001901	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057025	Hospital-5	3000001904	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057026	Hospital-5	3000001905	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057027	Hospital-5	3000001908	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057028	Hospital-5	3000001910	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057029	Hospital-5	3000001911	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057030	Hospital-5	3000001912	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057031	Hospital-5	3000001913	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057032	Hospital-5	3000001915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057033	Hospital-5	3000001917	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057034	Hospital-5	3000001918	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057035	Hospital-5	3000001920	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057036	Hospital-5	3000001921	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057037	Hospital-5	3000001922	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057038	Hospital-5	3000001923	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057039	Hospital-5	3000001924	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057040	Hospital-5	3000001929	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057041	Hospital-5	3000001930	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057042	Hospital-5	3000001931	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057043	Hospital-5	3000001932	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057044	Hospital-5	3000001933	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057045	Hospital-5	3000001934	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057046	Hospital-5	3000001935	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057047	Hospital-5	3000001937	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057048	Hospital-5	3000001939	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057049	Hospital-5	3000001940	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057050	Hospital-5	3000001942	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057051	Hospital-5	3000001944	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057052	Hospital-5	3000001945	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057053	Hospital-5	3000001947	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057054	Hospital-5	3000001951	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057055	Hospital-5	3000001952	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057056	Hospital-5	3000001953	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057057	Hospital-5	3000001954	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057058	Hospital-8_E	3ENVRHAPMRJA56Q6JM596GTQK!	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057059	Hospital-6	4000002001	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057060	Hospital-6	4000002002	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057061	Hospital-6	4000002003	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057062	Hospital-6	4000002004	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057063	Hospital-6	4000002005	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057064	Hospital-6	4000002006	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057065	Hospital-6	4000002008	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057066	Hospital-6	4000002009	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057067	Hospital-6	4000002010	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057068	Hospital-6	4000002013	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057069	Hospital-6	4000002015	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057070	Hospital-6	4000002016	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057071	Hospital-6	4000002019	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057072	Hospital-6	4000002020	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057073	Hospital-6	4000002021	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057074	Hospital-6	4000002022	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057075	Hospital-6	4000002023	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057076	Hospital-6	4000002024	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057077	Hospital-6	4000002029	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057078	Hospital-6	4000002030	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057079	Hospital-6	4000002031	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057080	Hospital-6	4000002032	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057081	Hospital-6	4000002034	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057082	Hospital-6	4000002035	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057083	Hospital-6	4000002036	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057084	Hospital-6	4000002038	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057085	Hospital-6	4000002040	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057086	Hospital-6	4000002041	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057087	Hospital-6	4000002042	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057088	Hospital-6	4000002044	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057089	Hospital-6	4000002046	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057090	Hospital-6	4000002048	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057091	Hospital-6	4000002049	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057092	Hospital-6	4000002051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057093	Hospital-6	4000002053	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057094	Hospital-6	4000002055	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057095	Hospital-6	4000002056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057096	Hospital-6	4000002058	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057097	Hospital-6	4000002059	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057098	Hospital-6	4000002060	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057099	Hospital-6	4000002061	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057100	Hospital-6	4000002062	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057101	Hospital-6	4000002063	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057102	Hospital-6	4000002064	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057103	Hospital-6	4000002065	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057104	Hospital-6	4000002068	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057105	Hospital-6	4000002069	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057106	Hospital-6	4000002070	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057107	Hospital-6	4000002072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057108	Hospital-6	4000002073	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057109	Hospital-6	4000002075	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057110	Hospital-6	4000002076	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057111	Hospital-6	4000002078	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057112	Hospital-6	4000002079	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057113	Hospital-6	4000002080	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057114	Hospital-6	4000002081	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057115	Hospital-6	4000002082	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057116	Hospital-6	4000002084	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057117	Hospital-6	4000002085	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057118	Hospital-6	4000002088	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057119	Hospital-6	4000002091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057120	Hospital-6	4000002092	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057121	Hospital-6	4000002093	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057122	Hospital-6	4000002095	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057123	Hospital-6	4000002098	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057124	Hospital-6	4000002100	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057125	Hospital-6	4000002101	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057126	Hospital-6	4000002102	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057127	Hospital-6	4000002103	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057128	Hospital-6	4000002104	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057129	Hospital-6	4000002109	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057130	Hospital-6	4000002110	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057131	Hospital-6	4000002111	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057132	Hospital-6	4000002112	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057133	Hospital-6	4000002113	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057134	Hospital-6	4000002114	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057135	Hospital-6	4000002115	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057136	Hospital-6	4000002116	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057137	Hospital-6	4000002119	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057138	Hospital-6	4000002120	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057139	Hospital-6	4000002122	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057140	Hospital-6	4000002124	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057141	Hospital-6	4000002125	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057142	Hospital-6	4000002126	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057143	Hospital-6	4000002131	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057144	Hospital-6	4000002133	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057145	Hospital-6	4000002134	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057146	Hospital-7_E	4CUUFEBOV648G1U5N5GLP15GF7	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057147	Hospital-1_E	52JC97QSN3ARJ7BR6CKM5QC6ME	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057148	Hospital-8_E	61N65R47H747021F2N0PSUKI4J	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057149	Hospital-5_E	8E6JQCMEQ8NUNATMH6ACQIOQ4O	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057150	Hospital-7_E	8JF8NPQNHKU3BDUTUNK9PGB927	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057151	Hospital-1_E	AH0QL9668FSAB7AATOO28EOP46	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057152	Hospital-6_E	AKH21TVH2JUV!DGEELPIC8LA9F	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057153	Hospital-7_E	AV2VJ0VVTM5AT3LJC8OI3C5V8J	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057154	Hospital-5_E	BCTCRLHSKGBMQ7P90330VJKFAG	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057155	Hospital-1_E	BTM9MH1JJGAVBFKEOQ75J1F7L!	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057156	Hospital-6_E	C0V5BDD0VSNVG91VGCGEU4RC2D	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057157	Hospital-8_E	CQR959QOAGJUT94J1LD2INLL17	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057158	Hospital-5_E	DON2RP778SNOU95OS1BGLN4EV0	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057159	Hospital-5_E	E309EB1I0PB0!AEMA1OUJ260TD	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057160	Hospital-6_E	E5Q6I8P09I1R9BHO8FAKITU0QC	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057161	Hospital-1_E	F9S8MS240CMC38ND2NSA33AG6A	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057162	Hospital-7	S500003051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057163	Hospital-7	S500003052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057164	Hospital-7	S500003054	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057165	Hospital-7	S500003055	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057166	Hospital-7	S500003056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057167	Hospital-7	S500003057	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057168	Hospital-7	S500003058	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057169	Hospital-7	S500003059	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057170	Hospital-7	S500003060	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057171	Hospital-7	S500003065	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057172	Hospital-7	S500003066	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057173	Hospital-7	S500003069	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057174	Hospital-7	S500003070	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057175	Hospital-7	S500003071	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057176	Hospital-7	S500003072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057177	Hospital-7	S500003074	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057178	Hospital-7	S500003079	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057179	Hospital-7	S500003080	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057180	Hospital-7	S500003081	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057181	Hospital-7	S500003082	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057182	Hospital-7	S500003084	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057183	Hospital-7	S500003085	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057184	Hospital-7	S500003086	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057185	Hospital-7	S500003088	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057186	Hospital-7	S500003091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057187	Hospital-7	S500003092	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057188	Hospital-7	S500003094	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057189	Hospital-7	S500003096	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057190	Hospital-7	S500003098	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057191	Hospital-7	S500003099	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057192	Hospital-7	S500003101	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057193	Hospital-7	S500003105	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057194	Hospital-7	S500003106	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057195	Hospital-7	S500003107	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057196	Hospital-7	S500003108	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057197	Hospital-7	S500003109	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057198	Hospital-7	S500003110	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057199	Hospital-7	S500003111	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057200	Hospital-7	S500003112	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057201	Hospital-7	S500003114	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057202	Hospital-7	S500003115	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057203	Hospital-7	S500003117	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057204	Hospital-7	S500003118	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057205	Hospital-7	S500003119	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057206	Hospital-7	S500003120	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057207	Hospital-7	S500003122	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057208	Hospital-7	S500003125	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057209	Hospital-7	S500003126	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057210	Hospital-7	S500003127	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057211	Hospital-7	S500003129	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057212	Hospital-7	S500003130	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057213	Hospital-7	S500003131	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057214	Hospital-7	S500003132	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057215	Hospital-7	S500003134	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057216	Hospital-7	S500003135	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057217	Hospital-7	S500003138	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057218	Hospital-7	S500003140	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057219	Hospital-7	S500003141	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057220	Hospital-7	S500003142	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057221	Hospital-7	S500003145	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057222	Hospital-7	S500003147	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057223	Hospital-7	S500003148	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057224	Hospital-7	S500003150	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057225	Hospital-7	S500003151	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057226	Hospital-7	S500003152	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057227	Hospital-7	S500003154	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057228	Hospital-7	S500003156	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057229	Hospital-7	S500003159	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057230	Hospital-7	S500003160	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057231	Hospital-7	S500003161	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057232	Hospital-7	S500003162	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057233	Hospital-7	S500003164	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057234	Hospital-7	S500003165	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057235	Hospital-7	S500003166	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057236	Hospital-7	S500003167	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057237	Hospital-7	S500003169	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057238	Hospital-7	S500003170	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057239	Hospital-7	S500003172	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057240	Hospital-7	S500003174	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057241	Hospital-7	S500003175	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057242	Hospital-7	S500003176	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057243	Hospital-7	S500003177	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057244	Hospital-7	S500003181	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057245	Hospital-7	S500003182	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057246	Hospital-7	S500003184	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057247	Hospital-8	U500004011	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057248	Hospital-8	U500004012	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057249	Hospital-8	U500004013	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057250	Hospital-8	U500004015	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057251	Hospital-8	U500004016	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057252	Hospital-8	U500004017	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057253	Hospital-8	U500004018	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057254	Hospital-8	U500004019	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057255	Hospital-8	U500004020	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057256	Hospital-8	U500004023	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057257	Hospital-8	U500004025	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057258	Hospital-8	U500004026	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057259	Hospital-8	U500004027	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057260	Hospital-8	U500004029	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057261	Hospital-8	U500004030	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057262	Hospital-8	U500004031	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057263	Hospital-8	U500004032	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057264	Hospital-8	U500004033	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057265	Hospital-8	U500004039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057266	Hospital-8	U500004040	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057267	Hospital-8	U500004041	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057268	Hospital-8	U500004042	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057269	Hospital-8	U500004046	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057270	Hospital-8	U500004048	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057271	Hospital-8	U500004050	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057272	Hospital-8	U500004051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057273	Hospital-8	U500004052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057274	Hospital-8	U500004056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057275	Hospital-8	U500004058	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057276	Hospital-8	U500004059	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057277	Hospital-8	U500004061	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057278	Hospital-8	U500004063	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057279	Hospital-8	U500004065	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057280	Hospital-8	U500004066	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057281	Hospital-8	U500004067	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057282	Hospital-8	U500004068	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057283	Hospital-8	U500004069	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057284	Hospital-8	U500004070	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057285	Hospital-8	U500004071	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057286	Hospital-8	U500004072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057287	Hospital-8	U500004073	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057288	Hospital-8	U500004075	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057289	Hospital-8	U500004078	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057290	Hospital-8	U500004079	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057291	Hospital-8	U500004080	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057292	Hospital-8	U500004082	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057293	Hospital-8	U500004083	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057294	Hospital-8	U500004085	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057295	Hospital-8	U500004086	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057296	Hospital-8	U500004087	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057297	Hospital-8	U500004088	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057298	Hospital-8	U500004089	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057299	Hospital-8	U500004090	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057300	Hospital-8	U500004091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057301	Hospital-8	U500004092	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057302	Hospital-8	U500004095	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057303	Hospital-8	U500004098	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057304	Hospital-8	U500004100	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057305	Hospital-8	U500004101	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057306	Hospital-8	U500004102	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057307	Hospital-8	U500004103	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057308	Hospital-8	U500004105	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057309	Hospital-8	U500004107	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057310	Hospital-8	U500004108	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057311	Hospital-8	U500004110	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057312	Hospital-8	U500004111	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057313	Hospital-8	U500004112	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057314	Hospital-8	U500004116	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057315	Hospital-8	U500004119	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057316	Hospital-8	U500004120	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057317	Hospital-8	U500004121	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057318	Hospital-8	U500004122	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057319	Hospital-8	U500004123	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057320	Hospital-8	U500004125	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057321	Hospital-8	U500004126	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057322	Hospital-8	U500004127	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057323	Hospital-8	U500004129	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057324	Hospital-8	U500004130	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057325	Hospital-8	U500004132	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057326	Hospital-8	U500004135	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057327	Hospital-8	U500004136	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057328	Hospital-8	U500004137	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057329	Hospital-8	U500004141	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
990057330	Hospital-8	U500004143	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
\.


--
-- Data for Name: im_project_patients; Type: TABLE DATA; Schema: i2b2imdata; Owner: i2b2imdata
--

COPY i2b2imdata.im_project_patients (project_id, global_id, patient_project_status, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
demo	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105560546	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056800	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056814	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056826	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056836	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056880	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056887	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056888	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056889	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056890	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056891	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056892	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056893	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056894	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056895	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056896	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056897	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056899	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056900	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056901	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056902	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056903	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056904	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056905	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056906	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056907	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056908	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056909	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056910	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056911	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056912	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056913	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056914	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056916	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056917	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056918	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056919	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056920	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056921	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056922	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056923	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056924	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056925	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056927	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056928	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056929	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056930	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056931	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056932	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056933	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056934	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056935	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056936	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056937	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056938	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056939	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056940	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056941	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056942	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056943	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056944	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056945	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056946	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056947	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056948	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056950	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056951	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056952	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056953	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056954	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056955	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056957	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056958	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056959	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056960	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056961	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056962	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056963	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056964	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056965	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056966	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056967	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056968	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056969	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056970	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056971	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056972	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056973	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056974	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056975	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056976	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056977	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056978	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056979	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056980	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056981	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056982	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056983	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056984	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056985	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056986	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056987	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056988	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056989	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056990	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056991	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056992	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056993	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056994	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056995	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056996	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056997	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056998	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990056999	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057000	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057001	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057002	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057003	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057004	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057005	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057006	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057007	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057008	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057009	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057010	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057011	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057012	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057013	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057014	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057015	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057016	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057017	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057018	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057019	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057020	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057021	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057022	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057023	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057024	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057025	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057026	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057027	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057028	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057029	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057030	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057031	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057032	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057033	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057034	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057035	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057036	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057037	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057038	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057040	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057041	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057042	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057043	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057044	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057045	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057046	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057047	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057048	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057049	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057050	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057053	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057054	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057055	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057057	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057058	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057059	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057060	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057061	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057062	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057063	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057064	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057065	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057066	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057067	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057068	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057069	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057070	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057071	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057073	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057074	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057075	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057076	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057077	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057078	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057079	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057080	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057081	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057082	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057083	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057084	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057085	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057086	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057087	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057088	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057089	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057090	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057092	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057093	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057094	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057095	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057096	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057097	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057098	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057099	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057100	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057101	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057102	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057103	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057104	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057105	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057106	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057107	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057108	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057109	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057110	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057111	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057112	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057113	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057114	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057115	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057116	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057117	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057118	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057119	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057120	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057121	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057122	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057123	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057124	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057125	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057126	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057127	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057128	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057129	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057130	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057131	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057132	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057133	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057134	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057135	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057136	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057137	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057138	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057139	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057140	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057141	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057142	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057143	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057144	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057145	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057146	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057147	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057148	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057149	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057150	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057151	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057152	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057153	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057154	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057155	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057156	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057157	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057158	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057159	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057160	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057161	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057162	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057163	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057164	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057165	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057166	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057167	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057168	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057169	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057170	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057171	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057172	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057173	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057174	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057175	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057176	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057177	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057178	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057179	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057180	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057181	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057182	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057183	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057184	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057185	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057186	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057187	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057188	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057189	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057190	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057191	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057192	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057193	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057194	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057195	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057196	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057197	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057198	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057199	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057200	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057201	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057202	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057203	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057204	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057205	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057206	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057207	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057208	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057209	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057210	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057211	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057212	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057213	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057214	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057215	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057216	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057217	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057218	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057219	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057220	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057221	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057222	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057223	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057224	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057225	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057226	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057227	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057228	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057229	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057230	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057231	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057232	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057233	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057234	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057235	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057236	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057237	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057238	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057239	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057240	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057241	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057242	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057243	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057244	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057245	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057246	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057248	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057249	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057250	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057251	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057252	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057253	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057254	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057255	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057256	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057257	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057258	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057259	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057260	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057261	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057262	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057264	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057265	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057266	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057267	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057268	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057269	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057270	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057271	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057272	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057273	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057274	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057275	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057276	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057277	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057278	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057279	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057280	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057281	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057282	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057283	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057284	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057285	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057286	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057287	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057288	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057289	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057290	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057291	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057292	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057293	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057294	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057295	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057296	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057297	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057298	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057299	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057300	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057301	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057302	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057303	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057304	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057305	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057306	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057307	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057308	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057309	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057310	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057311	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057312	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057313	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057314	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057315	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057316	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057317	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057318	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057319	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057320	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057321	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057322	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057323	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057325	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057326	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057327	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057328	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057329	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo	990057330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo1	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056888	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056889	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056890	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056891	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056892	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056893	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056894	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056895	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056896	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056897	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056899	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056900	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056901	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056902	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056903	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056904	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056905	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056906	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056907	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056908	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056909	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056910	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056911	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056912	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056913	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056914	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056916	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056917	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056918	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056919	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056920	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056921	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056922	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056923	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056924	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056925	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056927	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056928	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056929	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056930	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056931	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056932	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056933	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056934	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056935	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056936	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056937	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056938	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056939	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056940	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056941	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056942	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056943	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056944	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056945	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056946	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056947	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056948	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056950	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056951	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056952	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056953	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056954	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056955	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056957	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056958	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056959	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056960	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056961	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056962	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056963	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056964	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056965	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056966	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056967	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056968	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056969	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056970	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056971	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990056972	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990057147	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990057151	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990057155	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo1	990057161	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo10	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056888	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056889	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056890	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056891	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056892	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056893	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056894	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056895	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056896	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056897	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056899	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056900	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056901	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056902	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056903	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056904	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056905	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056906	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056907	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056908	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056909	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056910	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056911	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056912	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056913	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056914	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056916	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056917	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056918	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056919	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056920	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056921	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056922	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056923	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056924	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056925	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056927	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056928	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056929	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056930	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056931	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056932	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056933	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056934	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056935	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056936	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056937	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056938	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056939	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056940	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056941	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056942	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056943	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056944	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056945	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056946	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056947	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056948	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056950	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056951	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056952	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056953	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056954	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056955	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056957	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056958	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056959	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056960	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056961	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056962	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056963	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056964	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056965	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056966	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056967	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056968	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056969	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056970	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056971	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990056972	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990057147	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990057151	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990057155	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo10	990057161	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105560546	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo11	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056800	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056814	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056826	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056836	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056880	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056888	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056889	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056890	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056891	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056892	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056893	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056894	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056895	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056896	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056897	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056899	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056900	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056901	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056902	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056903	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056904	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056905	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056906	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056907	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056908	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056909	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056910	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056911	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056912	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056913	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056914	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056916	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056917	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056918	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056919	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056920	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056921	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056922	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056923	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056924	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056925	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056927	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056928	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056929	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056930	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056931	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056932	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056933	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056934	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056935	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056936	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056937	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056938	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056939	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056940	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056941	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056942	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056943	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056944	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056945	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056946	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056947	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056948	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056950	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056951	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056952	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056953	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056954	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056955	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056957	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056958	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056959	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056960	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056961	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056962	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056963	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056964	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056965	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056966	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056967	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056968	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056969	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056970	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056971	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056972	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056973	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056974	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056975	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056976	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056977	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056978	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056979	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056980	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056981	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056982	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056983	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056984	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056985	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056986	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056987	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056988	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056989	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056990	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056991	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056992	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056993	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056994	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056995	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056996	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056997	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056998	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990056999	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057000	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057001	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057002	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057003	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057004	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057005	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057006	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057007	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057008	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057009	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057010	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057011	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057012	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057013	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057014	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057015	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057016	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057017	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057018	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057019	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057020	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057021	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057022	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057023	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057024	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057025	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057026	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057027	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057028	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057029	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057030	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057031	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057032	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057033	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057034	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057035	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057036	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057037	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057038	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057040	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057041	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057042	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057043	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057044	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057045	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057046	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057047	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057048	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057049	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057050	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057053	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057054	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057055	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057057	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057059	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057060	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057061	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057062	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057063	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057064	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057065	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057066	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057067	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057068	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057069	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057070	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057071	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057073	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057074	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057075	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057076	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057077	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057078	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057079	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057080	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057081	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057082	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057083	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057084	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057085	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057086	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057087	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057088	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057089	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057090	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057092	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057093	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057094	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057095	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057096	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057097	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057098	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057099	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057100	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057101	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057102	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057103	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057104	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057105	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057106	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057107	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057108	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057109	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057110	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057111	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057112	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057113	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057114	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057115	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057116	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057117	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057118	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057119	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057120	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057121	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057122	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057123	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057124	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057125	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057126	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057127	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057128	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057129	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057130	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057131	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057132	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057133	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057134	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057135	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057136	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057137	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057138	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057139	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057140	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057141	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057142	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057143	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057144	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057145	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057147	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057149	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057151	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057152	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057154	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057155	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057156	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057158	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057159	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057160	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo11	990057161	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo2	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo2	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo3	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo4	102344360	A	2010-05-17 09:35:00	2013-01-22 10:15:00	\N	DEMO_PHS	\N
demo4	102344370	A	2006-04-11 11:15:00	2013-01-22 10:15:00	\N	DEMO_PHS	\N
demo4	102344381	A	2008-12-02 16:36:00	2013-01-22 10:15:00	\N	DEMO_PHS	\N
demo4	103593382	A	2009-04-20 15:11:00	2013-01-22 10:15:00	\N	DEMO_PHS	\N
demo4	106003404	A	2010-06-03 07:45:00	2013-01-22 10:15:00	\N	DEMO_PHS	\N
demo5	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105560546	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo5	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056800	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056814	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056826	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056836	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056880	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056973	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056974	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056975	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056976	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056977	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056978	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056979	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056980	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056981	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056982	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056983	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056984	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056985	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056986	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056987	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056988	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056989	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056990	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056991	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056992	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056993	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056994	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056995	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056996	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056997	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056998	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990056999	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057000	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057001	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057002	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057003	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057004	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057005	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057006	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057007	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057008	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057009	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057010	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057011	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057012	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057013	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057014	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057015	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057016	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057017	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057018	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057019	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057020	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057021	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057022	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057023	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057024	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057025	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057026	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057027	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057028	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057029	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057030	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057031	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057032	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057033	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057034	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057035	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057036	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057037	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057038	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057040	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057041	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057042	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057043	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057044	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057045	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057046	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057047	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057048	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057049	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057050	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057053	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057054	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057055	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057057	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057149	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057154	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057158	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo5	990057159	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105560546	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo6	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056800	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056814	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056826	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056836	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056880	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057146	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057150	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057153	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057162	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057163	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057164	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057165	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057166	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057167	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057168	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057169	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057170	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057171	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057172	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057173	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057174	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057175	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057176	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057177	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057178	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057179	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057180	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057181	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057182	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057183	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057184	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057185	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057186	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057187	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057188	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057189	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057190	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057191	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057192	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057193	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057194	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057195	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057196	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057197	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057198	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057199	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057200	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057201	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057202	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057203	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057204	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057205	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057206	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057207	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057208	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057209	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057210	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057211	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057212	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057213	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057214	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057215	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057216	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057217	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057218	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057219	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057220	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057221	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057222	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057223	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057224	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057225	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057226	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057227	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057228	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057229	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057230	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057231	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057232	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057233	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057234	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057235	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057236	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057237	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057238	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057239	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057240	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057241	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057242	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057243	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057244	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057245	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo6	990057246	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105560546	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo7	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056800	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056814	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056826	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056836	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056880	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990056887	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057058	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057148	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057157	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057248	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057249	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057250	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057251	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057252	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057253	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057254	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057255	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057256	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057257	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057258	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057259	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057260	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057261	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057262	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057264	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057265	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057266	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057267	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057268	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057269	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057270	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057271	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057272	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057273	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057274	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057275	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057276	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057277	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057278	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057279	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057280	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057281	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057282	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057283	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057284	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057285	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057286	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057287	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057288	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057289	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057290	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057291	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057292	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057293	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057294	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057295	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057296	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057297	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057298	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057299	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057300	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057301	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057302	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057303	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057304	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057305	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057306	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057307	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057308	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057309	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057310	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057311	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057312	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057313	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057314	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057315	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057316	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057317	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057318	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057319	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057320	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057321	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057322	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057323	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057325	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057326	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057327	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057328	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057329	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo7	990057330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105560546	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo8	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056800	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056814	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056826	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056836	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056880	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057059	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057060	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057061	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057062	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057063	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057064	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057065	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057066	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057067	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057068	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057069	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057070	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057071	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057073	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057074	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057075	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057076	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057077	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057078	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057079	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057080	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057081	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057082	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057083	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057084	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057085	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057086	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057087	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057088	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057089	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057090	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057091	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057092	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057093	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057094	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057095	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057096	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057097	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057098	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057099	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057100	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057101	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057102	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057103	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057104	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057105	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057106	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057107	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057108	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057109	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057110	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057111	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057112	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057113	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057114	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057115	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057116	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057117	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057118	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057119	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057120	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057121	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057122	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057123	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057124	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057125	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057126	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057127	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057128	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057129	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057130	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057131	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057132	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057133	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057134	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057135	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057136	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057137	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057138	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057139	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057140	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057141	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057142	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057143	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057144	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057145	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057152	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057156	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo8	990057160	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	100790915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	100790926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	100791247	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	101164949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	101809330	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344360	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344362	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344364	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344367	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344369	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344370	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344373	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344376	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344379	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102344381	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102637795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102785439	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	102788263	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	103593382	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	103703039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	103703072	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	103943507	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	103943509	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	104308528	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	104334898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105541340	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105541343	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105541501	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105560546	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105560548	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105560549	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105802853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105807520	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105810956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	105893324	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	106003404	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO_PHS	\N
demo9	990056789	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056790	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056791	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056792	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056793	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056794	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056795	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056796	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056797	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056798	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056799	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056800	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056801	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056802	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056803	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056804	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056805	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056806	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056807	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056808	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056809	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056810	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056811	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056812	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056813	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056814	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056815	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056816	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056817	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056818	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056819	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056820	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056821	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056822	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056823	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056824	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056825	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056826	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056827	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056828	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056829	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056830	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056831	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056832	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056833	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056834	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056835	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056836	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056837	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056838	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056839	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056840	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056841	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056842	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056843	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056844	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056845	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056846	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056847	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056848	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056849	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056850	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056851	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056852	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056853	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056854	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056855	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056856	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056857	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056858	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056859	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056860	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056861	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056862	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056863	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056864	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056865	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056866	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056867	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056868	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056869	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056870	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056871	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056872	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056873	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056874	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056875	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056876	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056877	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056878	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056879	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056880	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056881	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056882	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056883	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056884	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056885	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056886	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056888	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056889	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056890	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056891	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056892	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056893	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056894	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056895	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056896	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056897	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056898	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056899	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056900	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056901	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056902	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056903	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056904	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056905	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056906	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056907	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056908	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056909	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056910	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056911	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056912	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056913	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056914	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056915	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056916	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056917	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056918	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056919	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056920	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056921	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056922	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056923	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056924	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056925	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056926	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056927	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056928	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056929	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056930	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056931	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056932	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056933	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056934	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056935	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056936	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056937	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056938	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056939	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056940	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056941	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056942	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056943	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056944	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056945	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056946	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056947	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056948	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056949	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056950	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056951	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056952	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056953	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056954	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056955	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056956	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056957	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056958	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056959	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056960	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056961	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056962	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056963	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056964	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056965	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056966	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056967	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056968	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056969	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056970	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056971	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056972	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056973	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056974	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056975	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056976	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056977	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056978	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056979	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056980	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056981	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056982	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056983	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056984	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056985	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056986	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056987	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056988	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056989	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056990	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056991	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056992	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056993	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056994	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056995	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056996	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056997	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056998	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990056999	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057000	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057001	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057002	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057003	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057004	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057005	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057006	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057007	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057008	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057009	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057010	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057011	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057012	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057013	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057014	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057015	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057016	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057017	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057018	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057019	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057020	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057021	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057022	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057023	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057024	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057025	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057026	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057027	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057028	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057029	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057030	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057031	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057032	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057033	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057034	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057035	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057036	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057037	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057038	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057039	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057040	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057041	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057042	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057043	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057044	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057045	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057046	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057047	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057048	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057049	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057050	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057051	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057052	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057053	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057054	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057055	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057056	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057057	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057147	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057149	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057151	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057154	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057155	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057158	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057159	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
demo9	990057161	A	2013-02-01 11:40:00	2010-08-18 09:50:00	\N	DEMO	\N
\.


--
-- Data for Name: im_project_sites; Type: TABLE DATA; Schema: i2b2imdata; Owner: i2b2imdata
--

COPY i2b2imdata.im_project_sites (project_id, lcl_site, project_status, update_date, download_date, import_date, sourcesystem_cd, upload_id) FROM stdin;
demo	Hospital-1	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo	Hospital-2	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo	Hospital-3	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo	Hospital-4	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo	Hospital-5	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo	Hospital-7	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo	Hospital-8	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo	Hospital-6	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo1	Hospital-1	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo10	Hospital-1	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo10	Hospital-3	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo11	Hospital-1	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo11	Hospital-5	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo11	Hospital-6	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo2	Hospital-2	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo3	Hospital-3	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo4	Hospital-4	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo5	Hospital-5	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo6	Hospital-7	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo7	Hospital-8	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo8	Hospital-6	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo9	Hospital-1	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
demo9	Hospital-5	A	2013-02-01 11:40:00	\N	\N	DEMO	\N
\.


--
-- Data for Name: birn; Type: TABLE DATA; Schema: i2b2metadata; Owner: i2b2metadata
--

COPY i2b2metadata.birn (c_hlevel, c_fullname, c_name, c_synonym_cd, c_visualattributes, c_totalnum, c_basecode, c_metadataxml, c_facttablecolumn, c_tablename, c_columnname, c_columndatatype, c_operator, c_dimcode, c_comment, c_tooltip, m_applied_path, update_date, download_date, import_date, sourcesystem_cd, valuetype_cd, m_exclusion_cd, c_path, c_symbol) FROM stdin;
\.


--
-- Data for Name: custom_meta; Type: TABLE DATA; Schema: i2b2metadata; Owner: i2b2metadata
--

COPY i2b2metadata.custom_meta (c_hlevel, c_fullname, c_name, c_synonym_cd, c_visualattributes, c_totalnum, c_basecode, c_metadataxml, c_facttablecolumn, c_tablename, c_columnname, c_columndatatype, c_operator, c_dimcode, c_comment, c_tooltip, m_applied_path, update_date, download_date, import_date, sourcesystem_cd, valuetype_cd, m_exclusion_cd, c_path, c_symbol) FROM stdin;
0	\\Custom Metadata\\	Custom Metadata	N	CAE	\N			concept_cd	concept_dimension	concept_path	T	LIKE	\\Custom Metadata\\	\N	Custom Metadata	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
\.


--
-- Data for Name: i2b2; Type: TABLE DATA; Schema: i2b2metadata; Owner: i2b2metadata
--

COPY i2b2metadata.i2b2 (c_hlevel, c_fullname, c_name, c_synonym_cd, c_visualattributes, c_totalnum, c_basecode, c_metadataxml, c_facttablecolumn, c_tablename, c_columnname, c_columndatatype, c_operator, c_dimcode, c_comment, c_tooltip, m_applied_path, update_date, download_date, import_date, sourcesystem_cd, valuetype_cd, m_exclusion_cd, c_path, c_symbol) FROM stdin;
1	\\DRG\\	DRG	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\	\N	\\DRG\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ZUSCHLAGTAGE\\	ZUSCHLAGTAGE	N	LA 	\N	ZUSCHLAGTA:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>ZUSCHLAGTAGE</TestID>   <TestName>ZUSCHLAGTAGE</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ZUSCHLAGTAGE\\	\N	\\DRG\\ZUSCHLAGTAGE\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ZUSCHLAGKOOPBETRAG\\	ZUSCHLAGKOOPBETRAG	N	LA 	\N	ZUSCHLAGKO:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>ZUSCHLAGKOOPBETRAG</TestID>   <TestName>ZUSCHLAGKOOPBETRAG</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ZUSCHLAGKOOPBETRAG\\	\N	\\DRG\\ZUSCHLAGKOOPBETRAG\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ZUSCHLAGGEW\\	ZUSCHLAGGEW	N	LA 	\N	ZUSCHLAGGE:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>ZUSCHLAGGEW</TestID>   <TestName>ZUSCHLAGGEW</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ZUSCHLAGGEW\\	\N	\\DRG\\ZUSCHLAGGEW\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\VWDVERWENDET\\	VWDVERWENDET	N	LA 	\N	VWDVERWEND:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>VWDVERWENDET</TestID>   <TestName>VWDVERWENDET</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\VWDVERWENDET\\	\N	\\DRG\\VWDVERWENDET\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\RELGEW\\	RELGEW	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\	\N	\\DRG\\RELGEW\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\RELGEW\\2,723\\	2,723	N	LA 	\N	RELGEW:7	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\2,723\\	\N	\\DRG\\RELGEW\\2,723\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\RELGEW\\1,031\\	1,031	N	LA 	\N	RELGEW:4	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\1,031\\	\N	\\DRG\\RELGEW\\1,031\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\RELGEW\\0,9\\	0,9	N	LA 	\N	RELGEW:5	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\0,9\\	\N	\\DRG\\RELGEW\\0,9\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\RELGEW\\0,843\\	0,843	N	LA 	\N	RELGEW:3	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\0,843\\	\N	\\DRG\\RELGEW\\0,843\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\RELGEW\\0,624\\	0,624	N	LA 	\N	RELGEW:1	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\0,624\\	\N	\\DRG\\RELGEW\\0,624\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\RELGEW\\0,478\\	0,478	N	LA 	\N	RELGEW:6	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\0,478\\	\N	\\DRG\\RELGEW\\0,478\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\RELGEW\\0,295\\	0,295	N	LA 	\N	RELGEW:2	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\RELGEW\\0,295\\	\N	\\DRG\\RELGEW\\0,295\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\PCCL\\	PCCL	N	LA 	\N	PCCL:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>PCCL</TestID>   <TestName>PCCL</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\PCCL\\	\N	\\DRG\\PCCL\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\EFFGEW\\0,295\\	0,295	N	LA 	\N	EFFGEW:2	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\0,295\\	\N	\\DRG\\EFFGEW\\0,295\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\DRGVERSION\\	DRGVERSION	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGVERSION\\	\N	\\DRG\\DRGVERSION\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGVERSION\\G-DRG V2009\\	G-DRG V2009	N	LA 	\N	DRGVERSION:1	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGVERSION\\G-DRG V2009\\	\N	\\DRG\\DRGVERSION\\G-DRG V2009\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\DRGCODE\\	DRGCODE	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\	\N	\\DRG\\DRGCODE\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGCODE\\J11C\\	J11C	N	LA 	\N	DRGCODE:1	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\J11C\\	\N	\\DRG\\DRGCODE\\J11C\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\PATIENTSTATUS\\	PATIENTSTATUS	N	LA 	\N	PATIENTSTA:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>PATIENTSTATUS</TestID>   <TestName>PATIENTSTATUS</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\PATIENTSTATUS\\	\N	\\DRG\\PATIENTSTATUS\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\MDC\\	MDC	N	LA 	\N	MDC:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>MDC</TestID>   <TestName>MDC</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\MDC\\	\N	\\DRG\\MDC\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\GST\\	GST	N	LA 	\N	GST:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>GST</TestID>   <TestName>GST</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\GST\\	\N	\\DRG\\GST\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGVWD\\	FLAGVWD	N	LA 	\N	FLAGVWD:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>FLAGVWD</TestID>   <TestName>FLAGVWD</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGVWD\\	\N	\\DRG\\FLAGVWD\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGTAGESFALL\\	FLAGTAGESFALL	N	LA 	\N	FLAGTAGESF:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:49</CreationDateTime>   <TestID>FLAGTAGESFALL</TestID>   <TestName>FLAGTAGESFALL</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGTAGESFALL\\	\N	\\DRG\\FLAGTAGESFALL\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGGESCHLECHT\\	FLAGGESCHLECHT	N	LA 	\N	FLAGGESCHL:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>FLAGGESCHLECHT</TestID>   <TestName>FLAGGESCHLECHT</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGGESCHLECHT\\	\N	\\DRG\\FLAGGESCHLECHT\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGENTLGRUND\\	FLAGENTLGRUND	N	LA 	\N	FLAGENTLGR:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>FLAGENTLGRUND</TestID>   <TestName>FLAGENTLGRUND</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGENTLGRUND\\	\N	\\DRG\\FLAGENTLGRUND\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGCODE\\F62C\\	F62C	N	LA 	\N	DRGCODE:3	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\F62C\\	\N	\\DRG\\DRGCODE\\F62C\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGCODE\\F59B\\	F59B	N	LA 	\N	DRGCODE:5	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\F59B\\	\N	\\DRG\\DRGCODE\\F59B\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGEINWEISUNG\\	FLAGEINWEISUNG	N	LA 	\N	FLAGEINWEI:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>FLAGEINWEISUNG</TestID>   <TestName>FLAGEINWEISUNG</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGEINWEISUNG\\	\N	\\DRG\\FLAGEINWEISUNG\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGAUFNGEWICHT2\\	FLAGAUFNGEWICHT2	N	LA 	\N	FLAGAUFNGE:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>FLAGAUFNGEWICHT2</TestID>   <TestName>FLAGAUFNGEWICHT2</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGAUFNGEWICHT2\\	\N	\\DRG\\FLAGAUFNGEWICHT2\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGAUFNGEWICHT1\\	FLAGAUFNGEWICHT1	N	LA 	\N	FLAGAUFNGE:1	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>FLAGAUFNGEWICHT1</TestID>   <TestName>FLAGAUFNGEWICHT1</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGAUFNGEWICHT1\\	\N	\\DRG\\FLAGAUFNGEWICHT1\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGANZBEATMUNGSTD\\	FLAGANZBEATMUNGSTD	N	LA 	\N	FLAGANZBEA:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>FLAGANZBEATMUNGSTD</TestID>   <TestName>FLAGANZBEATMUNGSTD</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGANZBEATMUNGSTD\\	\N	\\DRG\\FLAGANZBEATMUNGSTD\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\FLAGALTER\\	FLAGALTER	N	LA 	\N	FLAGALTER:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>FLAGALTER</TestID>   <TestName>FLAGALTER</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\FLAGALTER\\	\N	\\DRG\\FLAGALTER\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ENTGSTATUS\\	ENTGSTATUS	N	LA 	\N	ENTGSTATUS:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>ENTGSTATUS</TestID>   <TestName>ENTGSTATUS</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ENTGSTATUS\\	\N	\\DRG\\ENTGSTATUS\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\EFFGEW\\	EFFGEW	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\	\N	\\DRG\\EFFGEW\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\EFFGEW\\2,723\\	2,723	N	LA 	\N	EFFGEW:7	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\2,723\\	\N	\\DRG\\EFFGEW\\2,723\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\EFFGEW\\1,031\\	1,031	N	LA 	\N	EFFGEW:4	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\1,031\\	\N	\\DRG\\EFFGEW\\1,031\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\EFFGEW\\0,843\\	0,843	N	LA 	\N	EFFGEW:3	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\0,843\\	\N	\\DRG\\EFFGEW\\0,843\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\EFFGEW\\0,624\\	0,624	N	LA 	\N	EFFGEW:1	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\0,624\\	\N	\\DRG\\EFFGEW\\0,624\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\EFFGEW\\0,564\\	0,564	N	LA 	\N	EFFGEW:5	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\0,564\\	\N	\\DRG\\EFFGEW\\0,564\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\EFFGEW\\0,478\\	0,478	N	LA 	\N	EFFGEW:6	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\EFFGEW\\0,478\\	\N	\\DRG\\EFFGEW\\0,478\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGCODE\\D24B\\	D24B	N	LA 	\N	DRGCODE:7	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\D24B\\	\N	\\DRG\\DRGCODE\\D24B\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGCODE\\C08B\\	C08B	N	LA 	\N	DRGCODE:6	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\C08B\\	\N	\\DRG\\DRGCODE\\C08B\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGCODE\\C02B\\	C02B	N	LA 	\N	DRGCODE:4	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\C02B\\	\N	\\DRG\\DRGCODE\\C02B\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\DRG\\DRGCODE\\B80Z\\	B80Z	N	LA 	\N	DRGCODE:2	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\DRGCODE\\B80Z\\	\N	\\DRG\\DRGCODE\\B80Z\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\BASISRATE\\	BASISRATE	N	LA 	\N	BASISRATE:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>BASISRATE</TestID>   <TestName>BASISRATE</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\BASISRATE\\	\N	\\DRG\\BASISRATE\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ABTTYP\\	ABTTYP	N	LA 	\N	ABTTYP:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>ABTTYP</TestID>   <TestName>ABTTYP</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ABTTYP\\	\N	\\DRG\\ABTTYP\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ABSCHLAGVERLTAGE\\	ABSCHLAGVERLTAGE	N	LA 	\N	ABSCHLAGVE:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>ABSCHLAGVERLTAGE</TestID>   <TestName>ABSCHLAGVERLTAGE</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ABSCHLAGVERLTAGE\\	\N	\\DRG\\ABSCHLAGVERLTAGE\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ABSCHLAGVERLGEW\\	ABSCHLAGVERLGEW	N	LA 	\N	ABSCHLAGVE:1	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>ABSCHLAGVERLGEW</TestID>   <TestName>ABSCHLAGVERLGEW</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ABSCHLAGVERLGEW\\	\N	\\DRG\\ABSCHLAGVERLGEW\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DRG\\ABSCHLAGTAGE\\	ABSCHLAGTAGE	N	LA 	\N	ABSCHLAGTA:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>ABSCHLAGTAGE</TestID>   <TestName>ABSCHLAGTAGE</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\ABSCHLAGTAGE\\	\N	\\DRG\\ABSCHLAGTAGE\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
1	\\Fall\\	Fall	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\	\N	\\Fall\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Fall\\ENTLASSUNGSGRUND\\	ENTLASSUNGSGRUND	N	LA 	\N	ENTLASSUNG:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>ENTLASSUNGSGRUND</TestID>   <TestName>ENTLASSUNGSGRUND</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\ENTLASSUNGSGRUND\\	\N	\\Fall\\ENTLASSUNGSGRUND\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I50.13\\	I50.13	N	LA 	\N	ICD_KODE:7	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I50.13\\	\N	\\ICD-10\\ICD_KODE\\I50.13\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I48.10\\	I48.10	N	LA 	\N	ICD_KODE:37	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I48.10\\	\N	\\ICD-10\\ICD_KODE\\I48.10\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Fall\\ENTLASSUNGSDATUM\\	ENTLASSUNGSDATUM	N	LA 	\N	ENTLASSUNG:1	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>ENTLASSUNGSDATUM</TestID>   <TestName>ENTLASSUNGSDATUM</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\ENTLASSUNGSDATUM\\	\N	\\Fall\\ENTLASSUNGSDATUM\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Fall\\AUFNAHMEGRUND\\	AUFNAHMEGRUND	N	LA 	\N	AUFNAHMEGR:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>AUFNAHMEGRUND</TestID>   <TestName>AUFNAHMEGRUND</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\AUFNAHMEGRUND\\	\N	\\Fall\\AUFNAHMEGRUND\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Fall\\AUFNAHMEDATUM\\	AUFNAHMEDATUM	N	LA 	\N	AUFNAHMEDA:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:50</CreationDateTime>   <TestID>AUFNAHMEDATUM</TestID>   <TestName>AUFNAHMEDATUM</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\AUFNAHMEDATUM\\	\N	\\Fall\\AUFNAHMEDATUM\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Fall\\AUFNAHMEANLASS\\	AUFNAHMEANLASS	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\AUFNAHMEANLASS\\	\N	\\Fall\\AUFNAHMEANLASS\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Fall\\AUFNAHMEANLASS\\Notfall\\	Notfall	N	LA 	\N	AUFNAHMEAN:2	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\AUFNAHMEANLASS\\Notfall\\	\N	\\Fall\\AUFNAHMEANLASS\\Notfall\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Fall\\AUFNAHMEANLASS\\Einweisung durch einen Arzt\\	Einweisung durch einen Arzt	N	LA 	\N	AUFNAHMEAN:1	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\AUFNAHMEANLASS\\Einweisung durch einen Arzt\\	\N	\\Fall\\AUFNAHMEANLASS\\Einweisung durch einen Arzt\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
1	\\ICD-10\\	ICD-10	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\	\N	\\ICD-10\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\ICD-10\\ICD_KODE\\	ICD_KODE	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\	\N	\\ICD-10\\ICD_KODE\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\Z96.1\\	Z96.1	N	LA 	\N	ICD_KODE:19	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\Z96.1\\	\N	\\ICD-10\\ICD_KODE\\Z96.1\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\Z95.1\\	Z95.1	N	LA 	\N	ICD_KODE:26	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\Z95.1\\	\N	\\ICD-10\\ICD_KODE\\Z95.1\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\Z86.4\\	Z86.4	N	LA 	\N	ICD_KODE:36	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\Z86.4\\	\N	\\ICD-10\\ICD_KODE\\Z86.4\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\Z03.4\\	Z03.4	N	LA 	\N	ICD_KODE:11	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\Z03.4\\	\N	\\ICD-10\\ICD_KODE\\Z03.4\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\T88.8\\	T88.8	N	LA 	\N	ICD_KODE:14	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\T88.8\\	\N	\\ICD-10\\ICD_KODE\\T88.8\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\T82.5\\	T82.5	N	LA 	\N	ICD_KODE:22	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\T82.5\\	\N	\\ICD-10\\ICD_KODE\\T82.5\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\S13.4\\	S13.4	N	LA 	\N	ICD_KODE:5	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\S13.4\\	\N	\\ICD-10\\ICD_KODE\\S13.4\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\S06.0\\	S06.0	N	LA 	\N	ICD_KODE:6	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\S06.0\\	\N	\\ICD-10\\ICD_KODE\\S06.0\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\S05.8\\	S05.8	N	LA 	\N	ICD_KODE:27	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\S05.8\\	\N	\\ICD-10\\ICD_KODE\\S05.8\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\S00.85\\	S00.85	N	LA 	\N	ICD_KODE:4	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\S00.85\\	\N	\\ICD-10\\ICD_KODE\\S00.85\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\R59.0\\	R59.0	N	LA 	\N	ICD_KODE:33	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\R59.0\\	\N	\\ICD-10\\ICD_KODE\\R59.0\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\N25.8\\	N25.8	N	LA 	\N	ICD_KODE:23	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\N25.8\\	\N	\\ICD-10\\ICD_KODE\\N25.8\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\N18.0\\	N18.0	N	LA 	\N	ICD_KODE:20	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\N18.0\\	\N	\\ICD-10\\ICD_KODE\\N18.0\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\M54.16\\	M54.16	N	LA 	\N	ICD_KODE:2	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\M54.16\\	\N	\\ICD-10\\ICD_KODE\\M54.16\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\J34.3\\	J34.3	N	LA 	\N	ICD_KODE:30	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\J34.3\\	\N	\\ICD-10\\ICD_KODE\\J34.3\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\J34.2\\	J34.2	N	LA 	\N	ICD_KODE:32	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\J34.2\\	\N	\\ICD-10\\ICD_KODE\\J34.2\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I97.8\\	I97.8	N	LA 	\N	ICD_KODE:29	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I97.8\\	\N	\\ICD-10\\ICD_KODE\\I97.8\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I71.2\\	I71.2	N	LA 	\N	ICD_KODE:9	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I71.2\\	\N	\\ICD-10\\ICD_KODE\\I71.2\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I44.3\\	I44.3	N	LA 	\N	ICD_KODE:31	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I44.3\\	\N	\\ICD-10\\ICD_KODE\\I44.3\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I31.3\\	I31.3	N	LA 	\N	ICD_KODE:15	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I31.3\\	\N	\\ICD-10\\ICD_KODE\\I31.3\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I27.28\\	I27.28	N	LA 	\N	ICD_KODE:10	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I27.28\\	\N	\\ICD-10\\ICD_KODE\\I27.28\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I10.90\\	I10.90	N	LA 	\N	ICD_KODE:3	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I10.90\\	\N	\\ICD-10\\ICD_KODE\\I10.90\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\I10.00\\	I10.00	N	LA 	\N	ICD_KODE:21	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\I10.00\\	\N	\\ICD-10\\ICD_KODE\\I10.00\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\H52.1\\	H52.1	N	LA 	\N	ICD_KODE:28	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\H52.1\\	\N	\\ICD-10\\ICD_KODE\\H52.1\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\H25.1\\	H25.1	N	LA 	\N	ICD_KODE:25	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\H25.1\\	\N	\\ICD-10\\ICD_KODE\\H25.1\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\H02.4\\	H02.4	N	LA 	\N	ICD_KODE:16	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\H02.4\\	\N	\\ICD-10\\ICD_KODE\\H02.4\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\H02.3\\	H02.3	N	LA 	\N	ICD_KODE:18	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\H02.3\\	\N	\\ICD-10\\ICD_KODE\\H02.3\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\F40.00\\	F40.00	N	LA 	\N	ICD_KODE:34	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\F40.00\\	\N	\\ICD-10\\ICD_KODE\\F40.00\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\F17.2\\	F17.2	N	LA 	\N	ICD_KODE:12	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\F17.2\\	\N	\\ICD-10\\ICD_KODE\\F17.2\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\F17.1\\	F17.1	N	LA 	\N	ICD_KODE:35	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\F17.1\\	\N	\\ICD-10\\ICD_KODE\\F17.1\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\F10.1\\	F10.1	N	LA 	\N	ICD_KODE:39	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\F10.1\\	\N	\\ICD-10\\ICD_KODE\\F10.1\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\E79.0\\	E79.0	N	LA 	\N	ICD_KODE:17	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\E79.0\\	\N	\\ICD-10\\ICD_KODE\\E79.0\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\E78.5\\	E78.5	N	LA 	\N	ICD_KODE:8	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\E78.5\\	\N	\\ICD-10\\ICD_KODE\\E78.5\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\E78.2\\	E78.2	N	LA 	\N	ICD_KODE:24	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\E78.2\\	\N	\\ICD-10\\ICD_KODE\\E78.2\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\E66.92\\	E66.92	N	LA 	\N	ICD_KODE:1	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\E66.92\\	\N	\\ICD-10\\ICD_KODE\\E66.92\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\E11.20+\\	E11.20+	N	LA 	\N	ICD_KODE:13	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\E11.20+\\	\N	\\ICD-10\\ICD_KODE\\E11.20+\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\ICD-10\\ICD_KODE\\C32.9\\	C32.9	N	LA 	\N	ICD_KODE:38	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\ICD_KODE\\C32.9\\	\N	\\ICD-10\\ICD_KODE\\C32.9\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
1	\\DIAGNOSEART\\	DIAGNOSEART	N	DA 	\N		\N	modifier_cd	modifier_dimension	modifier_path	T	LIKE	\\ICD-10\\ICD_KODE\\DIAGNOSEART\\	\N	\\ICD-10\\ICD_KODE\\DIAGNOSEART\\	\\ICD-10\\ICD_KODE\\	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DIAGNOSEART\\ND\\	ND	N	RA 	\N	ICD-10_ICD:1	\N	modifier_cd	modifier_dimension	modifier_path	T	LIKE	\\ICD-10\\ICD_KODE\\DIAGNOSEART\\ND\\	\N	\\ICD-10\\ICD_KODE\\DIAGNOSEART\\ND\\	\\ICD-10\\ICD_KODE\\	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\DIAGNOSEART\\HD\\	HD	N	RA 	\N	ICD-10_ICD:2	\N	modifier_cd	modifier_dimension	modifier_path	T	LIKE	\\ICD-10\\ICD_KODE\\DIAGNOSEART\\HD\\	\N	\\ICD-10\\ICD_KODE\\DIAGNOSEART\\HD\\	\\ICD-10\\ICD_KODE\\	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
1	\\Demografische Daten\\	Demografische Daten	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\	\N	\\Demografische Daten\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Demografische Daten\\Race\\	Race	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Race\\	\N	\\Demografische Daten\\Race\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Race\\Not recorded\\	Not recorded	N	LA 	\N	DEM|RACE:@	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Race\\Not recorded\\	\N	\\Demografische Daten\\Race\\Not recorded\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Demografische Daten\\Vital status\\	Vital status	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Vital status\\	\N	\\Demografische Daten\\Vital status\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Vital status\\Not recorded\\	Not recorded	N	LA 	\N	DEM|VITAL:@	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Vital status\\Not recorded\\	\N	\\Demografische Daten\\Vital status\\Not recorded\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Demografische Daten\\Age\\	Age	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\	\N	\\Demografische Daten\\Age\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\Not recorded\\	Not recorded	N	LA 	\N	DEM|AGE:@	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\Not recorded\\	\N	\\Demografische Daten\\Age\\Not recorded\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\Unknown\\	Unknown	N	LA 	\N	DEM|AGE:u	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\Unknown\\	\N	\\Demografische Daten\\Age\\Unknown\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\>= 85 years old\\	>= 85 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\125 years old\\	125 years old	N	LA 	\N	DEM|AGE:125	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\125 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\125 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\124 years old\\	124 years old	N	LA 	\N	DEM|AGE:124	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\124 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\124 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\123 years old\\	123 years old	N	LA 	\N	DEM|AGE:123	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\123 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\123 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\122 years old\\	122 years old	N	LA 	\N	DEM|AGE:122	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\122 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\122 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\121 years old\\	121 years old	N	LA 	\N	DEM|AGE:121	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\121 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\121 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\120 years old\\	120 years old	N	LA 	\N	DEM|AGE:120	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\120 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\120 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\119 years old\\	119 years old	N	LA 	\N	DEM|AGE:119	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\119 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\119 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\118 years old\\	118 years old	N	LA 	\N	DEM|AGE:118	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\118 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\118 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\117 years old\\	117 years old	N	LA 	\N	DEM|AGE:117	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\117 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\117 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\116 years old\\	116 years old	N	LA 	\N	DEM|AGE:116	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\116 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\116 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\115 years old\\	115 years old	N	LA 	\N	DEM|AGE:115	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\115 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\115 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\114 years old\\	114 years old	N	LA 	\N	DEM|AGE:114	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\114 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\114 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\113 years old\\	113 years old	N	LA 	\N	DEM|AGE:113	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\113 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\113 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\112 years old\\	112 years old	N	LA 	\N	DEM|AGE:112	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\112 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\112 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\111 years old\\	111 years old	N	LA 	\N	DEM|AGE:111	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\111 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\111 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\110 years old\\	110 years old	N	LA 	\N	DEM|AGE:110	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\110 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\110 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\109 years old\\	109 years old	N	LA 	\N	DEM|AGE:109	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\109 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\109 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\108 years old\\	108 years old	N	LA 	\N	DEM|AGE:108	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\108 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\108 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\107 years old\\	107 years old	N	LA 	\N	DEM|AGE:107	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\107 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\107 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\106 years old\\	106 years old	N	LA 	\N	DEM|AGE:106	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\106 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\106 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\105 years old\\	105 years old	N	LA 	\N	DEM|AGE:105	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\105 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\105 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\104 years old\\	104 years old	N	LA 	\N	DEM|AGE:104	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\104 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\104 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\103 years old\\	103 years old	N	LA 	\N	DEM|AGE:103	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\103 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\103 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\102 years old\\	102 years old	N	LA 	\N	DEM|AGE:102	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\102 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\102 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\101 years old\\	101 years old	N	LA 	\N	DEM|AGE:101	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\101 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\101 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\100 years old\\	100 years old	N	LA 	\N	DEM|AGE:100	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\100 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\100 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\99 years old\\	99 years old	N	LA 	\N	DEM|AGE:99	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\99 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\99 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\98 years old\\	98 years old	N	LA 	\N	DEM|AGE:98	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\98 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\98 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\97 years old\\	97 years old	N	LA 	\N	DEM|AGE:97	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\97 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\97 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\96 years old\\	96 years old	N	LA 	\N	DEM|AGE:96	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\96 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\96 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\95 years old\\	95 years old	N	LA 	\N	DEM|AGE:95	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\95 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\95 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\94 years old\\	94 years old	N	LA 	\N	DEM|AGE:94	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\94 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\94 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\93 years old\\	93 years old	N	LA 	\N	DEM|AGE:93	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\93 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\93 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\92 years old\\	92 years old	N	LA 	\N	DEM|AGE:92	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\92 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\92 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\91 years old\\	91 years old	N	LA 	\N	DEM|AGE:91	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\91 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\91 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\90 years old\\	90 years old	N	LA 	\N	DEM|AGE:90	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\90 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\90 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\89 years old\\	89 years old	N	LA 	\N	DEM|AGE:89	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\89 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\89 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\88 years old\\	88 years old	N	LA 	\N	DEM|AGE:88	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\88 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\88 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\87 years old\\	87 years old	N	LA 	\N	DEM|AGE:87	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\87 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\87 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\86 years old\\	86 years old	N	LA 	\N	DEM|AGE:86	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\86 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\86 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\>= 85 years old\\85 years old\\	85 years old	N	LA 	\N	DEM|AGE:85	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\>= 85 years old\\85 years old\\	\N	\\Demografische Daten\\Age\\>= 85 years old\\85 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\75-84 years old\\	75-84 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\84 years old\\	84 years old	N	LA 	\N	DEM|AGE:84	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\84 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\84 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\83 years old\\	83 years old	N	LA 	\N	DEM|AGE:83	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\83 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\83 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\82 years old\\	82 years old	N	LA 	\N	DEM|AGE:82	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\82 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\82 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\81 years old\\	81 years old	N	LA 	\N	DEM|AGE:81	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\81 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\81 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\80 years old\\	80 years old	N	LA 	\N	DEM|AGE:80	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\80 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\80 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\79 years old\\	79 years old	N	LA 	\N	DEM|AGE:79	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\79 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\79 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\78 years old\\	78 years old	N	LA 	\N	DEM|AGE:78	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\78 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\78 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\77 years old\\	77 years old	N	LA 	\N	DEM|AGE:77	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\77 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\77 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\76 years old\\	76 years old	N	LA 	\N	DEM|AGE:76	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\76 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\76 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\75-84 years old\\75 years old\\	75 years old	N	LA 	\N	DEM|AGE:75	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\75-84 years old\\75 years old\\	\N	\\Demografische Daten\\Age\\75-84 years old\\75 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\65-74 years old\\	65-74 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\74 years old\\	74 years old	N	LA 	\N	DEM|AGE:74	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\74 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\74 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\73 years old\\	73 years old	N	LA 	\N	DEM|AGE:73	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\73 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\73 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\72 years old\\	72 years old	N	LA 	\N	DEM|AGE:72	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\72 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\72 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\71 years old\\	71 years old	N	LA 	\N	DEM|AGE:71	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\71 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\71 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\70 years old\\	70 years old	N	LA 	\N	DEM|AGE:70	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\70 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\70 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\69 years old\\	69 years old	N	LA 	\N	DEM|AGE:69	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\69 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\69 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\68 years old\\	68 years old	N	LA 	\N	DEM|AGE:68	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\68 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\68 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\67 years old\\	67 years old	N	LA 	\N	DEM|AGE:67	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\67 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\67 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\66 years old\\	66 years old	N	LA 	\N	DEM|AGE:66	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\66 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\66 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\65-74 years old\\65 years old\\	65 years old	N	LA 	\N	DEM|AGE:65	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\65-74 years old\\65 years old\\	\N	\\Demografische Daten\\Age\\65-74 years old\\65 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\55-64 years old\\	55-64 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\64 years old\\	64 years old	N	LA 	\N	DEM|AGE:64	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\64 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\64 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\63 years old\\	63 years old	N	LA 	\N	DEM|AGE:63	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\63 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\63 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\62 years old\\	62 years old	N	LA 	\N	DEM|AGE:62	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\62 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\62 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\61 years old\\	61 years old	N	LA 	\N	DEM|AGE:61	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\61 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\61 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\60 years old\\	60 years old	N	LA 	\N	DEM|AGE:60	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\60 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\60 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\59 years old\\	59 years old	N	LA 	\N	DEM|AGE:59	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\59 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\59 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\58 years old\\	58 years old	N	LA 	\N	DEM|AGE:58	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\58 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\58 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\57 years old\\	57 years old	N	LA 	\N	DEM|AGE:57	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\57 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\57 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\56 years old\\	56 years old	N	LA 	\N	DEM|AGE:56	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\56 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\56 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\55-64 years old\\55 years old\\	55 years old	N	LA 	\N	DEM|AGE:55	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\55-64 years old\\55 years old\\	\N	\\Demografische Daten\\Age\\55-64 years old\\55 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\45-54 years old\\	45-54 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\54 years old\\	54 years old	N	LA 	\N	DEM|AGE:54	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\54 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\54 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\53 years old\\	53 years old	N	LA 	\N	DEM|AGE:53	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\53 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\53 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\52 years old\\	52 years old	N	LA 	\N	DEM|AGE:52	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\52 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\52 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\51 years old\\	51 years old	N	LA 	\N	DEM|AGE:51	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\51 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\51 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\50 years old\\	50 years old	N	LA 	\N	DEM|AGE:50	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\50 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\50 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\49 years old\\	49 years old	N	LA 	\N	DEM|AGE:49	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\49 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\49 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\48 years old\\	48 years old	N	LA 	\N	DEM|AGE:48	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\48 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\48 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\47 years old\\	47 years old	N	LA 	\N	DEM|AGE:47	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\47 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\47 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\46 years old\\	46 years old	N	LA 	\N	DEM|AGE:46	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\46 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\46 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\45-54 years old\\45 years old\\	45 years old	N	LA 	\N	DEM|AGE:45	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\45-54 years old\\45 years old\\	\N	\\Demografische Daten\\Age\\45-54 years old\\45 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\35-44 years old\\	35-44 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\44 years old\\	44 years old	N	LA 	\N	DEM|AGE:44	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\44 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\44 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\43 years old\\	43 years old	N	LA 	\N	DEM|AGE:43	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\43 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\43 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\42 years old\\	42 years old	N	LA 	\N	DEM|AGE:42	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\42 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\42 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\41 years old\\	41 years old	N	LA 	\N	DEM|AGE:41	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\41 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\41 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\40 years old\\	40 years old	N	LA 	\N	DEM|AGE:40	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\40 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\40 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\39 years old\\	39 years old	N	LA 	\N	DEM|AGE:39	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\39 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\39 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\38 years old\\	38 years old	N	LA 	\N	DEM|AGE:38	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\38 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\38 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\37 years old\\	37 years old	N	LA 	\N	DEM|AGE:37	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\37 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\37 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\36 years old\\	36 years old	N	LA 	\N	DEM|AGE:36	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\36 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\36 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\35-44 years old\\35 years old\\	35 years old	N	LA 	\N	DEM|AGE:35	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\35-44 years old\\35 years old\\	\N	\\Demografische Daten\\Age\\35-44 years old\\35 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\18-34 years old\\	18-34 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\34 years old\\	34 years old	N	LA 	\N	DEM|AGE:34	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\34 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\34 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\33 years old\\	33 years old	N	LA 	\N	DEM|AGE:33	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\33 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\33 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\32 years old\\	32 years old	N	LA 	\N	DEM|AGE:32	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\32 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\32 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\31 years old\\	31 years old	N	LA 	\N	DEM|AGE:31	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\31 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\31 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\30 years old\\	30 years old	N	LA 	\N	DEM|AGE:30	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\30 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\30 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\29 years old\\	29 years old	N	LA 	\N	DEM|AGE:29	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\29 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\29 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\28 years old\\	28 years old	N	LA 	\N	DEM|AGE:28	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\28 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\28 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\27 years old\\	27 years old	N	LA 	\N	DEM|AGE:27	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\27 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\27 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\26 years old\\	26 years old	N	LA 	\N	DEM|AGE:26	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\26 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\26 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\25 years old\\	25 years old	N	LA 	\N	DEM|AGE:25	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\25 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\25 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\24 years old\\	24 years old	N	LA 	\N	DEM|AGE:24	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\24 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\24 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\23 years old\\	23 years old	N	LA 	\N	DEM|AGE:23	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\23 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\23 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\22 years old\\	22 years old	N	LA 	\N	DEM|AGE:22	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\22 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\22 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\21 years old\\	21 years old	N	LA 	\N	DEM|AGE:21	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\21 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\21 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\20 years old\\	20 years old	N	LA 	\N	DEM|AGE:20	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\20 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\20 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\19 years old\\	19 years old	N	LA 	\N	DEM|AGE:19	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\19 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\19 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\18-34 years old\\18 years old\\	18 years old	N	LA 	\N	DEM|AGE:18	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\18-34 years old\\18 years old\\	\N	\\Demografische Daten\\Age\\18-34 years old\\18 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\10-17 years old\\	10-17 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\17 years old\\	17 years old	N	LA 	\N	DEM|AGE:17	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\17 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\17 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\16 years old\\	16 years old	N	LA 	\N	DEM|AGE:16	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\16 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\16 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\15 years old\\	15 years old	N	LA 	\N	DEM|AGE:15	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\15 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\15 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\14 years old\\	14 years old	N	LA 	\N	DEM|AGE:14	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\14 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\14 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\13 years old\\	13 years old	N	LA 	\N	DEM|AGE:13	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\13 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\13 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\12 years old\\	12 years old	N	LA 	\N	DEM|AGE:12	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\12 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\12 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\11 years old\\	11 years old	N	LA 	\N	DEM|AGE:11	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\11 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\11 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\10-17 years old\\10 years old\\	10 years old	N	LA 	\N	DEM|AGE:10	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\10-17 years old\\10 years old\\	\N	\\Demografische Daten\\Age\\10-17 years old\\10 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Age\\0-9 years old\\	0-9 years old	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\9 years old\\	9 years old	N	LA 	\N	DEM|AGE:9	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\9 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\9 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\8 years old\\	8 years old	N	LA 	\N	DEM|AGE:8	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\8 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\8 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\7 years old\\	7 years old	N	LA 	\N	DEM|AGE:7	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\7 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\7 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\6 years old\\	6 years old	N	LA 	\N	DEM|AGE:6	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\6 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\6 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\5 years old\\	5 years old	N	LA 	\N	DEM|AGE:5	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\5 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\5 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\4 years old\\	4 years old	N	LA 	\N	DEM|AGE:4	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\4 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\4 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\3 years old\\	3 years old	N	LA 	\N	DEM|AGE:3	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\3 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\3 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\2 years old\\	2 years old	N	LA 	\N	DEM|AGE:2	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\2 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\2 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\1 years old\\	1 years old	N	LA 	\N	DEM|AGE:1	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\1 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\1 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
4	\\Demografische Daten\\Age\\0-9 years old\\0 years old\\	0 years old	N	LA 	\N	DEM|AGE:0	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Age\\0-9 years old\\0 years old\\	\N	\\Demografische Daten\\Age\\0-9 years old\\0 years old\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Demografische Daten\\Zip code\\	Zip code	N	LA 	\N	ZIP CODE:0	<?xml version="1.0"?><ValueMetadata>   <Version>3.02</Version>   <CreationDateTime>2020-03-14 14:54:52</CreationDateTime>   <TestID>Zip code</TestID>   <TestName>Zip code</TestName>   <DataType>Float</DataType>   <CodeType>XML_TOKEN_CODETYPE</CodeType>   <Loinc>XML_TOKEN_LOINC</Loinc>   <Flagstouse>HL</Flagstouse>   <Oktousevalues>Y</Oktousevalues>   <MaxStringLength/>   <LowofLowValue/>   <HighofLowValue/>   <LowofHighValue/>   <HighofHighValue/>   <LowofToxicValue/>   <HighofToxicValue/>   <EnumValues/>   <CommentsDeterminingExclusion>       <Com/>   </CommentsDeterminingExclusion>   <UnitValues>       <NormalUnits> </NormalUnits>       <EqualUnits> </EqualUnits>       <ExcludingUnits/>       <ConvertingUnits>           <Units/>           <MultiplyingFactor/>       </ConvertingUnits>   </UnitValues>   <Analysis>       <Enums />       <Counts />       <New />   </Analysis></ValueMetadata>	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Zip code\\	\N	\\Demografische Daten\\Zip code\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
2	\\Demografische Daten\\Gender\\	Gender	N	FA 	\N		\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Gender\\	\N	\\Demografische Daten\\Gender\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Gender\\Not Recorded\\	Not Recorded	N	LA 	\N	DEM|SEX:@	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Gender\\Not Recorded\\	\N	\\Demografische Daten\\Gender\\Not Recorded\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Gender\\Unknown\\	Unknown	N	LA 	\N	DEM|SEX:u	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Gender\\Unknown\\	\N	\\Demografische Daten\\Gender\\Unknown\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Gender\\NonBinary\\	NonBinary	N	LA 	\N	DEM|SEX:nb	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Gender\\NonBinary\\	\N	\\Demografische Daten\\Gender\\NonBinary\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Gender\\Female\\	Female	N	LA 	\N	DEM|SEX:f	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Gender\\Female\\	\N	\\Demografische Daten\\Gender\\Female\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
3	\\Demografische Daten\\Gender\\Male\\	Male	N	LA 	\N	DEM|SEX:m	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\Gender\\Male\\	\N	\\Demografische Daten\\Gender\\Male\\	@	2020-03-14 14:54:49.288	\N	\N	P21	\N	\N	\N	\N
\.


--
-- Data for Name: icd10_icd9; Type: TABLE DATA; Schema: i2b2metadata; Owner: i2b2metadata
--

COPY i2b2metadata.icd10_icd9 (c_hlevel, c_fullname, c_name, c_synonym_cd, c_visualattributes, c_totalnum, c_basecode, c_metadataxml, c_facttablecolumn, c_tablename, c_columnname, c_columndatatype, c_operator, c_dimcode, c_comment, c_tooltip, m_applied_path, update_date, download_date, import_date, sourcesystem_cd, valuetype_cd, m_exclusion_cd, c_path, c_symbol, plain_code) FROM stdin;
\.


--
-- Data for Name: ont_process_status; Type: TABLE DATA; Schema: i2b2metadata; Owner: i2b2metadata
--

COPY i2b2metadata.ont_process_status (process_id, process_type_cd, start_date, end_date, process_step_cd, process_status_cd, crc_upload_id, status_cd, message, entry_date, change_date, changedby_char) FROM stdin;
\.


--
-- Name: ont_process_status_process_id_seq; Type: SEQUENCE SET; Schema: i2b2metadata; Owner: i2b2metadata
--

SELECT pg_catalog.setval('i2b2metadata.ont_process_status_process_id_seq', 1, false);


--
-- Data for Name: schemes; Type: TABLE DATA; Schema: i2b2metadata; Owner: i2b2metadata
--

COPY i2b2metadata.schemes (c_key, c_name, c_description) FROM stdin;
\.


--
-- Data for Name: table_access; Type: TABLE DATA; Schema: i2b2metadata; Owner: i2b2metadata
--

COPY i2b2metadata.table_access (c_table_cd, c_table_name, c_protected_access, c_hlevel, c_fullname, c_name, c_synonym_cd, c_visualattributes, c_totalnum, c_basecode, c_metadataxml, c_facttablecolumn, c_dimtablename, c_columnname, c_columndatatype, c_operator, c_dimcode, c_comment, c_tooltip, c_entry_date, c_change_date, c_status_cd, valuetype_cd) FROM stdin;
i2b2	I2B2	N	1	\\DRG\\	DRG	N	FA 	\N	\N	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\DRG\\	\N	DRG	\N	\N	\N	\N
i2b2	I2B2	N	1	\\Fall\\	Fall	N	FA 	\N	\N	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Fall\\	\N	Fall	\N	\N	\N	\N
i2b2	I2B2	N	1	\\ICD-10\\	ICD-10	N	FA 	\N	\N	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\ICD-10\\	\N	ICD-10	\N	\N	\N	\N
i2b2	I2B2	N	1	\\Demografische Daten\\	Demografische Daten	N	FA 	\N	\N	\N	concept_cd	concept_dimension	concept_path	T	LIKE	\\Demografische Daten\\	\N	Demografische Daten	\N	\N	\N	\N
\.


--
-- Data for Name: pm_approvals; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_approvals (approval_id, approval_name, approval_description, approval_activation_date, approval_expiration_date, object_cd, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Data for Name: pm_approvals_params; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_approvals_params (id, approval_id, param_name_cd, value, activation_date, expiration_date, datatype_cd, object_cd, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Name: pm_approvals_params_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_approvals_params_id_seq', 1, false);


--
-- Data for Name: pm_cell_data; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_cell_data (cell_id, project_path, name, method_cd, url, can_override, change_date, entry_date, changeby_char, status_cd) FROM stdin;
CRC	/	Data Repository	REST	http://192.168.178.40:9099/i2b2/services/QueryToolService/	1	\N	\N	\N	A
FRC	/	File Repository 	SOAP	http://192.168.178.40:9099/i2b2/services/FRService/	1	\N	\N	\N	A
IM	/	IM Cell	REST	http://192.168.178.40:9099/i2b2/services/IMService/	1	\N	\N	\N	A
ONT	/	Ontology Cell	REST	http://192.168.178.40:9099/i2b2/services/OntologyService/	1	\N	\N	\N	A
WORK	/	Workplace Cell	REST	http://192.168.178.40:9099/i2b2/services/WorkplaceService/	1	\N	\N	\N	A
\.


--
-- Data for Name: pm_cell_params; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_cell_params (id, datatype_cd, cell_id, project_path, param_name_cd, value, can_override, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Name: pm_cell_params_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_cell_params_id_seq', 1, false);


--
-- Data for Name: pm_global_params; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_global_params (id, datatype_cd, param_name_cd, project_path, value, can_override, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Name: pm_global_params_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_global_params_id_seq', 1, false);


--
-- Data for Name: pm_hive_data; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_hive_data (domain_id, helpurl, domain_name, environment_cd, active, change_date, entry_date, changeby_char, status_cd) FROM stdin;
i2b2	http://www.i2b2.org	i2b2demo	DEVELOPMENT	1	\N	\N	\N	A
\.


--
-- Data for Name: pm_hive_params; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_hive_params (id, datatype_cd, domain_id, param_name_cd, value, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Name: pm_hive_params_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_hive_params_id_seq', 1, false);


--
-- Data for Name: pm_project_data; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_project_data (project_id, project_name, project_wiki, project_key, project_path, project_description, change_date, entry_date, changeby_char, status_cd) FROM stdin;
Demo	i2b2 Demo	http://www.i2b2.org	\N	/Demo	\N	\N	\N	\N	A
\.


--
-- Data for Name: pm_project_params; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_project_params (id, datatype_cd, project_id, param_name_cd, value, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Name: pm_project_params_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_project_params_id_seq', 1, false);


--
-- Data for Name: pm_project_request; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_project_request (id, title, request_xml, change_date, entry_date, changeby_char, status_cd, project_id, submit_char) FROM stdin;
\.


--
-- Name: pm_project_request_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_project_request_id_seq', 1, false);


--
-- Data for Name: pm_project_user_params; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_project_user_params (id, datatype_cd, project_id, user_id, param_name_cd, value, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Name: pm_project_user_params_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_project_user_params_id_seq', 1, false);


--
-- Data for Name: pm_project_user_roles; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_project_user_roles (project_id, user_id, user_role_cd, change_date, entry_date, changeby_char, status_cd) FROM stdin;
@	i2b2	ADMIN	\N	\N	\N	A
Demo	AGG_SERVICE_ACCOUNT	USER	\N	\N	\N	A
Demo	AGG_SERVICE_ACCOUNT	MANAGER	\N	\N	\N	A
Demo	AGG_SERVICE_ACCOUNT	DATA_OBFSC	\N	\N	\N	A
Demo	AGG_SERVICE_ACCOUNT	DATA_AGG	\N	\N	\N	A
Demo	i2b2	MANAGER	\N	\N	\N	A
Demo	i2b2	USER	\N	\N	\N	A
Demo	i2b2	DATA_OBFSC	\N	\N	\N	A
Demo	demo	USER	\N	\N	\N	A
Demo	demo	DATA_DEID	\N	\N	\N	A
Demo	demo	DATA_OBFSC	\N	\N	\N	A
Demo	demo	DATA_AGG	\N	\N	\N	A
Demo	demo	DATA_LDS	\N	\N	\N	A
Demo	demo	EDITOR	\N	\N	\N	A
Demo	demo	DATA_PROT	\N	\N	\N	A
\.


--
-- Data for Name: pm_role_requirement; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_role_requirement (table_cd, column_cd, read_hivemgmt_cd, write_hivemgmt_cd, name_char, change_date, entry_date, changeby_char, status_cd) FROM stdin;
PM_HIVE_DATA	@	@	ADMIN	\N	\N	\N	\N	A
PM_HIVE_PARAMS	@	@	ADMIN	\N	\N	\N	\N	A
PM_PROJECT_DATA	@	@	MANAGER	\N	\N	\N	\N	A
PM_PROJECT_USER_ROLES	@	@	MANAGER	\N	\N	\N	\N	A
PM_USER_DATA	@	@	ADMIN	\N	\N	\N	\N	A
PM_PROJECT_PARAMS	@	@	MANAGER	\N	\N	\N	\N	A
PM_PROJECT_USER_PARAMS	@	@	MANAGER	\N	\N	\N	\N	A
PM_USER_PARAMS	@	@	ADMIN	\N	\N	\N	\N	A
PM_CELL_DATA	@	@	MANAGER	\N	\N	\N	\N	A
PM_CELL_PARAMS	@	@	MANAGER	\N	\N	\N	\N	A
PM_GLOBAL_PARAMS	@	@	ADMIN	\N	\N	\N	\N	A
\.


--
-- Data for Name: pm_user_data; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_user_data (user_id, full_name, password, email, project_path, change_date, entry_date, changeby_char, status_cd) FROM stdin;
demo	i2b2 User	1F4VCh9lgYFsDyDgXt8tgf+4xONQ2sba32m+i5wa3AZzEM4Cv2XtlM3+vRbcehOs08AUfI6ZBdKtm7j0ew2bIA	\N	\N	\N	2020-03-14 13:53:00.236902	\N	A
i2b2	i2b2 Admin	zdl1c1JGsSuXuwsLq8ZQtrqXCe1fQ+uIU47IeE6CKmfetqHCUYANfJT+ihny/gS5qQcws7OmpoiZLw4/iNFNog	\N	\N	\N	2020-03-14 13:53:00.62084	\N	A
AGG_SERVICE_ACCOUNT	AGG_SERVICE_ACCOUNT	ME/kcbjPuOZv2LwNcsXl5tLCFHtZvjdf2Nx+F86V6ghlig+chZOkD4Vw/mPJMmcAGsHNs/cVooC4zinV54CXCw	\N	\N	\N	2020-03-14 13:53:01.087418	\N	A
\.


--
-- Data for Name: pm_user_login; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_user_login (user_id, attempt_cd, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Data for Name: pm_user_params; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_user_params (id, datatype_cd, user_id, param_name_cd, value, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Name: pm_user_params_id_seq; Type: SEQUENCE SET; Schema: i2b2pm; Owner: i2b2pm
--

SELECT pg_catalog.setval('i2b2pm.pm_user_params_id_seq', 1, false);


--
-- Data for Name: pm_user_session; Type: TABLE DATA; Schema: i2b2pm; Owner: i2b2pm
--

COPY i2b2pm.pm_user_session (user_id, session_id, expired_date, change_date, entry_date, changeby_char, status_cd) FROM stdin;
\.


--
-- Data for Name: workplace; Type: TABLE DATA; Schema: i2b2workdata; Owner: i2b2workdata
--

COPY i2b2workdata.workplace (c_name, c_user_id, c_group_id, c_share_id, c_index, c_parent_index, c_visualattributes, c_protected_access, c_tooltip, c_work_xml, c_work_xml_schema, c_work_xml_i2b2_type, c_entry_date, c_change_date, c_status_cd) FROM stdin;
\.


--
-- Data for Name: workplace_access; Type: TABLE DATA; Schema: i2b2workdata; Owner: i2b2workdata
--

COPY i2b2workdata.workplace_access (c_table_cd, c_table_name, c_protected_access, c_hlevel, c_name, c_user_id, c_group_id, c_share_id, c_index, c_parent_index, c_visualattributes, c_tooltip, c_entry_date, c_change_date, c_status_cd) FROM stdin;
\.


--
-- Data for Name: batch_job_execution; Type: TABLE DATA; Schema: ts_batch; Owner: postgres
--

COPY ts_batch.batch_job_execution (job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location) FROM stdin;
1	2	1	2020-03-14 14:54:58.955	2020-03-14 14:54:59.041	2020-03-14 14:55:05.836	COMPLETED	COMPLETED		2020-03-14 14:55:05.837	\N
\.


--
-- Data for Name: batch_job_execution_context; Type: TABLE DATA; Schema: ts_batch; Owner: postgres
--

COPY ts_batch.batch_job_execution_context (job_execution_id, short_context, serialized_context) FROM stdin;
1	{"map":[{"entry":{"string":"dimensionsStore","org.transmartproject.batch.i2b2.dimensions.DimensionsStore":{"table":{"@class":"com.google.common.collect.TreeBasedTable","backingMap":[{"@class":"tree-map","comparator":{"@class":"com.google.common.collect.NaturalOrdering"},"entry":[{"string":"PAT","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":1,"list":[{"string":19,"boolean":[true,false]}]},{"string":2,"list":[{"string":18,"boolean":[true,false]}]},{"string":3,"list":[{"string":17,"boolean":[true,false]}]}]}]},{"string":"PRO","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":{"string":"Provider for P21","list":[{"null":"","boolean":[false,false]}]}}]},{"string":"VIS","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":"1@2009-01-15","list":[{"string":[10,1],"boolean":[false,false]}]},{"string":20393,"list":[{"string":[9,2],"boolean":[true,false]}]},{"string":26284,"list":[{"string":[8,2],"boolean":[true,false]}]},{"string":29340,"list":[{"string":[7,1],"boolean":[true,false]} ...	{"map":[{"entry":{"string":"dimensionsStore","org.transmartproject.batch.i2b2.dimensions.DimensionsStore":{"table":{"@class":"com.google.common.collect.TreeBasedTable","backingMap":[{"@class":"tree-map","comparator":{"@class":"com.google.common.collect.NaturalOrdering"},"entry":[{"string":"PAT","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":1,"list":[{"string":19,"boolean":[true,false]}]},{"string":2,"list":[{"string":18,"boolean":[true,false]}]},{"string":3,"list":[{"string":17,"boolean":[true,false]}]}]}]},{"string":"PRO","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":{"string":"Provider for P21","list":[{"null":"","boolean":[false,false]}]}}]},{"string":"VIS","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":"1@2009-01-15","list":[{"string":[10,1],"boolean":[false,false]}]},{"string":20393,"list":[{"string":[9,2],"boolean":[true,false]}]},{"string":26284,"list":[{"string":[8,2],"boolean":[true,false]}]},{"string":29340,"list":[{"string":[7,1],"boolean":[true,false]}]},{"string":29595,"list":[{"string":[6,1],"boolean":[true,false]}]},{"string":"2@2009-03-10","list":[{"string":[5,2],"boolean":[false,false]}]},{"string":31176,"list":[{"string":[4,3],"boolean":[true,false]}]},{"string":"3@2009-03-03","list":[{"string":[3,3],"boolean":[false,false]}]},{"string":43014,"list":[{"string":[2,3],"boolean":[true,false]}]},{"string":46548,"list":[{"string":[1,2],"boolean":[true,false]}]}]}]}]}],"factory":{"@class":"com.google.common.collect.TreeBasedTable$Factory","comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/backingMap\\/comparator"}},"columnComparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/backingMap\\/comparator"}}}}}]}
\.


--
-- Data for Name: batch_job_execution_params; Type: TABLE DATA; Schema: ts_batch; Owner: postgres
--

COPY ts_batch.batch_job_execution_params (job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying) FROM stdin;
1	STRING	PROVIDER_PATH	/	1970-01-01 01:00:00	0	0	Y
1	STRING	DATE_FORMAT	yyyy-MM-dd'T'HH:mm:ss	1970-01-01 01:00:00	0	0	Y
1	STRING	COLUMN_MAP_FILE	C:\\dev\\eclipse_workspaces\\promotion\\component-etl\\examples\\p21-minimal\\i2b2\\column_map.tsv	1970-01-01 01:00:00	0	0	Y
1	STRING	VISIT_IDE_SOURCE	UNSPECIFIED	1970-01-01 01:00:00	0	0	Y
1	DATE	run.date		2020-03-14 14:54:58.886	0	0	Y
1	STRING	CRC_SCHEMA	i2b2demodata	1970-01-01 01:00:00	0	0	Y
1	STRING	PROJECT_ID	default	1970-01-01 01:00:00	0	0	Y
1	STRING	SOURCE_SYSTEM	P21	1970-01-01 01:00:00	0	0	Y
1	STRING	PATIENT_IDE_SOURCE	UNSPECIFIED	1970-01-01 01:00:00	0	0	Y
1	STRING	INCREMENTAL	N	1970-01-01 01:00:00	0	0	Y
\.


--
-- Name: batch_job_execution_seq; Type: SEQUENCE SET; Schema: ts_batch; Owner: postgres
--

SELECT pg_catalog.setval('ts_batch.batch_job_execution_seq', 1, true);


--
-- Data for Name: batch_job_instance; Type: TABLE DATA; Schema: ts_batch; Owner: postgres
--

COPY ts_batch.batch_job_instance (job_instance_id, version, job_name, job_key) FROM stdin;
1	0	I2b2Job	e350aaae959ed87c6ad173a58e1481ff
\.


--
-- Name: batch_job_seq; Type: SEQUENCE SET; Schema: ts_batch; Owner: postgres
--

SELECT pg_catalog.setval('ts_batch.batch_job_seq', 1, true);


--
-- Data for Name: batch_step_execution; Type: TABLE DATA; Schema: ts_batch; Owner: postgres
--

COPY ts_batch.batch_step_execution (step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated) FROM stdin;
5	3	deleteI2b2Data	1	2020-03-14 14:55:04.328	2020-03-14 14:55:04.369	COMPLETED	1	0	0	0	0	0	0	0	COMPLETED		2020-03-14 14:55:04.369
6	3	assignDimensionIds	1	2020-03-14 14:55:04.4	2020-03-14 14:55:04.502	COMPLETED	1	0	0	13	0	0	0	0	COMPLETED		2020-03-14 14:55:04.502
7	3	insertProviders	1	2020-03-14 14:55:04.626	2020-03-14 14:55:04.81	COMPLETED	1	1	0	1	0	0	0	0	COMPLETED		2020-03-14 14:55:04.81
9	3	insertVisits	1	2020-03-14 14:55:04.624	2020-03-14 14:55:04.891	COMPLETED	1	10	0	10	0	0	0	0	COMPLETED		2020-03-14 14:55:04.891
8	3	insertPatients	1	2020-03-14 14:55:04.622	2020-03-14 14:55:04.892	COMPLETED	1	3	0	3	0	0	0	0	COMPLETED		2020-03-14 14:55:04.892
1	99	readVariables	1	2020-03-14 14:54:59.288	2020-03-14 14:55:03.286	COMPLETED	97	480	0	480	0	0	0	0	COMPLETED		2020-03-14 14:55:03.286
2	3	readWordMappings	1	2020-03-14 14:55:03.32	2020-03-14 14:55:03.362	COMPLETED	1	0	0	0	0	0	0	0	COMPLETED		2020-03-14 14:55:03.362
10	3	secondPass	1	2020-03-14 14:55:04.974	2020-03-14 14:55:05.782	COMPLETED	1	287	0	287	0	0	0	0	COMPLETED		2020-03-14 14:55:05.782
3	9	loadCodeLookups	1	2020-03-14 14:55:03.39	2020-03-14 14:55:03.544	COMPLETED	7	5	0	0	0	0	0	0	COMPLETED		2020-03-14 14:55:03.544
4	3	firstPass	1	2020-03-14 14:55:03.665	2020-03-14 14:55:04.25	COMPLETED	1	1442	0	1442	0	0	0	0	COMPLETED		2020-03-14 14:55:04.25
\.


--
-- Data for Name: batch_step_execution_context; Type: TABLE DATA; Schema: ts_batch; Owner: postgres
--

COPY ts_batch.batch_step_execution_context (step_execution_id, short_context, serialized_context) FROM stdin;
5	{"map":[{"entry":[{"string":["batch.taskletType","com.sun.proxy.$Proxy35"]},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
6	{"map":[{"entry":[{"string":["batch.taskletType","org.transmartproject.batch.i2b2.dimensions.AssignDimensionIdsTasklet"]},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
1	{"map":[{"entry":[{"string":["batch.taskletType","org.springframework.batch.core.step.item.ChunkOrientedTasklet"]},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
10	{"map":[{"entry":[{"string":"MultiResourceItemReader.resourceIndex","int":46},{"string":["batch.taskletType","org.springframework.batch.core.step.item.ChunkOrientedTasklet"]},{"string":"FlatFileItemReader.read.count","int":42},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
2	{"map":[{"entry":[{"string":["batch.taskletType","org.springframework.batch.core.step.item.ChunkOrientedTasklet"]},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
3	{"map":[{"entry":[{"string":["batch.taskletType","com.sun.proxy.$Proxy44"]},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
4	{"map":[{"entry":[{"string":"MultiResourceItemReader.resourceIndex","int":46},{"string":"I2b2FirstPassSplittingReader.upstreamPos","int":42},{"string":"dimensionsStore","org.transmartproject.batch.i2b2.dimensions.DimensionsStore":{"table":{"@class":"com.google.common.collect.TreeBasedTable","backingMap":[{"@class":"tree-map","comparator":{"@class":"com.google.common.collect.NaturalOrdering"},"entry":[{"string":"PAT","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":1,"list":[{"null":"","boolean":[true,false]}]},{"string":2,"list":[{"null":"","boolean":[true,false]}]},{"string":3,"list":[{"null":"","boolean":[true,false]}]}]}]},{"string":"PRO","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":{"string":"Provider for P21","list":[{"null":"","boolean":[false,false]}]}}]},{"string":"VIS","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":"1@2009-01-15","list":[{"null":"","boolean":[false,false],"string":1}]},{"string":20393,"list":[{"null":"","boolean":[true,false],"string":2}]}, ...	{"map":[{"entry":[{"string":"MultiResourceItemReader.resourceIndex","int":46},{"string":"I2b2FirstPassSplittingReader.upstreamPos","int":42},{"string":"dimensionsStore","org.transmartproject.batch.i2b2.dimensions.DimensionsStore":{"table":{"@class":"com.google.common.collect.TreeBasedTable","backingMap":[{"@class":"tree-map","comparator":{"@class":"com.google.common.collect.NaturalOrdering"},"entry":[{"string":"PAT","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":1,"list":[{"null":"","boolean":[true,false]}]},{"string":2,"list":[{"null":"","boolean":[true,false]}]},{"string":3,"list":[{"null":"","boolean":[true,false]}]}]}]},{"string":"PRO","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":{"string":"Provider for P21","list":[{"null":"","boolean":[false,false]}]}}]},{"string":"VIS","tree-map":[{"comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/..\\/comparator"},"entry":[{"string":"1@2009-01-15","list":[{"null":"","boolean":[false,false],"string":1}]},{"string":20393,"list":[{"null":"","boolean":[true,false],"string":2}]},{"string":26284,"list":[{"null":"","boolean":[true,false],"string":2}]},{"string":29340,"list":[{"null":"","boolean":[true,false],"string":1}]},{"string":29595,"list":[{"null":"","boolean":[true,false],"string":1}]},{"string":"2@2009-03-10","list":[{"null":"","boolean":[false,false],"string":2}]},{"string":31176,"list":[{"null":"","boolean":[true,false],"string":3}]},{"string":"3@2009-03-03","list":[{"null":"","boolean":[false,false],"string":3}]},{"string":43014,"list":[{"null":"","boolean":[true,false],"string":3}]},{"string":46548,"list":[{"null":"","boolean":[true,false],"string":2}]}]}]}]}],"factory":{"@class":"com.google.common.collect.TreeBasedTable$Factory","comparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/..\\/backingMap\\/comparator"}},"columnComparator":{"@class":"com.google.common.collect.NaturalOrdering","@reference":"..\\/backingMap\\/comparator"}}}},{"string":["batch.taskletType","org.springframework.batch.core.step.item.ChunkOrientedTasklet"]},{"string":"I2b2FirstPassSplittingReader.position","int":0},{"string":"FlatFileItemReader.read.count","int":42},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}
7	{"map":[{"entry":[{"string":["batch.taskletType","org.springframework.batch.core.step.item.ChunkOrientedTasklet"]},{"string":"insertProvidersReader.read.count","int":2},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
9	{"map":[{"entry":[{"string":["batch.taskletType","org.springframework.batch.core.step.item.ChunkOrientedTasklet"]},{"string":"insertVisitsReader.read.count","int":11},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
8	{"map":[{"entry":[{"string":"insertPatientsReader.read.count","int":4},{"string":["batch.taskletType","org.springframework.batch.core.step.item.ChunkOrientedTasklet"]},{"string":["batch.stepType","org.springframework.batch.core.step.tasklet.TaskletStep"]}]}]}	\N
\.


--
-- Name: batch_step_execution_seq; Type: SEQUENCE SET; Schema: ts_batch; Owner: postgres
--

SELECT pg_catalog.setval('ts_batch.batch_step_execution_seq', 10, true);


--
-- Name: analysis_plugin_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_analysis_plugin
    ADD CONSTRAINT analysis_plugin_pk PRIMARY KEY (plugin_id);


--
-- Name: analysis_plugin_result_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_analysis_plugin_result_type
    ADD CONSTRAINT analysis_plugin_result_pk PRIMARY KEY (plugin_id, result_type_id);


--
-- Name: code_lookup_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.code_lookup
    ADD CONSTRAINT code_lookup_pk PRIMARY KEY (table_cd, column_cd, code_cd);


--
-- Name: concept_dimension_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.concept_dimension
    ADD CONSTRAINT concept_dimension_pk PRIMARY KEY (concept_path);


--
-- Name: encounter_mapping_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.encounter_mapping
    ADD CONSTRAINT encounter_mapping_pk PRIMARY KEY (encounter_ide, encounter_ide_source, project_id, patient_ide, patient_ide_source);


--
-- Name: modifier_dimension_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.modifier_dimension
    ADD CONSTRAINT modifier_dimension_pk PRIMARY KEY (modifier_path);


--
-- Name: observation_fact_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.observation_fact
    ADD CONSTRAINT observation_fact_pk PRIMARY KEY (patient_num, concept_cd, modifier_cd, start_date, encounter_num, instance_num, provider_id);


--
-- Name: patient_dimension_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.patient_dimension
    ADD CONSTRAINT patient_dimension_pk PRIMARY KEY (patient_num);


--
-- Name: patient_mapping_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.patient_mapping
    ADD CONSTRAINT patient_mapping_pk PRIMARY KEY (patient_ide, patient_ide_source, project_id);


--
-- Name: pk_sourcemaster_sourcecd; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.source_master
    ADD CONSTRAINT pk_sourcemaster_sourcecd PRIMARY KEY (source_cd);


--
-- Name: pk_st_id; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.set_type
    ADD CONSTRAINT pk_st_id PRIMARY KEY (id);


--
-- Name: pk_up_upstatus_idsettypeid; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.set_upload_status
    ADD CONSTRAINT pk_up_upstatus_idsettypeid PRIMARY KEY (upload_id, set_type_id);


--
-- Name: provider_dimension_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.provider_dimension
    ADD CONSTRAINT provider_dimension_pk PRIMARY KEY (provider_path, provider_id);


--
-- Name: qt_patient_enc_collection_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_patient_enc_collection
    ADD CONSTRAINT qt_patient_enc_collection_pkey PRIMARY KEY (patient_enc_coll_id);


--
-- Name: qt_patient_set_collection_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_patient_set_collection
    ADD CONSTRAINT qt_patient_set_collection_pkey PRIMARY KEY (patient_set_coll_id);


--
-- Name: qt_pdo_query_master_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_pdo_query_master
    ADD CONSTRAINT qt_pdo_query_master_pkey PRIMARY KEY (query_master_id);


--
-- Name: qt_privilege_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_privilege
    ADD CONSTRAINT qt_privilege_pkey PRIMARY KEY (protection_label_cd);


--
-- Name: qt_query_instance_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_instance
    ADD CONSTRAINT qt_query_instance_pkey PRIMARY KEY (query_instance_id);


--
-- Name: qt_query_master_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_master
    ADD CONSTRAINT qt_query_master_pkey PRIMARY KEY (query_master_id);


--
-- Name: qt_query_result_instance_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_result_instance
    ADD CONSTRAINT qt_query_result_instance_pkey PRIMARY KEY (result_instance_id);


--
-- Name: qt_query_result_type_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_result_type
    ADD CONSTRAINT qt_query_result_type_pkey PRIMARY KEY (result_type_id);


--
-- Name: qt_query_status_type_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_status_type
    ADD CONSTRAINT qt_query_status_type_pkey PRIMARY KEY (status_type_id);


--
-- Name: qt_xml_result_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_xml_result
    ADD CONSTRAINT qt_xml_result_pkey PRIMARY KEY (xml_result_id);


--
-- Name: upload_status_pkey; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.upload_status
    ADD CONSTRAINT upload_status_pkey PRIMARY KEY (upload_id);


--
-- Name: visit_dimension_pk; Type: CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.visit_dimension
    ADD CONSTRAINT visit_dimension_pk PRIMARY KEY (encounter_num, patient_num);


--
-- Name: analsis_job_pk; Type: CONSTRAINT; Schema: i2b2hive; Owner: i2b2hive
--

ALTER TABLE ONLY i2b2hive.crc_analysis_job
    ADD CONSTRAINT analsis_job_pk PRIMARY KEY (job_id);


--
-- Name: crc_db_lookup_pk; Type: CONSTRAINT; Schema: i2b2hive; Owner: i2b2hive
--

ALTER TABLE ONLY i2b2hive.crc_db_lookup
    ADD CONSTRAINT crc_db_lookup_pk PRIMARY KEY (c_domain_id, c_project_path, c_owner_id);


--
-- Name: im_db_lookup_pk; Type: CONSTRAINT; Schema: i2b2hive; Owner: i2b2hive
--

ALTER TABLE ONLY i2b2hive.im_db_lookup
    ADD CONSTRAINT im_db_lookup_pk PRIMARY KEY (c_domain_id, c_project_path, c_owner_id);


--
-- Name: ont_db_lookup_pk; Type: CONSTRAINT; Schema: i2b2hive; Owner: i2b2hive
--

ALTER TABLE ONLY i2b2hive.ont_db_lookup
    ADD CONSTRAINT ont_db_lookup_pk PRIMARY KEY (c_domain_id, c_project_path, c_owner_id);


--
-- Name: work_db_lookup_pk; Type: CONSTRAINT; Schema: i2b2hive; Owner: i2b2hive
--

ALTER TABLE ONLY i2b2hive.work_db_lookup
    ADD CONSTRAINT work_db_lookup_pk PRIMARY KEY (c_domain_id, c_project_path, c_owner_id);


--
-- Name: im_mpi_demographics_pk; Type: CONSTRAINT; Schema: i2b2imdata; Owner: i2b2imdata
--

ALTER TABLE ONLY i2b2imdata.im_mpi_demographics
    ADD CONSTRAINT im_mpi_demographics_pk PRIMARY KEY (global_id);


--
-- Name: im_mpi_mapping_pk; Type: CONSTRAINT; Schema: i2b2imdata; Owner: i2b2imdata
--

ALTER TABLE ONLY i2b2imdata.im_mpi_mapping
    ADD CONSTRAINT im_mpi_mapping_pk PRIMARY KEY (lcl_site, lcl_id, update_date);


--
-- Name: im_project_patients_pk; Type: CONSTRAINT; Schema: i2b2imdata; Owner: i2b2imdata
--

ALTER TABLE ONLY i2b2imdata.im_project_patients
    ADD CONSTRAINT im_project_patients_pk PRIMARY KEY (project_id, global_id);


--
-- Name: im_project_sites_pk; Type: CONSTRAINT; Schema: i2b2imdata; Owner: i2b2imdata
--

ALTER TABLE ONLY i2b2imdata.im_project_sites
    ADD CONSTRAINT im_project_sites_pk PRIMARY KEY (project_id, lcl_site);


--
-- Name: ont_process_status_pkey; Type: CONSTRAINT; Schema: i2b2metadata; Owner: i2b2metadata
--

ALTER TABLE ONLY i2b2metadata.ont_process_status
    ADD CONSTRAINT ont_process_status_pkey PRIMARY KEY (process_id);


--
-- Name: schemes_pk; Type: CONSTRAINT; Schema: i2b2metadata; Owner: i2b2metadata
--

ALTER TABLE ONLY i2b2metadata.schemes
    ADD CONSTRAINT schemes_pk PRIMARY KEY (c_key);


--
-- Name: pm_approvals_params_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_approvals_params
    ADD CONSTRAINT pm_approvals_params_pkey PRIMARY KEY (id);


--
-- Name: pm_cell_data_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_cell_data
    ADD CONSTRAINT pm_cell_data_pkey PRIMARY KEY (cell_id, project_path);


--
-- Name: pm_cell_params_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_cell_params
    ADD CONSTRAINT pm_cell_params_pkey PRIMARY KEY (id);


--
-- Name: pm_global_params_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_global_params
    ADD CONSTRAINT pm_global_params_pkey PRIMARY KEY (id);


--
-- Name: pm_hive_data_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_hive_data
    ADD CONSTRAINT pm_hive_data_pkey PRIMARY KEY (domain_id);


--
-- Name: pm_hive_params_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_hive_params
    ADD CONSTRAINT pm_hive_params_pkey PRIMARY KEY (id);


--
-- Name: pm_project_data_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_data
    ADD CONSTRAINT pm_project_data_pkey PRIMARY KEY (project_id);


--
-- Name: pm_project_params_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_params
    ADD CONSTRAINT pm_project_params_pkey PRIMARY KEY (id);


--
-- Name: pm_project_request_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_request
    ADD CONSTRAINT pm_project_request_pkey PRIMARY KEY (id);


--
-- Name: pm_project_user_params_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_user_params
    ADD CONSTRAINT pm_project_user_params_pkey PRIMARY KEY (id);


--
-- Name: pm_project_user_roles_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_project_user_roles
    ADD CONSTRAINT pm_project_user_roles_pkey PRIMARY KEY (project_id, user_id, user_role_cd);


--
-- Name: pm_role_requirement_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_role_requirement
    ADD CONSTRAINT pm_role_requirement_pkey PRIMARY KEY (table_cd, column_cd, read_hivemgmt_cd, write_hivemgmt_cd);


--
-- Name: pm_user_data_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_user_data
    ADD CONSTRAINT pm_user_data_pkey PRIMARY KEY (user_id);


--
-- Name: pm_user_login_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_user_login
    ADD CONSTRAINT pm_user_login_pkey PRIMARY KEY (entry_date, user_id);


--
-- Name: pm_user_params_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_user_params
    ADD CONSTRAINT pm_user_params_pkey PRIMARY KEY (id);


--
-- Name: pm_user_session_pkey; Type: CONSTRAINT; Schema: i2b2pm; Owner: i2b2pm
--

ALTER TABLE ONLY i2b2pm.pm_user_session
    ADD CONSTRAINT pm_user_session_pkey PRIMARY KEY (session_id, user_id);


--
-- Name: workplace_access_pk; Type: CONSTRAINT; Schema: i2b2workdata; Owner: i2b2workdata
--

ALTER TABLE ONLY i2b2workdata.workplace_access
    ADD CONSTRAINT workplace_access_pk PRIMARY KEY (c_index);


--
-- Name: workplace_pk; Type: CONSTRAINT; Schema: i2b2workdata; Owner: i2b2workdata
--

ALTER TABLE ONLY i2b2workdata.workplace
    ADD CONSTRAINT workplace_pk PRIMARY KEY (c_index);


--
-- Name: batch_job_execution_context_pkey; Type: CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_job_execution_context
    ADD CONSTRAINT batch_job_execution_context_pkey PRIMARY KEY (job_execution_id);


--
-- Name: batch_job_execution_pkey; Type: CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_job_execution
    ADD CONSTRAINT batch_job_execution_pkey PRIMARY KEY (job_execution_id);


--
-- Name: batch_job_instance_pkey; Type: CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_job_instance
    ADD CONSTRAINT batch_job_instance_pkey PRIMARY KEY (job_instance_id);


--
-- Name: batch_step_execution_context_pkey; Type: CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_step_execution_context
    ADD CONSTRAINT batch_step_execution_context_pkey PRIMARY KEY (step_execution_id);


--
-- Name: batch_step_execution_pkey; Type: CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_step_execution
    ADD CONSTRAINT batch_step_execution_pkey PRIMARY KEY (step_execution_id);


--
-- Name: job_inst_un; Type: CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_job_instance
    ADD CONSTRAINT job_inst_un UNIQUE (job_name, job_key);


--
-- Name: cd_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX cd_idx_uploadid ON i2b2demodata.concept_dimension USING btree (upload_id);


--
-- Name: cl_idx_name_char; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX cl_idx_name_char ON i2b2demodata.code_lookup USING btree (name_char);


--
-- Name: cl_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX cl_idx_uploadid ON i2b2demodata.code_lookup USING btree (upload_id);


--
-- Name: em_encnum_idx; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX em_encnum_idx ON i2b2demodata.encounter_mapping USING btree (encounter_num);


--
-- Name: em_idx_encpath; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX em_idx_encpath ON i2b2demodata.encounter_mapping USING btree (encounter_ide, encounter_ide_source, patient_ide, patient_ide_source, encounter_num);


--
-- Name: em_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX em_idx_uploadid ON i2b2demodata.encounter_mapping USING btree (upload_id);


--
-- Name: md_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX md_idx_uploadid ON i2b2demodata.modifier_dimension USING btree (upload_id);


--
-- Name: of_idx_allobservation_fact; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX of_idx_allobservation_fact ON i2b2demodata.observation_fact USING btree (patient_num, encounter_num, concept_cd, start_date, provider_id, modifier_cd, instance_num, valtype_cd, tval_char, nval_num, valueflag_cd, quantity_num, units_cd, end_date, location_cd, confidence_num);


--
-- Name: of_idx_clusteredconcept; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX of_idx_clusteredconcept ON i2b2demodata.observation_fact USING btree (concept_cd);


--
-- Name: of_idx_encounter_patient; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX of_idx_encounter_patient ON i2b2demodata.observation_fact USING btree (encounter_num, patient_num, instance_num);


--
-- Name: of_idx_modifier; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX of_idx_modifier ON i2b2demodata.observation_fact USING btree (modifier_cd);


--
-- Name: of_idx_sourcesystem_cd; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX of_idx_sourcesystem_cd ON i2b2demodata.observation_fact USING btree (sourcesystem_cd);


--
-- Name: of_idx_start_date; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX of_idx_start_date ON i2b2demodata.observation_fact USING btree (start_date, patient_num);


--
-- Name: of_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX of_idx_uploadid ON i2b2demodata.observation_fact USING btree (upload_id);


--
-- Name: of_text_search_unique; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE UNIQUE INDEX of_text_search_unique ON i2b2demodata.observation_fact USING btree (text_search_index);


--
-- Name: pa_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pa_idx_uploadid ON i2b2demodata.patient_dimension USING btree (upload_id);


--
-- Name: pd_idx_allpatientdim; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pd_idx_allpatientdim ON i2b2demodata.patient_dimension USING btree (patient_num, vital_status_cd, birth_date, death_date, sex_cd, age_in_years_num, language_cd, race_cd, marital_status_cd, income_cd, religion_cd, zip_cd);


--
-- Name: pd_idx_dates; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pd_idx_dates ON i2b2demodata.patient_dimension USING btree (patient_num, vital_status_cd, birth_date, death_date);


--
-- Name: pd_idx_name_char; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pd_idx_name_char ON i2b2demodata.provider_dimension USING btree (provider_id, name_char);


--
-- Name: pd_idx_statecityzip; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pd_idx_statecityzip ON i2b2demodata.patient_dimension USING btree (statecityzip_path, patient_num);


--
-- Name: pd_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pd_idx_uploadid ON i2b2demodata.provider_dimension USING btree (upload_id);


--
-- Name: pk_archive_obsfact; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pk_archive_obsfact ON i2b2demodata.archive_observation_fact USING btree (encounter_num, patient_num, concept_cd, provider_id, start_date, modifier_cd, archive_upload_id);


--
-- Name: pm_encpnum_idx; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pm_encpnum_idx ON i2b2demodata.patient_mapping USING btree (patient_ide, patient_ide_source, patient_num);


--
-- Name: pm_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pm_idx_uploadid ON i2b2demodata.patient_mapping USING btree (upload_id);


--
-- Name: pm_patnum_idx; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX pm_patnum_idx ON i2b2demodata.patient_mapping USING btree (patient_num);


--
-- Name: qt_apnamevergrp_idx; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX qt_apnamevergrp_idx ON i2b2demodata.qt_analysis_plugin USING btree (plugin_name, version_cd, group_id);


--
-- Name: qt_idx_pqm_ugid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX qt_idx_pqm_ugid ON i2b2demodata.qt_pdo_query_master USING btree (user_id, group_id);


--
-- Name: qt_idx_qi_mstartid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX qt_idx_qi_mstartid ON i2b2demodata.qt_query_instance USING btree (query_master_id, start_date);


--
-- Name: qt_idx_qi_ugid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX qt_idx_qi_ugid ON i2b2demodata.qt_query_instance USING btree (user_id, group_id);


--
-- Name: qt_idx_qm_ugid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX qt_idx_qm_ugid ON i2b2demodata.qt_query_master USING btree (user_id, group_id, master_type_cd);


--
-- Name: qt_idx_qpsc_riid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX qt_idx_qpsc_riid ON i2b2demodata.qt_patient_set_collection USING btree (result_instance_id);


--
-- Name: vd_idx_allvisitdim; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX vd_idx_allvisitdim ON i2b2demodata.visit_dimension USING btree (encounter_num, patient_num, inout_cd, location_cd, start_date, length_of_stay, end_date);


--
-- Name: vd_idx_dates; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX vd_idx_dates ON i2b2demodata.visit_dimension USING btree (encounter_num, start_date, end_date);


--
-- Name: vd_idx_uploadid; Type: INDEX; Schema: i2b2demodata; Owner: i2b2demodata
--

CREATE INDEX vd_idx_uploadid ON i2b2demodata.visit_dimension USING btree (upload_id);


--
-- Name: crc_idx_aj_qnstid; Type: INDEX; Schema: i2b2hive; Owner: i2b2hive
--

CREATE INDEX crc_idx_aj_qnstid ON i2b2hive.crc_analysis_job USING btree (queue_name, status_type_id);


--
-- Name: meta_appl_path_icd10_icd9_idx; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_appl_path_icd10_icd9_idx ON i2b2metadata.icd10_icd9 USING btree (m_applied_path);


--
-- Name: meta_applied_path_idx_birn; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_applied_path_idx_birn ON i2b2metadata.birn USING btree (m_applied_path);


--
-- Name: meta_applied_path_idx_custom; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_applied_path_idx_custom ON i2b2metadata.custom_meta USING btree (m_applied_path);


--
-- Name: meta_applied_path_idx_i2b2; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_applied_path_idx_i2b2 ON i2b2metadata.i2b2 USING btree (m_applied_path);


--
-- Name: meta_exclusion_icd10_icd9_idx; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_exclusion_icd10_icd9_idx ON i2b2metadata.icd10_icd9 USING btree (m_exclusion_cd);


--
-- Name: meta_exclusion_idx_i2b2; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_exclusion_idx_i2b2 ON i2b2metadata.i2b2 USING btree (m_exclusion_cd);


--
-- Name: meta_fullname_idx_birn; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_fullname_idx_birn ON i2b2metadata.birn USING btree (c_fullname);


--
-- Name: meta_fullname_idx_custom; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_fullname_idx_custom ON i2b2metadata.custom_meta USING btree (c_fullname);


--
-- Name: meta_fullname_idx_i2b2; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_fullname_idx_i2b2 ON i2b2metadata.i2b2 USING btree (c_fullname);


--
-- Name: meta_fullname_idx_icd10_icd9; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_fullname_idx_icd10_icd9 ON i2b2metadata.icd10_icd9 USING btree (c_fullname);


--
-- Name: meta_hlevel_icd10_icd9_idx; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_hlevel_icd10_icd9_idx ON i2b2metadata.icd10_icd9 USING btree (c_hlevel);


--
-- Name: meta_hlevel_idx_i2b2; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_hlevel_idx_i2b2 ON i2b2metadata.i2b2 USING btree (c_hlevel);


--
-- Name: meta_synonym_icd10_icd9_idx; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_synonym_icd10_icd9_idx ON i2b2metadata.icd10_icd9 USING btree (c_synonym_cd);


--
-- Name: meta_synonym_idx_i2b2; Type: INDEX; Schema: i2b2metadata; Owner: i2b2metadata
--

CREATE INDEX meta_synonym_idx_i2b2 ON i2b2metadata.i2b2 USING btree (c_synonym_cd);


--
-- Name: fk_up_set_type_id; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.set_upload_status
    ADD CONSTRAINT fk_up_set_type_id FOREIGN KEY (set_type_id) REFERENCES i2b2demodata.set_type(id);


--
-- Name: qt_fk_pesc_ri; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_patient_enc_collection
    ADD CONSTRAINT qt_fk_pesc_ri FOREIGN KEY (result_instance_id) REFERENCES i2b2demodata.qt_query_result_instance(result_instance_id);


--
-- Name: qt_fk_psc_ri; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_patient_set_collection
    ADD CONSTRAINT qt_fk_psc_ri FOREIGN KEY (result_instance_id) REFERENCES i2b2demodata.qt_query_result_instance(result_instance_id);


--
-- Name: qt_fk_qi_mid; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_instance
    ADD CONSTRAINT qt_fk_qi_mid FOREIGN KEY (query_master_id) REFERENCES i2b2demodata.qt_query_master(query_master_id);


--
-- Name: qt_fk_qi_stid; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_instance
    ADD CONSTRAINT qt_fk_qi_stid FOREIGN KEY (status_type_id) REFERENCES i2b2demodata.qt_query_status_type(status_type_id);


--
-- Name: qt_fk_qri_rid; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_result_instance
    ADD CONSTRAINT qt_fk_qri_rid FOREIGN KEY (query_instance_id) REFERENCES i2b2demodata.qt_query_instance(query_instance_id);


--
-- Name: qt_fk_qri_rtid; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_result_instance
    ADD CONSTRAINT qt_fk_qri_rtid FOREIGN KEY (result_type_id) REFERENCES i2b2demodata.qt_query_result_type(result_type_id);


--
-- Name: qt_fk_qri_stid; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_query_result_instance
    ADD CONSTRAINT qt_fk_qri_stid FOREIGN KEY (status_type_id) REFERENCES i2b2demodata.qt_query_status_type(status_type_id);


--
-- Name: qt_fk_xmlr_riid; Type: FK CONSTRAINT; Schema: i2b2demodata; Owner: i2b2demodata
--

ALTER TABLE ONLY i2b2demodata.qt_xml_result
    ADD CONSTRAINT qt_fk_xmlr_riid FOREIGN KEY (result_instance_id) REFERENCES i2b2demodata.qt_query_result_instance(result_instance_id);


--
-- Name: job_exec_ctx_fk; Type: FK CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_job_execution_context
    ADD CONSTRAINT job_exec_ctx_fk FOREIGN KEY (job_execution_id) REFERENCES ts_batch.batch_job_execution(job_execution_id);


--
-- Name: job_exec_params_fk; Type: FK CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_job_execution_params
    ADD CONSTRAINT job_exec_params_fk FOREIGN KEY (job_execution_id) REFERENCES ts_batch.batch_job_execution(job_execution_id);


--
-- Name: job_exec_step_fk; Type: FK CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_step_execution
    ADD CONSTRAINT job_exec_step_fk FOREIGN KEY (job_execution_id) REFERENCES ts_batch.batch_job_execution(job_execution_id);


--
-- Name: job_inst_exec_fk; Type: FK CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_job_execution
    ADD CONSTRAINT job_inst_exec_fk FOREIGN KEY (job_instance_id) REFERENCES ts_batch.batch_job_instance(job_instance_id);


--
-- Name: step_exec_ctx_fk; Type: FK CONSTRAINT; Schema: ts_batch; Owner: postgres
--

ALTER TABLE ONLY ts_batch.batch_step_execution_context
    ADD CONSTRAINT step_exec_ctx_fk FOREIGN KEY (step_execution_id) REFERENCES ts_batch.batch_step_execution(step_execution_id);


--
-- Name: SCHEMA i2b2demodata; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA i2b2demodata FROM PUBLIC;
REVOKE ALL ON SCHEMA i2b2demodata FROM postgres;
GRANT ALL ON SCHEMA i2b2demodata TO postgres;
GRANT ALL ON SCHEMA i2b2demodata TO i2b2demodata;


--
-- Name: SCHEMA i2b2hive; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA i2b2hive FROM PUBLIC;
REVOKE ALL ON SCHEMA i2b2hive FROM postgres;
GRANT ALL ON SCHEMA i2b2hive TO postgres;
GRANT ALL ON SCHEMA i2b2hive TO i2b2hive;


--
-- Name: SCHEMA i2b2imdata; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA i2b2imdata FROM PUBLIC;
REVOKE ALL ON SCHEMA i2b2imdata FROM postgres;
GRANT ALL ON SCHEMA i2b2imdata TO postgres;
GRANT ALL ON SCHEMA i2b2imdata TO i2b2imdata;


--
-- Name: SCHEMA i2b2metadata; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA i2b2metadata FROM PUBLIC;
REVOKE ALL ON SCHEMA i2b2metadata FROM postgres;
GRANT ALL ON SCHEMA i2b2metadata TO postgres;
GRANT ALL ON SCHEMA i2b2metadata TO i2b2metadata;


--
-- Name: SCHEMA i2b2pm; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA i2b2pm FROM PUBLIC;
REVOKE ALL ON SCHEMA i2b2pm FROM postgres;
GRANT ALL ON SCHEMA i2b2pm TO postgres;
GRANT ALL ON SCHEMA i2b2pm TO i2b2pm;


--
-- Name: SCHEMA i2b2workdata; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA i2b2workdata FROM PUBLIC;
REVOKE ALL ON SCHEMA i2b2workdata FROM postgres;
GRANT ALL ON SCHEMA i2b2workdata TO postgres;
GRANT ALL ON SCHEMA i2b2workdata TO i2b2workdata;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

