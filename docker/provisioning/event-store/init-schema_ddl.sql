-----------------------------------------------------
-----------------------------------------------------
-- DDL                                             --
-----------------------------------------------------
-----------------------------------------------------

--DROP SCHEMA dq CASCADE;
CREATE SCHEMA dq AUTHORIZATION postgres;

-----------------------------------------------------
-- TABLE dq.screen_category                         -
-----------------------------------------------------
-- DROP TABLE dq.screen_category;
CREATE TABLE dq.screen_category (
	screen_subcategory_pk int NOT NULL,
	screen_category_name varchar(64) NOT NULL,
	screen_subcategory_name varchar(64) NOT NULL,
	CONSTRAINT screen_subcategory_pk PRIMARY KEY (screen_subcategory_pk),
	CONSTRAINT screen_subcategory_un UNIQUE (screen_category_name, screen_subcategory_name)
);
-- Permissions
ALTER TABLE dq.screen_category OWNER TO postgres;
GRANT ALL ON TABLE dq.screen_category TO postgres;

-----------------------------------------------------
-- TABLE dq.screen_dimension                        -
-----------------------------------------------------
-- DROP TABLE dq.screen_dimension;
CREATE TABLE dq.screen_dimension (
	screen_pk int NOT NULL,
	screen_subcategory_fk int NOT NULL,
	screen_name varchar(64) NOT NULL,
	screen_short_name varchar(64) NOT NULL,
	default_severity_score float8 NULL,
	exception_action varchar(32) NULL,
	CONSTRAINT screen_dimension_pk PRIMARY KEY (screen_pk),
	CONSTRAINT screen_dimension_category_fk FOREIGN KEY (screen_subcategory_fk) REFERENCES dq.screen_category(screen_subcategory_pk),
	CONSTRAINT screen_dimension_un UNIQUE (screen_short_name)
);
-- Permissions
ALTER TABLE dq.screen_dimension OWNER TO postgres;
GRANT ALL ON TABLE dq.screen_dimension TO postgres;

-----------------------------------------------------
-- TABLE dq.job_dimension                           -
-----------------------------------------------------
-- DROP TABLE dq.job_dimension;
CREATE TABLE dq.job_dimension (
	job_pk serial NOT NULL,
	job_name varchar(512) NOT NULL,
	job_start timestamptz NOT NULL,
	job_finish timestamptz NULL,
	source_system varchar(512) NOT NULL,
	phase varchar(16) NOT NULL,
	status varchar(16) NOT NULL,
	CONSTRAINT job_dimension_pk PRIMARY KEY (job_pk)
);
-- Permissions
ALTER TABLE dq.job_dimension OWNER TO postgres;
GRANT ALL ON TABLE dq.job_dimension TO postgres;

-----------------------------------------------------
-- TABLE dq.baseline_dimension                      -
-----------------------------------------------------
-- DROP TABLE dq.baseline_dimension;
CREATE TABLE dq.baseline_dimension (
	baseline_pk serial NOT NULL,
	job_fk int NOT NULL,
	entity_name varchar(512) NOT NULL,
	row_count int NOT NULL,
	CONSTRAINT baseline_dimension_pk PRIMARY KEY (baseline_pk),
	CONSTRAINT baseline_dimension_job_fk FOREIGN KEY (job_fk) REFERENCES dq.job_dimension(job_pk),
	CONSTRAINT baseline_dimension_un UNIQUE (job_fk, entity_name)
);
-- Permissions
ALTER TABLE dq.baseline_dimension OWNER TO postgres;
GRANT ALL ON TABLE dq.baseline_dimension TO postgres;

-----------------------------------------------------
-- TABLE dq.error_event_fact                              -
-----------------------------------------------------
-- DROP TABLE dq.error_event_fact;
CREATE TABLE dq.error_event_fact (
	event_pk bigserial NOT NULL,
	job_fk int NOT NULL,
	screen_fk int NOT NULL,
	source_entity_name varchar(512) NOT NULL,
	source_entity_key_attr varchar(128) NOT NULL,
	source_entity_key varchar(128) NOT NULL,
	source_entity_error_attr varchar(128) NOT NULL,
	source_entity_error_val varchar(128) NOT NULL,
	final_severity_score float8 NOT NULL,
	action_performed varchar(32) NOT NULL,
	info varchar(2048) NULL,
	CONSTRAINT error_event_fact_pk PRIMARY KEY (event_pk),
	CONSTRAINT error_event_fact_job_fk FOREIGN KEY (job_fk) REFERENCES dq.job_dimension(job_pk),
	CONSTRAINT error_event_fact_screen_fk FOREIGN KEY (screen_fk) REFERENCES dq.screen_dimension(screen_pk)
);
-- Permissions
ALTER TABLE dq.error_event_fact OWNER TO postgres;
GRANT ALL ON TABLE dq.error_event_fact TO postgres;

-----------------------------------------------------
-- TABLE dq.event_count_fact                        -
-----------------------------------------------------
-- DROP TABLE dq.event_count_fact;
CREATE TABLE dq.event_count_fact (
	event_pk bigserial NOT NULL,
	job_fk int NOT NULL,
	screen_fk int NOT NULL,
	entity_name varchar(512) NOT NULL,
	attr_name varchar(128) NOT NULL,
	passed boolean NOT NULL,
	row_count int NOT NULL,
	CONSTRAINT event_count_fact_pk PRIMARY KEY (event_pk),
	CONSTRAINT event_count_fact_job_fk FOREIGN KEY (job_fk) REFERENCES dq.job_dimension(job_pk),
	CONSTRAINT event_count_fact_screen_fk FOREIGN KEY (screen_fk) REFERENCES dq.screen_dimension(screen_pk),
	CONSTRAINT event_count_fact_un UNIQUE (job_fk, screen_fk, entity_name, attr_name, passed)
);
-- Permissions
ALTER TABLE dq.event_count_fact OWNER TO postgres;
GRANT ALL ON TABLE dq.event_count_fact TO postgres;

-----------------------------------------------------
-- TABLE dq.schema_version                          -
-----------------------------------------------------

-- DROP TABLE dq.schema_version;
CREATE TABLE dq.schema_version (
	init_date timestamptz,
	version varchar(32),
	CONSTRAINT schema_version_un UNIQUE (version)
);
-- Permissions
ALTER TABLE dq.schema_version OWNER TO postgres;
GRANT ALL ON TABLE dq.schema_version TO postgres;
INSERT INTO dq.schema_version (version, init_date) VALUES ('1.1.2-RELEASE', NULL);


-----------------------------------------------------
-- VIEW dq.v_screen_categories                      -
-----------------------------------------------------
CREATE OR REPLACE VIEW dq.v_screen_categories AS
SELECT 
    sc.screen_category_name,
    sc.screen_subcategory_name,
    sd.screen_pk,
    sd.screen_name,
    sc.screen_subcategory_pk
FROM dq.screen_category sc
  LEFT JOIN dq.screen_dimension sd ON sc.screen_subcategory_pk = sd.screen_subcategory_fk
ORDER BY sc.screen_category_name, sc.screen_subcategory_name;
-- Permissions
GRANT ALL ON TABLE dq.v_screen_categories TO postgres;

-----------------------------------------------------
-- VIEW dq.v_screen_count_breakdown                 -
-----------------------------------------------------
CREATE OR REPLACE VIEW dq.v_screen_count_breakdown AS
SELECT sc.screen_category_name,
  sc.screen_subcategory_name,
  event_breakdown.screen_name,
  event_breakdown.source_entity_name,
  event_breakdown.source_entity_error_attr,
  event_breakdown.count
FROM dq.screen_category sc
JOIN (
  SELECT
    job_screen_entity_attrname_combis.screen_name,
    job_screen_entity_attrname_combis.screen_pk,
    job_screen_entity_attrname_combis.screen_subcategory_fk,
    job_screen_entity_attrname_combis.job_pk,
    job_screen_entity_attrname_combis.source_entity_name,
    ef.source_entity_error_attr,
    COUNT(ef.source_entity_key)
  FROM (
	SELECT
       sd.screen_name,sd.screen_pk,
       sd.screen_subcategory_fk,
       jd.job_pk,
       entity_attr_names.source_entity_name
     FROM
       dq.screen_dimension AS sd,
       dq.job_dimension AS jd,
       (SELECT DISTINCT
          ef.source_entity_name
        FROM dq.error_event_fact ef) AS entity_attr_names
	) AS job_screen_entity_attrname_combis
  JOIN dq.error_event_fact ef ON
    job_screen_entity_attrname_combis.job_pk             = ef.job_fk AND
    job_screen_entity_attrname_combis.screen_pk          = ef.screen_fk AND
    job_screen_entity_attrname_combis.source_entity_name = ef.source_entity_name
  GROUP BY
    job_screen_entity_attrname_combis.screen_name,
    job_screen_entity_attrname_combis.screen_pk,
    job_screen_entity_attrname_combis.screen_subcategory_fk,
    job_screen_entity_attrname_combis.job_pk,
    job_screen_entity_attrname_combis.source_entity_name,
    ef.source_entity_error_attr
) AS event_breakdown ON
  sc.screen_subcategory_pk = event_breakdown.screen_subcategory_fk
WHERE event_breakdown.job_pk = (( SELECT max(jd_1.job_pk) AS max
           FROM dq.job_dimension jd_1
          WHERE jd_1.status::text = 'COMPLETED'::text)) OR event_breakdown.job_pk IS NULL
ORDER BY event_breakdown.source_entity_name, sc.screen_category_name, sc.screen_subcategory_name;
-- Permissions
ALTER VIEW dq.v_screen_count_breakdown OWNER TO postgres;
GRANT ALL ON TABLE dq.v_screen_count_breakdown TO postgres;

-----------------------------------------------------
-- VIEW dq.v_last_job_duration_seconds              -
-----------------------------------------------------
CREATE OR REPLACE VIEW dq.v_last_job_duration_seconds AS
SELECT jd.job_name, EXTRACT(EPOCH FROM (jd.job_finish - jd.job_start)) AS duration
FROM dq.job_dimension jd
WHERE 
  jd.job_pk = (SELECT MAX(job_pk) FROM dq.job_dimension jd2 WHERE jd2.status = 'COMPLETED');
-- Permissions
ALTER VIEW dq.v_last_job_duration_seconds OWNER TO postgres;
GRANT ALL ON TABLE dq.v_last_job_duration_seconds TO postgres;

-----------------------------------------------------
-- VIEW dq.v_pass_fail                              -
-----------------------------------------------------
CREATE OR REPLACE VIEW dq.v_pass_fail AS
SELECT base_coordinates.screen_fk, base_coordinates.entity_name, base_coordinates.attr_name, COALESCE(passed_results.row_count, 0) AS passed, COALESCE (failed_results.row_count, 0) AS failed,
COALESCE(passed_results.row_count, 0) + COALESCE (failed_results.row_count, 0) AS total,
(COALESCE(passed_results.row_count, 0)*1.0) / ((COALESCE(passed_results.row_count, 0) + COALESCE (failed_results.row_count, 0))*1.0) AS qrate,
(COALESCE(failed_results.row_count, 0)*1.0) / ((COALESCE(passed_results.row_count, 0) + COALESCE (failed_results.row_count, 0))*1.0) AS erate
FROM 
 (SELECT DISTINCT ecf.screen_fk, ecf.entity_name, ecf.attr_name FROM dq.event_count_fact ecf
  JOIN dq.job_dimension jd ON ecf.job_fk = jd.job_pk 
  WHERE jd.status ='COMPLETED' AND
    jd.job_pk = (SELECT MAX(jd2.job_pk) FROM dq.job_dimension jd2))
  AS base_coordinates
LEFT OUTER JOIN (SELECT ecf.screen_fk, ecf.entity_name, ecf.attr_name, SUM(ecf.row_count) as row_count
  FROM dq.event_count_fact ecf
  JOIN dq.job_dimension jd ON ecf.job_fk = jd.job_pk 
  WHERE jd.status ='COMPLETED' AND
    jd.job_pk = (SELECT MAX(jd2.job_pk) FROM dq.job_dimension jd2) AND
    ecf.passed = false
  GROUP BY ecf.screen_fk, ecf.entity_name, ecf.attr_name
  ) as failed_results
ON base_coordinates.screen_fk = failed_results.screen_fk and 
   base_coordinates.entity_name= failed_results.entity_name and
   base_coordinates.attr_name = failed_results.attr_name
LEFT OUTER JOIN (SELECT ecf.screen_fk, ecf.entity_name, ecf.attr_name, SUM(ecf.row_count) as row_count
  FROM dq.event_count_fact ecf
  JOIN dq.job_dimension jd ON ecf.job_fk = jd.job_pk 
  WHERE jd.status ='COMPLETED' AND
    jd.job_pk = (SELECT MAX(jd2.job_pk) FROM dq.job_dimension jd2) AND
    ecf.passed = true
  GROUP BY ecf.screen_fk, ecf.entity_name, ecf.attr_name
  ) as passed_results
ON base_coordinates.screen_fk = passed_results.screen_fk and 
   base_coordinates.entity_name= passed_results.entity_name and
   base_coordinates.attr_name = passed_results.attr_name;
-- Permissions
ALTER VIEW dq.v_pass_fail OWNER TO postgres;
GRANT ALL ON TABLE dq.v_pass_fail TO postgres;