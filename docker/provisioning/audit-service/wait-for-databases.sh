#!/bin/bash
# wait-for-databases.sh

set -e

cmd="$@"

until PGPASSWORD=$DWH_POSTGRES_PW psql -h "$DWH_HOST" -p "$DWH_PORT" -U "postgres" -c '\q' ; do
  >&2 echo "Warehouse is unavailable - waiting one more second"
  sleep 1
done

until PGPASSWORD=$ES_POSTGRES_PW psql -h "$ES_HOST" -p "$ES_PORT" -U "postgres" -c '\q' ; do
  >&2 echo "Event store is unavailable - waiting one more second"
  sleep 1
done

>&2 echo "Databases are up - executing command '$cmd'"
$cmd