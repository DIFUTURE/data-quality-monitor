/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.audit;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import de.difuture.component.dq.common.Util;
import io.prometheus.client.Gauge;
import io.prometheus.client.exporter.HTTPServer;


/**
 * Encapsulates an audit server, which collects and aggregates
 * data quality information from specific data warehouse and the associated
 * event store.
 * 
 * @author Helmut Spengler
 */
public class AuditService {
    
   /** For self-logging */
    static final Logger logger = LoggerFactory.getLogger(AuditService.class);

	/**
	 * Main Method.
	 * @param args Command line arguments
	 * @throws IOException if problems occur when reading the configuration file
	 * @throws SQLException if a database access error occurs when performing the checks
	 */
	public static void main(String[] args) throws IOException, SQLException {
	    
	    /** The wall-clock times for the execution of the different audits */
	    Gauge sensorPerformanceGauge = Gauge.build().name("sensor_durations_seconds").help("Performance metrics for sensor").labelNames("check_id", "description").register();

	    // Check if all required environment variables are set
		checkEnvironment();
		
		// Acquire Util instance
		Util util = Util.getInstance();

		// Connect to event store
        Connection eventStoreConnection = util.getDatabaseConnection(
        		System.getenv("ES_HOST"), System.getenv("ES_PORT"),
        		System.getenv("ES_DB"), "postgres", System.getenv("ES_POSTGRES_PW"));
        // Connect to data warehouse
        Connection dwhConnection = util.getDatabaseConnection(
                System.getenv("DWH_HOST"), System.getenv("DWH_PORT"),
                System.getenv("DWH_TYPE"), "postgres", System.getenv("DWH_POSTGRES_PW"));
        
        // Report software version
        Package thePackage = AuditService.class.getPackage();
    	logger.info("This is " + thePackage.getImplementationTitle() + " " + thePackage.getImplementationVersion());
        
        // Parse and prepare checklist
		Constructor constructor = new Constructor(Checks.class);
		Yaml yaml = new Yaml(constructor);
		InputStream inputStream = new FileInputStream("checks.yml");
		Checks checks = (Checks) yaml.load(inputStream);
        List<Check> checkList = checks.initCheckList(Target.valueOf(System.getenv("DWH_TYPE")), System.getenv("DWH_ID"), System.getenv("DWH_HOST"), eventStoreConnection, dwhConnection);

        // Prepare server start
        int auditServerPort = Integer.parseInt(System.getenv("PORT_SENSOR"));

        // Start HTTP server
        logger.info("Starting audit server on port " + auditServerPort);
		@SuppressWarnings("unused")
		HTTPServer server = new HTTPServer(auditServerPort);
		while (true) {
			performChecks(checkList, eventStoreConnection, dwhConnection, sensorPerformanceGauge);
			try {
                Thread.sleep(1000*Integer.valueOf(System.getenv("SCRAPE_INTERVAL_SECONDS")));
            } catch (InterruptedException e) {
                logger.error("Something went wrong during Thread.sleep()");
                e.printStackTrace();
            }
		}
	}

	/**
	 * Execute a list of checks
	 * @param checks the list of checks to be executed
	 * @param eventStoreConnection the connection to the event store database
	 * @param dwhConnection the connection to the warehouse database
	 * @param sensorPerformanceGauge for measuring the wall-clock times for the execution of the different audits
	 * @throws SQLException if a database access error occurs
	 */
	private static void performChecks(List<Check> checks, Connection eventStoreConnection, Connection dwhConnection, Gauge sensorPerformanceGauge) throws SQLException {
		long start = System.currentTimeMillis();
		for (Check check : checks) {
			if (check.isActive()) {
				check.execute (eventStoreConnection, dwhConnection, sensorPerformanceGauge, Target.valueOf(System.getenv("DWH_TYPE")));
			} 
		}
		// TODO: add base labels from class Check()
		sensorPerformanceGauge.labels("all", "Overall time needed for collecting metrics").set(((double) System.currentTimeMillis() - start) / 1000d);
	}


	/**
	 * Validate all required environment variables.
	 */
	private static void checkEnvironment() {
		checkVar("PORT_SENSOR");

		checkVar("DWH_ID");
		checkVar("DWH_HOST");
		checkVar("DWH_PORT");
		checkVar("DWH_POSTGRES_PW");
		checkVar("DWH_TYPE");

		checkVar("ES_HOST");
		checkVar("ES_PORT");
		checkVar("ES_DB");
		checkVar("ES_POSTGRES_PW");
		
        checkVar("SCRAPE_INTERVAL_SECONDS");
	}


	/**
	 * Check if an environment variable exists and is non-null.
	 * @param var the environment variable to be checked
	 */
	private static void checkVar(String var) {
		String value = System.getenv(var);
		if (null == value || "".equals(System.getenv(value))) {
			throw new RuntimeException ("Environment variable '" + var + "' may not be empty");
		}        
	}
}