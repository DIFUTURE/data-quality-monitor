/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.audit;

import java.util.Arrays;

/**
 * A single fact created by an audit. Represents a data point comprising
 * the coordinates of the data point and its value. Here, typically a count
 * or a rate.
 * @author Helmut Spengler
 *
 */
public class AuditFact {
	
	/** The coordinates of the data point */
	private final String[] coordinates;
    
	/** The value of the data point */
	private final double value;
    
    /**
     * Constructor
     * @param coordinates the coordinates of the data point
     * @param value Tte value of the data point
     */
    public AuditFact(String []coordinates, double value) {
        super();
        this.coordinates = coordinates;
        this.value = value;
    }
    
    @Override
    public String toString() {
        return Arrays.toString(coordinates) + ": " + Double.toString(value);
    }

    // Getters
    
    /**
     * Getter
     * @return the coordinates of the data point
     */
    public String[] getCoordinates() {
        return coordinates;
    }

    /**
     * Getter
     * @return the value of the data point
     */
    public double getValue() {
        return value;
    }
}