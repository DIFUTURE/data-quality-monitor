/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.audit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Data structure for multidimensional representation of audit results. Each data point
 * of an audit result is represented as a fact which comprises its coordinates and its
 * value (here, this is typically a count or a rate).
 * @author Helmut Spengler
 */
public class AuditFacts {
	  
    /** The dimensions */
    private final String[] dimensions;
    
    /** The facts */
    private List<AuditFact> facts;
    
    /**
     * Constructor.
     * @param dimensions the dimensions spawning the data cube
     */
    public AuditFacts(String[] dimensions) {
        super();
        this.dimensions = dimensions;
        facts = new ArrayList<>();
    }
    
    /**
     * Add a fact
     * @param fact the fact to add
     */
    public void addFact(AuditFact fact) {
        facts.add(fact);
    }
    
    @Override
    public String toString() {
        
    	// Prepare
        StringBuffer sb = new StringBuffer();            
        sb.append("Dimensions: " + Arrays.toString(getDimensions()) + "\n");
        
        // Iterate of each fact
        for (AuditFact fact : getFacts()) {
            sb.append(fact.toString() + "\n");
        }
        
        return sb.toString();
    }

    // Getters
    /**
     * Return the dimensions
     * @return the dimensions spawning the data cube
     */
    public String[] getDimensions() {
        return dimensions;
    }

    /**
     * Return the facts
     * @return the facts
     */
    public List<AuditFact> getFacts() {
        return facts;
    }
}