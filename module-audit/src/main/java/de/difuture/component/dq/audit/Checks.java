/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.audit;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.yaml.snakeyaml.constructor.Constructor;

/**
 * Encapsulates all available audit checks. Represents an object model for the
 * configuration which is needed by the YAML parser.
 * @author Helmut Spengler
 */
public class Checks extends Constructor {
    
    /**
     * Raw object model of the user configured checks,
     * e.g. as received from a YAML parser. Each list element represents
     * a particular check. Each check is defined by a set of parameters,
     * which are represented by a map
     * */
    private List<Map<String,Object>> checks;

    /**
     * Setter
     * 
     * @param checks raw object model for the user configured checks
     */
    public void setChecks(List<Map<String, Object>> checks) {
        this.checks = checks;
    }

    /**
     * Return the computable list of checks
     * @param dwhType the target database type
     * @param dwhId the ID of the data warehouse
     * @param dwhHost the host address or IP of the data warehouse
     * @param eventStoreConnection database connection to the event store
     * @param dwhConnection database connection to the DWH
     * @return the computable list of checks
     */
    public List<Check> initCheckList(Target dwhType, String dwhId, String dwhHost, Connection eventStoreConnection, Connection dwhConnection) {
        List<Check> result = new ArrayList<>();

        // Prepare uniqueness check for ids
        Set<String> checkIds = new HashSet<>();
        
        // Transform checks
        for (Map<String, Object> rawCheck : checks) {
            
            // Transform
            Check check = new Check(rawCheck, dwhType, dwhId, dwhHost, eventStoreConnection, dwhConnection);
            
            // Check uniqueness
            if (checkIds.contains(check.getId())) {
                throw new RuntimeException("Duplicate check id '" + check.getId() + "'.");
            } else {
                checkIds.add(check.getId());
            }
            
            result.add(check);
        }
        return result;
    }
}
