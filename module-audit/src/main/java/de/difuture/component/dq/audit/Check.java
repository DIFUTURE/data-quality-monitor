/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.audit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.prometheus.client.Gauge;

/**
 * Represents a single check
 * @author Helmut Spengler
 */
public class Check {
    
   /** For self-logging */
    static final Logger logger = LoggerFactory.getLogger(Check.class);
    
    /** The base label names */
    private final List<String> labelNamesBase = Arrays.asList(new String[]{"target", "dwh_id", "dwh_host", "desc"});

	/** Unique identifier/primary key for database */
	private final String id;
	/** Textual description of the check */
	private final String description;
	/** Whether this check is active */
	private final boolean isActive;
	/** The queries */
	private final Map<Target, String> queries;
	/** The audit facts */
	private final AuditFacts auditFacts;
	/** The according gauge */
	private final Gauge gauge;
	/** The SQL statement representing the check */
	private final String querySql;
	/** The base label values */
	private final List<String> labelValuesBase;

	/**
	 * Constructor
	 * @param checkMap parameter map (as received from YAML input)
	 * @param dwhType the target database type
	 * @param dwhId the ID of the data warehouse
	 * @param dwhHost the host address or IP of the data warehouse
	 * @param eventStoreConnection database connection to the event store
	 * @param dwhConnection database connection to the DWH
	 */
	public Check(Map<String,Object> checkMap, Target dwhType, String dwhId, String dwhHost, Connection eventStoreConnection, Connection dwhConnection) {
		super();

		// Map must contain data
		if (checkMap == null || checkMap.isEmpty())
			throw new RuntimeException("YML Map may not be empty");

		//
		// Configure the simple members
        //      
        id              = readString (checkMap, "id");
        queries         = readQueries(checkMap, "queries");
        description     = readString (checkMap, "description");
        labelValuesBase =  Arrays.asList(new String[]{dwhType.toString(), dwhId, dwhHost, description});
        // Select proper SQL
        querySql = getSql(dwhType);        
        // Only activate if we've found a proper SQL
        if (null == querySql) {
            isActive = false;
            auditFacts = null;
            gauge = null;
            return;
        } else {
            isActive = readBoolean(checkMap, "isActive");
        }
		
		//
		// Configure auditFacts and gauge
		//
		// Parse dimensions and initialize auditFacts
        String[] dimensions;
        
        try {
            @SuppressWarnings("resource")
            PreparedStatement pStmt = isEventStoreCheck() ? eventStoreConnection.prepareStatement(querySql) : dwhConnection.prepareStatement(querySql);
            ResultSetMetaData rsMetaData = pStmt.getMetaData();

            // Check if result from SQL query is valid
            int numDimensions = rsMetaData.getColumnCount() - 1;

            dimensions = new String[numDimensions];
            for (int i = 0; i < numDimensions; i++) {
                dimensions[i] = rsMetaData.getColumnName(i+1);
            }

            // Prepare result
            auditFacts = new AuditFacts(dimensions);
        } catch (SQLException e) {
            throw new RuntimeException("Unable to parse dimensions for check '" + id + "'.");
        }

        // Configure gauge
        List<String> labelNames = new ArrayList<String>(labelNamesBase);
        for (String dimension : dimensions) {
            labelNames.add(dimension);
        }
		gauge = Gauge.build().name(id).help(description).labelNames(labelNames.toArray(new String[labelNames.size()])).register();
	}    

	/**
	 * Execute all checks that are configured and active for this particular DWH type
	 * @param eventStoreConnection the connection to the event store database
	 * @param dwhConnection the connection to the warehouse database
	 * @param sensPerfGauge the gauge to write the execution times for the checks to
	 * @param dwhType the target system
	 * @throws SQLException if a database access error occurs
	 */
	public void execute (Connection eventStoreConnection, Connection dwhConnection, Gauge sensPerfGauge, Target dwhType) throws SQLException {

	    // Only execute check if it is active
	    if (!isActive) {
	        return;
	    }

	    @SuppressWarnings("resource")
	    PreparedStatement pStmt = isEventStoreCheck() ? eventStoreConnection.prepareStatement(querySql) : dwhConnection.prepareStatement(querySql);

	    // Execute SQL query and report execution time
	    long start = System.currentTimeMillis();
	    ResultSet rs = pStmt.executeQuery();
	    sensPerfGauge.labels(getId(), getDescription()).set(((double) System.currentTimeMillis() - start) / 1000d);

	    // Prepare result
	    int numDimensions = auditFacts.getDimensions().length;
	    while(rs.next()){
	        String[] coordinates = new String[numDimensions];
	        for (int i = 0; i < numDimensions; i++) {
	            
	            // Read coordinate name
	            String coordinate = rs.getString(i+1);
	            
                // Check if the coordinate is valid
                if (coordinate == null) {
                    throw new RuntimeException("Something is wrong with the column names of SQL query '" + querySql + "'");
                }
	            coordinates[i] = coordinate;
	        }
	        auditFacts.addFact(new AuditFact(coordinates, rs.getDouble(numDimensions+1)));
	    }

	    // Publish result
	    for (AuditFact fact : auditFacts.getFacts()) {
	        
	        // Prepare labels
	        String[] labels = labelValuesBase.toArray(new String[labelNamesBase.size() + numDimensions]);

            // Populate the coordinates for the monitoring component
	        for (int i = 0; i < numDimensions; i++) {
	            labels[labelNamesBase.size()+i] = fact.getCoordinates()[i];
	        }
	        // Populate the actual result for the monitoring component
	        gauge.labels(labels).set(fact.getValue());
	    }

	    pStmt.close();
	}    

	/** Extract the proper SQL for the DWH type
	 * @param dwhType the DWH type
	 * @return the SQL. Null, if no SQL could be found
	 */
	private String getSql(Target dwhType) {

	    switch (dwhType) {
	    case i2b2:
	        if (queries.containsKey(Target.i2b2)) {
	            return queries.get(Target.i2b2);
            } else if  (queries.containsKey(Target.i2b2_tm) ) {
                return queries.get(Target.i2b2_tm);
            } else if  (queries.containsKey(Target.es) ) {
                return queries.get(Target.es);
	        } else {
	            return null;
	        }
	    case tm:
	        if (queries.containsKey(Target.tm)) {
	            return queries.get(Target.i2b2);
	        } else if  (queries.containsKey(Target.i2b2_tm) ) {
	            return queries.get(Target.i2b2_tm);
            } else if  (queries.containsKey(Target.es) ) {
                return queries.get(Target.es);
	        } else {
                return null;
	        }
	    case omop:
	        if (queries.containsKey(Target.omop)) {
	            return queries.get(Target.omop);
            } else if  (queries.containsKey(Target.es) ) {
                return queries.get(Target.es);
	        } else {
                return null;
	        }
	    default:
	        throw new RuntimeException("Unsupported DWH-Type " + dwhType.toString());
	    }
	}

    /** Is this a event-store check?
	 * @return whether this is an event store check
	 */
	private boolean isEventStoreCheck() {
        if (queries.containsKey(Target.es)) {
            return true;
        } else {
            return false;
        }
    }

    /**
	 * Read a boolean from a parameter map
	 * @param checkMap the parameter map
	 * @param parameter for which the value is to be extracted
	 * parameter is undefined of empty
	 * @return the value of the parameter
	 */
	private boolean readBoolean(Map<String, Object> checkMap, String parameter) {
		if (checkMap.get(parameter) == null || "".equals(checkMap.get(parameter))) {
			throw new RuntimeException("'" + parameter +"' may not be empty");
		} else {
			try {
				return (Boolean) checkMap.get(parameter);
			} catch (ClassCastException e) {
				throw new RuntimeException ("Unable to convert String '" + checkMap.get(parameter) + "' to Boolean");
			}
		}        
	}

    /**
     * Read a string from a parameter map
     * @param checkMap the parameter map
     * @param parameter for which the value is to be extracted
     * @return the value of the parameter
     */
    private String readString(Map<String, Object> checkMap, String parameter) {
        if (checkMap.get(parameter) == null || "".equals(checkMap.get(parameter))) {
            throw new RuntimeException("'" + parameter +"' may not be empty");
        } else {
            return (String) checkMap.get(parameter);
        }
    }

    /**
     * Read all queries of a check
     * @param checkMap the parameter map for the check
     * @param queryAttributeName the name of the attribute containing the array of queries
     * @return a list of queries
     */
    private Map<Target, String> readQueries(Map<String, Object> checkMap, String queryAttributeName) {

        // Query list must exist ...
        if (checkMap.get(queryAttributeName) == null || "".equals(checkMap.get(queryAttributeName))) {
            
            throw new RuntimeException("Attribute '" + queryAttributeName +"' in check '" + id + "' may not be empty");
        }
        // ... and must be greater than zero
        @SuppressWarnings("unchecked")
        List<Object> objList = (List<Object>) checkMap.get(queryAttributeName);
        if (objList.size() == 0) {
            throw new RuntimeException("Query list for check '" + id + "' may not be empty");
        }
        
        // Prepare result
        Map<Target, String> sqlMap = new HashMap<>();
        
        // Iterate over queries
        for (Object queryObj : objList) {
            
            // make sure that event store queries are not combined with any other queries
            if (sqlMap.containsKey(Target.es)) {
                throw new RuntimeException("Query of type '" + Target.es + "' in check '" + id + "' may not be combined with any other query");
            }
            
            @SuppressWarnings("unchecked")
            Map<String, Object> queryMap = (Map<String, Object>) queryObj;
            
            // Extract the target
            String targetAttrName = "target";
            checkAttributeExistence(queryMap, targetAttrName);
            Target target = Target.valueOf(readString(queryMap, targetAttrName));
            
            // make sure that event store queries are not combined with any other queries
            if (Target.es == target) {
                if (sqlMap.size() > 0) {
                    throw new RuntimeException("Query of type '" + Target.es + "' in check '" + id + "' may not be combined with any other query");
                }
            }
            
            // make sure that each query type exists only once per check
            if (sqlMap.containsKey(target)) {
                throw new RuntimeException("Duplicate query of type '" + target.toString() + "' in check '" + id + "'.");
            }
            
            // Extract the SQL
            String sqlAttrName = "sql";
            checkAttributeExistence(queryMap, sqlAttrName);
            String sql = readString(queryMap, sqlAttrName);
            
            // Populate the map
            sqlMap.put(target, sql);
        }
        
        
        return sqlMap;
    }

    /**
     * Check if an attribute exists in a map. Throw a runtime exception, of not
     * @param queryMap the map
     * @param attrName the name of the attribute
     */
    private void checkAttributeExistence(Map<String, Object> queryMap, String attrName) {
        if (!queryMap.containsKey(attrName)) {
            throw new RuntimeException("One of the queries in check '" + id + "' doesn't contain a '" + attrName + "' attribute.");
        }
    }


	// Getters

    /**
	 * Getter
	 * @return the unique id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Getter
	 * @return the textual description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter
	 * @return whether this check is active
	 */
	public boolean isActive() {
		return isActive;
	}
}