package de.difuture.component.dq.audit;

/**
 * Represents the different query targets. Can be the event-store, or a certain type or category of data warehouse.
 * @author Helmut Spengler
 *
 */
public enum Target {
    // TODO consider changing name to DwhType
    
    /** i2b2 */
    i2b2,
    
    /** tranSMART */
    tm,
    
    /** i2b2 or tranSMART */
    i2b2_tm,
    
    /** OMOP CDM */
    omop,
    
    /** Event store */
    es
}