/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.common;

import java.util.HashSet;
import java.util.Set;

/**
 * Reference for the available screens and their associated primary keys
 * @author Helmut Spengler
 *
 */
public enum Screen {
    
    ///////////////////
    // Conformance ////
    ///////////////////
    
    // Value conformance
    // -----------------

    /** Invalid format, e.g. wrong decimal separator */
    INVALID_FORMAT(1, ScreenSubCategory.VALUE_CONFORMANCE, "Invalid format", "cv_invalid_format", 1.0, ExceptionAction.NONE),
    
    /** Wrong data type, e.g. expected a date but received a number */
    INVALID_DATATYPE(2, ScreenSubCategory.VALUE_CONFORMANCE, "Invalid datatype", "cv_invalid_datatype", 1.0, ExceptionAction.NONE),
    
    /** Category is not supported for categorical attribute */
    INVALID_CATEGORY(3, ScreenSubCategory.VALUE_CONFORMANCE, "Invalid category", "cv_invalid_category", 1.0, ExceptionAction.NONE),
        
    // Relational conformance
    // ----------------------
    
    /** Violation of primary/foreign key constraints */
    REFERENTIAL_INTEGRITY_VIOLATION(4, ScreenSubCategory.RELATIONAL_CONFORMANCE, "Referential integrity violation", "cr_ref_int_viol", 1.0, ExceptionAction.NONE),
    
    // Computational conformance
    // -------------------------

    /** Wrong score calculation */
    WRONG_SCORE_CALCULATION(5, ScreenSubCategory.COMPUTATIONAL_CONFORMANCE, "Wrong score calculation", "cc_wrong_score_calc", 1.0, ExceptionAction.NONE),
    
    ///////////////////
    // Completeness ///
    ///////////////////
    
    /** Missing value */
    MISSING_VALUE(6, ScreenSubCategory.COMPLETENESS, "Missing value", "c_missing_val", 1.0, ExceptionAction.NONE),
    
    /** Insufficient Precision */
    INSUFFICIENT_PRECISION(7, ScreenSubCategory.COMPLETENESS, "Insufficient precision", "c_insuff_prec", 1.0, ExceptionAction.NONE),
    
    ///////////////////
    // Plausibility ///
    ///////////////////
    
    // Uniqueness plausibility
    // -----------------------
    
    /** Same entity with different key */
    DUPLICATE_ENTITY(8, ScreenSubCategory.UNIQUENESS_PLAUSIBILITY, "Duplicate entity with different key", "pu_dupl_entity_diff_key", 1.0, ExceptionAction.NONE),
    
    // Temporal plausibility
    // ---------------------
    
    /** Two dates in the wrong order */
    DATE_INVERSION(9, ScreenSubCategory.TEMPORAL_PLAUSIBILITY, "End date before start date", "pt_end_before_start", 1.0, ExceptionAction.NONE),
    
    // Atemporal plausibility
    // ----------------------
    
    /** Invalid range, e.g. negative age */
    VALUE_RANGE_VIOLATION(10, ScreenSubCategory.ATEMPORAL_PLAUSIBILITY, "Value range violation", "pa_val_range_viol", 1.0, ExceptionAction.NONE);
    
    // Check for duplicate primary keys
    static {
        Set<Integer> screenPks = new HashSet<>();
        for (Screen screen : Screen.values()) {
            if (!screenPks.add(screen.getPk())) {
                throw new RuntimeException("Key " + screen.getPk() + "for screen " + screen.toString() + " already exists.");
            }            
        }
    }
		
	//
	// Members
	//
	/** The sub-category this screen belongs to */
	private final ScreenSubCategory screenSubCategory;
    
	/** Unique identifier of the screen as used in the database */
    private final int pk;
    
    /** Description of the screen */
    private final String description;
    
    /** Short description of the screen */
    private final String shortDescription;
    
    /** Default severity score */
    private final double defaultSeverityScore;
    
    /** Default exception action */
    private final ExceptionAction defaultExceptionAction;
    
    /**
     * Constructor
     * 
     * @param pk unique identifier of the screen as used in the database
     * @param screenSubCategory the screen sub-category
     * @param desc description of the screen
     * @param shortDesc short description of the screen
     * @param defaultSeverityScore the default severity score
     * @param defaultExceptionAction the action to perform when an issue is encountered
     */
    private Screen (int pk, ScreenSubCategory screenSubCategory, String desc, String shortDesc, double defaultSeverityScore, ExceptionAction defaultExceptionAction) {
        this.pk = pk;
        this.screenSubCategory = screenSubCategory;
        this.description = desc;
        this.shortDescription = shortDesc;
        this.defaultSeverityScore = defaultSeverityScore;
        this.defaultExceptionAction = defaultExceptionAction;
    }
    
    /**
     * Get the unique identifier
     * 
     * @return the unique identifier
     */
    public int getPk() {
        return this.pk;
    }
    
    /**
     * Get the screen sub-category
     * 
     * @return the screen-subcategory
     */
    public ScreenSubCategory getScreenSubCategory() {
        return this.screenSubCategory;
    }
    
    /**
     * Get the description
     * 
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }
    
    /**
     * Get the short description
     * 
     * @return the short description
     */
    public String getShortDescription() {
        return this.shortDescription;
    }
    
    /**
     * Get the default severity score
     * 
     * @return the default severity score
     */
    public double getDefaultSeverityScore() {
        return this.defaultSeverityScore;
    }
    
    /**
     * Get the exception action
     * 
     * @return the exception action
     */
    public ExceptionAction getDefaultExceptionAction() {
        return this.defaultExceptionAction;
    }    
}
