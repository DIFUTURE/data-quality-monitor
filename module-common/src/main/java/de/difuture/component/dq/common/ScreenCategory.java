/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.common;

/**
 * Reference for the available screen categories.
 * 
 * @author Helmut Spengler
 */
public enum ScreenCategory {
    
    /** Conformance */
    CONFORMANCE("Conformance"),
	/** Completeness */
	COMPLETENESS("Completeness"),
	/** Plausibility */
	PLAUSIBILITY("Plausibility");
	
	/** Description */
	private final String description;
	
	/**
	 * Constructor
	 * 
	 * @param desc description
	 */
	private ScreenCategory (String desc) {
		this.description = desc;
	}
    
    /**
     * Get the description
     * 
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }
}
