/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.common;

import java.util.HashSet;
import java.util.Set;

/**
 * Reference for the available subcategories.
 * 
 * @author Helmut Spengler
 */
public enum ScreenSubCategory {
    
    /** Value conformance */
	VALUE_CONFORMANCE(1, ScreenCategory.CONFORMANCE, "Value conformance"),

	/** Relational conformance */
	RELATIONAL_CONFORMANCE(2, ScreenCategory.CONFORMANCE, "Relational conformance"),

	/** Computational conformance */
	COMPUTATIONAL_CONFORMANCE(3, ScreenCategory.CONFORMANCE, "Computational conformance"),

	/** Completeness */
	COMPLETENESS(4, ScreenCategory.COMPLETENESS, "Completeness"),

	/** Uniqueness plausibility */
	UNIQUENESS_PLAUSIBILITY(5, ScreenCategory.PLAUSIBILITY, "Uniqueness plausibility"),

	/** Atemporal plausibility */
	ATEMPORAL_PLAUSIBILITY(6, ScreenCategory.PLAUSIBILITY, "Atemporal plausibility"),

	/** Temporal plausibility */
	TEMPORAL_PLAUSIBILITY(7, ScreenCategory.PLAUSIBILITY, "Temporal plausibility");
    
    // Check for duplicate primary keys
    static {
        Set<Long> screenSubCatPks = new HashSet<>();
        for (ScreenSubCategory screenSubCat : ScreenSubCategory.values()) {
            if (!screenSubCatPks.add(screenSubCat.getPk())) {
                throw new RuntimeException("Key " + screenSubCat.getPk() + " for ScreenSubCategory " + screenSubCat.toString() + " already exists.");
            }            
        }
    }
	
	//
	// Members
	// 
	/** The category this sub-category belongs to */
	private final ScreenCategory screenCategory;
    
	/** Unique identifier of the screen as used in the database */
    private final long pk;
    
	/** Description */
    private final String description;
    
    /**
     * Constructor
     * 
     * @param pk the primary key for the database
     * @param screenCategory the category this sub-category belongs to
     * @param desc description of the ScreenSubCategory
     */
    private ScreenSubCategory (long pk, ScreenCategory screenCategory, String desc) {
    	this.pk = pk;
        this.screenCategory = screenCategory;
        this.description = desc;
    }
    
    /**
     * Get the category
     * 
     * @return the screen category
     */
    public ScreenCategory getScreenCategory() {
        return this.screenCategory;
    }
    
    /**
     * Get the unique identifier
     * 
     * @return the unique identifier
     */
    public long getPk() {
        return this.pk;
    }
    
    /**
     * Get the description
     * 
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }
}
