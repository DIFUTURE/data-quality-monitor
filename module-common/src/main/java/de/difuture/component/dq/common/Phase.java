/*
 * Data Quality Monitor
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.difuture.component.dq.common;

/**
 * Identifies the phase of the ETL process
 * 
 * @author Helmut Spengler
 */
public enum Phase {
	/** Related to source data */
    SOURCE,        
    /** Related to the staging subphase */
    STAGE,        
    /** Related to the extract subphase */
    EXTRACT,        
    /** Related to the transform subphase */
    TRANSFORM,        
    /** Related to the loading subphase */
    LOAD,
    /** Related to the data in the warehouse */
    DWH
}