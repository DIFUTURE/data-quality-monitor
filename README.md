# System Requirements
- JDK 8
- mvn >= 3
- docker >= 19 (has not been tested with Docker Toolbox)
- docker-compose >= 1.25
- Bourne-again shell (Bash) >= 4.3

# Overview
Clinical and translational data warehouses are important infrastructure building blocks for modern data-driven approaches in medical research. However, the lack of clear definitions of source data and controlled data collection procedures often raises concerns about the quality of data provided in such warehouses and, consequently, about the evidence level of related findings.

To address these problems, we present an architecture that helps to monitor data quality issues when importing data into warehousing solutions using ETL (Extraction, Transformation, Load) processes. Our approach provides software developers with an API (Application Programming Interface) for logging detailed and structured information about data quality issues encountered. This information can then be displayed in dynamic dashboards, the evolution of data quality can be monitored over time, and quality issues can be traced back to their source. Our architecture supports several well-known data quality dimensions, addressing conformance, completeness, and plausibility.

![Image of Architecture](img/OverallArchitecture.png)

This project provides an open-source implementation of this architecture, which is compatible with common clinical and translational data warehousing platforms, such as i2b2 and tranSMART, and which can be used in conjunction with many ETL environments.

A 15-minute presentation about this project can be viewed on YouTube (https://www.youtube.com/watch?v=JfyheJeOFCA). A more detailed description is provided in the article titled *Improving Data Quality in Medical Research: A Monitoring Architecture for Clinical and Translational Data Warehouses* by H. Spengler, I. Gatz, Florian Kohlmayer, and Fabian Prasser, which has been published in the *Proceedings of the 2020 IEEE 33th International Symposium on Computer-Based Medical Systems (CBMS)*.

# Build
Execute following commands:

```
cd docker
./dockerize.sh
```

# Usage

## Run minimal demo-example
```
cd docker
docker-compose -f compose-demo-dwh.yml -f docker-compose.yml up -d
docker-compose -f compose-demo-dwh.yml -f docker-compose.yml logs -f
```
- go to http://localhost:3001, login with admin/admin
- go to dashboard "Overview"

The panels containing information about the source data and the ETL process will be empty at this point, as is the schema of the event store. You can populate the event store with with example data using the command

```
java -jar ../module-screen/target/component-dqmonitor-screen-1.1.2-RELEASE-jar-with-dependencies.jar
```

## Stop and remove containers and clean up
```
docker-compose -f compose-demo-dwh.yml -f docker-compose.yml down -v
```



## API
Each event to be logged needs to be assigned to an instance of class `ScreenLogger.Job`, which is created when the beginning of of an ETL job is signaled to the framework using the method

```java
public Job jobStarted(Phase phase, String jobName, String sourceSystem)
```

This instance of class `Job` can then be used to register data quality related events using the methods

```java
public void pass(Job job, PassEvent passEvent)
```

and

```java
 public void fail(Job job, FailEvent failEvent)
```

Finally, the end of the ETL job can be signaled using the method

```java
public void jobFinished(Job job, Status status)
```

For a comprehensive example on how to use the API, see the method `createDataQualityEvents()` method of test class [TestScreening](module-screen/src/test/java/de/difuture/component/dq/screen/TestScreening.java).

## Custom configuration

### Connection settings for audit server
In the file [docker-compose.yml](docker/docker-compose.yml), you can configure the listening port and the database connections for the `audit-server` by editing the respective environment variables.

### Configuration of screens and screen categories

[Screens](module-common/src/main/java/de/difuture/component/dq/common/Screen.java), [screen subcategories](module-common/src/main/java/de/difuture/component/dq/common/ScreenSubCategory.java) and [screen categories](module-common/src/main/java/de/difuture/component/dq/common/ScreenCategory.java) are defined by the respective Java enums (click hyperlinks) and are intended to be customized by the users of this framework. If the method `jobStarted()` method of class [ScreenLogger](module-screen/src/main/java/de/difuture/component/dq/screen/ScreenLogger.java)finds an empty schema (which should be the case if you have a fresh event store installation) with the defined screens, screen subcategories and screen categories.

### Configuration of audit checks
The audit checks can be configured in the file [checks.yml](docker/provisioning/audit-service/checks.yml)